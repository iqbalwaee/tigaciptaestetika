-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3307
-- Generation Time: Feb 12, 2018 at 07:58 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `zmc_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl_phinxlog`
--

CREATE TABLE `acl_phinxlog` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NULL DEFAULT NULL,
  `end_time` timestamp NULL DEFAULT NULL,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acl_phinxlog`
--

INSERT INTO `acl_phinxlog` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20141229162641, 'CakePhpDbAcl', '2018-02-01 03:04:00', '2018-02-01 03:04:01', 0);

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE `acos` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(225) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT '0',
  `sort` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `parent_id`, `name`, `model`, `foreign_key`, `alias`, `lft`, `rght`, `status`, `sort`) VALUES
(1, NULL, 'controllers', NULL, NULL, 'controllers', 1, 92, 0, 0),
(2, 1, 'Error', NULL, NULL, 'Error', 2, 3, 1, 0),
(3, 1, 'Pages', NULL, NULL, 'Pages', 4, 13, 1, 0),
(19, 1, 'Acl', NULL, NULL, 'Acl', 14, 15, 1, 0),
(20, 1, 'Bake', NULL, NULL, 'Bake', 16, 17, 1, 0),
(21, 1, 'DebugKit', NULL, NULL, 'DebugKit', 18, 45, 1, 0),
(22, 21, 'Composer', NULL, NULL, 'Composer', 19, 22, 1, 0),
(23, 22, 'checkDependencies', NULL, NULL, 'checkDependencies', 20, 21, 1, 0),
(24, 21, 'MailPreview', NULL, NULL, 'MailPreview', 23, 30, 1, 0),
(25, 24, 'index', NULL, NULL, 'index', 24, 25, 1, 0),
(26, 24, 'sent', NULL, NULL, 'sent', 26, 27, 1, 0),
(27, 24, 'email', NULL, NULL, 'email', 28, 29, 1, 0),
(28, 21, 'Panels', NULL, NULL, 'Panels', 31, 36, 1, 0),
(29, 28, 'index', NULL, NULL, 'index', 32, 33, 1, 0),
(30, 28, 'view', NULL, NULL, 'view', 34, 35, 1, 0),
(31, 21, 'Requests', NULL, NULL, 'Requests', 37, 40, 1, 0),
(32, 31, 'view', NULL, NULL, 'view', 38, 39, 1, 0),
(33, 21, 'Toolbar', NULL, NULL, 'Toolbar', 41, 44, 1, 0),
(34, 33, 'clearCache', NULL, NULL, 'clearCache', 42, 43, 1, 0),
(35, 1, 'Migrations', NULL, NULL, 'Migrations', 46, 47, 1, 0),
(56, 1, 'AuditStash', NULL, NULL, 'AuditStash', 48, 49, 1, 0),
(59, 1, 'Josegonzalez\\Upload', NULL, NULL, 'Josegonzalez\\Upload', 50, 51, 1, 0),
(60, 1, 'AppSettings', NULL, NULL, 'AppSettings', 52, 55, 0, 100),
(61, 60, 'index', NULL, NULL, 'index', 53, 54, 0, 0),
(62, 1, 'Dashboard', NULL, NULL, 'Dashboard', 56, 59, 0, 0),
(63, 62, 'index', NULL, NULL, 'index', 57, 58, 0, 0),
(64, 1, 'Errors', NULL, NULL, 'Errors', 60, 63, 1, 0),
(65, 64, 'unauthorized', NULL, NULL, 'unauthorized', 61, 62, 1, 0),
(66, 1, 'Groups', NULL, NULL, 'Groups', 64, 77, 0, 1),
(67, 66, 'index', NULL, NULL, 'index', 65, 66, 0, 0),
(68, 66, 'view', NULL, NULL, 'view', 67, 68, 0, 0),
(69, 66, 'add', NULL, NULL, 'add', 69, 70, 0, 0),
(70, 66, 'edit', NULL, NULL, 'edit', 71, 72, 0, 0),
(71, 66, 'delete', NULL, NULL, 'delete', 73, 74, 0, 0),
(72, 66, 'configure', NULL, NULL, 'configure', 75, 76, 1, 0),
(73, 3, 'index', NULL, NULL, 'index', 5, 6, 1, 0),
(74, 3, 'logout', NULL, NULL, 'logout', 7, 8, 1, 0),
(75, 3, 'editProfile', NULL, NULL, 'editProfile', 9, 10, 1, 0),
(76, 3, 'activitiesLog', NULL, NULL, 'activitiesLog', 11, 12, 1, 0),
(77, 1, 'Users', NULL, NULL, 'Users', 78, 91, 0, 2),
(78, 77, 'index', NULL, NULL, 'index', 79, 80, 0, 0),
(79, 77, 'view', NULL, NULL, 'view', 81, 82, 0, 0),
(80, 77, 'add', NULL, NULL, 'add', 83, 84, 0, 0),
(81, 77, 'edit', NULL, NULL, 'edit', 85, 86, 0, 0),
(82, 77, 'delete', NULL, NULL, 'delete', 87, 88, 0, 0),
(83, 77, 'configure', NULL, NULL, 'configure', 89, 90, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `app_settings`
--

CREATE TABLE `app_settings` (
  `id` int(11) NOT NULL,
  `keyField` varchar(225) NOT NULL,
  `valueField` varchar(225) NOT NULL,
  `type` enum('text','long text','image','select') NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_settings`
--

INSERT INTO `app_settings` (`id`, `keyField`, `valueField`, `type`, `status`) VALUES
(1, 'App.Name', 'Z-APP', 'text', 0),
(2, 'App.Logo', '/webroot/assets/img/logo.png', 'image', 0),
(3, 'App.Logo.Login', '/webroot/assets/img/logo_login.png', 'image', 0),
(4, 'App.Logo.Width', '160', 'text', 0),
(5, 'App.Logo.Height', '30', 'text', 0),
(6, 'App.Logo.Login.Width', '160', 'text', 0),
(7, 'App.Logo.Login.Height', '160', 'text', 0),
(8, 'App.Login.Cover', '/webroot/assets/img/cover_login.jpg', 'image', 0),
(9, 'App.Description', 'Management application', 'long text', 0),
(10, 'App.Favico', '/webroot/assets/img/favico.png', 'long text', 0);

-- --------------------------------------------------------

--
-- Table structure for table `aros`
--

CREATE TABLE `aros` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `model` varchar(255) DEFAULT NULL,
  `foreign_key` int(11) DEFAULT NULL,
  `alias` varchar(255) DEFAULT NULL,
  `lft` int(11) DEFAULT NULL,
  `rght` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros`
--

INSERT INTO `aros` (`id`, `parent_id`, `model`, `foreign_key`, `alias`, `lft`, `rght`) VALUES
(1, NULL, 'Groups', 1, NULL, 1, 10),
(2, 1, 'Users', 1, NULL, 2, 3),
(3, 1, 'Users', 2, NULL, 4, 5),
(4, 1, 'Users', 3, NULL, 6, 7),
(5, NULL, 'Groups', 2, NULL, 11, 12),
(6, 1, 'Users', 4, NULL, 8, 9);

-- --------------------------------------------------------

--
-- Table structure for table `aros_acos`
--

CREATE TABLE `aros_acos` (
  `id` int(11) NOT NULL,
  `aro_id` int(11) NOT NULL,
  `aco_id` int(11) NOT NULL,
  `_create` varchar(2) NOT NULL DEFAULT '0',
  `_read` varchar(2) NOT NULL DEFAULT '0',
  `_update` varchar(2) NOT NULL DEFAULT '0',
  `_delete` varchar(2) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `aros_acos`
--

INSERT INTO `aros_acos` (`id`, `aro_id`, `aco_id`, `_create`, `_read`, `_update`, `_delete`) VALUES
(1, 1, 6, '1', '1', '1', '1'),
(2, 1, 7, '1', '1', '1', '1'),
(3, 1, 8, '1', '1', '1', '1'),
(4, 1, 9, '1', '1', '1', '1'),
(5, 1, 10, '1', '1', '1', '1'),
(6, 1, 11, '1', '1', '1', '1'),
(7, 1, 12, '1', '1', '1', '1'),
(8, 1, 13, '1', '1', '1', '1'),
(9, 1, 14, '1', '1', '1', '1'),
(10, 1, 15, '1', '1', '1', '1'),
(11, 1, 16, '1', '1', '1', '1'),
(12, 1, 17, '1', '1', '1', '1'),
(13, 1, 18, '1', '1', '1', '1'),
(14, 2, 6, '1', '1', '1', '1'),
(15, 2, 7, '1', '1', '1', '1'),
(16, 2, 8, '1', '1', '1', '1'),
(17, 2, 9, '1', '1', '1', '1'),
(18, 2, 13, '1', '1', '1', '1'),
(19, 2, 14, '1', '1', '1', '1'),
(20, 2, 15, '1', '1', '1', '1'),
(21, 2, 16, '1', '1', '1', '1'),
(22, 2, 17, '1', '1', '1', '1'),
(23, 2, 18, '1', '1', '1', '1'),
(24, 2, 10, '1', '1', '1', '1'),
(25, 2, 11, '1', '1', '1', '1'),
(26, 2, 12, '1', '1', '1', '1'),
(57, 1, 36, '1', '1', '1', '1'),
(58, 1, 37, '1', '1', '1', '1'),
(59, 2, 36, '1', '1', '1', '1'),
(60, 1, 40, '1', '1', '1', '1'),
(61, 2, 40, '1', '1', '1', '1'),
(62, 2, 37, '1', '1', '1', '1'),
(63, 1, 47, '1', '1', '1', '1'),
(64, 1, 49, '1', '1', '1', '1'),
(65, 1, 48, '1', '1', '1', '1'),
(66, 1, 52, '1', '1', '1', '1'),
(67, 1, 51, '1', '1', '1', '1'),
(68, 1, 50, '1', '1', '1', '1'),
(69, 1, 41, '1', '1', '1', '1'),
(70, 1, 45, '1', '1', '1', '1'),
(71, 1, 44, '1', '1', '1', '1'),
(72, 1, 43, '1', '1', '1', '1'),
(73, 1, 42, '1', '1', '1', '1'),
(74, 1, 46, '1', '1', '1', '1'),
(75, 2, 47, '1', '1', '1', '1'),
(76, 2, 49, '1', '1', '1', '1'),
(77, 2, 48, '1', '1', '1', '1'),
(78, 2, 52, '1', '1', '1', '1'),
(79, 2, 51, '1', '1', '1', '1'),
(80, 2, 50, '1', '1', '1', '1'),
(81, 2, 41, '1', '1', '1', '1'),
(82, 2, 45, '1', '1', '1', '1'),
(83, 2, 44, '1', '1', '1', '1'),
(84, 2, 43, '1', '1', '1', '1'),
(85, 2, 42, '1', '1', '1', '1'),
(86, 2, 46, '1', '1', '1', '1'),
(87, 1, 57, '1', '1', '1', '1'),
(88, 1, 58, '1', '1', '1', '1'),
(89, 2, 57, '1', '1', '1', '1'),
(90, 2, 58, '1', '1', '1', '1'),
(91, 3, 57, '-1', '-1', '-1', '-1'),
(92, 3, 58, '-1', '-1', '-1', '-1'),
(93, 3, 36, '1', '1', '1', '1'),
(94, 3, 37, '1', '1', '1', '1'),
(95, 3, 47, '1', '1', '1', '1'),
(96, 3, 49, '1', '1', '1', '1'),
(97, 3, 48, '1', '1', '1', '1'),
(98, 3, 52, '1', '1', '1', '1'),
(99, 3, 51, '1', '1', '1', '1'),
(100, 3, 50, '1', '1', '1', '1'),
(101, 3, 41, '1', '1', '1', '1'),
(102, 3, 45, '1', '1', '1', '1'),
(103, 3, 44, '1', '1', '1', '1'),
(104, 3, 43, '1', '1', '1', '1'),
(105, 3, 42, '1', '1', '1', '1'),
(106, 3, 46, '1', '1', '1', '1'),
(107, 3, 6, '1', '1', '1', '1'),
(108, 3, 12, '1', '1', '1', '1'),
(109, 3, 7, '1', '1', '1', '1'),
(110, 3, 11, '1', '1', '1', '1'),
(111, 3, 10, '1', '1', '1', '1'),
(112, 3, 9, '1', '1', '1', '1'),
(113, 3, 8, '1', '1', '1', '1'),
(114, 3, 13, '1', '1', '1', '1'),
(115, 3, 40, '1', '1', '1', '1'),
(116, 3, 16, '1', '1', '1', '1'),
(117, 3, 15, '1', '1', '1', '1'),
(118, 3, 14, '1', '1', '1', '1'),
(119, 3, 18, '1', '1', '1', '1'),
(120, 3, 17, '1', '1', '1', '1'),
(121, 1, 62, '1', '1', '1', '1'),
(122, 1, 63, '1', '1', '1', '1'),
(123, 1, 66, '1', '1', '1', '1'),
(124, 1, 68, '1', '1', '1', '1'),
(125, 1, 67, '1', '1', '1', '1'),
(126, 1, 71, '1', '1', '1', '1'),
(127, 1, 70, '1', '1', '1', '1'),
(128, 1, 69, '1', '1', '1', '1'),
(129, 1, 77, '1', '1', '1', '1'),
(130, 1, 82, '1', '1', '1', '1'),
(131, 1, 81, '1', '1', '1', '1'),
(132, 1, 80, '1', '1', '1', '1'),
(133, 1, 79, '1', '1', '1', '1'),
(134, 1, 83, '1', '1', '1', '1'),
(135, 1, 78, '1', '1', '1', '1'),
(136, 1, 60, '1', '1', '1', '1'),
(137, 1, 61, '1', '1', '1', '1'),
(138, 2, 62, '1', '1', '1', '1'),
(139, 2, 63, '1', '1', '1', '1'),
(140, 2, 66, '1', '1', '1', '1'),
(141, 2, 68, '1', '1', '1', '1'),
(142, 2, 67, '1', '1', '1', '1'),
(143, 2, 71, '1', '1', '1', '1'),
(144, 2, 70, '1', '1', '1', '1'),
(145, 2, 69, '1', '1', '1', '1'),
(146, 2, 77, '1', '1', '1', '1'),
(147, 2, 82, '1', '1', '1', '1'),
(148, 2, 81, '1', '1', '1', '1'),
(149, 2, 80, '1', '1', '1', '1'),
(150, 2, 79, '1', '1', '1', '1'),
(151, 2, 83, '1', '1', '1', '1'),
(152, 2, 78, '1', '1', '1', '1'),
(153, 2, 60, '1', '1', '1', '1'),
(154, 2, 61, '1', '1', '1', '1');

-- --------------------------------------------------------

--
-- Table structure for table `audit_logs`
--

CREATE TABLE `audit_logs` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `controller` varchar(225) DEFAULT NULL,
  `_action` varchar(225) DEFAULT NULL,
  `type` varchar(250) DEFAULT NULL,
  `primary_key` int(11) DEFAULT NULL,
  `source` varchar(250) DEFAULT NULL,
  `parent_source` varchar(250) DEFAULT NULL,
  `original` json DEFAULT NULL,
  `changed` json DEFAULT NULL,
  `meta` json DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `audit_logs`
--

INSERT INTO `audit_logs` (`id`, `timestamp`, `user_id`, `controller`, `_action`, `type`, `primary_key`, `source`, `parent_source`, `original`, `changed`, `meta`) VALUES
(1, '2018-02-06 15:08:55', 1, 'users', 'add', 'create', 10, 'users', NULL, '\"{\\\"id\\\":10,\\\"username\\\":\\\"sadsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"sadsa23dsadsa@gmail.com\\\",\\\"group_id\\\":2,\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":10,\\\"username\\\":\\\"sadsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"sadsa23dsadsa@gmail.com\\\",\\\"group_id\\\":2,\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"[]\"'),
(2, '2018-02-06 15:11:34', 1, 'groups', 'delete', 'delete', 10, 'users', NULL, 'null', 'null', '\"[]\"'),
(3, '2018-02-06 15:11:34', 1, 'groups', 'delete', 'delete', 2, 'groups', NULL, 'null', 'null', '\"[]\"'),
(4, '2018-02-08 10:38:04', 1, 'users', 'add', 'create', 2, 'users', NULL, '\"{\\\"id\\\":2,\\\"username\\\":\\\"admin\\\",\\\"name\\\":\\\"dsadsa\\\",\\\"email\\\":\\\"dsadsads@dsadsa.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":2,\\\"username\\\":\\\"admin\\\",\\\"name\\\":\\\"dsadsa\\\",\\\"email\\\":\\\"dsadsads@dsadsa.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(5, '2018-02-08 10:38:27', 1, 'users', 'add', 'create', 3, 'users', NULL, '\"{\\\"id\\\":3,\\\"username\\\":\\\"dsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"321321@gmail.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":3,\\\"username\\\":\\\"dsadsad\\\",\\\"name\\\":\\\"KAS PUSAT\\\",\\\"email\\\":\\\"321321@gmail.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(6, '2018-02-09 03:50:34', 1, 'users', 'edit', 'update', 2, 'users', NULL, '\"{\\\"username\\\":\\\"admin\\\",\\\"name\\\":\\\"dsadsa\\\",\\\"email\\\":\\\"dsadsads@dsadsa.com\\\",\\\"modified_by\\\":null}\"', '\"{\\\"username\\\":\\\"rezama\\\",\\\"name\\\":\\\"rezama\\\",\\\"email\\\":\\\"rezama@app.com\\\",\\\"modified_by\\\":1}\"', '\"[]\"'),
(7, '2018-02-09 04:14:55', 1, 'groups', 'add', 'create', 2, 'groups', NULL, '\"{\\\"id\\\":2,\\\"name\\\":\\\"SPV\\\",\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":2,\\\"name\\\":\\\"SPV\\\",\\\"status\\\":false,\\\"created_by\\\":1}\"', '\"[]\"'),
(8, '2018-02-09 04:15:23', 1, 'users', 'add', 'create', 4, 'users', NULL, '\"{\\\"id\\\":4,\\\"username\\\":\\\"SPV\\\",\\\"name\\\":\\\"spv\\\",\\\"email\\\":\\\"spv@app.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"{\\\"id\\\":4,\\\"username\\\":\\\"SPV\\\",\\\"name\\\":\\\"spv\\\",\\\"email\\\":\\\"spv@app.com\\\",\\\"group_id\\\":1,\\\"status\\\":true,\\\"created_by\\\":1}\"', '\"[]\"'),
(9, '2018-02-09 04:15:32', 1, 'users', 'edit', 'update', 2, 'users', NULL, '\"{\\\"status\\\":true,\\\"modified_by\\\":1}\"', '\"{\\\"status\\\":false,\\\"modified_by\\\":1}\"', '\"[]\"');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'administrator', 1, NULL, '2018-02-05 08:03:19', NULL, '2018-02-05 08:03:19'),
(2, 'SPV', 0, 1, '2018-02-09 04:14:55', NULL, '2018-02-09 04:14:55');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(60) NOT NULL,
  `name` varchar(225) DEFAULT NULL,
  `email` varchar(225) DEFAULT NULL,
  `group_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `name`, `email`, `group_id`, `status`, `created_by`, `created`, `modified_by`, `modified`) VALUES
(1, 'administrator', '$2y$10$FFIjh73z6y6Yq09cwqWAMuzwZ3eKp8XJ3jwU1ZKlR4Rg0BtV5eogy', 'administrator', 'administrator@app.com', 1, 1, 1, '2018-02-05 08:44:27', 1, '2018-02-06 11:06:20'),
(2, 'rezama', '$2y$10$.3Kf6oCfc2pE74Pj1UqiK.15Y5o6Qde4ewTLHTaUQfzGxFwfMsSui', 'rezama', 'rezama@app.com', 1, 0, 1, '2018-02-08 10:38:03', 1, '2018-02-09 04:15:32'),
(3, 'dsadsad', '$2y$10$R1zuUw2tQZ7IOHKUR/ne6.SFbGLxYmtpQbRhKPhFvMQksKf4ZzWuG', 'KAS PUSAT', '321321@gmail.com', 1, 1, 1, '2018-02-08 10:38:26', NULL, '2018-02-08 10:38:26'),
(4, 'SPV', '$2y$10$67VKrPYuvfpXTOD1pibrxOKbqrRUl//tIQUIqsP.s5cX.1psIJcJ6', 'spv', 'spv@app.com', 1, 1, 1, '2018-02-09 04:15:23', NULL, '2018-02-09 04:15:23');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acl_phinxlog`
--
ALTER TABLE `acl_phinxlog`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `acos`
--
ALTER TABLE `acos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `app_settings`
--
ALTER TABLE `app_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `aros`
--
ALTER TABLE `aros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lft` (`lft`,`rght`),
  ADD KEY `alias` (`alias`);

--
-- Indexes for table `aros_acos`
--
ALTER TABLE `aros_acos`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `aro_id` (`aro_id`,`aco_id`),
  ADD KEY `aco_id` (`aco_id`);

--
-- Indexes for table `audit_logs`
--
ALTER TABLE `audit_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `username` (`username`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acos`
--
ALTER TABLE `acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=84;

--
-- AUTO_INCREMENT for table `app_settings`
--
ALTER TABLE `app_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `aros`
--
ALTER TABLE `aros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `aros_acos`
--
ALTER TABLE `aros_acos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;

--
-- AUTO_INCREMENT for table `audit_logs`
--
ALTER TABLE `audit_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
