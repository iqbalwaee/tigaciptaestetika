<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
//

class ApisController extends AppController
{
	public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(php_sapi_name() !== 'cli'){
            $this->Auth->allow();
        }
    }
    /**
     * Get Provinces method
     *
     * @return \Cake\Http\Response|void
     */
    public function getCities($idProvince)
    {
        $this->loadModel('Cities');
        if($this->request->is('ajax')){
            $search = "";
            if($this->request->query('search')){
                $search = $this->request->query('search');
            }
            $data = $this->Cities->find('all',[
                'conditions' => [
                    'province_id'=> $idProvince
                ],
                'order' => [
                    'name' => 'ASC'
                ]
            ]);
            $results = [];
            $a =0;
            foreach($data as $key => $val){
                $results[$a]['id'] = $val->id;
                $results[$a]['text'] = $val->name;
                $a++;
            } 
            $this->set(compact('results'));
            $this->set('_serialize',['results']);
        }
    }

    public function checkBuilding()
    {
        $this->loadModel('ConsHouse');
        if($this->request->is('ajax')){
            $data = $this->request->data;
            $house_name = $data['house_name'];
            $building = $data['building']['text'];
            // dd($data);
            $building = str_replace(" ","",$building);
            $building = str_replace("-","",$building);
            $building = str_replace("/","",$building);
            $building = str_replace(".","",$building);
            $building = str_replace("\\","",$building);
            $count = $this->ConsHouse->find()->where([
                'name_house LIKE' => $house_name,
                'cons_building LIKE' => '%,'.$building.',%'
            ])->count();
            $this->set(compact('count'));
            $this->set('_serialize',['count']);
        }
    }

    public function getDistricts($idCity)
    {
        $this->loadModel('Districts');
        if($this->request->is('ajax')){
            $search = "";
            if($this->request->query('search')){
                $search = $this->request->query('search');
            }
            $data = $this->Districts->find('all',[
                'conditions' => [
                    'city_id'=> $idCity
                ],
                'order' => [
                    'name' => 'ASC'
                ]
            ]);
            $results = [];
            $a =0;
            foreach($data as $key => $val){
                $results[$a]['id'] = $val->id;
                $results[$a]['text'] = $val->name;
                $a++;
            } 
            $this->set(compact('results'));
            $this->set('_serialize',['results']);
        }
    }
    public function getVillages($id)
    {
        $this->loadModel('Villages');
        if($this->request->is('ajax')){
            $search = "";
            if($this->request->query('search')){
                $search = $this->request->query('search');
            }
            $data = $this->Villages->find('all',[
                'conditions' => [
                    'district_id'=> $id
                ],
                'order' => [
                    'name' => 'ASC'
                ]
            ]);
            $results = [];
            $a =0;
            foreach($data as $key => $val){
                $results[$a]['id'] = $val->id;
                $results[$a]['text'] = $val->name;
                $a++;
            } 
            $this->set(compact('results'));
            $this->set('_serialize',['results']);
        }
    }
    public function uploadImage()
    {
        $data = $this->request->getData();
        $file = $data['file'];
        $ext = pathinfo($file['name'], PATHINFO_EXTENSION);
        $file_name = md5(rand(0,932819).date('y-mdhi')).'.'.$ext;
        $destination = WWW_ROOT. 'media'.DS.$file_name;
        if(move_uploaded_file($file['tmp_name'] ,$destination )){
            $results = ['code' => 200, 'url'=>Router::url('media/'.$file_name,true)];
        }else{
            $results = ['code' => 50, 'url'=>NULL];
        }
        
        $this->set(compact('results'));
        $this->set('_serialize','results');
    }
}
