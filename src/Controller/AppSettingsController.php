<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * AppSettings Controller
 *
 * @property \App\Model\Table\AppSettingsTable $AppSettings
 *
 * @method \App\Model\Entity\AppSetting[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AppSettingsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('Imagine');
        $this->loadComponent('RequestHandler');
        if(!empty($this->userData)){
            if(php_sapi_name() !== 'cli'){
                $this->Auth->allow(['index','add','edit','view','delete']);
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $appSettings = $this->AppSettings->find('all',[
            'conditions' => [
                'status' => 0
            ]
        ]);
        $settings = [];
        foreach($appSettings as $key => $r){
            $settings += [str_replace(".","_",$r->keyField) => $r];
        }

        $appSettings = $settings;
        $dataSave = $this->AppSettings->newEntity();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            $dataSave = $this->AppSettings->patchEntity($dataSave,$data);
            if(empty($dataSave->errors())){
                unset($data['files']);
                foreach($data as $w => $d){
                    $id = $appSettings[$w]['id'];
                    $dataOriginal = $this->AppSettings->get($id);
                    $valueSave = "";
                    if($w == 'App_Logo'){
                        $source = $d['tmp_name'];
                        if(!empty($source)){
                            $uploadFolder = WWW_ROOT.'assets'.DS.'img'.DS.'logo';
                            $saveDir = '/webroot/assets/img/logo';
                            $extension  = pathinfo($d['name'], PATHINFO_EXTENSION);
                            $tmp        = $uploadFolder.'.'. $extension;
                            $saveDir    = $saveDir.'.'.$extension;
                            $width  = $data['App_Logo_Width'];
                            $height = $data['App_Logo_Height'];
                            $this->Imagine->gdImageCropAndSave($source, $tmp,['width'=>$width,'height'=>$height]);
                            $valueSave = $saveDir;
                        }
                    }elseif($w == 'App_Logo_Login'){
                        $source = $d['tmp_name'];
                        if(!empty($source)){
                            $uploadFolder = WWW_ROOT.'assets'.DS.'img'.DS.'logo_login';
                            $saveDir = '/webroot/assets/img/logo_login';
                            $extension  = pathinfo($d['name'], PATHINFO_EXTENSION);
                            $tmp        = $uploadFolder.'.'. $extension;
                            $saveDir    = $saveDir.'.'.$extension;
                            $width  = $data['App_Logo_Login_Width'];
                            $height = $data['App_Logo_Login_Height'];
                            $this->Imagine->gdImageCropAndSave($source, $tmp,['width'=>$width,'height'=>$height]);
                            $valueSave = $saveDir;
                        }
                    }elseif($w == 'App_Login_Cover'){
                        $source = $d['tmp_name'];
                        if(!empty($source)){
                            $uploadFolder = WWW_ROOT.'assets'.DS.'img'.DS.'cover_login';
                            $saveDir = '/webroot/assets/img/cover_login';
                            $extension  = pathinfo($d['name'], PATHINFO_EXTENSION);
                            $tmp        = $uploadFolder.'.'. $extension;
                            $saveDir    = $saveDir.'.'.$extension;
                            $width  = 1920;
                            $height = 1080;
                            $this->Imagine->gdImageCropAndSave($source, $tmp,['width'=>$width,'height'=>$height]);
                            $valueSave = $saveDir;
                        }
                    }elseif($w == 'App_Favico'){
                        $source = $d['tmp_name'];
                        if(!empty($source)){
                            $uploadFolder = WWW_ROOT.'assets'.DS.'img'.DS.'favico';
                            $saveDir = '/webroot/assets/img/favico';
                            $extension  = pathinfo($d['name'], PATHINFO_EXTENSION);
                            $tmp        = $uploadFolder.'.'. $extension;
                            $saveDir    = $saveDir.'.'.$extension;
                            $width  = 30;
                            $height = 30;
                            $this->Imagine->gdImageCropAndSave($source, $tmp,['width'=>$width,'height'=>$height]);
                            $valueSave = $saveDir;
                        }
                    }elseif($w == 'App_Kop_Logo'){
                        $source = $d['tmp_name'];
                        if(!empty($source)){
                            $uploadFolder = WWW_ROOT.'assets'.DS.'img'.DS.'kop_logo';
                            $saveDir = '/webroot/assets/img/kop_logo';
                            $extension  = pathinfo($d['name'], PATHINFO_EXTENSION);
                            $tmp        = $uploadFolder.'.'. $extension;
                            $saveDir    = $saveDir.'.'.$extension;
                            $this->Imagine->onlySave($source, $tmp);
                            $valueSave = $saveDir;
                        }
                    }elseif($w == 'App_DPP_PIC_Signature'){
                        $source = $d['tmp_name'];
                        if(!empty($source)){
                            $uploadFolder = WWW_ROOT.'assets'.DS.'img'.DS.'signature_dpp';
                            $saveDir = '/webroot/assets/img/signature_dpp';
                            $extension  = pathinfo($d['name'], PATHINFO_EXTENSION);
                            $tmp        = $uploadFolder.'.'. $extension;
                            $saveDir    = $saveDir.'.'.$extension;
                            $this->Imagine->onlySave($source, $tmp);
                            $valueSave = $saveDir;
                        }
                    }else{
                        $valueSave = $d;
                    }
                    
                    if(!empty($valueSave)){
                        $dataUpdate['id'] = $id;
                        $dataUpdate['valueField'] = $valueSave;
                        $this->AppSettings->save($this->AppSettings->patchEntity($dataOriginal,$dataUpdate,['validate'=>false]));
                    }
                }
                $this->Redis->destroyCacheAppSettings();
                $this->Flash->success(__('Data DPP berhasil diedit.'));
                $this->redirect(['action'=>'index']);
            }else{
                $this->Flash->error(__('The app setting could not be saved. Please, try again.'));
            }
            
        }
        $titleModule = "App Settings";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "Edit ".$titleModule,
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','appSettings','dataSave'));
    }

}
