<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Core\Configure;
use Cake\Mailer\Email;

/**
 * ApplicationLetters Controller
 *
 * @property \App\Model\Table\ApplicationLettersTable $ApplicationLetters
 *
 * @method \App\Model\Entity\ApplicationLetter[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ApplicationLettersController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(!empty($this->userData)){
            if(php_sapi_name() !== 'cli'){
                $this->Auth->allow(['index','add','edit','view','delete','printSPk','uploadFiles','deletePicture','editCaption','applicationOutput','printOutput','printLaporanAkhir','slf','printSuratPernyataan','testMail','insertPictures','daftarSimak','printSimak','applicationSimak','printSimakPemeriksaan','clearSimakPemeriksaan','printPictures','hapusGambar','printPengawasan','uploadPengawasan','applicationRooms','printApplicationRooms','printSuratPernyataanV2','applicationJudgments','printApplicationJudgments','invoicing','printInvoice','printSlfNew', 'applicationNewSimak', 'printApplicationNewSimak']);
            }
        }else{
            if(php_sapi_name() !== 'cli'){
                $this->Auth->allow(['testMail','applicationOutput','printOutput','printLaporanAkhir','slf','printSuratPernyataan','printSuratPernyataanV2','printPengawasan','applicationJudgments','printApplicationJudgments','printSlfNew','printApplicationNewSimak']);
            }
        }
    }
    public function applicationRooms($id){
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => ['ApplicationPictures','HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors','ApplicationRoomms']
        ]);
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        if($this->request->is(['post','put'])){
            $getdata = $this->request->getData();
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter,$getdata);
            if($this->ApplicationLetters->save($applicationLetter)){
                $this->Flash->success(__('Pengawasan Unit Rumah Berhasil Disimpan.'));
                return $this->redirect(['action' => 'applicationRooms',$id]);
            }
            $this->Flash->error(__('Pengawasan Unit Rumah , silahkan ulangi kembali'));
            
        }
        $titleModule = "Pengajuan";
        $titlesubModule = "Pengawasan Kelengkapan Unit Rumah";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'applicationRooms',$id]) => 'Pengawasan Kelengkapan Unit Rumah',
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter','rooms','roomsUtilities','consultant'));
    }

    public function invoicing($id){
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => ['ApplicationInvoices','ApplicationInvoices.ApplicationInvoicesDetails','HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors','ApplicationRoomms']
        ]);
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        if($this->request->is(['post','put'])){
            $getdata = $this->request->getData();
            $getdata['application_invoice']['rate'] = $this->Utilities->generalitationNumber($getdata['application_invoice']['rate']);
            $getdata['application_invoice']['date'] = $this->Utilities->generalitationDateFormat($getdata['application_invoice']['date']);
            $getdata['application_invoice']['qty'] = $this->Utilities->generalitationNumber($getdata['application_invoice']['qty']);
            
            foreach($getdata['application_invoice']['application_invoices_details'] as $key => $row){
                if(empty($row['text'])){
                    unset($getdata['application_invoice']['application_invoices_details'][$key]);
                    if(!empty($row['id'])){
                        $this->ApplicationLetters->ApplicationInvoices->ApplicationInvoicesDetails->deleteAll(['id' => $row['id']]);
                    }
                }else{
                    $getdata['application_invoice']['application_invoices_details'][$key]['rate'] = $this->Utilities->generalitationNumber($getdata['application_invoice']['application_invoices_details'][$key]['rate']);
                    $getdata['application_invoice']['application_invoices_details'][$key]['qty'] = $this->Utilities->generalitationNumber($getdata['application_invoice']['application_invoices_details'][$key]['qty'],false);
                }
            }
            // dd($getdata);
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter,$getdata,[
                'associated' => [
                    'ApplicationInvoices',
                    'ApplicationInvoices.ApplicationInvoicesDetails',
                ]
            ]);
            
            // dd($applicationLetter);
            if($this->ApplicationLetters->save($applicationLetter)){
                $this->Flash->success(__('Invoicing Berhasil Disimpan.'));
                return $this->redirect(['action' => 'invoicing',$id]);
            }
            $this->Flash->error(__('Invoicing , silahkan ulangi kembali'));
            
        }
        if(!empty($applicationLetter->application_invoice)){
            $applicationLetter->application_invoice->date = $applicationLetter->application_invoice->date->format('d-m-Y');
        }
        $titleModule = "Pengajuan";
        $titlesubModule = "Invoicing";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'invoicing',$id]) => 'Invoicing',
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter','rooms','roomsUtilities','consultant'));
    }

    public function printInvoice($id){
        $this->viewBuilder()->autoLayout(false);
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => ['ApplicationInvoices','ApplicationInvoices.ApplicationInvoicesDetails','HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors','ApplicationRoomms']
        ]);
        
        $this->loadModel('RoomsUtilities');
        $this->loadModel('Consultants');
        
        $consultant = $this->Consultants->get(1);
        $titleModule = "Pengajuan";
        $titlesubModule = "Print Invoice";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'invoicing',$id]) => 'Print Invoice',
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter','rooms','roomsUtilities','roomsUtilities2','consultant'));
        $this->RequestHandler->renderAs($this, 'pdf');
    }

    public function applicationJudgments($id){
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => ['ApplicationPictures','HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors','ApplicationJudgments']
        ]);
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        if($this->request->is(['post','put'])){
            $getdata = $this->request->getData();
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter,$getdata);
            if($this->ApplicationLetters->save($applicationLetter)){
                $this->Flash->success(__('Penilaian Kelayakan Unit Rumah Berhasil Disimpan.'));
                return $this->redirect(['action' => 'applicationJudgments',$id]);
            }
            $this->Flash->error(__('Penilaian Kelayakan Rumah gagal disimpan, silahkan ulangi kembali'));
            
        }
        $titleModule = "Pengajuan";
        $titlesubModule = "Penilaian Kelayakan Unit Rumah";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'applicationRooms',$id]) => 'Penilaian Kelayakan Unit Rumah',
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter','rooms','roomsUtilities','consultant'));
    }
    public function printApplicationRooms($id){
        $this->viewBuilder()->autoLayout(false);
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => ['ApplicationPictures','HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors','ApplicationRoomms']
        ]);
        
        $this->loadModel('RoomsUtilities');
        $this->loadModel('Consultants');
        
        $consultant = $this->Consultants->get(1);
        $titleModule = "Pengajuan";
        $titlesubModule = "Pengawasan Kelengkapan Unit Rumah";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'applicationRooms',$id]) => 'Pengawasan Kelengkapan Unit Rumah',
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter','rooms','roomsUtilities','roomsUtilities2','consultant'));
        $this->RequestHandler->renderAs($this, 'pdf');
    }

    public function applicationNewSimak($id)
    {
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => ['ApplicationPictures','HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors','ApplicationNewSimaks']
        ]);

        if($this->request->is(['post','put'])){
            $getdata = $this->request->getData();
            unset($getdata['files']);
            // dd($getdata['application_new_simak']['data_12']);
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter, $getdata);
            if($this->ApplicationLetters->save($applicationLetter)){
                $this->Flash->success(__('SIMAK Baru Berhasil Disimpan.'));
                return $this->redirect(['action' => 'applicationNewSimak',$id]);
            }
            $this->Flash->error(__('SIMAK Baru gagal disimpan, silahkan ulangi kembali'));
            
        }
        $this->set(compact('applicationLetter'));

        $titleModule = "Form Pengajuan";
        $titlesubModule = "SIMAK Baru";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'applicationNewSimak',$id]) => $titlesubModule,
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','consultant'));
    }

    public function printApplicationJudgments($id){
        $this->viewBuilder()->autoLayout(false);
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => ['ApplicationPictures','HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors','ApplicationJudgments']
        ]);
        
        $this->loadModel('RoomsUtilities');
        $this->loadModel('Consultants');
        
        $consultant = $this->Consultants->get(1);
        $titleModule = "Pengajuan";
        $titlesubModule = "Penilaian Kelayakan Unit Rumah";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'applicationRooms',$id]) => 'Penilaian Kelayakan Unit Rumah',
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter','rooms','roomsUtilities','roomsUtilities2','consultant'));
        $this->RequestHandler->renderAs($this, 'pdf');
    }
    public function printApplicationNewSimak($id){
        $this->viewBuilder()->autoLayout(false)->options([
            'wakwaw'=>true
        ]);
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => ['ApplicationPictures','HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors','ApplicationNewSimaks']
        ]);
        
        $this->loadModel('RoomsUtilities');
        $this->loadModel('Consultants');
        
        $consultant = $this->Consultants->get(1);
        $titleModule = "Pengajuan";
        $titlesubModule = "SIMAK Baru";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'applicationRooms',$id]) => 'SIMAK Baru',
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter','rooms','roomsUtilities','roomsUtilities2','consultant'));
        $this->RequestHandler->renderAs($this, 'pdf');
    }
    public function clearSimakPemeriksaan($id)
    {
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => [
                'ApplicationSimaksPemeriksaans',
            ]
        ]);
        if($this->ApplicationLetters->ApplicationSimaksPemeriksaans->delete($applicationLetter->application_simaks_pemeriksaan)){
            $this->Flash->success(__('Daftar simak pemeriksaan berhasil di hapus.'));
            return $this->redirect(['action' => 'applicationSimak',$id]);
        }
        
        
    }
    public function printPictures($id)
    {
        $this->viewBuilder()->autoLayout(false);
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['ApplicationPictures','HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors']
        ]);
        $code = $consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->output_date->format('m')).'/CON/'.$applicationLetter->output_date->format('Y');

        $this->set(compact('applicationLetter','consultant'));

        $titleModule = "Print Foto Perumahan";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','consultant'));
        $this->RequestHandler->renderAs($this, 'pdf');
        
    }
    public function hapusGambar($id){
        $gambar = $this->ApplicationLetters->ApplicationPictures->get($id);
        $appId = $gambar->application_letter_id;
        $field = $this->request->query('type');
        $field_dir = $field."_dir";
        $gambar->$field = "";
        $gambar->$field_dir = "";
        if($this->ApplicationLetters->ApplicationPictures->updateAll([
            $field => NULL,
            $field_dir => NULL
        ],['id' => $id])){
            $this->Flash->success(__('Gambar berhasil di hapus.'));
            return $this->redirect(['action' => 'insertPictures',$appId]);
        }
    }
    public function testMail()
    {
        $this->loadModel('Developers');
        $id = 6;
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','Developers.DppFroms']
        ]);
        $email = new Email();
        $email
            ->template('activity', 'default')
            ->emailFormat('html')
            ->subject('[WEBSITE TCE][AKTIVITAS PENGAJUAN]')
            ->viewVars([
                'defaultAppSettings' => $this->defaultAppSettings,
                'applicationLetter' => $applicationLetter,
                'to' => $applicationLetter->developer->name,
                'catatan' => 'BENGEUT'
            ])
            ->to('aiqbalsyah@gmail.com')
            ->from('maildev.tce@gmail.com')
            ->send();
        
        $email = new Email();
        $email
            ->template('activity', 'default')
            ->emailFormat('html')
            ->subject('[WEBSITE TCE][AKTIVITAS PENGAJUAN]')
            ->viewVars([
                'defaultAppSettings' => $this->defaultAppSettings,
                'applicationLetter' => $applicationLetter,
                'to' => 'Pengurus DPP',
                'catatan' => 'TOT'
            ])
            ->to('aiqbalsyah@gmail.com')
            ->from('maildev.tce@gmail.com')
            ->send();
        $this->autoRender= false;
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->ApplicationLetters;
            $searchAble = [
                'ApplicationLetters.id',
                'ApplicationLetters.name_house',
                'ApplicationLetters.code',
                'Developers.name',
            ];
            $conditions = [];
            if($this->userData->group_id == 4){
                $conditions = [
                    [
                        'keyField' => 'Developers.dpp_from_id',
                        'condition' => '=',
                        'value' => $this->userData->master_dpd->province_id
                    ]
                ];
            }
            if($this->userData->group_id == 2){
                $conditions = [
                    [
                        'keyField' => 'Developers.id',
                        'condition' => '=',
                        'value' => $this->userData->developer->id
                    ]
                ];
            }
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => ['ApplicationLetters.status','ApplicationLetters.created'],
                'defaultSort' => ['ASC','DESC'],
                'contain' => [
                    'Developers'
                ],
                'defaultSearch' => $conditions
                    
            ];
            $MasterDpds   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $MasterDpds['data'];
            $meta = $MasterDpds['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Form Pengajuan";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    public function printSpk($id)
    {
        $this->viewBuilder()->autoLayout(false);
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers']
        ]);
        $this->set('titleModule','SPK NO: '.$applicationLetter->spk_code);
        $this->set(compact('applicationLetter','consultant'));
        $this->RequestHandler->renderAs($this, 'pdf');
    }

    public function daftarSimak($id)
    {
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => [
                'ApplicationSimaks'
            ]
        ]);

        if($this->request->is(['post','put'])){
            $data = $this->request->getData();
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter,$data,[
                'associated' => ['ApplicationSimaks']
            ]);
            if($this->ApplicationLetters->save($applicationLetter)){
                $this->Flash->success(__('Daftar simak berhasil di simpan.'));
                return $this->redirect(['action' => 'daftarSimak',$id]);
            }
            $this->Flash->error(__('Daftar Simak gagal disimpan, silahkan ulangi kembali'));
        }

        $this->set(compact('applicationLetter'));

        $titleModule = "Form Pengajuan";
        $titlesubModule = "Daftar Simak";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'daftarSimak',$id]) => $titlesubModule,
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','consultant'));   
    }

    public function applicationSimak($id)
    {
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => [
                'ApplicationSimaksPemeriksaans',
                'Developers',
                'Provinces',
                'Cities',
                'Districts'
            ]
        ]);

        if($this->request->is(['post','put'])){
            $data = $this->request->getData();
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter,$data,[
                'associated' => ['ApplicationSimaksPemeriksaans']
            ]);
            if($this->ApplicationLetters->save($applicationLetter)){
                $this->Flash->success(__('Daftar simak pemeriksaan berhasil di simpan.'));
                return $this->redirect(['action' => 'applicationSimak',$id]);
            }
            $this->Flash->error(__('Daftar Simak gagal disimpan, silahkan ulangi kembali'));
        }

        $this->set(compact('applicationLetter'));

        $titleModule = "Form Pengajuan";
        $titlesubModule = "Daftar Simak Pemeriksaan";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'applicationSimak',$id]) => $titlesubModule,
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','consultant'));
        
    }

    public function printSimak($id)
    {

        $this->viewBuilder()->autoLayout(false);
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => [
                'ApplicationSimaks'
            ]
        ]);

        $this->set(compact('applicationLetter','consultant'));

        $titleModule = "Form Pengajuan";
        $titlesubModule = "Daftar Simak";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'printSimak',$id]) => $titlesubModule,
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','consultant'));
        $this->RequestHandler->renderAs($this, 'pdf');
        
    }

    public function printSimakPemeriksaan($id)
    {

        $this->viewBuilder()->autoLayout(false);
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => [
                'ApplicationSimaksPemeriksaans',
                'Developers',
                'Provinces',
                'Cities',
                'Districts'
            ]
        ]);

        $this->set(compact('applicationLetter','consultant'));

        $titleModule = "Form Pengajuan";
        $titlesubModule = "Daftar Simak Pemeriksaan";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => 'View Pengajuan',
            Router::url(['action' => 'printSimakPemeriksaan',$id]) => $titlesubModule,
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','consultant'));
        // $this->RequestHandler->renderAs($this, 'pdf');
        
    }

    public function insertPictures($id)
    {
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => [
                'ApplicationPictures'
            ]
        ]);

        if($this->request->is(['post','put'])){
            $data = $this->request->getData();
            if(empty($data['tampak_depan']['name'])){
                unset($data['tampak_depan']);
            }
            if(empty($data['jalan_depan']['name'])){
                unset($data['jalan_depan']);
            }
            if(empty($data['ruang_tamu']['name'])){
                unset($data['ruang_tamu']);
            }
            if(empty($data['kamar_tidur']['name'])){
                unset($data['kamar_tidur']);
            }
            if(empty($data['plafond']['name'])){
                unset($data['plafond']);
            }
            if(empty($data['pintu_kusen_jendela']['name'])){
                unset($data['pintu_kusen_jendela']);
            }
            if(empty($data['toilet']['name'])){
                unset($data['toilet']);
            }
            if(empty($data['sumur_septictank']['name'])){
                unset($data['sumur_septictank']);
            }
            if(empty($data['kwh_listrik']['name'])){
                unset($data['kwh_listrik']);
            }
            if(empty($data['jaringan_air']['name'])){
                unset($data['jaringan_air']);
            }
            if(empty($data['saluran_pembuangan']['name'])){
                unset($data['saluran_pembuangan']);
            }
            if(empty($data['jalan_utama']['name'])){
                unset($data['jalan_utama']);
            }
            if(empty($data['jalan_komplek']['name'])){
                unset($data['jalan_komplek']);
            }
            if(empty($data['fasilitas_umum']['name'])){
                unset($data['fasilitas_umum']);
            }
            if(empty($data['tampak_atas_1']['name'])){
                unset($data['tampak_atas_1']);
            }
            if(empty($data['tampak_atas_2']['name'])){
                unset($data['tampak_atas_2']);
            }
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter,$data,[
                'associated' => ['ApplicationPictures']
            ]);
            // dd($applicationLetter);
            if($this->ApplicationLetters->save($applicationLetter)){
                $this->Flash->success(__('Gambar berhasil di simpan.'));
                return $this->redirect(['action' => 'printPictures',$id]);
            }
            // $this->Flash->error(__('Gambar gagal disimpan, silahkan ulangi kembali'));
        }
        $titleModule = "Form Pengajuan";
        $titlesubModule = "Upload Gambar Perumahan";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => "View ".$titleModule,
            Router::url(['action' => 'insertPictures',$id]) => $titlesubModule,
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter'));
    }

    public function printOutput($id)
    {
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors']
        ]);
        $code = $consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y');
        $this->set('titleModule','SLF No. : '.$code);
        $this->set(compact('applicationLetter','consultant'));
            
            
            // if($pdf == 1){
               
                $this->viewBuilder()->layout('printSlf')->options([
                    'pdfConfig' => [
                        'orientation' => 'portrait',
                        'filename' => $applicationLetter->code.".pdf",
                    ]
                ]);
                $this->RequestHandler->renderAs($this, 'pdf');
            // }
        
        // $this->RequestHandler->renderAs($this, 'pdf');
       
    }

    public function printPengawasan($id)
    {
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors']
        ]);
        $code = $consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y');
        $this->set('titleModule','SLF No. : '.$code);
        $this->set(compact('applicationLetter','consultant'));
            
            
            // if($pdf == 1){
               
                $this->viewBuilder()->layout('printPengawasan');
                $this->RequestHandler->renderAs($this, 'pdf');
            // }
        
        // $this->RequestHandler->renderAs($this, 'pdf');
       
    }

    public function printSuratPernyataan($id)
    {
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','Developers.DppFroms','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors']
        ]);
        $code = $applicationLetter->code;
        $this->set('titleModule','Surat Pernyataan No. : '.$code);
        $this->set(compact('applicationLetter','consultant'));
        $this->viewBuilder()->layout('printPernyataan');
            $this->RequestHandler->renderAs($this, 'pdf');
       
    }

    public function printSuratPernyataanV2($id)
    {
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors']
        ]);
        $code = $consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y');
        $this->set('titleModule','SLF No. : '.$code);
        $this->set(compact('applicationLetter','consultant'));
            
            
            // if($pdf == 1){
               
                $this->viewBuilder()->layout(false)->options([
                    'pdfConfig' => [
                        'orientation' => 'portrait',
                        'filename' => $applicationLetter->code.".pdf",
                    ]
                ]);
                $this->RequestHandler->renderAs($this, 'pdf');
            // }
        
        // $this->RequestHandler->renderAs($this, 'pdf');
       
    }

    public function printSlfNew($id)
    {
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors']
        ]);
        $code = $consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y');
        $this->set('titleModule','SLF No. : '.$code);
        $this->set(compact('applicationLetter','consultant'));
            
            
            // if($pdf == 1){
               
                $this->viewBuilder()->layout(false)->options([
                    'pdfConfig' => [
                        'orientation' => 'portrait',
                        'filename' => $applicationLetter->code.".pdf",
                    ]
                ]);
                $this->RequestHandler->renderAs($this, 'pdf');
            // }
        
        // $this->RequestHandler->renderAs($this, 'pdf');
       
    }

    public function printLaporanAkhir($id)
    {
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors']
        ]);
        $this->set('titleModule','Lembar Pemeriksaan No. : '.$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y'));
        $this->set(compact('applicationLetter','consultant'));
        // $this->viewBuilder()->layout('print');;
        $this->RequestHandler->renderAs($this, 'pdf');
    }

    public function slf($id)
    {
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors']
        ]);
        $this->set('titleModule','Sertifikat Laik Fungsi : '.$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y'));
        $this->set(compact('applicationLetter','consultant'));
        $this->viewBuilder()->layout('printDraftSlf');
        $this->RequestHandler->renderAs($this, 'pdf');
    }

    /**
     * View method
     *
     * @param string|null $id Application Letter id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => [
                'HouseTypes', 
                'Provinces', 
                'Cities', 
                'Districts', 
                'Villages', 
                'Developers',
                'HistoriesLetters',
                'HistoriesLetters.CreatedUsers',
                'ApplicationLettersPictures',
                'ApplicationOutputs',
                'ApplicationRoomms',
                'ApplicationJudgments',
                'ApplicationPictures',
                'applicationNewSimaks',
                'Supervisors' => [
                    'joinType' => 'left'
                ]
            ]
        ]);
        if($this->request->is('ajax')){
            $data= $this->request->getData();
            if($data['status'] == 2){
                $data['approved_by_dpp_date'] = date("Y-m-d");
                $getCode = $this->ApplicationLetters->find('all',[
                    'conditions' => [
                        'approved_by_dpp_date LIKE' => date("Y-m-")."%"
                    ]
                ])
                ->order('spk_code DESC')
                ->first();
                $code = 1;
                if(!empty($getCode)){
                    $code = $getCode->spk_code +1;
                }
                $data['spk_code'] = $code;
            }

            if($data['status'] == 1){
                $data['histories_letters'][0]['status'] = "Disetujui DPD";
            }elseif($data['status'] == 2){
                $data['histories_letters'][0]['status'] = "Disetujui DPP";
            }elseif($data['status'] == 3){
                $data['histories_letters'][0]['status'] = "Disetujui Konsultan";
            }elseif($data['status'] == 11){
                $data['histories_letters'][0]['status'] = "Ditolak DPD";
            }elseif($data['status'] == 22){
                $data['histories_letters'][0]['status'] = "Ditolak DPP";
            }elseif($data['status'] == 33){
                $data['histories_letters'][0]['status'] = "Ditolak Konsultan";
            }
            $catatan = "";
            if(empty($data['note'])){
                $data['note'] = '';
            }
            

            if(empty($data['message'])){
                $data['message'] = '';
            }
            $pesan = $data['note'];
            $catatan = $data['message'];
            
            $data['histories_letters'][0]['message'] = $data['message'];
            $data['histories_letters'][0]['note'] = $data['note'];
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter, $data,[
                'associated' => [
                    'HistoriesLetters'
                ]
            ]);
            if ($this->ApplicationLetters->save($applicationLetter)) {
                $toDev = $applicationLetter->developer->email;
                if (Configure::read('debug')) {
                    $toDev = "aiqbalsyah@gmail.com";
                }
                $email = new Email();
                $email
                    ->template('activity', 'default')
                    ->emailFormat('html')
                    ->subject('[WEBSITE TCE][AKTIVITAS PENGAJUAN]')
                    ->viewVars([
                        'defaultAppSettings' => $this->defaultAppSettings,
                        'applicationLetter' => $applicationLetter,
                        'to' => $applicationLetter->developer->name,
                        'catatan' => $catatan
                    ])
                    ->to($toDev)
                    ->from('maildev.tce@gmail.com')
                    ->send();
                
                if($applicationLetter->status == 1  || $applicationLetter->status == 33){
                    $this->loadModel('MasterDpps');
                    $dpp = $this->MasterDpps->get(1);
                    $to = 'Pengurus DPP';
                    $toEmail = $dpp->email;
                    if (Configure::read('debug')) {
                        $toEmail = "aiqbalsyah@gmail.com";
                    }
                }elseif($applicationLetter->status == 2 || $applicationLetter->status == 22){
                    $this->loadModel('Consultants');
                    $consultant = $this->Consultants->get(1);
                    $to = 'Konsultan';
                    $toEmail = $consultant->email;
                    if (Configure::read('debug')) {
                        $toEmail = "aiqbalsyah@gmail.com";
                    }
                }
                if(!empty($toEmail)){
                    $email = new Email();
                    $email
                        ->template('activity', 'default')
                        ->emailFormat('html')
                        ->subject('[WEBSITE TCE][AKTIVITAS PENGAJUAN]')
                        ->viewVars([
                            'defaultAppSettings' => $this->defaultAppSettings,
                            'applicationLetter' => $applicationLetter,
                            'to' => $to,
                            'catatan' => $pesan
                        ])
                        ->to($toEmail)
                        ->from('maildev.tce@gmail.com')
                        ->send();
                }
                
                $code = 200;
                $message = "Berhasil melakukan set status menjadi : ". $data['histories_letters'][0]['status'];
            }else{
                $code = 50;
                $message = "Gagal melakukan set status";
            }
            $this->set(compact('code','message'));
            $this->set('_serialize',['code','message']);
        }else{
            $this->set('applicationLetter', $applicationLetter);
            $titleModule = "Form Pengajuan";
            $titlesubModule = "View  ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => "List ".$titleModule,
                Router::url(['action' => 'view',$id]) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule','consultant'));
        }
        
    }

    public function uploadFiles($id)
    {
        $applicationPicture = $this->ApplicationLetters->ApplicationLettersPictures->newEntity();
        if($this->request->is(['post','put'])){
            $data = $this->request->getData();
            $data['application_letter_id'] = $id;
            $applicationPicture = $this->ApplicationLetters->ApplicationLettersPictures->patchEntity($applicationPicture,$data,['validate' => false]);
            if ($this->ApplicationLetters->ApplicationLettersPictures->save($applicationPicture)) {
                $message = __('The file uploaded.');
                $status = 'success';
                $code = 200;
            } else {
                $code = 99;
                $message = __('The file could not be uploaded. Please, try again.');
                $status = 'error';
            }
            $this->set('code',['code' => $code,'applicationPicture' => $applicationPicture]);
            $this->set('_serialize','code');
        }else{
            $applicationLetter = $this->ApplicationLetters->get($id,[
                'contain' => [
                    'ApplicationLettersPictures' => [
                        'sort' => 'id DESC'
                    ]
                ]
            ]);
            $titleModule = "Form Pengajuan";
            $titlesubModule = "Upload File";
            $breadCrumbs = [
                Router::url(['action' => 'index']) => "List ".$titleModule,
                Router::url(['action' => 'view',$id]) => "View ".$titleModule,
                Router::url(['action' => 'uploadFiles',$id]) => $titlesubModule,
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter'));
        }
    }
    public function editCaption()
    {
        if($this->request->is(['post','put'])){
            $data = $this->request->getData();
            $id = $data['id'];
            $applicationPicture = $this->ApplicationLetters->ApplicationLettersPictures->get($id);
            $applicationPicture = $this->ApplicationLetters->ApplicationLettersPictures->patchEntity($applicationPicture,$data,['validate' => false]);
            if ($this->ApplicationLetters->ApplicationLettersPictures->save($applicationPicture)) {
                $message = __('Caption berhasil diubah');
                $status = 'success';
                $code = 200;
            } else {
                $code = 99;
                $message = __('The file could not be uploaded. Please, try again.');
                $status = 'error';
            }
            $this->set('code',['code' => $code,'applicationPicture' => $applicationPicture,'message'=> $message]);
            $this->set('_serialize','code');
        }
    }

    public function applicationOutput($id)
    {
        $applicationLetter = $this->ApplicationLetters->get($id,[
            'contain' => [
                'ApplicationOutputs'
            ]
        ]);
        
        $supervisors = $this->ApplicationLetters->Supervisors->find('list')->order(['name asc']);
        $titleModule = "Form Pengajuan";
        $titlesubModule = "Input Surat Pernyataan NO. Pengajuan : ". $applicationLetter->code;
        
        if($this->request->is(['post','put'])){
            $data = $this->request->getData();
            $data['output_date'] = $this->Utilities->generalitationDateFormat($data['output_date']);
            $data['application_output']['application_letter_id'] = $id;
            $data['application_output']['code_house'] = $applicationLetter->sample_house;
            $data['histories_letters'][0]['status'] = "Konsultan Membuat Laporan Akhir";    
            if(!empty($applicationLetter->application_output)){
                $data['histories_letters'][0]['status'] = "Konsultan Edit Laporan Akhir";    
            }
            if(empty($applicationLetter->output_date)){
                $date_2 = "";
            }else{
                $date_2 = $applicationLetter->output_date->format('Y-m-d');
            }
            
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter,$data,[
                'associated' => [
                    'ApplicationOutputs',
                    'HistoriesLetters'
                ]
            ]);

            if ($this->ApplicationLetters->save($applicationLetter)) {
                $this->Flash->success(__('Surat pernyataan berhasil di simpan.'));
                // $this->loadModel('Consultants');
                // $consultant = $this->Consultants->get(1);
                // $CakePdf = new \CakePdf\Pdf\CakePdf();
                // $CakePdf->template('print_output', 'default');
                // $CakePdf->viewVars(['applicationLetter'=>$applicationLetter,'consultant'=>$consultant]);
                // $path = WWW_ROOT . 'files' . DS . 'surat_pernyataan' . DS . 'surat_pernyataan_'. \Cake\Utility\Inflector::underscore($applicationLetter->name_house) . '.pdf';
                // $pdf = $CakePdf->write($path);
                return $this->redirect(['action' => 'applicationOutput',$id]);
            }
            $this->Flash->error(__('Surat pernyataan gagal disimpan, silahkan ulangi kembali'));
        }
        if(!empty($applicationLetter->output_date)){
            $applicationLetter->output_date = $applicationLetter->output_date->format('d-m-Y');
        }
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => "View ".$titleModule,
            Router::url(['action' => 'applicationOutput',$id]) => $titlesubModule,
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule','applicationLetter','supervisors'));
        
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $applicationLetter = $this->ApplicationLetters->newEntity();
        $provinces = $this->ApplicationLetters->Provinces->find('list')->order('name ASC');;
        $cities = [];
        $districts = [];
        $villages = [];
        $building_list= [];
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            // pr($data);
            // $this->autoRender =false;
            $data['developer_id'] = $this->userData->developer->id;
            $data['price_per_unit'] = $this->Utilities->generalitationNumber($data['price_per_unit']);
            $building_list = $data['building'];
            foreach($data['building'] as $key => $val){
                $building_list[$val] = $val;
            }
            $data['building'] = implode($data['building'],",");
            $data['histories_letters'][0]['status'] = "Pengajuan Baru";
            $data['histories_letters'][0]['note'] = "";
            $data['histories_letters'][0]['message'] = "";
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter, $data,[
                'associated' => [
                    'HistoriesLetters'
                ]
            ]);
            if ($this->ApplicationLetters->save($applicationLetter)) {
                $id = $applicationLetter->id;
                $applicationLetter = $this->ApplicationLetters->get($id, [
                    'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','Developers.DppFroms']
                ]);
                $email = new Email();
                $email->template('pengajuan', 'default')
                ->emailFormat('html')
                ->subject('[WEBSITE TCE][PENGAJUAN BARU]')
                ->viewVars([
                    'defaultAppSettings' => $this->defaultAppSettings,
                    'applicationLetter' => $applicationLetter
                ]);
                
                if (Configure::read('debug')) {
                    $email->to('aiqbalsyah@gmail.com');
                }else{
                    $this->loadModel('MasterDpps');
                    $dpp = $this->MasterDpps->get(1);
                    $this->loadModel('MasterDpds');
                    $dpd = $this->MasterDpds->find('all',[
                        'conditions' => [
                            'province_id' => $applicationLetter->developer->dpp_from_id
                        ],
                        'contain' => ['Users']
                    ])->first();
                    $email->to($dpd->user->email)->cc($dpp->email);
                }
                $email->from('maildev.tce@gmail.com')
                ->send();
                $this->Flash->success(__('Pengajuan berhasil disimpan.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Pengajuan gagal disimpan, silahkan ulangi kembali'));
            
            $cities = $this->ApplicationLetters->Cities->find('list',['conditions'=>[
                'province_id' => $applicationLetter->province_id
            ]])->order('name ASC');;
            $districts = $this->ApplicationLetters->Districts->find('list',['conditions'=>[
                'city_id' => $applicationLetter->city_id
            ]])->order('name ASC');;
            $villages = $this->ApplicationLetters->Villages->find('list',['conditions'=>[
                'district_id' => $applicationLetter->district_id
            ]])->order('name ASC');;
        }
        $houseTypes = $this->ApplicationLetters->HouseTypes->find('list');
        
        $this->set(compact('applicationLetter', 'houseTypes', 'provinces', 'cities', 'districts', 'villages', 'developers','building_list'));
        
        $titleModule = "Form Pengajuan";
        $titlesubModule = "Tambah  Pengajuan Baru";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Application Letter id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => []
        ]);
        $provinces = $this->ApplicationLetters->Provinces->find('list')->order('name ASC');;
        $cities = [];
        $districts = [];
        $villages = [];
        $building_list = [];
        $building_lists = explode(",",$applicationLetter->building);
        foreach($building_lists as $key => $val){
            $building_list[$val] = $val;
        }
        if ($this->request->is(['post','put'])) {
            $data = $this->request->getData();
            // pr($data);
            // $this->autoRender =false;
            // $data['developer_id'] = $this->userData->developer->id;
            $data['price_per_unit'] = $this->Utilities->generalitationNumber($data['price_per_unit']);
            $building_list = $data['building'];
            foreach($data['building'] as $key => $val){
                $building_list[$val] = $val;
            }
            $data['building'] = implode($data['building'],",");
            $data['histories_letters'][0]['status'] = "Edit Data";
            $data['histories_letters'][0]['note'] = "";
            $data['histories_letters'][0]['message'] = "";
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter, $data,[
                'associated' => [
                    'HistoriesLetters'
                ]
            ]);
            if ($this->ApplicationLetters->save($applicationLetter)) {
                $this->Flash->success(__('Pengajuan berhasil diedit.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Pengajuan gagal disimpan, silahkan ulangi kembali'));
        }
        $cities = $this->ApplicationLetters->Cities->find('list',['conditions'=>[
            'province_id' => $applicationLetter->province_id
        ]])->order('name ASC');;
        $districts = $this->ApplicationLetters->Districts->find('list',['conditions'=>[
            'city_id' => $applicationLetter->city_id
        ]])->order('name ASC');;
        $villages = $this->ApplicationLetters->Villages->find('list',['conditions'=>[
            'district_id' => $applicationLetter->district_id
        ]])->order('name ASC');;
        $houseTypes = $this->ApplicationLetters->HouseTypes->find('list');
        
        $this->set(compact('applicationLetter', 'houseTypes', 'provinces', 'cities', 'districts', 'villages', 'developers','building_list'));
        
        $titleModule = "Form Pengajuan";
        $titlesubModule = "Edit Data Pengajuan";
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }
    public function uploadPengawasan($id = null)
    {
        $applicationLetter = $this->ApplicationLetters->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['post','put'])) {
            $data = $this->request->getData();
            $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter, $data,[
                
            ]);
            if ($this->ApplicationLetters->save($applicationLetter)) {
                $this->Flash->success(__('Upload Pengawasan Berhasil Diupload.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Upload Pengawasan gagal disimpan, silahkan ulangi kembali'));
        }
        
        $this->set(compact('applicationLetter'));
        
        $titleModule = "Form Pengajuan";
        $titlesubModule = "Upload Pengawasan";
        $breadCrumbs = [
            Router::url(['action' => 'view',$id]) => "View ".$titleModule,
            Router::url(['action' => 'uploadPengawasan',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Application Letter id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->ApplicationLetters->get($id);
        if ($this->ApplicationLetters->delete($group)) {
            $code = 200;
            $message = __('Pengajuan berhasil dihapus');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The group could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }

    public function deletePicture($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->ApplicationLetters->ApplicationLettersPictures->get($id);
        if ($this->ApplicationLetters->ApplicationLettersPictures->delete($group)) {
            $code = 200;
            $message = __('Gambar berhasil dihapus');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The group could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
