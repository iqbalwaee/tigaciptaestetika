<?php
// src/Controller/Component/UtilitiesComponent.php
namespace App\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Controller\Component;
use Cake\Cache\Cache;
use Cake\Routing\Router;

class UtilitiesComponent extends Component
{
    public $components = ['Auth','Acl.Acl'];

    public function generateUrlImage($img_dir=null,$img,$prefix = null,$img_not_found = null)
    {
		//full_path_dir
        $baseDir = "";
        if($img_dir == null){
            $img_dir = $img;
            if(substr($img_dir,0,1) == "/" || substr($img_dir,0,1) == "\""){
                $img_dir = substr($img_dir,1);
            }
            $changeSlash 		= str_replace("\\",DS,$img_dir);
            $changeSlash		= str_replace("/",DS,$changeSlash); 
            $changeSlash        = str_replace("webroot\\","",$changeSlash);
            $changeSlash        = str_replace("webroot/","",$changeSlash);
            $full_path = WWW_ROOT.$changeSlash;
            $noDir = true;
        }else{
            $img_dir = $baseDir.$img_dir."/";
            $img = $prefix.$img;
            $changeSlash 		= str_replace("\\",DS,$img_dir);
            $changeSlash		= str_replace("/",DS,$changeSlash); 
            $full_path = ROOT.DS.$changeSlash.$img;
            $noDir = false;
        }
        
		//check image exist
		if(file_exists($full_path)){
            if($noDir == false){
                $dir 		= str_replace("\\","/",$img_dir).$img;
            }else{
                $dir 		= str_replace("\\","/",$img_dir);
            }
			
			$dir = str_replace("webroot/","",$dir);
			$url = Router::url("/".$dir,true);
		}else{
			if($img_not_found == null){
				$url = Router::url("/img/no-image.png",true);
			}else{
				$url = Router::url("/".$img_not_found,true);
			}
		}
		return $url;
    }

    public function getErrors($errors){
        $li = "";
        foreach($errors as $key => $error){
            if(is_array($error)){
                $li .= $this->getErrors($error);
            }else{
                $li .= "<li>".$error."</li>";
            }
        }
        return $li;
    }
    
    public function monthArray()
    {
        $array = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        return $array;
    }
    
    public function indonesiaDateFormat($tanggal,$time = false,$toIndonesia = true)
    {
        $bulan = $this->monthArray();
        $datepick = explode(" ", $tanggal);
        $split = explode("-", $datepick[0]);
        if($toIndonesia == true){
            if($time == false){
                return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
            }else{
                return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0]. ' ' . $datepick[1];
            }
        }else{
            if($time == false){
                return $split[2] . '-' . $split[1]. '-' . $split[0];
            }else{
                return $split[2] . '-' . $split[1] . '-' . $split[0].' '.$datepick[1];
            }
        }
        
    }  

    public function generalitationDateFormat($tanggal,$time = false)
    {
        $datepick = explode(" ", $tanggal);
        $split = explode("-", $datepick[0]);
        if($time){
            return $split[2] . '-' . $split[1] . '-' . $split[0]. " " . $datepick[1]; 
        }else{
            return $split[2] . '-' . $split[1] . '-' . $split[0]; 
        }
           
    }
    
	public function generalitationNumber($number,$dec = true){
        if($dec == true ){
            return str_replace(",","",$number);
        }else{
            $data = str_replace(",","",$number);
            return number_format($data,0) ;
        }
        
    }

    public function romawi($n){
        $hasil = "";
        $iromawi = array("","I","II","III","IV","V","VI","VII","VIII","IX","X",20=>"XX",30=>"XXX",40=>"XL",50=>"L",
        60=>"LX",70=>"LXX",80=>"LXXX",90=>"XC",100=>"C",200=>"CC",300=>"CCC",400=>"CD",500=>"D",600=>"DC",700=>"DCC",
        800=>"DCCC",900=>"CM",1000=>"M",2000=>"MM",3000=>"MMM");
        if(array_key_exists($n,$iromawi)){
            $hasil = $iromawi[$n];
        }elseif($n >= 11 && $n <= 99){
            $i = $n % 10;
            $hasil = $iromawi[$n-$i] . $this->romawi($n % 10);
        }elseif($n >= 101 && $n <= 999){
            $i = $n % 100;
            $hasil = $iromawi[$n-$i] . $this->romawi($n % 100);
        }else{
            $i = $n % 1000;
            $hasil = $iromawi[$n-$i] . $this->romawi($n % 1000);
        }
        return $hasil;
    }

}

?>