<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Consultants Controller
 *
 * @property \App\Model\Table\ConsultantsTable $Consultants
 *
 * @method \App\Model\Entity\Consultant[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ConsultantsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(!empty($this->userData)){
            if(php_sapi_name() !== 'cli'){
                $this->Auth->allow(['index','removeSka']);
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $id = 1;
        $consultant = $this->Consultants->get($id, [
            'contain' => [
                'Users'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['user']['name'] = $data['name'];
            $data['user']['email'] = $data['email'];
            $consultant = $this->Consultants->patchEntity($consultant, $data,[
                'associated'=>[
                    'Users' => [
                        'validate'=>'special'
                    ]
                ]
            ]);
            if(empty($consultant->user->password)){
                unset($consultant->user->password);
            }
            if ($this->Consultants->save($consultant)) {
                $this->Flash->success(__('Data konsultan berhasil diubah.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Data konsultan gagal diubah, silahkan ulangi kembali.'));
        }
        $this->set(compact('consultant', 'users'));
        $titleModule = "Data Konsultan";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    public function removeSka($ska)
    {
        $id = 1;
        $ska = 'ska_'.$ska;
        $ska_dir = $ska.'_dir';
        $this->Consultants->updateAll(
            [  // fields
                $ska => NULL,
                $ska_dir => NULL
            ],
            [  // conditions
                'id' => 1
            ]
        );
        return $this->redirect(['action' => 'index']);
    }

}
