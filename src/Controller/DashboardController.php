<?php
namespace App\Controller;

use App\Controller\AppController;

class DashboardController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        if(php_sapi_name() !== 'cli'){
            $this->Auth->allow(['index']);
        }
        
    }
    public function index()
    {
        // $this->Utilities->destroyCacheUrlHome();
    }
}