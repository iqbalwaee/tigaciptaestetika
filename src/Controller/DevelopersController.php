<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * Developers Controller
 *
 * @property \App\Model\Table\DevelopersTable $Developers
 *
 * @method \App\Model\Entity\Developer[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DevelopersController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(!empty($this->userData)){
            if(php_sapi_name() !== 'cli'){
                $this->Auth->allow(['index','add','edit','view','delete']);
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->Developers;
            $searchAble = [
                'Developers.id',
                'Developers.name',
                'Developers.ceo_name',
                'Developers.email',
                'Developers.name',
                'DppFroms.name',
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => ['Developers.status','Developers.id'],
                'defaultSort' => ['asc','desc'],
                'contain' => [
                    'DppFroms'
                ]
            ];
            $groups   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $groups['data'];
            $meta = $groups['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Data Developer";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Developer id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $developer = $this->Developers->get($id, [
            'contain' => ['DppFroms', 'Users']
        ]);

        $this->set('developer', $developer);

        $titleModule = "Data Developer";
        $titlesubModule = "View  ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Developer id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $developer = $this->Developers->get($id, [
            'contain' => [
                'Users'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $data['user']['name'] = $data['name'];
            $data['user']['email'] = $data['email'];
            $developer = $this->Developers->patchEntity($developer, $data,[
                'associated' => [
                    'Users' => [
                        'validate'=> 'special'
                    ]
                ]
            ]);
            if($developer->user->password == ""){
                unset($developer->user->password);
            }
            if ($this->Developers->save($developer)) {
                $this->Flash->success(__('Data Developer berhasil di ubah'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Data developer gagal diubah, Silahkan ulangi kembali.'));
        }
        $dppFroms = $this->Developers->DppFroms->find('list')->order('name ASC');
        $this->set(compact('developer', 'dppFroms', 'users'));

        $titleModule = "Data Developer";
        $titlesubModule = "Edit  ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Developer id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $data = $this->Developers->get($id);
        if ($this->Developers->delete($data)) {
            $code = 200;
            $message = __('Data developer telah dihapus.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('Data developer gagal dihapus');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
