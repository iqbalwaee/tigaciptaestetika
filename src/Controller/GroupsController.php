<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Routing\Router;
/**
 * Groups Controller
 *
 * @property \App\Model\Table\GroupsTable $Groups
 *
 * @method \App\Model\Entity\Group[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class GroupsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->Groups;
            $searchAble = [
                'Groups.id',
                'Groups.name'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'Groups.id',
                'defaultSort' => 'desc',
                    
            ];
            $groups   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $groups['data'];
            $meta = $groups['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "Group";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
        
        
    }

    /**
     * View method
     *
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => ['Users','CreatedUsers']
        ]);

        $this->set('group', $group);
        $this->set('_serialize',['group']);
        $titleModule = "Group";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $group = $this->Groups->newEntity();
        if ($this->request->is('post')) {
            $group = $this->Groups->patchEntity($group, $this->request->getData());
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The group could not be saved. Please, try again.'));
        }
        $this->set(compact('group'));
        $titleModule = "Group";
        $titlesubModule = "Create ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $group = $this->Groups->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $group = $this->Groups->patchEntity($group, $this->request->getData());
            if ($this->Groups->save($group)) {
                $this->Flash->success(__('The group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The group could not be saved. Please, try again.'));
        }
        $this->set(compact('group'));
        $titleModule = "Group";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Group id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->Groups->get($id);
        if ($this->Groups->delete($group)) {
            $code = 200;
            $message = __('The group has been deleted.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The group could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }

    public function configure($id = null)
    {
        $group = $this->Groups->get($id,['contain'=> ['Aros','Users','Users.Aros']]);
        $users  = $group->users;
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            foreach($data['aco_id'] as $key => $r){
                //check parent child aco//
                $this->Acl->deny($group,$key);
                $allow = false;
                foreach($r as $skey => $rd){
                    if($rd == 0){
                        $this->Acl->deny($group,$skey);
                        foreach($users as $keyuser => $user){
                            $this->Acl->deny($user,$skey);
                            $acl = explode("/",$skey);
                            $url = Router::url(['controller'=>$acl[2],'action'=>$acl[3]]);
                            $this->Redis->deleteCacheAcos($user->id,$url);
                        }
                    }else{
                        $this->Acl->allow($group,$skey);
                        if($allow == false){
                            $allow = true;
                        }
                    }
                }
                if($allow){
                    $this->Acl->allow($group,$key);
                }else{
                    foreach($users as $keyuser => $user){
                        $this->Redis->destroyCacheUrlHome($user->aro->id);
                        $this->Redis->destroyCacheSideNav($user->aro->id);
                        $aroUser = $user->aro;
                        $this->Acl->deny($user,$key);
                    }
                }
                

            }
            $this->Flash->success(__('The group has been configured.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->loadModel('Acos');
        $acoParent = $this->Acos->find('all',['conditions'=>['alias'=>'controllers']])->first();
        $acos  = $this->Acos->find('all', ['conditions'=>[
            'status' => 0,
            'Acos.lft >' => $acoParent->lft,
            'Acos.rght <' => $acoParent->rght
        ]])
        ->select([
            'Acos__id' => 'Acos.id', 
            'Acos__parent_id' => 'Acos.parent_id', 
            'Acos__name' => 'Acos.name', 
            'Acos__model' => 'Acos.model', 
            'Acos__foreign_key' => 'Acos.foreign_key', 
            'Acos__alias' => 'Acos.alias', 
            'Acos__lft' => 'Acos.lft', 
            'Acos__rght' => 'Acos.rght', 
            'Acos__status' => 'Acos.status',
            'Acos__read' => 'IFNULL(UserPermissions._read,-1)'
            ]
        )
        ->join([
            [
                'table' => 'aros_acos',
                'alias' => 'UserPermissions',
                'type' => 'LEFT',
                'conditions' => 'Acos.id = (UserPermissions.aco_id) AND UserPermissions.aro_id = '.$group->aro->id,
            ],[
                'table' => 'aros',
                'alias' => 'UserAros',
                'type' => 'LEFT',
                'conditions' => 'UserAros.id = (UserPermissions.aro_id) AND UserAros.model = "Groups" AND UserAros.foreign_key = '.$id,
            ]
        ])
        ->group('Acos__id')
        ->order('Acos.sort ASC')
        ->find('threaded');
        $this->set(compact('group','acos'));
        $titleModule = "Group";
        $titlesubModule = "Configure ".$titleModule. " ". $group->name;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'configure',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }
}
