<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\Utility\Inflector;

/**
 * MasterDpds Controller
 *
 * @property \App\Model\Table\MasterDpdsTable $MasterDpds
 *
 * @method \App\Model\Entity\MasterDpd[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MasterDpdsController extends AppController
{


    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(!empty($this->userData)){
            if(php_sapi_name() !== 'cli'){
                $this->Auth->allow(['index','add','edit','view','delete']);
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->MasterDpds;
            $searchAble = [
                'MasterDpds.id',
                'MasterDpds.name',
                'MasterDpds.email',
                'Provinces.name',
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'MasterDpds.id',
                'defaultSort' => 'desc',
                'contain' => [
                    'Provinces'
                ]
                    
            ];
            $MasterDpds   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $MasterDpds['data'];
            $meta = $MasterDpds['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "DPD";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Master Dpd id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $masterDpd = $this->MasterDpds->get($id, [
            'contain' => ['Provinces', 'Users']
        ]);

        $this->set('masterDpd', $masterDpd);

        $titleModule = "DPD";
        $titlesubModule = "View  ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $masterDpd = $this->MasterDpds->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $getProvince = $this->MasterDpds->Provinces->get($data['province_id']);
            $data['user']['username'] = Inflector::camelize($getProvince->name);
            $data['user']['name'] = $data['name'];
            $data['user']['email'] = $data['email'];
            $data['user']['group_id'] = 4;
            $data['user']['status'] = 1;
            $masterDpd = $this->MasterDpds->patchEntity($masterDpd, $data,[
                'associated'=>[
                    'Users' => [
                        'validate'=>'special'
                    ]
                ]
            ]);
            if ($this->MasterDpds->save($masterDpd)) {
                $this->Flash->success(__('Data DPD berhasil disimpan.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Data DPD gagal disimpan, silahkan ulangi kembali.'));
        }
        $provinces = $this->MasterDpds->Provinces->find('list')->order('name asc');
        $this->set(compact('masterDpd', 'provinces', 'users'));
        $titleModule = "DPD";
        $titlesubModule = "Add  ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Master Dpd id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $masterDpd = $this->MasterDpds->get($id, [
            'contain' => [
                'Users'
            ]
        ]);
        unset($masterDpd->user->password);
        if ($this->request->is(['post','put'])) {
            $data = $this->request->getData();
            $getProvince = $this->MasterDpds->Provinces->get($data['province_id']);
            $data['user']['username'] = Inflector::camelize($getProvince->name);
            $data['user']['name'] = $data['name'];
            $data['user']['email'] = $data['email'];
            $data['user']['group_id'] = 4;
            $data['user']['status'] = 1;
            $masterDpd = $this->MasterDpds->patchEntity($masterDpd, $data,[
                'associated'=>[
                    'Users' => [
                        'validate'=>'special'
                    ]
                ]
            ]);
            if($masterDpd->user->password == ""){
                unset($masterDpd->user->password);
            }
            if ($this->MasterDpds->save($masterDpd)) {
                $this->Flash->success(__('Data DPD berhasil diubah.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('Data DPD gagal diubah,silahkan ulangi kembali'));
        }
        $provinces = $this->MasterDpds->Provinces->find('list')->order('name asc');
        $this->set(compact('masterDpd', 'provinces', 'users'));
        $titleModule = "DPD";
        $titlesubModule = "Edit  ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Master Dpd id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->MasterDpds->get($id);
        if ($this->MasterDpds->delete($group)) {
            $code = 200;
            $message = __('The master dpd has been deleted.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The master dpd could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
