<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;

/**
 * MasterDpps Controller
 *
 * @property \App\Model\Table\MasterDppsTable $MasterDpps
 *
 * @method \App\Model\Entity\MasterDpp[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class MasterDppsController extends AppController
{

    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(!empty($this->userData)){
            if(php_sapi_name() !== 'cli'){
                $this->Auth->allow(['index','add','edit','view','delete']);
            }
        }
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if($this->request->is('ajax')){
            $source = $this->MasterDpps;
            $searchAble = [
                'MasterDpps.id',
                'MasterDpps.name',
                'MasterDpps.email',
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'MasterDpps.id',
                'defaultSort' => 'desc',
                    
            ];
            $MasterDpps   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $MasterDpps['data'];
            $meta = $MasterDpps['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "DPP";
            $titlesubModule = "List ".$titleModule;
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
    }

    /**
     * View method
     *
     * @param string|null $id Master Dpp id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $masterDpp = $this->MasterDpps->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('masterDpp', $masterDpp);
        $titleModule = "DPP";
        $titlesubModule = "View  ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $masterDpp = $this->MasterDpps->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $data['user']['name'] = $data['name'];
            $data['user']['email'] = $data['email'];
            $data['user']['group_id'] = 3;
            $data['user']['status'] = 1;
            $masterDpp = $this->MasterDpps->patchEntity($masterDpp, $data,[
                'associated'=>[
                    'Users' => [
                        'validate'=>'special'
                    ]
                ]
            ]);
            if ($this->MasterDpps->save($masterDpp)) {
                $this->Flash->success(__('The master Dpp has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The master Dpp could not be saved. Please, try again.'));
        }
        $this->set(compact('masterDpp', 'provinces', 'users'));
        $titleModule = "Dpp";
        $titlesubModule = "Add  ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Master Dpp id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $masterDpp = $this->MasterDpps->get($id, [
            'contain' => [
                'Users'
            ]
        ]);
        unset($masterDpp->user->password);
        if ($this->request->is(['post','put'])) {
            $data = $this->request->getData();
            $data['user']['name'] = $data['name'];
            $data['user']['email'] = $data['email'];
            $data['user']['group_id'] = 3;
            $data['user']['status'] = 1;
            $masterDpp = $this->MasterDpps->patchEntity($masterDpp, $data,[
                'associated'=>[
                    'Users' => [
                        'validate'=>'special'
                    ]
                ]
            ]);
            if($masterDpp->user->password == ""){
                unset($masterDpp->user->password);
            }
            if ($this->MasterDpps->save($masterDpp)) {
                $this->Flash->success(__('The master Dpp has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The master Dpp could not be saved. Please, try again.'));
        }
        $this->set(compact('masterDpp', 'provinces', 'users'));
        $titleModule = "Dpp";
        $titlesubModule = "Add  ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Master Dpp id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $group = $this->MasterDpps->get($id);
        if ($this->MasterDpps->delete($group)) {
            $code = 200;
            $message = __('The master Dpp has been deleted.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The master Dpp could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }
}
