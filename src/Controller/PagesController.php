<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
use Cake\I18n\Time;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Core\Configure;
use Cake\Mailer\Email;

class PagesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        if(php_sapi_name() !== 'cli'){
            $this->Auth->allow(['index','logout','editProfile','activitiesLog','uploadMedia','register','dashboardDeveloper','logoff','checkPengawas','uploadPengawas','qrCode','supervisorProfile','home','registerDeveloper','loginDeveloper','loginDpd','loginDpp','loginKonsultan']);
        }
    }

    public function qrCode($id)
    {
        $this->viewBuilder()->autoLayout(false);
        $this->loadModel('Consultants');
        $this->loadModel('ApplicationLetters');
        $consultant = $this->Consultants->get(1);
        $applicationLetter = $this->ApplicationLetters->find('all',[
            'conditions' =>[
                'ApplicationLetters.code' => $id
            ],
            'contain' => ['HouseTypes', 'Provinces', 'Cities', 'Districts', 'Villages', 'Developers','HistoriesLetters','HistoriesLetters.CreatedUsers','ApplicationOutputs','Supervisors']
        ])->first();
        $this->set('titleModule','Lembar Pemeriksaan No. : '.$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y'));
        $this->set(compact('applicationLetter','consultant'));
    }

    public function supervisorProfile()
    {
        $this->loadModel('Consultants');
        $this->loadModel('ApplicationLetters');
        $consultant = $this->Consultants->get(1);
        $this->set('titleModule','Data Konsultan');
        $this->set(compact('consultant'));
    }


    public function register()
    {
        $this->loadModel('Developers');
        $developer = $this->Developers->newEntity([
            'associated' => [
                'Users'
            ]
        ]);
        if($this->request->is('post')){
            $data = $this->request->getData();
            $data['user']['name'] = $data['name'];
            $data['user']['email'] = $data['email'];
            $data['user']['status'] = 0;
            $data['user']['group_id'] = 2;
            $developer = $this->Developers->patchEntity($developer,$data,[
                'associated' => [
                    'Users' => [
                        'validate'=> 'special'
                    ]
                ]
            ]);
            if($this->Developers->save($developer)){
                $id = $developer->id;
                
                $developer = $this->Developers->get($id, [
                    'contain' => ['DppFroms', 'Users']
                ]);
                if (Configure::read('debug')) {
                    $to = 'aiqbalsyah@gmail.com';
                }else{
                    $this->loadModel('MasterDpps');
                    $dpp = $this->MasterDpps->get(1);
                    $to = $dpp->email;
                }
                $email = new Email();
                $email
                    ->template('notifications', 'default')
                    ->emailFormat('html')
                    ->subject('[WEBSITE TCE][PENDAFTARAN BARU]')
                    ->viewVars([
                        'defaultAppSettings' => $this->defaultAppSettings,
                        'developer' => $developer
                    ])
                    ->to($to)
                    ->from('maildev.tce@gmail.com')
                    ->send();
                $code = 200;
                $message  = "Pendaftaran berhasil. Admin kami akan segera menghubungi anda setelah data terverifikasi. Terimakasih";
                $url = $this->request->base;
            }else{
                $code = 50;
                $message = $this->Utilities->getErrors($developer->errors());
                $url = $this->request->base;
            }
            $this->set(compact('code','message','url'));
            $this->set('_serialize',['code','message','url']);
        }
    }
    
    public function checkPengawas(){
        $this->loadModel('Consultants');
        $this->loadModel('ApplicationLetters');
        $consultant = $this->Consultants->get(1);
        $this->set('titleModule','Check Data Pengawas');
        $this->set(compact('consultant'));
        if($this->request->is(['post','put'])){
            $data = $this->request->getData();
            $code = 50;
            $message = "NIK TIDAK DITEMUKAN";
            $this->loadModel('Supervisors');
            $supervisor = $this->Supervisors->find('all',[
                'conditions' => [
                    'Supervisors.nik' => $data['nik']
                ],
                'contain' => [
                    'ApplicationLetters' => function($q){
                        $q->order(['ApplicationLetters.id' => 'DESC']);
                        return $q;
                    },
                    'ApplicationLetters.Developers'
                ]
            ])->first();
            if(!empty($supervisor)){
                $code = 200;
                $message = "NIK DITEMUKAN";
                $supervisor->url_image = $this->Utilities->generateUrlImage($supervisor->identity_file_dir,$supervisor->identity_file);
                foreach($supervisor->application_letters as $key => $r){
                    $r->url_lembar_pemeriksaan = "";
                    if(!empty($r->lembar_pemeriksaan)){
                        $r->url_lembar_pemeriksaan = $this->Utilities->generateUrlImage($r->lembar_pemeriksaan_dir,$r->lembar_pemeriksaan);
                    }
                    $r->url_surat_pernyataan = "";
                    if(!empty($r->surat_pernyataan)){
                        $r->url_surat_pernyataan = $this->Utilities->generateUrlImage($r->surat_pernyataan_dir,$r->surat_pernyataan);
                    }
                    $r->url_form_checklist = "";
                    if(!empty($r->form_checklist)){
                        $r->url_form_checklist = $this->Utilities->generateUrlImage($r->form_checklist_dir,$r->form_checklist);
                    }
                }
            }
            $this->set('data',$data);
            $this->set('message',$message);
            $this->set('code',$code);
            $this->set('supervisor',$supervisor);
            $this->set('_serialize',['data','code','message','supervisor']);
        }
        
    }

    public function uploadPengawas()
    {
        $data = $this->request->getData();
        $id = $data['id'];
        $this->loadModel('ApplicationLetters');
        $applicationLetter = $this->ApplicationLetters->get($id);
        $applicationLetter = $this->ApplicationLetters->patchEntity($applicationLetter,$data,[
            'validate' => 'spesial'
        ]);
        
        if($this->ApplicationLetters->save($applicationLetter)){
            $code = 200;
            $message = "Data berhasil diupload";
            $applicationLetter->url_lembar_pemeriksaan = "";
            if(!empty($applicationLetter->lembar_pemeriksaan)){
                $applicationLetter->url_lembar_pemeriksaan = $this->Utilities->generateUrlImage($applicationLetter->lembar_pemeriksaan_dir,$applicationLetter->lembar_pemeriksaan);
            }
            $applicationLetter->url_surat_pernyataan = "";
            if(!empty($applicationLetter->surat_pernyataan)){
                $applicationLetter->url_surat_pernyataan = $this->Utilities->generateUrlImage($applicationLetter->surat_pernyataan_dir,$applicationLetter->surat_pernyataan);
            }
            $applicationLetter->url_form_checklist = "";
            if(!empty($applicationLetter->form_checklist)){
                $applicationLetter->url_form_checklist = $this->Utilities->generateUrlImage($applicationLetter->form_checklist_dir,$applicationLetter->form_checklist);
            }
        }else{
            $code = "50";
            $message = $this->Utilities->getErrors($applicationLetter->errors());
        }
        $this->set('data',$data);
        $this->set('message',$message);
        $this->set('code',$code);
        $this->set('application_letter',$applicationLetter);
        $this->set('_serialize',['data','code','message','application_letter']);
    }
    public function home()
    {
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $this->set('consultant',$consultant);
    }
    public function registerDeveloper(){
        $titleModule = "Register Developer";
        $titlesubModule = "Register Sebagai Developer ";
        $breadCrumbs = [
            Router::url(['action' => 'registerDeveloper']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        $this->loadModel('Provinces');
        $this->loadModel('Developers');
        $provinces = $this->Provinces->find('list')->order('name ASC');
        $developer = $this->Developers->newEntity(null,[
            'associated' => [
                'Users' => []
            ]
        ]);
        if($this->request->is('post')){
            $data = $this->request->getData();
            $data['user']['name'] = $data['name'];
            $data['user']['email'] = $data['email'];
            $data['user']['status'] = 0;
            $data['user']['group_id'] = 2;
            $developer = $this->Developers->patchEntity($developer,$data,[
                'associated' => [
                    'Users' => [
                        'validate'=> 'special'
                    ]
                ]
            ]);
            if($this->Developers->save($developer)){
                $id = $developer->id;
                
                $developer = $this->Developers->get($id, [
                    'contain' => ['DppFroms', 'Users']
                ]);
                if (Configure::read('debug')) {
                    $to = 'aiqbalsyah@gmail.com';
                }else{
                    $this->loadModel('MasterDpps');
                    $dpp = $this->MasterDpps->get(1);
                    $to = $dpp->email;
                }
                $email = new Email();
                $email
                    ->template('notifications', 'default')
                    ->emailFormat('html')
                    ->subject('[WEBSITE TCE][PENDAFTARAN BARU]')
                    ->viewVars([
                        'defaultAppSettings' => $this->defaultAppSettings,
                        'developer' => $developer
                    ])
                    ->to($to)
                    ->from('maildev.tce@gmail.com')
                    ->send();
                $code = 200;
                $message  = "Pendaftaran berhasil. Admin kami akan segera menghubungi anda setelah data terverifikasi. Terimakasih";
                $url = $this->request->base;
            }else{
                $code = 50;
                $message = $this->Utilities->getErrors($developer->errors());
                $url = $this->request->base;
            }
            $this->set(compact('code','message','url'));
            $this->set('_serialize',['code','message','url']);
        }
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $this->set(compact('provinces','developer','consultant'));
    }
    public function loginDeveloper()
    {
        $titleModule = "Login Developer";
        $titlesubModule = "Login Sebagai Developer ";
        $breadCrumbs = [
            Router::url(['action' => 'loginDeveloper']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        if ($this->request->is('post')) {
            $this->loadModel('Users');
            $username = $this->request->getData('username');
            $password = $this->request->getData('password');
            $checkUser  = $this->Users->find('all',[
                'contain' => [
                    'Aros','Groups'
                ],
                'conditions' => [
                    'username' => $username,
                    'Users.status' => 1
                ]
            ]);
            $checkUser->contain(['Developers']);
            $checkUser->where(['group_id'=> 2])
            ->where(['Developers.status' => 1]);
            $checkUser = $checkUser->first();
            $url = "";
            if(!empty($checkUser)){
                $hasher = new DefaultPasswordHasher;
                if($hasher->check($password,$checkUser->password)){
                    $code       = 200;
                    $message    = "Welcome ". $checkUser->name; 
                    $url = Router::url($this->Auth->redirectUrl(),true);
                }else{

                    $username = $checkUser->username;
                    $code       = 50;
                    $message    = "Invalid password for ". $username. ", Please try again."; 
                }
            }else{
                $code       = 50;
                $message    = "Invalid username ". $username; 
            }
            $user = $checkUser;
            
            $this->set(compact('code','message','url','user'));
            $this->set('_serialize',['code','message','user','url']);
            if ($code == 200) {
                $this->Auth->setUser($user);
                $this->Redis->createCacheUserAuth($user);
            } else {
                $code       = 50;
                $message    = "Invalid password for ". $username. ", Please try again."; 
            }
            if(!$this->request->is('ajax')){
                return $this->redirect('/');
            }
        }
       
    }
    public function loginDpd()
    {
        $titleModule = "Login DPD";
        $titlesubModule = "Login Sebagai DPD ";
        $breadCrumbs = [
            Router::url(['action' => 'loginDpd']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        $this->loadModel('Provinces');
        $provinces = $this->Provinces->find('list')->order('name ASC');
        $this->loadModel('Consultants');
        $consultant = $this->Consultants->get(1);
        $this->set(compact('provinces','developer','consultant'));
        if ($this->request->is('post')) {
            $this->loadModel('Users');
            $username = $this->request->getData('username');
            $password = $this->request->getData('password');
            $checkUser  = $this->Users->find('all',[
                'contain' => [
                    'Aros','Groups'
                ],
                'conditions' => [
                    'username' => $username,
                    'Users.status' => 1
                ]
            ]);
            $checkUser  = $this->Users->find('all',[
                'contain' => [
                    'Aros','Groups','MasterDpds'
                ],
                'conditions' => [
                    'MasterDpds.province_id' => $username,
                    'Users.status' => 1
                ]
            ]);
            $checkUser->where(['group_id'=> 4]);
            $checkUser = $checkUser->first();
            $url = "";
            if(!empty($checkUser)){
                $hasher = new DefaultPasswordHasher;
                if($hasher->check($password,$checkUser->password)){
                    $code       = 200;
                    $message    = "Welcome ". $checkUser->name; 
                    $url = Router::url($this->Auth->redirectUrl(),true);
                }else{

                    $username = $checkUser->username;
                    $code       = 50;
                    $message    = "Invalid password for ". $username. ", Please try again."; 
                }
            }else{
                $code       = 50;
                $message    = "Invalid username ". $username; 
            }
            $user = $checkUser;
            
            $this->set(compact('code','message','url','user'));
            $this->set('_serialize',['code','message','user','url']);
            if ($code == 200) {
                $this->Auth->setUser($user);
                $this->Redis->createCacheUserAuth($user);
            } else {
                $code       = 50;
                $message    = "Invalid password for ". $username. ", Please try again."; 
            }
            if(!$this->request->is('ajax')){
                return $this->redirect('/');
            }
        }
       
    }
    public function loginDpp()
    {
        $titleModule = "Login DPP";
        $titlesubModule = "Login Sebagai DPP ";
        $breadCrumbs = [
            Router::url(['action' => 'loginDpp']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        if ($this->request->is('post')) {
            $this->loadModel('Users');
            $username = $this->request->getData('username');
            $password = $this->request->getData('password');
            $checkUser  = $this->Users->find('all',[
                'contain' => [
                    'Aros','Groups'
                ],
                'conditions' => [
                    'username' => $username,
                    'Users.status' => 1
                ]
            ]);
            $checkUser->contain(['MasterDpps']);
            $checkUser->where(['group_id'=> 3]);
            $checkUser = $checkUser->first();
            $url = "";
            if(!empty($checkUser)){
                $hasher = new DefaultPasswordHasher;
                if($hasher->check($password,$checkUser->password)){
                    $code       = 200;
                    $message    = "Welcome ". $checkUser->name; 
                    $url = Router::url($this->Auth->redirectUrl(),true);
                }else{

                    $username = $checkUser->username;
                    $code       = 50;
                    $message    = "Invalid password for ". $username. ", Please try again."; 
                }
            }else{
                $code       = 50;
                $message    = "Invalid username ". $username; 
            }
            $user = $checkUser;
            
            $this->set(compact('code','message','url','user'));
            $this->set('_serialize',['code','message','user','url']);
            if ($code == 200) {
                $this->Auth->setUser($user);
                $this->Redis->createCacheUserAuth($user);
            } else {
                $code       = 50;
                $message    = "Invalid password for ". $username. ", Please try again."; 
            }
            if(!$this->request->is('ajax')){
                return $this->redirect('/');
            }
        }
       
    }
    public function loginKonsultan()
    {
        $titleModule = "Login Konstultan";
        $titlesubModule = "Login Sebagai Konsultan";
        $breadCrumbs = [
            Router::url(['action' => 'loginKonsultan']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        if ($this->request->is('post')) {
            $this->loadModel('Users');
            $username = $this->request->getData('username');
            $password = $this->request->getData('password');
            $checkUser  = $this->Users->find('all',[
                'contain' => [
                    'Aros','Groups'
                ],
                'conditions' => [
                    'username' => $username,
                    'Users.status' => 1
                ]
            ]);
            $checkUser->contain(['Consultants']);
            $checkUser->where(['group_id'=> 5]);
            $checkUser = $checkUser->first();
            $url = "";
            if(!empty($checkUser)){
                $hasher = new DefaultPasswordHasher;
                if($hasher->check($password,$checkUser->password)){
                    $code       = 200;
                    $message    = "Welcome ". $checkUser->name; 
                    $url = Router::url($this->Auth->redirectUrl(),true);
                }else{

                    $username = $checkUser->username;
                    $code       = 50;
                    $message    = "Invalid password for ". $username. ", Please try again."; 
                }
            }else{
                $code       = 50;
                $message    = "Invalid username ". $username; 
            }
            $user = $checkUser;
            
            $this->set(compact('code','message','url','user'));
            $this->set('_serialize',['code','message','user','url']);
            if ($code == 200) {
                $this->Auth->setUser($user);
                $this->Redis->createCacheUserAuth($user);
            } else {
                $code       = 50;
                $message    = "Invalid password for ". $username. ", Please try again."; 
            }
            if(!$this->request->is('ajax')){
                return $this->redirect('/');
            }
        }
       
    }

    public function index()
    {
        if(empty($this->Auth->user())){
            if($this->request->_name == "landing"){
                $this->loadModel('Provinces');
                $this->loadModel('Developers');
                $provinces = $this->Provinces->find('list')->order('name ASC');
                $developer = $this->Developers->newEntity(null,[
                    'associated' => [
                        'Users' => []
                    ]
                ]);
                $this->loadModel('Consultants');
                $consultant = $this->Consultants->get(1);
                $this->set(compact('provinces','developer','consultant'));
                
            }else{
                $this->viewBuilder()->layout('login');
            }
            
            if ($this->request->is('post')) {
                $this->loadModel('Users');
                $username = $this->request->getData('username');
                $password = $this->request->getData('password');
                $type = $this->request->getData('type');
                $checkUser  = $this->Users->find('all',[
                    'contain' => [
                        'Aros','Groups'
                    ],
                    'conditions' => [
                        'username' => $username,
                        'Users.status' => 1
                    ]
                ]);
                if(!empty($type)){
                    if($type == 'developer'){
                        $checkUser->contain(['Developers']);
                        $checkUser->where(['group_id'=> 2])
                        ->where(['Developers.status' => 1]);
                    }elseif($type == 'dpd'){
                        $checkUser  = $this->Users->find('all',[
                            'contain' => [
                                'Aros','Groups','MasterDpds'
                            ],
                            'conditions' => [
                                'MasterDpds.province_id' => $username,
                                'Users.status' => 1
                            ]
                        ]);
                        $checkUser->where(['group_id'=> 4]);
                    }elseif($type == 'dpp'){
                        $checkUser->contain(['MasterDpps']);
                        $checkUser->where(['group_id'=> 3]);
                        
                    }elseif($type == 'consultant'){
                        $checkUser->contain(['Consultants']);
                        $checkUser->where(['group_id'=> 5]);
                        
                    }
                }
                $checkUser = $checkUser->first();
                $url = "";
                if(!empty($checkUser)){
                    $hasher = new DefaultPasswordHasher;
                    if($hasher->check($password,$checkUser->password)){
                        $code       = 200;
                        $message    = "Welcome ". $checkUser->name; 
                        $url = Router::url($this->Auth->redirectUrl(),true);
                    }else{

                        $username = $checkUser->username;
                        $code       = 50;
                        $message    = "Invalid password for ". $username. ", Please try again."; 
                    }
                }else{
                    $code       = 50;
                    $message    = "Invalid username ". $username; 
                }
                $user = $checkUser;
                
                $this->set(compact('code','message','url','user'));
                $this->set('_serialize',['code','message','user','url']);
                if ($code == 200) {
                    $this->Auth->setUser($user);
                    $this->Redis->createCacheUserAuth($user);
                } else {
                    $code       = 50;
                    $message    = "Invalid password for ". $username. ", Please try again."; 
                }
                if(!$this->request->is('ajax')){
                    return $this->redirect('/');
                }
            }else{
               $this->render('login');
            }
            
            
        }else{
            if($this->userData->group_id == 2 || $this->userData->group_id == 3 || $this->userData->group_id == 4 || $this->userData->group_id == 5){
                $this->dashboardDeveloper();
            }else{
                $home_url = $this->Redis->readCacheUrlHome($this->userData->aro->id);
                return $this->redirect([
                    'controller'=> $home_url['controller'],
                    'action' => $home_url['action']
                ]);
            }
           
        }
    }

    public function dashboardDeveloper(){
        $this->render('dashboard_developer');
    }
    
    public function logout()
    {
        $this->Redis->destroyCacheUrlHome($this->userData->aro->id);
        $this->Redis->destroyCacheSideNav($this->userData->aro->id);
        $this->Redis->destroyCacheUserAuth($this->userData->id);
        $this->Auth->logout();
        $this->redirect('/');
    }
 
    public function editProfile()
    {
        $id = $this->userId;
        if($this->userData['group_id'] == 2){
            $user = $this->Users->get($id, [
                'contain' => ['Aros','Groups','Developers']
            ]);
        }elseif($this->userData['group_id'] == 3){
            $user = $this->Users->get($id, [
                'contain' => ['Aros','Groups','MasterDpps']
            ]);
        }elseif($this->userData['group_id'] == 4){
            $user = $this->Users->get($id, [
                'contain' => ['Aros','Groups','MasterDpds']
            ]);
        }elseif($this->userData['group_id'] == 5){
            $user = $this->Users->get($id, [
                'contain' => ['Aros','Groups','Consultants']
            ]);
        }else{
            $user = $this->Users->get($id, [
                'contain' => ['Aros','Groups']
            ]);
        }
        
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            if($this->userData['group_id'] == 2){
                $data['developer']['email'] = $data['email'];
                $data['developer']['name'] = $data['name'];
                $user = $this->Users->patchEntity($user, $data,[
                    'validate'=>'editProfile',
                    'associated' => [
                        'Developers'
                    ]
                ]);
            }elseif($this->userData['group_id'] == 3){
                $data['master_dpp']['email'] = $data['email'];
                $data['master_dpp']['name'] = $data['name'];
                $user = $this->Users->patchEntity($user, $data,[
                    'validate'=>'editProfile',
                    'associated' => [
                        'MasterDpps'
                    ]
                ]);
            }elseif($this->userData['group_id'] == 4){
                $data['master_dpd']['email'] = $data['email'];
                $data['master_dpd']['name'] = $data['name'];
                $user = $this->Users->patchEntity($user, $data,[
                    'validate'=>'editProfile',
                    'associated' => [
                        'MasterDpds'
                    ]
                ]);
            }elseif($this->userData['group_id'] == 5){
                $data['consultant']['email'] = $data['email'];
                $data['consultant']['name'] = $data['name'];
                $user = $this->Users->patchEntity($user, $data,[
                    'validate'=>'editProfile',
                    'associated' => [
                        'Consultants'
                    ]
                ]);
            }else{
                $user = $this->Users->patchEntity($user, $data,[
                    'validate'=>'editProfile'
                ]);
            }
            if(empty($user->password)){
                unset($user->password);
            }
            if ($this->Users->save($user)) {
                $this->Redis->destroyCacheUserAuth($user);
                $this->Redis->createCacheUserAuth($user);
                $this->Flash->success(__('The profile has been updated.'));
                $this->autoRender = false;
                return $this->redirect(['action' => 'editProfile']);
            }
            $this->Flash->error(__('The profile could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
        $titleModule = "Profile";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'editProfile']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    public function activitiesLog()
    {
        $id = $this->userId;
        $user = $this->Users->get($id, [
            'contain' => []
        ]);
        $this->request->allowMethod(['get']);
        $this->loadModel('AuditLogs');
        $auditLogs = $this->AuditLogs->find('all',[
            'contain'=>[
                'Users' => [
                    'conditions' => [
                        'user_id' => $id
                    ]
                ]
            ],
            'order'=>['AuditLogs.timestamp' => 'DESC'],
            'conditions' => [
                
            ]
                    
        ]);
        if($this->request->is('ajax')){
            $auditLogs->where('DATE(timestamp) >= DATE_SUB(NOW(), INTERVAL 10 DAY)');
        }
        $logs = [];
        foreach($auditLogs as $key => $auditLog){
            $time =new Time($auditLog->timestamp);
            $time = $time->timeAgoInWords(['accuracy' => 'day','end'=>'+7 day']);
            $logs[$key] = [
                'time' => $time,
                'description' => 'Has been '.$auditLog->type. ' '. $auditLog->source,
                'id' => $auditLog->id
            ];
        }
        $code = 200;
        $message = __('Get data activity logs');
        $status = 'success';
        $this->set('code',$code);
        $this->set('message',$message);
        $this->set('auditLogs',$logs);
        $titleModule = "Activities Logs";
        $titlesubModule = "List ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'activitiesLogs']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        if($this->request->is('ajax')){
            $this->set('_serialize',['code','message','auditLogs']);
        }
    }

    public function uploadMedia()
    {
        $data = $this->request->data['file'];
        $uploadFolder = WWW_ROOT.'assets'.DS.'img'.DS.'media/'.$data['name'];
        $saveDir = '/assets/img/media/'.$data['name'];
        $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
        move_uploaded_file($data['tmp_name'],$uploadFolder);
        $url = Router::url($saveDir,true);
        $this->set('url',$url);
        $this->set('_serialize',['url']);
    }

    public function logoff()
    {
        $this->viewBuilder()->layout('unauthorized');
    }

}