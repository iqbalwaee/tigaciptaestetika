<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Routing\Router;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->Auth->allow(['configure']);
    }
    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        
        if($this->request->is('ajax')){
            $source = $this->Users;
            $searchAble = [
                'Users.id',
                'Users.username'
            ];
            $data = [
                'source'=>$source,
                'searchAble' => $searchAble,
                'defaultField' => 'Users.id',
                'defaultSort' => 'desc',
                'defaultSearch' => [
                    // [    
                    //     'keyField' => 'group_id',
                    //     'condition' => '=',
                    //     'value' => 1
                    // ]
                ],
                'contain' => ['Groups']
                    
            ];
            $asd   = $this->Datatables->make($data);  
            //$this->set('data', $asd);
            $data = $asd['data'];
            $meta = $asd['meta'];
            $this->set('data',$data);
            $this->set('meta',$meta);
            $this->set('_serialize',['data','meta']);
        }else{
            $titleModule = "User";
            $titlesubModule = "List User";
            $breadCrumbs = [
                Router::url(['action' => 'index']) => $titlesubModule
            ];
            $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        }
        
        
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        
        $user = $this->Users->get($id, [
            'contain' => ['Groups','CreatedUsers','ModifiedUsers']
        ]);
        $titleModule = "User";
        $titlesubModule = "View ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'view']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
        $this->set('user', $user);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200,'conditions'=>[
            'status'=>1
        ]]);
        $this->set(compact('user', 'groups'));
        $titleModule = "User";
        $titlesubModule = "Create ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'add']) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Groups','Aros']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if(empty($user->password)){
                unset($user->password);
            }
            if ($this->Users->save($user)) {
                $this->Redis->destroyCacheUserAuth($user);
                $this->Redis->createCacheUserAuth($user);
                $this->Redis->deleteAllCacheAcos($id);
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $groups = $this->Users->Groups->find('list', ['limit' => 200,['conditions'=>[
            'status' => 1,
            'OR' => [
                'id' => $user->group_id
            ]
        ]]]);
        $this->set(compact('user', 'groups'));
        $titleModule = "User";
        $titlesubModule = "Edit ".$titleModule;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'edit',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id,['contain'=>'Aros']);
        if ($this->Users->delete($user)) {
            $this->Redis->destroyCacheUserAuth($user);
            $this->Redis->deleteAllCacheAcos($user->id);
            $this->Redis->destroyCacheUrlHome($user->aro->id);
            $this->Redis->destroyCacheSideNav($user->aro->id);
            $code = 200;
            $message = __('The user has been deleted.');
            $status = 'success';
        } else {
            $code = 99;
            $message = __('The user could not be deleted. Please, try again.');
            $status = 'error';
        }
        if($this->request->is('ajax')){
            $this->set('code',$code);
            $this->set('message',$message);
            $this->set('_serialize',['code','message']);
        }else{
            $this->Flash->{$status}($message);
            return $this->redirect(['action' => 'index']);
        }
    }

    public function configure($id)
    {
        $user = $this->Users->get($id,[
            'contain'=>['Aros']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->data;
            foreach($data['aco_id'] as $key => $r){
                //check parent child aco//
                $this->Acl->deny($user,$key);
                $allow = false;
                foreach($r as $skey => $rd){
                    if($rd == 0){
                        $this->Acl->deny($user,$skey);
                        $acl = explode("/",$skey);
                        $url = Router::url(['controller'=>$acl[2],'action'=>$acl[3]]);
                        $this->Redis->deleteCacheAcos($user->id,$url);
                    }else{
                        $this->Acl->allow($user,$skey);
                        if($allow == false){
                            $allow = true;
                        }
                    }
                }
                if($allow){
                    $this->Acl->allow($user,$key);
                }
            }
            $this->Redis->destroyCacheUrlHome($user->aro->id);
            $this->Redis->destroyCacheSideNav($user->aro->id);
            $this->Redis->deleteAllCacheAcos($user->id);
            $this->Flash->success(__('The group has been configured.'));
            return $this->redirect(['action' => 'index']);
        }
        $this->loadModel('Acos');
        
        $acos = $this->Acos->find('all',[
            'conditions' => [
                'Aros.model' => 'Groups',
                'Aros.foreign_key' => $user->group_id,
                'Permissions._create' => 1
            ]
        ])
        ->select([
                'Acos__id' => 'Acos.id', 
                'Acos__parent_id' => 'Acos.parent_id', 
                'Acos__name' => 'Acos.name', 
                'Acos__model' => 'Acos.model', 
                'Acos__foreign_key' => 'Acos.foreign_key', 
                'Acos__alias' => 'Acos.alias', 
                'Acos__lft' => 'Acos.lft', 
                'Acos__rght' => 'Acos.rght', 
                'Acos__status' => 'Acos.status',
                'Acos__read' => 'IFNULL(UserPermissions._read,-1)'
            ]
        )
        ->join([
            [
                'table' => 'aros_acos',
                'alias' => 'Permissions',
                'type' => 'INNER',
                'conditions' => 'Acos.id = (Permissions.aco_id)',
            ],[
                'table' => 'aros',
                'alias' => 'Aros',
                'type' => 'INNER',
                'conditions' => 'Aros.id = (Permissions.aro_id)',
            ],[
                'table' => 'aros_acos',
                'alias' => 'UserPermissions',
                'type' => 'LEFT',
                'conditions' => 'Acos.id = (UserPermissions.aco_id) AND UserPermissions.aro_id = '.$user->aro->id,
            ],[
                'table' => 'aros',
                'alias' => 'UserAros',
                'type' => 'LEFT',
                'conditions' => 'UserAros.id = (UserPermissions.aro_id) AND UserAros.model = "Users" AND UserAros.foreign_key = '.$id,
            ]
        ])
        ->group('Acos__id')
        ->order('Acos.sort ASC')
        ->find('threaded');
        
        $this->set(compact('user','acos'));
        $titleModule = "User";
        $titlesubModule = "Configure ".$titleModule. " ". $user->username;
        $breadCrumbs = [
            Router::url(['action' => 'index']) => "List ".$titleModule,
            Router::url(['action' => 'configure',$id]) => $titlesubModule
        ];
        $this->set(compact('titleModule','breadCrumbs','titlesubModule'));
    }

}
