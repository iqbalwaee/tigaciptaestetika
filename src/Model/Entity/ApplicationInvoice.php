<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApplicationInvoice Entity
 *
 * @property int $id
 * @property string $code
 * @property int $application_letter_id
 * @property \Cake\I18n\FrozenDate $date
 * @property float $rate
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $created_by
 * @property int $modified_by
 * @property string $note
 *
 * @property \App\Model\Entity\ApplicationLetter $application_letter
 * @property \App\Model\Entity\ApplicationInvoicesDetail[] $application_invoices_details
 */
class ApplicationInvoice extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'application_letter_id' => true,
        'date' => true,
        'qty' => true,
        'rate' => true,
        'created' => true,
        'modified' => true,
        'created_by' => true,
        'modified_by' => true,
        'note' => true,
        'application_letter' => true,
        'application_invoices_details' => true
    ];
}
