<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApplicationLetter Entity
 *
 * @property int $id
 * @property string $primary_function
 * @property string $code
 * @property int $house_type_id
 * @property string $surface_area
 * @property string $building
 * @property string $name_house
 * @property int $province_id
 * @property int $city_id
 * @property int $district_id
 * @property int $village_id
 * @property string $village_name
 * @property string $address
 * @property string $sample_house
 * @property float $price_per_unit
 * @property string $sertifikat_hak
 * @property string $izin_pemanfaatan
 * @property string $pengesahaan
 * @property string $nomor_imb
 * @property string $izin_lainnya_1
 * @property string $izin_lainnya_2
 * @property int $developer_id
 * @property int $status
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\HouseType $house_type
 * @property \App\Model\Entity\Province $province
 * @property \App\Model\Entity\City $city
 * @property \App\Model\Entity\District $district
 * @property \App\Model\Entity\Village $village
 * @property \App\Model\Entity\Developer $developer
 */
class ApplicationLetter extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'primary_function' => true,
        'code' => true,
        'house_type_id' => true,
        'cordinate_area' => true,
        'clasification_kompleksitas' => true,
        'high_building' => true,
        'floor_count' => true,
        'surface_building' => true,
        'surface_area' => true,
        'building' => true,
        'name_house' => true,
        'province_id' => true,
        'city_id' => true,
        'district_id' => true,
        'village_id' => true,
        'village_name' => true,
        'address' => true,
        'sample_house' => true,
        'price_per_unit' => true,
        'sertifikat_hak' => true,
        'izin_pemanfaatan' => true,
        'pengesahaan' => true,
        'nomor_imb' => true,
        'izin_lainnya_label' => true,
        'izin_lainnya_value' => true,
        'developer_id' => true,
        'status' => true,
        'created' => true,
        'modified' => true,
        'house_type' => true,
        'province' => true,
        'city' => true,
        'district' => true,
        'village' => true,
        'developer' => true,
        'histories_letters' => true,
        'surat_pernyataan' => true,
        'surat_pernyataan_dir' => true,
        'lembar_pemeriksaan' => true,
        'lembar_pemeriksaan_dir' => true,
        'form_checklist' => true,
        'form_checklist_dir' => true,
        'resi_pengiriman' => true,
        'approved_by_dpp_date' => true,
        'spk_code' => true,
        'resi_pengiriman_dir' => true,
        'application_output' => true,
        'output_code' => true,
        'output_date' => true,
        'supervisor_id' => true,
        'application_picture' => true,
        'application_simak' => true,
        'application_new_simak' => true,
        'application_simaks_pemeriksaan' => true,
        'application_rooms' => true,
        'application_rooms_detail' => true,
        'application_rooms_comments' => true,
        'application_roomm' => true,
        'application_judgment' => true,
        'application_invoice' => true
    ];
}
