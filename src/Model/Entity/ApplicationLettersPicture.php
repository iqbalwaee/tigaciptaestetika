<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApplicationLettersPicture Entity
 *
 * @property int $id
 * @property int $application_letter_id
 * @property string $picture
 * @property string $picture_dir
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ApplicationLetter $application_letter
 */
class ApplicationLettersPicture extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'application_letter_id' => true,
        'name' => true,
        'picture' => true,
        'picture_dir' => true,
        'created' => true,
        'modified' => true,
        'application_letter' => true
    ];
}
