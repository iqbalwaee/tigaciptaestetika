<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApplicationNewSimak Entity
 *
 * @property int $id
 * @property int $application_letter_id
 * @property string $data_1
 * @property string $data_2
 * @property string $data_3
 * @property string $data_4
 * @property string $data_5
 * @property string $data_6
 * @property string $data_7
 * @property string $data_8
 * @property string $data_9
 * @property string $data_10
 * @property string $data_11
 *
 * @property \App\Model\Entity\ApplicationLetter $application_letter
 */
class ApplicationNewSimak extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'application_letter_id' => true,
        'data_1' => true,
        'data_2' => true,
        'data_3' => true,
        'data_4' => true,
        'data_5' => true,
        'data_6' => true,
        'data_7' => true,
        'data_8' => true,
        'data_9' => true,
        'data_10' => true,
        'data_11' => true,
        'data_12' => true,
        'application_letter' => true
    ];
}
