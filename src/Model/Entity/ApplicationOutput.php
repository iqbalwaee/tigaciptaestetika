<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApplicationOutput Entity
 *
 * @property int $id
 * @property int $application_letter_id
 * @property string $code_house
 * @property string $persyaratan_administratif
 * @property string $fungsi_bangunan
 * @property string $peruntukan
 * @property string $tata_bangunan
 * @property string $kelaikan
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ApplicationLetter $application_letter
 */
class ApplicationOutput extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'application_letter_id' => true,
        'code_house' => true,
        'persyaratan_administratif' => true,
        'fungsi_bangunan' => true,
        'peruntukan' => true,
        'tata_bangunan' => true,
        'kelaikan' => true,
        'created' => true,
        'modified' => true,
        'application_letter' => true
    ];
}
