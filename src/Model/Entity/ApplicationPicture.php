<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApplicationPicture Entity
 *
 * @property int $id
 * @property int $application_letter_id
 * @property string $tampak_depan
 * @property string $tampak_depan_dir
 * @property string $jalan_depan
 * @property string $jalan_depan_dir
 * @property string $ruang_tamu
 * @property string $ruang_tamu_dir
 * @property string $plafond
 * @property string $plafond_dir
 * @property string $pintu_kusen_jendela
 * @property string $pintu_kusen_jendela_dir
 * @property string $toilet
 * @property string $toilet_dir
 * @property string $sumur_septictank
 * @property string $sumur_septictank_dir
 * @property string $kwh_listrik
 * @property string $kwh_listrik_dir
 * @property string $jaringan_air
 * @property string $jaringan_air_dir
 * @property string $saluran_pembuangan
 * @property string $saluran_pembuangan_dir
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\ApplicationLetter $application_letter
 */
class ApplicationPicture extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'application_letter_id' => true,
        'tampak_depan' => true,
        'tampak_depan_dir' => true,
        'jalan_depan' => true,
        'jalan_depan_dir' => true,
        'jalan_utama' => true,
        'jalan_utama_dir' => true,
        'jalan_komplek' => true,
        'jalan_komplek_dir' => true,
        'fasilitas_umum' => true,
        'fasilitas_umum_dir' => true,
        'ruang_tamu' => true,
        'ruang_tamu_dir' => true,
        'kamar_tidur' => true,
        'kamar_tidur_dir' => true,
        'plafond' => true,
        'plafond_dir' => true,
        'pintu_kusen_jendela' => true,
        'pintu_kusen_jendela_dir' => true,
        'tampak_atas_1' => true,
        'tampak_atas_1_dir' => true,
        'tampak_atas_2' => true,
        'tampak_atas_2_dir' => true,
        'toilet' => true,
        'toilet_dir' => true,
        'sumur_septictank' => true,
        'sumur_septictank_dir' => true,
        'kwh_listrik' => true,
        'kwh_listrik_dir' => true,
        'jaringan_air' => true,
        'jaringan_air_dir' => true,
        'saluran_pembuangan' => true,
        'saluran_pembuangan_dir' => true,
        'created' => true,
        'modified' => true,
        'application_letter' => true
    ];
}
