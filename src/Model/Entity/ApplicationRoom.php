<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApplicationRoom Entity
 *
 * @property int $id
 * @property int $application_letter_id
 * @property int $room_id
 * @property int $rooms_utility_id
 * @property int $rooms_option_id
 * @property string $description
 *
 * @property \App\Model\Entity\ApplicationLetter $application_letter
 * @property \App\Model\Entity\Room $room
 * @property \App\Model\Entity\RoomsUtility $rooms_utility
 * @property \App\Model\Entity\RoomOption $room_option
 */
class ApplicationRoom extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'application_letter_id' => true,
        'room_id' => true,
        'rooms_utility_id' => true,
        'rooms_option_id' => true,
        'description' => true,
        'application_letter' => true,
        'room' => true,
        'rooms_utility' => true,
        'room_option' => true
    ];
}
