<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApplicationRoomsDetail Entity
 *
 * @property int $id
 * @property int $application_letter_id
 * @property string $note_1
 * @property int $note_2
 * @property int $note_3
 *
 * @property \App\Model\Entity\ApplicationLetter $application_letter
 */
class ApplicationRoomsDetail extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'application_letter_id' => true,
        'note_1' => true,
        'note_2' => true,
        'note_3' => true,
        'application_letter' => true
    ];
}
