<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ApplicationSimak Entity
 *
 * @property int $id
 * @property int $application_letter_id
 * @property string $element_struktural
 * @property string $pondasi
 * @property string $lokasi
 * @property string $bagian
 * @property string $tahun
 * @property string $panjang
 * @property string $tinggi
 * @property int $bahan_bangunan
 * @property int $tipe
 * @property int $garis
 * @property int $retak_struktur
 * @property int $retak_permukaan
 * @property int $pengangkatan
 * @property int $kebocoran
 * @property int $penurunan
 * @property int $sill_plate_rot
 * @property int $kondisi
 * @property int $estimasi
 * @property string $kesimpulan
 * @property string $pemeriksa
 * @property string $nama_pemeriksaa
 * @property \Cake\I18n\FrozenDate $date
 *
 * @property \App\Model\Entity\ApplicationLetter $application_letter
 */
class ApplicationSimak extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'application_letter_id' => true,
        'element_struktural' => true,
        'pondasi' => true,
        'lokasi' => true,
        'bagian' => true,
        'tahun' => true,
        'panjang' => true,
        'tinggi' => true,
        'bahan_bangunan' => true,
        'tipe' => true,
        'garis' => true,
        'retak_struktur' => true,
        'retak_permukaan' => true,
        'pengangkatan' => true,
        'kebocoran' => true,
        'penurunan' => true,
        'sill_plate_rot' => true,
        'kondisi' => true,
        'estimasi' => true,
        'kesimpulan' => true,
        'pemeriksa' => true,
        'nama_pemeriksaa' => true,
        'date' => true,
        'application_letter' => true
    ];
}
