<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Consultant Entity
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $address
 * @property string $phone
 * @property int $user_id
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $modified_by
 *
 * @property \App\Model\Entity\User $user
 */
class Consultant extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'code' => true,
        'name' => true,
        'email' => true,
        'bidang_keahlian' => true,
        'no_badan_usaha' => true,
        'address' => true,
        'phone' => true,
        'user_id' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'picture' => true,
        'picture_dir' => true,
        'compro' => true,
        'compro_dir' => true,
        'form_checklist' => true,
        'form_checklist_dir' => true,
        'ska_1' => true,
        'ska_2' => true,
        'ska_3' => true,
        'ska_1_dir' => true,
        'ska_2_dir' => true,
        'ska_3_dir' => true,
        'user' => true,
        'pic_name' => true,
        'pic_signature' => true
    ];
}
