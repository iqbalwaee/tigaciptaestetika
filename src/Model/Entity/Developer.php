<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Developer Entity
 *
 * @property int $id
 * @property string $name
 * @property string $ceo_name
 * @property string $kta_number
 * @property int $dpp_from_id
 * @property string $address
 * @property string $phone
 * @property string $email
 * @property int $user_id
 * @property bool $status
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 * @property \Cake\I18n\FrozenTime $modified
 * @property int $modified_by
 *
 * @property \App\Model\Entity\DppFrom $dpp_from
 * @property \App\Model\Entity\User $user
 */
class Developer extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'no_badan_usaha' => true,
        'bidang_keahlian' => true,
        'ceo_name' => true,
        'nik_ceo' => true,
        'kta_number' => true,
        'dpp_from_id' => true,
        'address' => true,
        'phone' => true,
        'email' => true,
        'user_id' => true,
        'pic' => true,
        'pic_phone' => true,
        'status' => true,
        'logo' => true,
        'logo_dir' => true,
        'created' => true,
        'created_by' => true,
        'modified' => true,
        'modified_by' => true,
        'dpp_from' => true,
        'user' => true
    ];
}
