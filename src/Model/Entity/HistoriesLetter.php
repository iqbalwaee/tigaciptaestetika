<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * HistoriesLetter Entity
 *
 * @property int $id
 * @property int $application_letter_id
 * @property string $status
 * @property string $message
 * @property string $note
 * @property \Cake\I18n\FrozenTime $created
 * @property int $created_by
 *
 * @property \App\Model\Entity\ApplicationLetter $application_letter
 */
class HistoriesLetter extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'application_letter_id' => true,
        'status' => true,
        'message' => true,
        'note' => true,
        'created' => true,
        'created_by' => true,
        'application_letter' => true
    ];
}
