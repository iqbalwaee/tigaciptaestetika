<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Supervisor Entity
 *
 * @property int $id
 * @property string $nik
 * @property string $name
 * @property string $email
 * @property string $address
 * @property string $phone
 * @property string $identity_file
 * @property string $identity_file_dir
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Supervisor extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'nik' => true,
        'name' => true,
        'email' => true,
        'address' => true,
        'phone' => true,
        'identity_file' => true,
        'identity_file_dir' => true,
        'no_sertifikat_keahlian' => true,
        'created' => true,
        'modified' => true
    ];
}
