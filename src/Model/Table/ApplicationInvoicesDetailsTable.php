<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ApplicationInvoicesDetails Model
 *
 * @property \App\Model\Table\ApplicationInvoicesTable|\Cake\ORM\Association\BelongsTo $ApplicationInvoices
 *
 * @method \App\Model\Entity\ApplicationInvoicesDetail get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationInvoicesDetail newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationInvoicesDetail[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationInvoicesDetail|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationInvoicesDetail patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationInvoicesDetail[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationInvoicesDetail findOrCreate($search, callable $callback = null, $options = [])
 */
class ApplicationInvoicesDetailsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_invoices_details');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ApplicationInvoices', [
            'foreignKey' => 'application_invoice_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('text')
            ->requirePresence('text', 'create')
            ->notEmpty('text');

        $validator
            ->integer('qty')
            ->requirePresence('qty', 'create')
            ->notEmpty('qty');

        $validator
            ->scalar('unit')
            ->maxLength('unit', 225)
            ->requirePresence('unit', 'create')
            ->allowEmpty('unit');

        $validator
            ->decimal('rate')
            ->requirePresence('rate', 'create')
            ->notEmpty('rate');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['application_invoice_id'], 'ApplicationInvoices'));

        return $rules;
    }
}
