<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * ApplicationInvoices Model
 *
 * @property \App\Model\Table\ApplicationLettersTable|\Cake\ORM\Association\BelongsTo $ApplicationLetters
 * @property \App\Model\Table\ApplicationInvoicesDetailsTable|\Cake\ORM\Association\HasMany $ApplicationInvoicesDetails
 *
 * @method \App\Model\Entity\ApplicationInvoice get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationInvoice newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationInvoice[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationInvoice|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationInvoice patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationInvoice[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationInvoice findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ApplicationInvoicesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_invoices');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ApplicationLetters', [
            'foreignKey' => 'application_letter_id',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('ApplicationInvoicesDetails', [
            'foreignKey' => 'application_invoice_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('code')
            ->maxLength('code', 225)
            ->allowEmpty('code');

        $validator
            ->date('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        $validator
            ->decimal('rate')
            ->requirePresence('rate', 'create')
            ->notEmpty('rate');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        $validator
            ->scalar('note')
            ->allowEmpty('note');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['application_letter_id'], 'ApplicationLetters'));

        return $rules;
    }
    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if($entity->isNew()){
            $consultant = TableRegistry::get('Consultants');
            $consultant = $consultant->get(1);
            $code = $consultant->code;
            $year = substr($entity->date,0,4);
            $month = substr($entity->date,5,2);
            $formula = 'INV'.date('Ym');
            $getLastCode = $this->find('all',[
                'conditions' => [
                    'YEAR(date)' => $year,
                    'MONTH(date)' => $month,
                ]
            ])
            ->select([
                'no_urut' => '(CAST(RIGHT(code, 4) as SIGNED))'
            ])
            ->order('no_urut DESC')
            ->first();
            if(!empty($getLastCode)){
                $urut = $getLastCode->no_urut + 1;
            }else{
                $urut = 1;
            }
            if($urut <= 9){
                $prefix = "000";
            }elseif($urut <= 99){
                $prefix = "00";
            }elseif($urut <= 999){
                $prefix = "0";
            }elseif($urut <= 9999){
                $prefix = "";
            }
            $entity->code = $formula.$prefix.$urut;
        }

        return true;
    }
}
