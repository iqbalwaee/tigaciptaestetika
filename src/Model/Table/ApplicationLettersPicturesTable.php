<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ApplicationLettersPictures Model
 *
 * @property \App\Model\Table\ApplicationLettersTable|\Cake\ORM\Association\BelongsTo $ApplicationLetters
 *
 * @method \App\Model\Entity\ApplicationLettersPicture get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationLettersPicture newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationLettersPicture[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationLettersPicture|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationLettersPicture patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationLettersPicture[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationLettersPicture findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ApplicationLettersPicturesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_letters_pictures');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ApplicationLetters', [
            'foreignKey' => 'application_letter_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'picture' => [
                'fields' => [
                    'dir' => 'picture_dir', // defaults to `dir`
                ],
                'path' => 'webroot{DS}assets{DS}albums{DS}{field-value:application_letter_id}',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['application_letter_id'], 'ApplicationLetters'));

        return $rules;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if(empty($entity->name)){
            $entity->name = $entity->picture;   
        }

        return true;
    }
}
