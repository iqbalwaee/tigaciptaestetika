<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

/**
 * ApplicationLetters Model
 *
 * @property \App\Model\Table\HouseTypesTable|\Cake\ORM\Association\BelongsTo $HouseTypes
 * @property \App\Model\Table\ProvincesTable|\Cake\ORM\Association\BelongsTo $Provinces
 * @property \App\Model\Table\CitiesTable|\Cake\ORM\Association\BelongsTo $Cities
 * @property \App\Model\Table\DistrictsTable|\Cake\ORM\Association\BelongsTo $Districts
 * @property \App\Model\Table\VillagesTable|\Cake\ORM\Association\BelongsTo $Villages
 * @property \App\Model\Table\DevelopersTable|\Cake\ORM\Association\BelongsTo $Developers
 *
 * @method \App\Model\Entity\ApplicationLetter get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationLetter newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationLetter[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationLetter|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationLetter patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationLetter[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationLetter findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ApplicationLettersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_letters');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('HouseTypes', [
            'foreignKey' => 'house_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Provinces', [
            'foreignKey' => 'province_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Cities', [
            'foreignKey' => 'city_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Districts', [
            'foreignKey' => 'district_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Villages', [
            'foreignKey' => 'village_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Developers', [
            'foreignKey' => 'developer_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Supervisors', [
            'foreignKey' => 'supervisor_id',
            'joinType' => 'LEFT'
        ]);

        $this->hasMany('HistoriesLetters', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('ApplicationPictures', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasOne('ApplicationOutputs', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->hasMany('ApplicationLettersPictures', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasOne('ApplicationRoomms', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasOne('ApplicationJudgments', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasMany('ApplicationRoomsComments', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasOne('ApplicationRoomsDetails', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasOne('ApplicationSimaks', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasOne('ApplicationNewSimaks', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasOne('ApplicationSimaksPemeriksaans', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->hasOne('ApplicationInvoices', [
            'foreignKey' => 'application_letter_id',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'surat_pernyataan' => [
                'fields' => [
                    'dir' => 'surat_pernyataan_dir', // defaults to `dir`
                ],
            ],
            'lembar_pemeriksaan' => [
                'fields' => [
                    'dir' => 'lembar_pemeriksaan_dir', // defaults to `dir`
                ],
            ],
            'form_checklist' => [
                'fields' => [
                    'dir' => 'form_checklist_dir', // defaults to `dir`
                ],
            ],
            'resi_pengiriman' => [
                'fields' => [
                    'dir' => 'resi_pengiriman_dir', // defaults to `dir`
                ],
                'nameCallback' => function ($table, $entity, $data, $field, $settings) {
                    $extension = pathinfo($data['name'], PATHINFO_EXTENSION);
                    return strtolower($entity->name_house.'_'.rand(10001,99999).'_resi_pengiriman.'.$extension);
                },
            ],
        ]);
        $this->addBehavior('AuditStash.AuditLog');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('primary_function')
            ->maxLength('primary_function', 225)
            ->requirePresence('primary_function', 'create')
            ->notEmpty('primary_function');

        

        $validator
            ->integer('house_type_id')
            ->requirePresence('house_type_id', 'create')
            ->notEmpty('house_type_id');

        $validator
            ->scalar('surface_area')
            ->maxLength('surface_area', 225)
            ->requirePresence('surface_area', 'create')
            ->notEmpty('surface_area');

        $validator
            ->scalar('building')
            ->requirePresence('building', 'create')
            ->notEmpty('building');

        $validator
            ->scalar('name_house')
            ->maxLength('name_house', 225)
            ->requirePresence('name_house', 'create')
            ->notEmpty('name_house');

        $validator
            ->scalar('village_name')
            ->maxLength('village_name', 225)
            ->allowEmpty('village_name');

        $validator
            ->scalar('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('sample_house')
            ->maxLength('sample_house', 225)
            ->requirePresence('sample_house', 'create')
            ->notEmpty('sample_house');

        $validator
            ->decimal('price_per_unit')
            ->requirePresence('price_per_unit', 'create')
            ->notEmpty('price_per_unit');

        $validator
            ->scalar('sertifikat_hak')
            ->maxLength('sertifikat_hak', 225)
            ->requirePresence('sertifikat_hak', 'create')
            ->notEmpty('sertifikat_hak');

        $validator
            ->scalar('izin_pemanfaatan')
            ->maxLength('izin_pemanfaatan', 225)
            ->requirePresence('izin_pemanfaatan', 'create')
            ->notEmpty('izin_pemanfaatan');

        $validator
            ->scalar('pengesahaan')
            ->maxLength('pengesahaan', 225)
            ->requirePresence('pengesahaan', 'create')
            ->notEmpty('pengesahaan');

        $validator
            ->scalar('nomor_imb')
            ->maxLength('nomor_imb', 225)
            ->requirePresence('nomor_imb', 'create')
            ->notEmpty('nomor_imb');

        $validator
            ->scalar('izin_lainnya_label')
            ->maxLength('izin_lainnya_label', 225)
            ->allowEmpty('izin_lainnya_label');

        $validator
            ->scalar('izin_lainnya_value')
            ->maxLength('izin_lainnya_value', 225)
            ->allowEmpty('izin_lainnya_value');

        $validator
            ->allowEmpty('status')
            ->allowEmpty('surat_pernyataan')
            ->allowEmpty('resi_pengiriman');;

        return $validator;
    }

    public function validationSpesial(Validator $validator)
    {
        $validator
            ->add('form_checklist', 'file', [
            'rule' => ['mimeType', ['application/pdf']],
            'on' => function ($context) {
                return !empty($context['data']['form_checklist']);
            },
            'message'=>'Harap upload file form check list dengan format pdf'])
            ->allowEmpty('form_checklist');

        $validator
            ->add('surat_pernyataan', 'file', [
            'rule' => ['mimeType', ['application/pdf']],
            'on' => function ($context) {
                return !empty($context['data']['surat_pernyataan']);
            },
            'message'=>'Harap upload file surat pernyataan dengan format pdf'])
            ->allowEmpty('surat_pernyataan');

        $validator
            ->add('lembar_pemeriksaan', 'file', [
            'rule' => ['mimeType', ['application/pdf']],
            'on' => function ($context) {
                return !empty($context['data']['lembar_pemeriksaan']);
            },
            'message'=>'Harap upload file lembar pemeriksaan dengan format pdf'])
            ->allowEmpty('lembar_pemeriksaan');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['house_type_id'], 'HouseTypes'));
        $rules->add($rules->existsIn(['province_id'], 'Provinces'));
        $rules->add($rules->existsIn(['city_id'], 'Cities'));
        $rules->add($rules->existsIn(['district_id'], 'Districts'));
        $rules->add($rules->existsIn(['village_id'], 'Villages'));
        // $rules->add($rules->existsIn(['developer_id'], 'Developers'));

        return $rules;
    }

    public function beforeSave(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        if($entity->isNew()){
            $consultant = TableRegistry::get('Consultants');
            $consultant = $consultant->get(1);
            $code = $consultant->code;
            $year = date('Y');
            $month = date('m');
            $date = date("d");
            $hour = date("H");
            $minute = date("i");
            $second = date("s");
            $formula = $code.date('YmdHis');
            $getLastCode = $this->find('all',[
                'conditions' => [
                    'DATE(created)' => date('Y-m-d'),
                ]
            ])
            ->select([
                'no_urut' => '(CAST(RIGHT(code, 4) as SIGNED))'
            ])
            ->order('no_urut DESC')
            ->first();
            if(!empty($getLastCode)){
                $urut = $getLastCode->no_urut + 1;
            }else{
                $urut = 1;
            }
            if($urut <= 9){
                $prefix = "000";
            }elseif($urut <= 99){
                $prefix = "00";
            }elseif($urut <= 999){
                $prefix = "0";
            }elseif($urut <= 9999){
                $prefix = "";
            }
            $entity->code = $formula.$prefix.$urut;
        }

        return true;
    }

}
