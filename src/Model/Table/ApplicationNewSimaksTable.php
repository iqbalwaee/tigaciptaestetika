<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ApplicationNewSimaks Model
 *
 * @property \App\Model\Table\ApplicationLettersTable|\Cake\ORM\Association\BelongsTo $ApplicationLetters
 *
 * @method \App\Model\Entity\ApplicationNewSimak get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationNewSimak newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationNewSimak[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationNewSimak|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationNewSimak patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationNewSimak[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationNewSimak findOrCreate($search, callable $callback = null, $options = [])
 */
class ApplicationNewSimaksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_new_simaks');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ApplicationLetters', [
            'foreignKey' => 'application_letter_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('data_1')
            ->allowEmpty('data_1');

        $validator
            ->scalar('data_2')
            ->allowEmpty('data_2');

        $validator
            ->scalar('data_3')
            ->allowEmpty('data_3');

        $validator
            ->scalar('data_4')
            ->allowEmpty('data_4');

        $validator
            ->scalar('data_5')
            ->allowEmpty('data_5');

        $validator
            ->scalar('data_6')
            ->allowEmpty('data_6');

        $validator
            ->scalar('data_7')
            ->allowEmpty('data_7');

        $validator
            ->scalar('data_8')
            ->allowEmpty('data_8');

        $validator
            ->scalar('data_9')
            ->allowEmpty('data_9');

        $validator
            ->scalar('data_10')
            ->allowEmpty('data_10');

        $validator
            ->scalar('data_11')
            ->allowEmpty('data_11');

        $validator
            ->scalar('data_12')
            ->allowEmpty('data_12');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['application_letter_id'], 'ApplicationLetters'));

        return $rules;
    }
}
