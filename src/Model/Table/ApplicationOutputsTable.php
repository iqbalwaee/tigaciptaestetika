<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ApplicationOutputs Model
 *
 * @property \App\Model\Table\ApplicationLettersTable|\Cake\ORM\Association\BelongsTo $ApplicationLetters
 *
 * @method \App\Model\Entity\ApplicationOutput get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationOutput newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationOutput[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationOutput|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationOutput patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationOutput[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationOutput findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ApplicationOutputsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_outputs');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ApplicationLetters', [
            'foreignKey' => 'application_letter_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('code_house')
            ->maxLength('code_house', 225)
            ->requirePresence('code_house', 'create')
            ->notEmpty('code_house');

        $validator
            ->scalar('persyaratan_administratif')
            ->maxLength('persyaratan_administratif', 225)
            ->allowEmpty('persyaratan_administratif');

        $validator
            ->scalar('fungsi_bangunan')
            ->maxLength('fungsi_bangunan', 225)
            ->allowEmpty('fungsi_bangunan');

        $validator
            ->scalar('peruntukan')
            ->maxLength('peruntukan', 225)
            ->allowEmpty('peruntukan');

        $validator
            ->scalar('tata_bangunan')
            ->maxLength('tata_bangunan', 225)
            ->allowEmpty('tata_bangunan');

        $validator
            ->scalar('kelaikan')
            ->maxLength('kelaikan', 225)
            ->allowEmpty('kelaikan');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['application_letter_id'], 'ApplicationLetters'));

        return $rules;
    }
}
