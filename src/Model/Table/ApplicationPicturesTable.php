<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use App\Model\Behavior\Imagine as Imagine;

/**
 * ApplicationPictures Model
 *
 * @property \App\Model\Table\ApplicationLettersTable|\Cake\ORM\Association\BelongsTo $ApplicationLetters
 *
 * @method \App\Model\Entity\ApplicationPicture get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationPicture newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationPicture[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationPicture|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationPicture patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationPicture[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationPicture findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ApplicationPicturesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_pictures');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('ApplicationLetters', [
            'foreignKey' => 'application_letter_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'tampak_depan' => [
                'fields' => [
                    'dir' => 'tampak_depan_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}tampak_depan',
            ],
            'jalan_depan' => [
                'fields' => [
                    'dir' => 'jalan_depan_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}jalan_depan',
            ],
            'jalan_utama' => [
                'fields' => [
                    'dir' => 'jalan_utama_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}jalan_utama',
            ],
            'jalan_komplek' => [
                'fields' => [
                    'dir' => 'jalan_komplek_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}jalan_komplek',
            ],
            'fasilitas_umum' => [
                'fields' => [
                    'dir' => 'fasilitas_umum_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}fasilitas_umum',
            ],
            'ruang_tamu' => [
                'fields' => [
                    'dir' => 'ruang_tamu_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}ruang_tamu',
            ],
            'kamar_tidur' => [
                'fields' => [
                    'dir' => 'kamar_tidur_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}kamar_tidur',
            ],
            'plafond' => [
                'fields' => [
                    'dir' => 'plafond_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}plafond',
            ],
            'pintu_kusen_jendela' => [
                'fields' => [
                    'dir' => 'pintu_kusen_jendela_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}pintu_kusen_jendela',
            ],
            'toilet' => [
                'fields' => [
                    'dir' => 'toilet_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}toilet',
            ],
            'sumur_septictank' => [
                'fields' => [
                    'dir' => 'sumur_septictank_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}sumur_septictank',
            ],
            'kwh_listrik' => [
                'fields' => [
                    'dir' => 'kwh_listrik_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}kwh_listrik',
            ],
            'jaringan_air' => [
                'fields' => [
                    'dir' => 'jaringan_air_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}jaringan_air',
            ],
            'saluran_pembuangan' => [
                'fields' => [
                    'dir' => 'saluran_pembuangan_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}saluran_pembuangan',
            ],
            'tampak_atas_1' => [
                'fields' => [
                    'dir' => 'tampak_atas_1_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}tampak_atas_1',
            ],
            'tampak_atas_2' => [
                'fields' => [
                    'dir' => 'tampak_atas_2_dir',
                ],'transformer' =>  function ($table, $entity, $data, $field, $settings) {
                    $imagineComponent = new Imagine;
                    $extension  = pathinfo($data['name'], PATHINFO_EXTENSION);
                    $tmp        = tempnam(sys_get_temp_dir(), 'upload') . '.' . $extension;
                    $source     = $data['tmp_name'];
                    $width      = 400;
                    $height     = 230;
                    $imagineComponent->resize($source, $tmp,['width'=>$width,'height'=>$height]);
                    return [
                        $data['tmp_name'] => $data['name'],
                        $tmp => 'thumb-' . $data['name'],
                    ];
                },
                'path' => 'webroot{DS}assets{DS}perumahan{DS}{field-value:application_letter_id}{DS}tampak_atas_2',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->provider('upload', \Josegonzalez\Upload\Validation\DefaultValidation::class);
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->allowEmpty('tampak_depan')
            ->add('tampak_depan',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('tampak_depan', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('tampak_depan', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);
    

        $validator
            ->allowEmpty('jalan_depan')
            ->add('jalan_depan',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('jalan_depan', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('jalan_depan', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);
            $validator
            ->allowEmpty('jalan_utama')
            ->add('jalan_utama',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('jalan_utama', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('jalan_utama', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);
            $validator
            ->allowEmpty('jalan_komplek')
            ->add('jalan_komplek',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('jalan_komplek', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('jalan_komplek', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);
            $validator
            ->allowEmpty('fasilitas_umum')
            ->add('fasilitas_umum',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('fasilitas_umum', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('fasilitas_umum', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);
        
        $validator
            ->allowEmpty('kamar_tidur')
            ->add('kamar_tidur',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('kamar_tidur', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('kamar_tidur', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);

        $validator
            ->allowEmpty('ruang_tamu')
            ->add('ruang_tamu',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('ruang_tamu', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('ruang_tamu', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);

        $validator
            ->allowEmpty('plafond')
            ->add('plafond',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('plafond', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('plafond', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);

        $validator
            ->allowEmpty('pintu_kusen_jendela')
            ->add('pintu_kusen_jendela',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('pintu_kusen_jendela', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('pintu_kusen_jendela', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);

        $validator
            ->allowEmpty('toilet')
            ->add('toilet',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('toilet', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('toilet', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);

        $validator
            ->allowEmpty('sumur_septictank')
            ->add('sumur_septictank',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('sumur_septictank', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('sumur_septictank', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);

        $validator
            ->allowEmpty('kwh_listrik')
            ->add('kwh_listrik',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('kwh_listrik', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('kwh_listrik', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);

        $validator
            ->allowEmpty('jaringan_air')
            ->add('jaringan_air',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('jaringan_air', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('jaringan_air', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);

    $validator
        ->allowEmpty('saluran_pembuangan')
        ->add('saluran_pembuangan',[
            'mimeType' => [
                'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                'message' => 'Please upload images only (gif, png, jpg).'
            ]
        ])->add('saluran_pembuangan', 'fileAboveMinHeight', [
        'rule' => ['isAboveMinHeight', 300],
        'message' => 'Tinggi gambar minimal 300px',
        'provider' => 'upload'
        ])->add('saluran_pembuangan', 'fileAboveMinWidth', [
            'rule' => ['isAboveMinWidth', 400],
            'message' => 'Lebar gambar minimal 400px',
            'provider' => 'upload'
        ]);

        $validator
            ->allowEmpty('tampak_atas_1')
            ->add('tampak_atas_1',[
                'mimeType' => [
                    'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                    'message' => 'Please upload images only (gif, png, jpg).'
                ]
            ])->add('tampak_atas_1', 'fileAboveMinHeight', [
            'rule' => ['isAboveMinHeight', 300],
            'message' => 'Tinggi gambar minimal 300px',
            'provider' => 'upload'
            ])->add('tampak_atas_1', 'fileAboveMinWidth', [
                'rule' => ['isAboveMinWidth', 400],
                'message' => 'Lebar gambar minimal 400px',
                'provider' => 'upload'
            ]);

            $validator
                ->allowEmpty('tampak_atas_2')
                ->add('tampak_atas_2',[
                    'mimeType' => [
                        'rule' => array('mimeType', array('image/gif', 'image/png', 'image/jpg', 'image/jpeg')),
                        'message' => 'Please upload images only (gif, png, jpg).'
                    ]
                ])->add('tampak_atas_2', 'fileAboveMinHeight', [
                'rule' => ['isAboveMinHeight', 300],
                'message' => 'Tinggi gambar minimal 300px',
                'provider' => 'upload'
                ])->add('tampak_atas_2', 'fileAboveMinWidth', [
                    'rule' => ['isAboveMinWidth', 400],
                    'message' => 'Lebar gambar minimal 400px',
                    'provider' => 'upload'
                ]);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['application_letter_id'], 'ApplicationLetters'));

        return $rules;
    }
}
