<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ApplicationRoomms Model
 *
 * @property \App\Model\Table\ApplicationLettersTable|\Cake\ORM\Association\BelongsTo $ApplicationLetters
 *
 * @method \App\Model\Entity\ApplicationRoomm get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationRoomm newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationRoomm[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationRoomm|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationRoomm patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationRoomm[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationRoomm findOrCreate($search, callable $callback = null, $options = [])
 */
class ApplicationRoommsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_roomms');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ApplicationLetters', [
            'foreignKey' => 'application_letter_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('data')
            ->maxLength('data', 4294967295)
            ->allowEmpty('data');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['application_letter_id'], 'ApplicationLetters'));

        return $rules;
    }
}
