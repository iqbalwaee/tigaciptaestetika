<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ApplicationRoomsComments Model
 *
 * @property \App\Model\Table\ApplicationLettersTable|\Cake\ORM\Association\BelongsTo $ApplicationLetters
 * @property |\Cake\ORM\Association\BelongsTo $RoomsUtilities
 *
 * @method \App\Model\Entity\ApplicationRoomsComment get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationRoomsComment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationRoomsComment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationRoomsComment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationRoomsComment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationRoomsComment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationRoomsComment findOrCreate($search, callable $callback = null, $options = [])
 */
class ApplicationRoomsCommentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_rooms_comments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ApplicationLetters', [
            'foreignKey' => 'application_letter_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RoomsUtilities', [
            'foreignKey' => 'rooms_utility_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('descriptions')
            ->maxLength('descriptions', 225)
            ->allowEmpty('descriptions');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['application_letter_id'], 'ApplicationLetters'));
        $rules->add($rules->existsIn(['rooms_utility_id'], 'RoomsUtilities'));

        return $rules;
    }
}
