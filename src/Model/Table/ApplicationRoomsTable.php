<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ApplicationRooms Model
 *
 * @property \App\Model\Table\ApplicationLettersTable|\Cake\ORM\Association\BelongsTo $ApplicationLetters
 * @property \App\Model\Table\RoomsTable|\Cake\ORM\Association\BelongsTo $Rooms
 * @property \App\Model\Table\RoomsUtilitiesTable|\Cake\ORM\Association\BelongsTo $RoomsUtilities
 * @property |\Cake\ORM\Association\BelongsTo $RoomsOptions
 *
 * @method \App\Model\Entity\ApplicationRoom get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationRoom newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationRoom[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationRoom|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationRoom patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationRoom[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationRoom findOrCreate($search, callable $callback = null, $options = [])
 */
class ApplicationRoomsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_rooms');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ApplicationLetters', [
            'foreignKey' => 'application_letter_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Rooms', [
            'foreignKey' => 'room_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RoomsUtilities', [
            'foreignKey' => 'rooms_utility_id'
        ]);
        $this->belongsTo('RoomsOptions', [
            'foreignKey' => 'rooms_option_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {

        $validator
            ->allowEmpty('description');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {

        return $rules;
    }
}
