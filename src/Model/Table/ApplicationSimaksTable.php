<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ApplicationSimaks Model
 *
 * @property \App\Model\Table\ApplicationLettersTable|\Cake\ORM\Association\BelongsTo $ApplicationLetters
 *
 * @method \App\Model\Entity\ApplicationSimak get($primaryKey, $options = [])
 * @method \App\Model\Entity\ApplicationSimak newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ApplicationSimak[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationSimak|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ApplicationSimak patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationSimak[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ApplicationSimak findOrCreate($search, callable $callback = null, $options = [])
 */
class ApplicationSimaksTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('application_simaks');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('ApplicationLetters', [
            'foreignKey' => 'application_letter_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('element_struktural')
            ->maxLength('element_struktural', 225)
            ->allowEmpty('element_struktural');

        $validator
            ->scalar('pondasi')
            ->maxLength('pondasi', 225)
            ->allowEmpty('pondasi');

        $validator
            ->scalar('lokasi')
            ->maxLength('lokasi', 225)
            ->allowEmpty('lokasi');

        $validator
            ->scalar('bagian')
            ->maxLength('bagian', 225)
            ->allowEmpty('bagian');

        $validator
            ->scalar('tahun')
            ->maxLength('tahun', 225)
            ->allowEmpty('tahun');

        $validator
            ->scalar('panjang')
            ->maxLength('panjang', 225)
            ->allowEmpty('panjang');

        $validator
            ->scalar('tinggi')
            ->maxLength('tinggi', 225)
            ->allowEmpty('tinggi');

        $validator
            ->allowEmpty('bahan_bangunan');

        $validator
            ->allowEmpty('tipe');

        $validator
            ->allowEmpty('garis');

        $validator
            ->allowEmpty('retak_struktur');

        $validator
            ->allowEmpty('retak_permukaan');

        $validator
            ->allowEmpty('pengangkatan');

        $validator
            ->allowEmpty('kebocoran');

        $validator
            ->allowEmpty('penurunan');

        $validator
            ->allowEmpty('sill_plate_rot');

        $validator
            ->allowEmpty('kondisi');

        $validator
            ->allowEmpty('estimasi');

        $validator
            ->scalar('kesimpulan')
            ->allowEmpty('kesimpulan');

        $validator
            ->scalar('pemeriksa')
            ->maxLength('pemeriksa', 225)
            ->allowEmpty('pemeriksa');

        $validator
            ->scalar('nama_pemeriksaa')
            ->maxLength('nama_pemeriksaa', 225)
            ->allowEmpty('nama_pemeriksaa');

        $validator
            ->date('date')
            ->allowEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['application_letter_id'], 'ApplicationLetters'));

        return $rules;
    }
}
