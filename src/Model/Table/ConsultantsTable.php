<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Consultants Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Consultant get($primaryKey, $options = [])
 * @method \App\Model\Entity\Consultant newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Consultant[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Consultant|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Consultant patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Consultant[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Consultant findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ConsultantsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('consultants');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'picture' => [
                'fields' => [
                    'dir' => 'picture_dir', // defaults to `dir`
                ],
            ],
            'compro' => [
                'fields' => [
                    'dir' => 'compro_dir', // defaults to `dir`
                ],
            ],
            'form_checklist' => [
                'fields' => [
                    'dir' => 'form_checklist_dir', // defaults to `dir`
                ],
            ],
            'pic_signature' => [
                'fields' => [
                    'dir' => 'pic_signature_dir', // defaults to `dir`
                ],
            ],
            'ska_1' => [
                'fields' => [
                    'dir' => 'ska_1_dir', // defaults to `dir`
                ],
            ],
            'ska_2' => [
                'fields' => [
                    'dir' => 'ska_2_dir', // defaults to `dir`
                ],
            ],
            'ska_3' => [
                'fields' => [
                    'dir' => 'ska_3_dir', // defaults to `dir`
                ],
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 225)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 225)
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->integer('created_by')
            ->allowEmpty('created_by');

        $validator
            ->allowEmpty('picture');

        $validator
            ->add('compro', 'file', [
            'rule' => ['mimeType', ['application/pdf']],
            'on' => function ($context) {
                return !empty($context['data']['compro']);
            },
            'message'=>'Harap upload file PDF'])
            ->allowEmpty('compro');

        $validator
            ->add('form_checklist', 'file', [
            'rule' => ['mimeType', ['application/pdf']],
            'on' => function ($context) {
                return !empty($context['data']['form_checklist']);
            },
            'message'=>'Harap upload file PDF'])
            ->allowEmpty('form_checklist');

        $validator
            ->add('pic_signature', 'file', [
            'rule' => ['mimeType', ['image/jpg','image/jpeg','image/png']],
            'on' => function ($context) {
                return !empty($context['data']['pic_signature']);
            },
            'message'=>'Harap upload file JPG,JPEG atau PNG'])
            ->allowEmpty('pic_signature');

        
        $validator
            ->add('ska_1', 'file', [
            'rule' => ['mimeType', ['image/jpg','image/jpeg','image/png']],
            'on' => function ($context) {
                return !empty($context['data']['ska_1']);
            },
            'message'=>'Harap upload file JPG,JPEG atau PNG'])
            ->allowEmpty('ska_1');

        $validator
            ->add('ska_2', 'file', [
            'rule' => ['mimeType', ['image/jpg','image/jpeg','image/png']],
            'on' => function ($context) {
                return !empty($context['data']['ska_2']);
            },
            'message'=>'Harap upload file JPG,JPEG atau PNG'])
            ->allowEmpty('ska_2');
        
        $validator
            ->add('ska_3', 'file', [
            'rule' => ['mimeType', ['image/jpg','image/jpeg','image/png']],
            'on' => function ($context) {
                return !empty($context['data']['ska_3']);
            },
            'message'=>'Harap upload file JPG,JPEG atau PNG'])
            ->allowEmpty('ska_3');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
