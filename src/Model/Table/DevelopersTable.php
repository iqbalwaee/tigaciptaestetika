<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Developers Model
 *
 * @property \App\Model\Table\DppFromsTable|\Cake\ORM\Association\BelongsTo $DppFroms
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Developer get($primaryKey, $options = [])
 * @method \App\Model\Entity\Developer newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Developer[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Developer|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Developer patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Developer[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Developer findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class DevelopersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('developers');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('DppFroms', [
            'className' => 'Provinces',
            'foreignKey' => 'dpp_from_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        
        $this->hasMany('ApplicationLetters', [
            'foreignKey' => 'developer_id',
            'joinType' => 'INNER',
            'dependent' => true,
            'cascadeCallbacks' => true,
        ]);
        $this->addBehavior('AuditStash.AuditLog');
        $this->addBehavior('Josegonzalez/Upload.Upload', [
            'logo' => [
                'fields' => [
                    'dir' => 'logo_dir', // defaults to `dir`
                ],
                'path' => 'webroot{DS}assets{DS}developerlogo{DS}{microtime}',
            ],
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 225)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('ceo_name')
            ->maxLength('ceo_name', 225)
            ->requirePresence('ceo_name', 'create')
            ->notEmpty('ceo_name');

        $validator
            ->scalar('nik_ceo')
            ->maxLength('nik_ceo', 225)
            ->requirePresence('nik_ceo', 'create')
            ->notEmpty('nik_ceo');

        $validator
            ->scalar('kta_number')
            ->maxLength('kta_number', 225)
            ->requirePresence('kta_number', 'create')
            ->notEmpty('kta_number');

        $validator
            ->scalar('address')
            ->requirePresence('address', 'create')
            ->notEmpty('address');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 20)
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->boolean('status')
            ->allowEmpty('status');

        $validator
            ->allowEmpty('logo');

        $validator
            ->allowEmpty('logo_dir');

        $validator
            ->integer('modified_by')
            ->allowEmpty('modified_by');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['dpp_from_id'], 'DppFroms'));
        // $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }

    public function afterDelete(\Cake\Event\Event $event, \Cake\ORM\Entity $entity, 
    \ArrayObject $options)
    {
        
        $user_id = $entity->user_id;
        $user = $this->Users->get($user_id);
        $this->Users->delete($user);
        return true;
    }
}
