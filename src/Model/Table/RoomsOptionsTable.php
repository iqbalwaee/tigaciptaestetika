<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RoomsOptions Model
 *
 * @property |\Cake\ORM\Association\BelongsTo $RoomsUtilities
 *
 * @method \App\Model\Entity\RoomsOption get($primaryKey, $options = [])
 * @method \App\Model\Entity\RoomsOption newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RoomsOption[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RoomsOption|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RoomsOption patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RoomsOption[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RoomsOption findOrCreate($search, callable $callback = null, $options = [])
 */
class RoomsOptionsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rooms_options');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('RoomsUtilities', [
            'foreignKey' => 'rooms_utility_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 225)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['rooms_utility_id'], 'RoomsUtilities'));

        return $rules;
    }
}
