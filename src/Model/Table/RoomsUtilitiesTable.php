<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * RoomsUtilities Model
 *
 * @property |\Cake\ORM\Association\HasMany $RoomsOptions
 *
 * @method \App\Model\Entity\RoomsUtility get($primaryKey, $options = [])
 * @method \App\Model\Entity\RoomsUtility newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\RoomsUtility[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\RoomsUtility|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\RoomsUtility patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\RoomsUtility[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\RoomsUtility findOrCreate($search, callable $callback = null, $options = [])
 */
class RoomsUtilitiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('rooms_utilities');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->hasMany('RoomsOptions', [
            'foreignKey' => 'rooms_utility_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 225)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
