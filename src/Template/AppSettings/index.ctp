<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($dataSave,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('App_Name',[
                        'label'=>'Nama Website',
                        'value' => $appSettings['App_Name']->valueField
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-3">
                    <?=$this->Form->control('App_Logo_Width',[
                        'label'=>'App Logo Width',
                        'value' => $appSettings['App_Logo_Width']->valueField
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('App_Logo_Height',[
                        'label'=>'App Logo Height',
                        'value' => $appSettings['App_Logo_Height']->valueField
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('App_Logo',[
                        'label'=>'Logo Website',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15" style="display:none;">
                <div class="col-md-3">
                    <?=$this->Form->control('App_Logo_Login_Width',[
                        'label'=>'App Logo Width',
                        'value' => $appSettings['App_Logo_Login_Width']->valueField
                    ]);;?>
                </div>
                <div class="col-md-3">
                    <?=$this->Form->control('App_Logo_Login_Height',[
                        'label'=>'App Logo Login Height',
                        'value' => $appSettings['App_Logo_Login_Height']->valueField
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('App_Logo_Login',[
                        'label'=>'App Logo Login',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
            </div> 
            <div class="row m--margin-bottom-15"  style="display:none;">
                <div class="col-md-4">
                    <?=$this->Form->control('App_Login_Cover',[
                        'label'=>'App Login Cover',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('App_Favico',[
                        'label'=>'App Favico',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div> 
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('App_DPP_PIC',[
                        'label'=>'DPP PIC',
                        'value' => $appSettings['App_DPP_PIC']->valueField
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('App_DPP_PIC_Signature',[
                        'label'=>'Tanda Tanggan PIC',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('App_Kop_Logo',[
                        'label'=>'DPP KOP Logo',
                        'class' => 'form-control m-input',
                        'type' => 'file'
                    ]);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('App_Kop_Header',[
                        'label'=>'App KOP Header',
                        'type' => 'textarea',
                        'class' => 'summernote',
                        'value' => $appSettings['App_Kop_Header']->valueField
                    ]);;?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->control('App_Kop_Footer',[
                        'label'=>'App KOP Footer',
                        'type' => 'textarea',
                        'class' => 'summernote',
                        'value' => $appSettings['App_Kop_Footer']->valueField
                    ]);;?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        $(".summernote").summernote({
            toolbar: [
                // [groupName, [list of button]]
                ['style', ['bold', 'italic', 'underline', 'clear']],
            ],
            height : 150
        })
    </script>
<?php $this->end();?>