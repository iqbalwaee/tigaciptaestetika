<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">							
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>									
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($applicationLetter,['class'=>'m-form m-form--fit m-form--label-align-right']) ?>
    <div class="m-portlet__body">
            <div class="m-form__section m-form__section--first">
                <div class="m-form__heading">
                    <h3 class="m-form__heading-title">
                        Input Data Perumahan:
                    </h3>
                </div>
                <?php
                    $myTemplates = [
                        'inputContainerError' => '<div class="form-group m-form__group row">{{content}}{{error}}</div>',
                        'inputContainer' => '<div class="form-group m-form__group row">{{content}}</div>',
                        'formGroup' => '{{label}}<div class="col-lg-8  col-md-8">{{input}}</div>',
                        'label' => '<label class="col-lg-2 col-md-4 col-form-label" {{attrs}}>{{text}}</label>',
                    ];
                    $this->Form->setTemplates($myTemplates);
                ?>
                <?=$this->Form->control('primary_function',['label' => 'Fungsi Utama','readonly','value'=>'Rumah Hunian']);?>
                <?=$this->Form->control('house_type_id',['options' => $houseTypes,'empty'=>'Pilih tipe rumah','label' => 'Tipe Bangunan']);?>
                <?=$this->Form->control('cordinate_area',['label' => 'Posisi Kordinat']);?>
                <?=$this->Form->control('clasification_kompleksitas',['label' => 'Klasifikasi Kompleksitas']);?>
                <?=$this->Form->control('high_building',['label' => 'Ketinggian Bangunan (M)']);?>
                <?=$this->Form->control('floor_count',['label' => 'Jumlah Lantai Bangunan']);?>
                <?=$this->Form->control('surface_building',['label' => 'Luas Bangunan   (M<sup>2</sup>)','escape'=>false]);?>
                <?=$this->Form->control('surface_area',['label' => 'Luas Tanah  (M<sup>2</sup>)','escape'=>false]);?>
                <?php
                    $myTemplates = [
                        'formGroup' => '{{label}}<div class="col-lg-8  col-md-8">{{input}}</div>',
                    ];
                    $this->Form->setTemplates($myTemplates);
                ?>
                <?=$this->Form->control('name_house',['label' => 'Nama Perumahaan']);?>

                <?php
                    $myTemplates = [
                        'formGroup' => '{{label}}<div class="col-lg-8  col-md-8">{{input}}<span class="m-form__help">{{after}}</span></div>',
                    ];
                    $this->Form->setTemplates($myTemplates);
                ?>
                <?=$this->Form->control('building[]',[
                    'label' => 'Letak Bangunan (Blok)',
                    'class'=>'form-control m-input',
                    'templateVars'=>['after'=>'Isi data blok rumah dengan mengetik nama blok lalu tekan enter. Misal A1 (tekan enter). '],
                    'type'=>'select',
                    'disabled',
                    'multiple',
                    ]
                );?>
                <?php
                    $myTemplates = [
                        'formGroup' => '{{label}}<div class="col-lg-8  col-md-8">{{input}}</div>',
                    ];
                    $this->Form->setTemplates($myTemplates);
                ?>
                <?=$this->Form->control('province_id',['label' => 'Provinsi','options'=>$provinces,'empty'=>'Pilih Provinsi']);?>
                <?=$this->Form->control('city_id',['label' => 'Kota / Kabupaten','options'=>$cities,'empty'=>'Pilih Kota / Kabupaten']);?>
                <?=$this->Form->control('district_id',['label' => 'Kecamatan','options'=>$districts, 'empty' => 'Pilih Kecamatan']);?>
                <?=$this->Form->control('village_id',['label' => 'Kelurahan','options'=>$villages,'empty'=>'Pilih Kelurahan']);?>
                <?php
                    $myTemplates = [
                        'formGroup' => '{{label}}<div class="col-lg-8  col-md-8">{{input}}<span class="m-form__help">{{after}}</span></div>',
                    ];
                    $this->Form->setTemplates($myTemplates);
                ?>
                <?=$this->Form->control('village_name',[
                    'label' => 'Kampung',
                    'class'=>'form-control m-input',
                    'templateVars'=>['after'=>'Isi nama kampung bila ada. '],
                    ]
                );?>
                <?php
                    $myTemplates = [
                        'formGroup' => '{{label}}<div class="col-lg-8  col-md-8">{{input}}</div>',
                    ];
                    $this->Form->setTemplates($myTemplates);
                ?>
                <?=$this->Form->control('address',[
                    'label' => 'Alamat',
                    'class'=>'form-control m-input',
                    ]
                );?>
                <?=$this->Form->control('sample_house',[
                    'label' => 'Rumah Sample',
                    'class'=>'form-control m-input',
                    'empty' => 'Pilih 1 Rumah yang mewakili semua unit blok rumah',
                    'options' => $building_list
                    ]
                );?>
                <?=$this->Form->control('price_per_unit',[
                    'label' => 'Harga Jual Per-Unit',
                    'class'=>'form-control m-input',
                    'textnumber' => 'true',
                    'type'=>'text'
                    ]
                );?> 
            </div>
            <div class="m-form__seperator m-form__seperator--dashed"></div>
            <div class="m-form__section m-form__seperator--dashed">
                <div class="m-form__heading">
                    <h3 class="m-form__heading-title">
                        Input Data Administrasi:
                    </h3>
                </div>
                <?=$this->Form->control('sertifikat_hak',['label' => 'Sertifikat Hak Atas Tanah']);?>
                <?=$this->Form->control('izin_pemanfaatan',['label' => 'Izin Pemanfaatan Tanah']);?>
                <?=$this->Form->control('pengesahaan',['label' => 'Pengesahan Site Plan']);?>
                <?=$this->Form->control('nomor_imb',['label' => 'Nomor IMB']);?>
                <div class="form-group m-form__group row other_document" ><label class="col-lg-2 col-md-4 col-form-label" for="">Input Izin Lainnya</label><div class="col-lg-4  col-md-4">
                    <input type="text" name="izin_lainnya_label" class="form-control m-input" maxlength="225" id="izin_lainnya_label" placeholder="Jenis Izin Lainnya">
                </div><div class="col-lg-4  col-md-4">
                    <input type="text" name="izin_lainnya_value" class="form-control m-input" maxlength="225" id="izin_lainnya_value" placeholder="Nomor Izin Lainnya">
                </div></div>                
            </div>
            <div class="m-form__seperator m-form__seperator--dashed"></div>
            <div class="m-form__section m-form__section--last m-form__seperator--dashed">
                <div class="m-form__heading">
                    <h3 class="m-form__heading-title">
                        Input Data Pendukung:
                    </h3>
                </div>
                <?=$this->Form->control('surat_pernyataan',['label' => 'Surat Pernyataan','type'=>'file']);?>
                <?=$this->Form->control('resi_pengiriman',['label' => 'Resi Pengiriman','type'=>'file']);?>
            </div>
        </div>
        <div class="m-form__actions m-form__actions--sm m-form__actions--solid">
            <button type="submit" class="btn btn-primary">
                Submit
            </button>
            <button type="reset" class="btn btn-secondary">
                Cancel
            </button>
        </div>
    <?= $this->Form->end();?>
</div>

<?php $this->start('script');?>
    <script>
        $("#name-house").on('keyup',function(e){
            if($(this).val() != ""){
                $("#building").prop('disabled',false)
            }else{
                $("#building").prop('disabled',true)
                $("#building").val([]);
                $("#building").trigger('change'); 
            }
        })
        $("#building").select2({
            placeholder:"Masukan blok rumah",
            multiple:true,
            tags:true,
            closeOnSelect:false,  
            allowClear: true,
        })
        .on("select2:select",function(e){
            $(".select2-search__field").val("")    
        })
        .on("select2:selecting",function(event){
            // console.log(e.params)
            //check ajax
            console.log(event);
            $.ajax({
                url : "<?=$this->Url->build('/apis/check-building');?>",
                dataType : 'json',
                type : 'post',
                data : {
                    'house_name' : $("#name-house").val(),
                    'building' : event.params.args.data
                },
                success : function(response){
                    // console.log(response);
                    if(response.count == false){
                        // e.preventDefault();
                        // get select2 instance
                        console.log(['MASOOOKKK',response])
                        var Select2 = $("#building").data('select2');
                        // remove prevented flag
                        console.log(event.params);
                        delete event.params.args.prevented;

                        // Call trigger on the observer with select2 instance as context
                        Select2.constructor.__super__.trigger.call(Select2, 'select', event.params.args);
                    }else{
                        //save
                        console.log(['GAGAL MASOOOKKK',response])
                        $(".select2-search__field").val("") 
                        swal("Ooopss!!","No. Blok : <b>" +  event.params.args.data.text + '</b> dengan nama perumahan : <b>' + $("#name-house").val() +'</b> sudah terdaftar. Silahkan input no. blok lain','error');
                        return false;
                    }
                },
                error : function(){
                    // return false;
                }
                // return false;
            })
            event.preventDefault();
            
            return false;
        }).on("change",function(e){
            // console.log(e)

            var data = $('#building').val();
            var html = '<option value="">Pilih 1 Rumah yang mewakili semua unit blok rumah</option>';
            $.each(data,function(e,i){
                html += '<option value="'+i+'">'+i+'</option>';
            })
            $("#sample-house").html(html);
        });
        $('#province-id').select2({
        }).on("select2:select",function(e){
            var result = e.params.data;
            if(result.id != 0&& result.id != ""){
                $.ajax({
                    url: '<?=$this->Url->build('/apis/get-cities');?>/'+ result.id,
                    dataType: 'json',
                    success: function(result){
                        var data = result.results;
                        $("#city-id").html('<option value="">Pilih Kota / Kabupaten</option>');
                        $("#city-id").val("");
                        $("#city-id").select2({"data":data}).trigger("change");
                        $("#district-id").html('<option value="">Pilih Kecamatan</option>');
                        $("#district-id").val("");
                        $("#district-id").select2().trigger("change");
                        $("#village-id").html('<option value="">Pilih Kelurahan</option>');
                        $("#village-id").val("");
                        $("#village-id").select2().trigger("change");
                    }
                })
            }
        })
        $('#city-id').select2({
        }).on("select2:select",function(e){
            var result = e.params.data;
        })

        $('#city-id').select2({
        }).on("select2:select",function(e){
            var result = e.params.data;
            if(result.id != 0&& result.id != ""){
                $.ajax({
                    url: '<?=$this->Url->build('/apis/get-districts');?>/'+ result.id,
                    dataType: 'json',
                    success: function(result){
                        var data = result.results;
                        $("#district-id").html('<option value="">Pilih Kecamatan</option>');
                        $("#district-id").val("");
                        $("#district-id").select2({"data":data}).trigger("change");
                        
                        $("#village-id").html('<option value="">Pilih Kelurahan</option>');
                        $("#village-id").val("");
                        $("#village-id").select2().trigger("change");
                    }
                })
            }
        })
        $('#district-id').select2({
        }).on("select2:select",function(e){
            var result = e.params.data;
            if(result.id != 0&& result.id != ""){
                $.ajax({
                    url: '<?=$this->Url->build('/apis/get-villages');?>/'+ result.id,
                    dataType: 'json',
                    success: function(result){
                        var data = result.results;
                        $("#village-id").html('<option value="">Pilih Kelurahan</option>');
                        $("#village-id").val("");
                        $("#village-id").select2({"data":data}).trigger("change");
                    }
                })
            }
        })
        $("#village-id").select2();
    </script>

<?php $this->end();?>