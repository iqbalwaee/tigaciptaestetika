<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>		
                <?php if(!empty($applicationLetter->application_judgment)):?>
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'printApplicationJudgments',$applicationLetter->id]);?>" target="_BLANK" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print PENILAIAN KELAYAKAN UNIT RUMAH" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-print"></i></a>	
                </li>
                <?php endif;?>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <style>
        
        .block{
            display:block;
            margin:auto;
        }
        .text-center{
            text-align:center;
        }
        .text-right{
            text-align:right;
        }
        .table-bordered tbody tr:first-child td{
            border-top:1px solid #000;
        }
        .table-sm th,.table-sm td{
            padding:2px 5px;
        }
    
        table.table-bordered {
            border-collapse: separate;
            border :  0px solid #000000;
            border-spacing: 0;
            font-size: 11px !important;
            width: 100%;
            border-color:  #000000 ;
            border-right: 1px solid;
        }
        table.table-bordered tr {
            border-color:  #000000 ;
            border-style: none ;
            border-width: 0 ;
        }
        table.table-bordered td {
            border-color:  #000000 ;
            border-left: 1px solid;
            border-bottom:1px solid ;
        }

        table.table-bordered th {
            border-color:  #000000 ;
            border-left: 1px solid;
            border-top:1px solid ;
            border-bottom:1px solid ;
        }
        table{
            font-size:11px !important;
        }
        .table-child{
            border-collapse: separate;
            border-spacing: 0;
            font-size: 11px !important;
            width: 100%;
        }
        .table-child tr td{
            border:none;
            border-top:none;
            border-bottom:none !important;
        }
        .table-child tbody tr:first-child td {
            border-top: none;
        }
        .table-note{
            border-collapse: separate;
            border-spacing: 0;
            font-size: 11px !important;
            width: 100%;
        }
        .table-note td{
            
        }
        .table-note-detail{
            border-collapse: separate;
            border-spacing: 0;
            font-size: 11px !important;
            width: 100%;
        }
        .table-note-detail td{
            vertical-align: top;
        }
    </style>
    <?= $this->Form->create($applicationLetter,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="m-form__section">
                <?php
                    $myTemplates = [
                        'inputContainerError' => '{{content}}{{error}}',
                        'inputContainer' => '{{content}}',
                        'formGroup' => '{{input}}',
                        'label' => '{{text}}',
                    ];
                    $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                    if(!empty($applicationLetter->output_date)):
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                    endif;
                    $this->Form->setTemplates($myTemplates);
                ?>
                <?php $valueData = '
                    <table class="table-bordered">
                    <thead>
                        <tr>
                            <th width="30px" class="text-center">NO</th>
                            <th class="text-center">KRITERIA</th>
                            <th class="text-center" width="8%">ADA</th>
                            <th class="text-center" width="8%">TIDAK</th>
                            <th class="text-center" width="20%">KETERANGAN</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center"><b>I</b></td>
                            <td><b>LINGKUNGAN</b></td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">1</td>
                            <td>Jalan Lingkungan di Perumahan</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td>Tersedianya Ruang Terbuka Hijau</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">3</td>
                            <td>Tersedianya Fasilitan Peribadatan</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">4</td>
                            <td>Tersedianya Pengolahan Limbah:</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center dejavu-font"></td>
                            <td><div class="padding-left:20px;">Tersedianya Bak Sampah</div></td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center dejavu-font"></td>
                            <td><div class="padding-left:20px;">Tersedianya Saluran Pembuangan</div></td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center"><b>II</b></td>
                            <td><b>BANGUNAN</b></td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center"><b>A</b></td>
                            <td><b>KELENGKAPAN BANGUNAN</b></td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">1</td>
                            <td>Pondasi</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td>Sloof</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">3</td>
                            <td>Kolom</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">4</td>
                            <td>Ring Balok</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">5</td>
                            <td>Kuda-Kuda & Atap</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">6</td>
                            <td>Dinding</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">7</td>
                            <td>Lantai</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">8</td>
                            <td>Kusen, Pintu & Jendela</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">9</td>
                            <td>Listrik</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">10</td>
                            <td>Sanitair</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center"><b>B</b></td>
                            <td><b>KESEHATAN BANGUNAN</b></td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center" rowspan="2">1</td>
                            <td style="border-bottom:none;">Pencahayaan dan Kualitas Udara</td>
                            <td class="text-center" rowspan="2"></td>
                            <td class="text-center" rowspan="2"></td>
                            <td rowspan="2"></td>
                        </tr>
                        <tr>
                            <td><i>(10% dari dinding yang berhadapan dengan ruang terbuka)</i></td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td>Ketersediaan Sumber Air Bersih, Minimal Satu Titik Untuk Bangunan</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center" rowspan="2"></td>
                            <td style="border-bottom:none;">KAMAR MANDI</td>
                            <td class="text-center" rowspan="2"></td>
                            <td class="text-center" rowspan="2"></td>
                            <td rowspan="2"></td>
                        </tr>
                        <tr>
                            <td style="padding:0px;">
                                <table width="100%" class="table-child">
                                    <tbody>
                                        <tr>
                                            <td width="5px">-</td>
                                            <td>Permukaan lantai tidak lidak licin dan memiliki kemiringannya mengarah ke floor drain</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center dejavu-font"></td>
                            <td style="padding:0px;">
                                <table width="100%" class="table-child">
                                    <tbody>
                                        <tr>
                                            <td width="5px">-</td>
                                            <td>Dinding dalam kamar mandi kedap air dan tidak mudah berlumut</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center" rowspan="2">3</td>
                            <td style="border-bottom:none;">Ketersediaan Instalasi Pengolahan Air Limbah</td>
                            <td class="text-center" rowspan="2"></td>
                            <td class="text-center" rowspan="2"></td>
                            <td rowspan="2"></td>
                        </tr>
                        <tr>
                            <td>Tangki Septik Mempunyai Resapan</td>
                        </tr>
                        <tr>
                            <td class="text-center">4</td>
                            <td>Pengolahan Air Kotor Menuju Saluran Pembuangan</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">5</td>
                            <td>Pembagian Ruangan Sesuai dengan Luas Bangungan</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center"><b>C</b></td>
                            <td><b>KENYAMANAN BANGUNAN</b></td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">1</td>
                            <td>Terdapat Sekat Antar Ruangan(privasi)</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td>Terdapat Ventilasi Udara (<i>5% dari luas lantai</i>)</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">3</td>
                            <td>Terdapat Pencahayaan Buatan Cukup</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">4</td>
                            <td>Permukaan Plafond Rata</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">5</td>
                            <td>Permukaan Lantai di Finishing Halus</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">6</td>
                            <td>Dinding di Plaster Halus dan di Cat</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">7</td>
                            <td>Atap Tidak Bocor</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center"><b>D</b></td>
                            <td><b>KEMUDAHAN BANGUNAN</b></td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">1</td>
                            <td>Pintu dan Jendela dalam Kondisi Baik</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="text-center">2</td>
                            <td>Pintu dan Jendela dengan Mudah Dibuka-Tutup Tanpa Gangguan</td>
                            <td class="text-center dejavu-font"></td>
                            <td class="text-center dejavu-font"></td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <br><br>
                <table width="100%" class="table-note">
                    <tbody>
                        <tr>
                            <td width="30px"></td>
                            <td>Berdasarkan penilaian diatas, data disimpulkan bahwa rumah ini dapat dinyatakan <b>Layak / Tidak Layak</b> huni dan telah memenuhi Persyaratan Kesehatan Rumah Tinggal, yakni sebagai berikut :</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <table width="100%" class="table-note-detail">
                                    <tbody>
                                        <tr>
                                            <td colspan="2"><b>CATATAN:</b></td>
                                        </tr>
                                        <tr>
                                            <td class="text-right" width="40px">1.</td>
                                            <td>Unit rumah pada blok <b>'.($applicationLetter->sample_house).'</b> telah layak huni dan layak fungsi</td>
                                        </tr>
                                        <tr>
                                            <td class="text-right">2.</td>
                                            <td>Pengembang Wajib memperbaiki :</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" style="border-collapse: collapse;">
                                                    <tbody>
                                                        <tr>
                                                            <td width="40px;">&nbsp;</td>
                                                            <td width="15px">a.</td>
                                                            <td style="border-bottom:1px dotted #000;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="40px;">&nbsp;</td>
                                                            <td width="15px">b.</td>
                                                            <td style="border-bottom:1px dotted #000;"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="40px;">&nbsp;</td>
                                                            <td width="15px">c.</td>
                                                            <td style="border-bottom:1px dotted #000;"></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>';?><?php if(!empty($applicationLetter->application_judgment->id)):?>
                        <?php 
                            $valueData = $applicationLetter->application_judgment->data;;
                        ?>
                        <?=$this->Form->control('application_judgment.id',['type'=>'hidden']);?>
                    <?php endif;?>
                <?=$this->Form->control('application_judgment.data',['label' => 'PENILAIAN KELAYAKAN UNIT RUMAH','type'=>'textarea','class'=>'summernote',
                        'value' => $valueData,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        $(".summernote").summernote({height:600, toolbar: [
            ['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']],
            ['misc', ['codeview']]
        ]})
    </script>

<?php $this->end();?>