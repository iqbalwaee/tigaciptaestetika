<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>		
                <?php if(!empty($applicationLetter->application_new_simak)):?>
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'printApplicationNewSimak',$applicationLetter->id]);?>" target="_BLANK" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print SIMAK Baru" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-print"></i></a>    
                </li>
                <?php endif;?>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>

    <style>
        
        .block{
            display:block;
            margin:auto;
        }
        .text-center{
            text-align:center;
        }
        .text-right{
            text-align:right;
        }
        .table-bordered tbody tr:first-child td{
            border-top:1px solid #000;
        }
        .table-sm th,.table-sm td{
            padding:2px 5px;
        }
    
        table.table-bordered {
            border-collapse: separate;
            border :  0px solid #000000;
            border-spacing: 0;
            font-size: 14px !important;
            width: 100%;
            border-color:  #000000 ;
            border-right: 1px solid;
        }
        table.table-bordered tr {
            border-color:  #000000 ;
            border-style: none ;
            border-width: 0 ;
        }
        table.table-bordered td {
            border-color:  #000000 ;
            border-left: 1px solid;
            border-bottom:1px solid;
            padding: 10px;
        }

        .table-thin-bordered{
            font-size:11px;
            font-weight:100;
            width:100%;
            border-collapse: collapse;
            border: 1px solid #000;
            margin-top:5px;
        }
        .table-thin-bordered > thead > tr > th{
            vertical-align:middle;
            border-bottom:1px solid #000;
            border-right:1px solid #000;
            text-align:center;
            padding:5px 10px;;
        }
        .table-thin-bordered > tbody > tr > td{
            vertical-align:top;
            border-bottom:1px solid #000;
            border-right:1px solid #000;
            padding:2px 10px;
        }

        table.table-bordered th {
            border-color:  #000000 ;
            border-left: 1px solid;
            border-top:1px solid ;
            border-bottom:1px solid ;
            padding: 10px;
        }
        table{
            font-size:14px !important;
        }
        .table-child{
            border-collapse: separate;
            border-spacing: 0;
            font-size: 14px !important;
            width: 100%;
        }
        .table-child tr td{
            border:none;
            border-top:none;
            border-bottom:none !important;
        }
        .table-child tbody tr:first-child td {
            border-top: none;
        }
        .table-note{
            border-collapse: separate;
            border-spacing: 0;
            font-size: 14px !important;
            width: 100%;
        }
        .table-note td{
            
        }
        .table-note-detail{
            border-collapse: separate;
            border-spacing: 0;
            font-size: 14px !important;
            width: 100%;
        }
        .table-note-detail td{
            vertical-align: top;
        }
        .signature-name{
            border-bottom:1px dotted #000; 
            width:100%;
        }
        .table-title{
            border-collapse: separate;
            border-spacing: 0;
            font-size: 16px !important;
            width: 100%;
        }
    </style>

    <?= $this->Form->create($applicationLetter, ['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
        <p style="font-size: 14px;">Contoh checklist : <span class="dejavu-font" style="font-size:24px;color:blue;cursor:pointer;" id="span-check">&#9745;</span></p>
        <p>Click Symbol Untuk Copy Check</p>
            <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-2x nav-tabs-line-success" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#data_1" role="tab" aria-selected="true">1</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_2" role="tab" aria-selected="false">2</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_3" role="tab" aria-selected="false">3</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_4" role="tab" aria-selected="false">4</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_5" role="tab" aria-selected="false">5</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_6" role="tab" aria-selected="false">6</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_7" role="tab" aria-selected="false">7</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_8" role="tab" aria-selected="false">8</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_9" role="tab" aria-selected="false">9</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_10" role="tab" aria-selected="false">10</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_11" role="tab" aria-selected="false">11</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#data_12" role="tab" aria-selected="false">12</a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="data_1" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData1 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"><b>1.</b></td>
                                    <td colspan="5"><b>PEMERIKSAAN PERSYARATAN TATA BANGUNAN GEDUNG</b></td>
                                </tr>
                                <tr>
                                    <td class="text-center">a.</td>
                                    <td>Pemeriksaan Persyaratan Peruntukan Bangunan Rumah (Fungsi Bangunan Gedung)</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">b.</td>
                                    <td>Pemanfaatan Setiap Ruang Dalam Bangunan Gedung</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">c.</td>
                                    <td>Pemanfaatan Ruang Luar Pada Persil Bangunan Gedung</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">d.</td>
                                    <td>Pemeriksaan Persyaratan Intensitas Bangunan Gedung</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Luas Lantai Dasar Bangunan</div></td>
                                    <td class="text-center ">
                                       m<sup>2</sup>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td>
                                        Hasil Pengukuran Kondisi Faktual : m<sup>2</sup>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Luas Total Lantai Bangunan</div></td>
                                    <td class="text-center ">
                                        m<sup>2</sup>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">3) Jumlah Lantai Bangunan</div></td>
                                    <td class="text-center ">
                                        Lantai
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">4) Ketinggian Bangunan</div></td>
                                    <td class="text-center ">
                                        meter
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">5) Luas Daerah Hijau Dalam Persil</div></td>
                                    <td class="text-center ">
                                        m<sup>2</sup>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">6) Jarak Sempadan: Jalan/Sungai/Pantai/Danau/Rel/Kereta Api/Jalur Tegangan Tinggi</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Jalan</div></td>
                                    <td class="text-center ">
                                        m<sup>2</sup>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Sungai</div></td>
                                    <td class="text-center ">
                                        m<sup>2</sup>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pantai</div></td>
                                    <td class="text-center ">
                                        m<sup>2</sup>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Danau</div></td>
                                    <td class="text-center ">
                                        m<sup>2</sup>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Rel Kereta Api</div></td>
                                    <td class="text-center ">
                                        m<sup>2</sup>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Jalur Tegangan Tinggi</div></td>
                                    <td class="text-center ">
                                        m<sup>2</sup>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">7) Jarak Bangunan Gedung Dengan Batas Persil</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Jarak Bangunan dengan Batas Kiri</div></td>
                                    <td class="text-center ">m</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Jarak Bangunan dengan Batas Kanan</div></td>
                                    <td class="text-center ">m</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Jarak Bangunan dengan Batas Belakang</div></td>
                                    <td class="text-center ">m</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">8) Jarak Antar Bangunan</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">e.</td>
                                    <td>Pemeriksaan Penampilan Bangunan Gedung</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData1 = $applicationLetter->application_new_simak->data_1;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_1',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData1,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_2" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData2 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Bentuk Bangunan Gedung</div></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Bentuk Denah Bangunan Gedung</div></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">3) Tampak Bangunan</div></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">4) Bentuk dan Penutup Atap Bangunan Gedung</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">5) Profil, Detail, dan Material Bangunan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">6) Batas Fisik Atau Pagar Pekarangan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">7) Kulit Atau Selubung Bangunan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">f.</td>
                                    <td>Pemeriksaan Tata Ruang-Dalam Bangunan Gedung</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Kebutuhan Ruang Utama</div></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Bidang-Bidang Dinding</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">3) Dinding-Dinding Penyekat</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">4) Pintu/Jendela</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData2 = $applicationLetter->application_new_simak->data_2;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_2',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData2,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_3" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData3 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">5) Tinggi Ruang</div></td>
                                    <td class="text-center ">meter</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">6) Tinggi Lantai Dasar meter</div></td>
                                    <td class="text-center ">meter</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">7) Ruang Rongga Atap</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">8) Penutup Lantai</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">9) Penutup Langit-Langit</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">g.</td>
                                    <td>Pemeriksaan Keseimbangan, Keserasian dan Keselarasan Dengan Lingkungan</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Tinggi (Pell) Pekarangan</div></td>
                                    <td class="text-center ">meter</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Ruang Terbuka Hijau Pekarangan</div></td>
                                    <td class="text-center ">m<sup>2</sup></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">3) Pemanfaatan Ruang Sempadan</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">4) Daerah Hijau Bangunan</div></td>
                                    <td class="text-center ">m<sup>2</sup></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">5) Tata Tanaman</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">6) Tata Perkerasan Pekarangan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">7) Sirkulasi Manusia dan Kendaraan</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Sirkulasi Manusia</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData3 = $applicationLetter->application_new_simak->data_3;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_3',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData3,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_4" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData4 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Sirkulasi Kendaraan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>                            
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">8) Perabot Lansekap (Landscape Furniture)</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">9) Pencahayaan Ruang Luar Bangunan Gedung</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center"><b>2.</b></td>
                                    <td colspan="5"><b>PEMERIKSAAN PERSYARATAN KESELAMATAN</b></td>
                                </tr>
                                <tr>
                                    <td class="text-center">a.</td>
                                    <td>Pemeriksaan Sistem Struktur Bangunan Gedung</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Pondasi (Apabila Dapat Diamati)</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengamatan Visual terhadap Kerusakan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengukuran</div></td>
                                    <td class="text-center ">Dimensi:</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Kesesuaian dengan Rencana Teknis dan Gambar Terbangun</div></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Penggunaan Peralatan Non Destruktif</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengujian Kekuatan dan Material (Apabila Diperlukan)</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData4 = $applicationLetter->application_new_simak->data_4;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_4',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData4,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_5" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData5 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>                           
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Kolom</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengamatan Visual terhadap Kerusakan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengukuran</div></td>
                                    <td class="text-center ">
                                        Dimensi: 
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Kesesuaian dengan Rencana Teknis dan Gambar Terbangun</div></td>
                                    <td class="text-center ">
                                        -
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Penggunaan Peralatan Non Destruktif</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengujian Kekuatan dan Material (Apabila Diperlukan)</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">3) Balok Lantai</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengamatan Visual terhadap Kerusakan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengukuran</div></td>
                                    <td class="text-center ">
                                        Dimensi: 
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Kesesuaian dengan Rencana Teknis dan Gambar Terbangun</div></td>
                                    <td class="text-center ">
                                        -
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Penggunaan Peralatan Non Destruktif</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengujian Kekuatan dan Material (Apabila Diperlukan)</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">4) Rangka Atap</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengamatan Visual terhadap Kerusakan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData5 = $applicationLetter->application_new_simak->data_5;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_5',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData5,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_6" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData6 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengukuran</div></td>
                                    <td class="text-center ">
                                        Dimensi: 
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Kesesuaian dengan Rencana Teknis dan Gambar Terbangun</div></td>
                                    <td class="text-center ">
                                        -
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Penggunaan Peralatan Non Destruktif</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengujian Kekuatan dan Material (Apabila Diperlukan)</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center">b.</td>
                                    <td>Pemeriksaan Sistem Instalasi Listrik</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Sumber Listrik</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Panel Listrik</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">3) Instalasi Listrik</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">4) Sistem Pembumian Listrik</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData6 = $applicationLetter->application_new_simak->data_6;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_6',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData6,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_7" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData7 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center"><b>3.</b></td>
                                    <td colspan="5"><b>PEMERIKSAAN PERSYARATAN KESEHATAN</b></td>
                                </tr>
                                <tr>
                                    <td class="text-center">a.</td>
                                    <td>Pemeriksaan Sistem Penghawaan</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Ventilasi Alam</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Ventilasi Mekanik</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">b.</td>
                                    <td>Pemeriksaan Sistem Pencahayaan</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Sistem Pencahayaan Alami</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Sistem Pencahayaan Buatan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">c.</td>
                                    <td>Pemeriksaan Sistem Penyediaan Air Bersih/Minum</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Sumber Air Bersih/Minum</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData7 = $applicationLetter->application_new_simak->data_7;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_7',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData7,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_8" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData8 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Sistem Distribusi Air Bersih/Minum</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">3) Kualitas Air Bersih/Minum</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">d.</td>
                                    <td>Pemeriksaan Sistem Pengelolaan Air Kotor dan/atau Air Limbah (Black Water)</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Peralatan Saniter</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Instalasi Inlet/Outlet</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">3) Sistem Jaringan Pembuangan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">4) Sistem Penampungan Dan Pengolahan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData8 = $applicationLetter->application_new_simak->data_8;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_8',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData8,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_9" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData9 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">e.</td>
                                    <td>Pemeriksaan Sistem Pengelolaan Kotoran dan Sampah</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Penampungan Sementara Dalam Persil</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">f.</td>
                                    <td>Pemeriksaan Sistem Pengelolaan Air Hujan (Grey Water)</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Sistem Penangkap Air Hujan, Termasuk Talang</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Sistem Penyaluran Air Hujan, Termasuk Pipa Tegak Dan Drainase Dalam Persil</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">3) Sistem Penampungan, Pengolahan, Peresapan Dan/Atau Pembuangan Air Hujan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">g.</td>
                                    <td>Pemeriksaan Penggunaan Bahan Bangunan Gedung</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Bahan Bangunan yang Mengandung Bahan Berbahaya/Beracun</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Ada<br>
                                        <span class="dejavu-font">&#9744;</span>  Ada, yaitu <br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Pengukuran Menggunakan Peralatan
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Bahan Bangunan yang Menyebabkan Efek Silau Dan Pantulan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Ada<br>
                                        <span class="dejavu-font">&#9744;</span>  Ada, yaitu <br>
                                    </td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Pengukuran Menggunakan Peralatan
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center"><b>4.</b></td>
                                    <td colspan="5"><b>PEMERIKSAAN PERSYARATAN KENYAMANAN</b></td>
                                </tr>
                                <tr>
                                    <td class="text-center">a.</td>
                                    <td>Pemeriksaan Ruang Gerak Dalam Bangunan Gedung</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Jumlah Pengguna atau Batas Okupansi</div></td>
                                    <td class="text-center ">Orang</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData9 = $applicationLetter->application_new_simak->data_9;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_9',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData9,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_10" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData10 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">b.</td>
                                    <td>Pemeriksaan Kondisi Udara Dalam Ruang</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Tempratur Ruang</div></td>
                                    <td class="text-center ">°C</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Kelembaban Ruang</div></td>
                                    <td class="text-center ">%</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                </tr>
                                <tr>
                                    <td class="text-center">c.</td>
                                    <td>Pemeriksaan Pandangan Dari dan Ke Dalam Bangunan Gedung</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Pandangan dari Dalam Ruang ke Luar Bangunan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Mengganggu<br>
                                        <span class="dejavu-font">&#9744;</span>  Mengganggu, yaitu <br>
                                    </td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Pandangan dari Luar Bangunan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Mengganggu<br>
                                        <span class="dejavu-font">&#9744;</span>  Mengganggu, yaitu <br>
                                    </td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                </tr>
                                <tr>
                                    <td class="text-center"><b>5.</b></td>
                                    <td colspan="5"><b>PEMERIKSAAN PERSYARATAN KEMUDAHAN</b></td>
                                </tr>
                                <tr>
                                    <td class="text-center">a.</td>
                                    <td>Pemeriksaan Sarana Hubungan Horisontal Antarruang/Antarbangunan</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Kondisi Bukaan Pintu</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengukuran Dimensi dan Arah Bukaan</div></td>
                                    <td class="text-center ">
                                        Dimensi: <br>
                                        Arah Bukaan:
                                    </td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengamatan Visual terhadap Kerusakan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pemeriksaan Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</div></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center">b.</td>
                                    <td>Pemeriksaan Kelengkapan Prasarana dan Sarana Bangunan Gedung</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">1) Toilet</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengukuran Dimensi dan Arah Bukaan</div></td>
                                    <td class="text-center ">
                                        Dimensi: <br>
                                        Arah Bukaan:
                                    </td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData10 = $applicationLetter->application_new_simak->data_10;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_10',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData10,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_11" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData11 = '
                        <table class="table-bordered">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center" rowspan="2">No</th>
                                    <th class="text-center" rowspan="2">Komponen Pemeriksaan</th>
                                    <th class="text-center" width="15%" rowspan="2">Hasil Pengukuran Kondisi Faktual</th>
                                    <th class="text-center" colspan="2">Checklist Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</th>
                                    <th class="text-center" width="15%" rowspan="2">Keterangan</th>
                                </tr>
                                <tr>
                                    <th class="text-center" width="15%">Sesuai</th>
                                    <th class="text-center" width="15%">Tidak Sesuai, Yaitu ...</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengamatan Visual terhadap Kerusakan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pemeriksaan Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</div></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:10px;">2) Tempat Sampah</div></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengukuran Dimensi dan Arah Bukaan</div></td>
                                    <td class="text-center ">
                                        Dimensi: <br>
                                        Arah Bukaan:
                                    </td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pengamatan Visual terhadap Kerusakan</div></td>
                                    <td class="text-left options-text ">
                                        <span class="dejavu-font">&#9744;</span>  Tidak Rusak<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Ringan<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Sedang<br>
                                        <span class="dejavu-font">&#9744;</span>  Rusak Berat<br>
                                    </td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center ">-</td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center "></td>
                                    <td><div style="padding-left:20px;">- Pemeriksaan Kesesuaian Kondisi Faktual Dengan Rencana Teknis Dan Gambar Terbangun</div></td>
                                    <td class="text-center ">-</td>
                                    <td class="text-center "></td>
                                    <td class="text-center "></td>
                                    <td class="text-center ">
                                        Hasil Pengetesan dan Pengujian (Apabila Diperlukan):
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <table width="100%" class="table-note">
                            <tbody>
                                <tr>
                                    <td width="30px"></td>
                                    <td>HASIL PEMERIKSAAN LAIK FUNGSI BANGUNAN</td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>
                                        <table width="100%" class="table-note-detail">
                                            <tbody>
                                                <tr>
                                                    <td class="text-left" width="23%">1. Kondisi Keseluruhan</td>
                                                    <td class="text-center" width="2%">:</td>
                                                    <td class="text-left ">
                                                        <span class="dejavu-font">&#9744;</span> Kurang  <span class="dejavu-font">&#9744;</span> Sedang  <span class="dejavu-font">&#9744;</span> Baik  <span class="dejavu-font">&#9744;</span> Sangat Baik
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left" width="23%">2. Estimasi Sisi Material</td>
                                                    <td class="text-center" width="2%">:</td>
                                                    <td style="border-bottom:1px dotted #000;"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left" width="23%">3. Kesimpulan</td>
                                                    <td class="text-center" width="2%">:</td>
                                                    <td style="border-bottom:1px dotted #000;"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left" width="23%">4. Pemeriksa</td>
                                                    <td class="text-center" width="2%">:</td>
                                                    <td style="border-bottom:1px dotted #000;">'.$applicationLetter->supervisor->name.'</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left" width="23%">5. Tanggal</td>
                                                    <td class="text-center" width="2%">:</td>
                                                    <td style="border-bottom:1px dotted #000;"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <table width="100%" style="font-size:14px;line-height:1;border-collapse: collapse;">
                            <tbody>
                                <tr>
                                    <td width="60%" style="vertical-align:top;padding-left:15px;">
                                        
                                    </td>
                                    <td width="40%" style="text-align:right;vertical-align:top;">
                                        <div style="width:200px;display:inline-block;text-align:center;font-size:12px !important;line-height:12px;">
                                            <br>
                                            <br>
                                            Tanda Tangan
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <br>
                                            <div class="signature-name">'.$applicationLetter->supervisor->name.'</div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData11 = $applicationLetter->application_new_simak->data_11;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_11',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData11,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
                <div class="tab-pane" id="data_12" role="tabpanel">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '{{content}}{{error}}',
                            'inputContainer' => '{{content}}',
                            'formGroup' => '{{input}}',
                            'label' => '{{text}}',
                        ];
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                        if(!empty($applicationLetter->output_date)):
                            $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                        endif;
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php $valueData12 = '
                        <table width="100%" class="table-title">
                            <tbody>
                                <tr>
                                    <td class="text-center ">
                                        <b>LAMPIRAN PEMERIKSAAN KELAIKAN</b>
                                        <br>
                                        <b>FUNGSI BANGUNAN RUMAH</b>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <table width="100%" class="table-note">
                            <tbody>
                                <tr>
                                    <td>
                                        <table width="100%" class="table-note-detail">
                                            <tbody>
                                                <tr>
                                                    <td class="text-left" width="23%">Nama Perusahaan</td>
                                                    <td class="text-center" width="2%">:</td>
                                                    <td class="signature-name">'.$applicationLetter->developer->name.'</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-left" width="23%">Lokasi</td>
                                                    <td class="text-center" width="2%">:</td>
                                                    <td class="signature-name">'.$applicationLetter->address.'</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <table class="table-thin-bordered">
                            <tbody>
                                <tr>
                                    <td class="text-center " width="5%">No.</td>
                                    <td class="text-center " width="25%">No. Blok Sampling</td>
                                    <td class="text-center " width="35%">Nama Perumahan</td>
                                    <td class="text-center " width="25%">Keterangan</td>
                                </tr>
                                <tr>
                                    <td class="text-center " width="5%">1</td>
                                    <td class="text-center " width="25%">'.$applicationLetter->sample_house.'</td>
                                    <td class="text-center " width="35%">'.$applicationLetter->name_house.'</td>
                                    <td class="text-center " width="25%"></td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <table class="table-thin-bordered" style="table-layout: fixed;">
                            <tbody>
                                <tr>
                                    <td class="text-center " width="50%">&nbsp;</td>
                                    <td class="text-center " width="50%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-center " width="50%" style="height:120px;padding:0px 15px !important;"></td>
                                    <td class="text-center " width="50%" style="height:120px;padding:0px 15px !important;"></td>
                                </tr>
                                <tr>
                                    <td class="text-center " width="50%">&nbsp;</td>
                                    <td class="text-center " width="50%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-center " width="50%" style="height:120px;padding:0px 15px !important;"></td>
                                    <td class="text-center " width="50%" style="height:120px;padding:0px 15px !important;"></td>
                                </tr>
                                <tr>
                                    <td class="text-center " width="50%">&nbsp;</td>
                                    <td class="text-center " width="50%">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="text-center " width="50%" style="height:120px;padding:0px 15px !important;"></td>
                                    <td class="text-center " width="50%" style="height:120px;padding:0px 15px !important;"></td>
                                </tr>
                            </tbody>
                        </table>
                        <br>
                        <br>
                        <table style="width:100%">
                            <tbody>
                                <tr>
                                    <td width="40%">
                                        <table class="table-thin-bordered" style="table-layout: fixed; width:100%">
                                            <tbody>
                                                <tr>
                                                    <td class="text-center " width="50%">Mengetahui</td>
                                                    <td class="text-center " width="50%">Diperiksa</td>
                                                </tr>
                                                <tr>
                                                    <td class="text-center " width="50%" style="min-height:250px; box-sizing: border-box">
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                    </td>
                                                    <td class="text-center " width="50%" style="min-height:250px; box-sizing: border-box">
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                        <br>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="60%" style="text-align:right;">
                                        '.
                                        '<img src="'.$this->Utilities->generateQRCode($this->Url->build([
                                            'controller' => 'pages',
                                            'action' => 'qrCode',
                                            $applicationLetter->code
                                        ],true)).'"  width="100px"/>'
                                        .'
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        
                    ';?>
                    <?php if(!empty($applicationLetter->application_new_simak->id)):?>
                        <?php 
                            $valueData12 = $applicationLetter->application_new_simak->data_12;
                        ?>
                        <?=$this->Form->control('application_new_simak.id', ['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_new_simak.data_12',[
                        'label' => 'SIMAK BARU',
                        'type' => 'textarea',
                        'class'=>'summernote',
                        'value' => $valueData12,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        for(i = 1; i<=12 ; i++){
            var sim = `#application-new-simak-data-${i}`
            $(sim).summernote({
                height:600,
                callbacks: {
                    onImageUpload:function(files,editor,$editable){
                        // console.log(editor);
                        // console.log(files);
                        
                        data = new FormData();
                        data.append("file", files[0]);
                        $.ajax({
                            data: data,
                            type: "POST",
                            url: "<?=$this->Url->build(['controller'=>'Apis','action'=>'uploadImage']);?>",
                            cache: false,
                            contentType: false,
                            processData: false,
                            dataType : "json",
                            success: function(response) {
                                if(response.code == 200){
                                    var node = $("<img />").attr({
                                        'src' : response.url,
                                        'alt' : response.url
                                    }).width('100%').height("100px");
                                    $(sim).summernote('insertNode', node[0]);
                                }
                                
                            }
                        });
                    }
                },
                toolbar: [
                    ['font', ['bold', 'italic', 'underline', 'strikethrough', 'clear']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height','picture']],
                    ['misc', ['codeview']]
                ]
            })
        }

        const spanCopy = document.getElementById("span-check");

        spanCopy.onclick = function() {
            document.execCommand("copy");
        }

        spanCopy.addEventListener("copy", function(event) {
        event.preventDefault();
        if (event.clipboardData) {
            event.clipboardData.setData("text/html", '<span class="dejavu-font">&#9745;</span>');
            console.log(event.clipboardData.getData("text"))
        }
        });
    </script>

<?php $this->end();?>