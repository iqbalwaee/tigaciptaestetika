<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" class="m-portlet__nav-link btn btn-success m-btn m-btn--pill m-btn--air">
                        <i class="fa fa-arrow-left"></i> Back
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($applicationLetter,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <?php $a = 0;?>
                <div class="m-form__section">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '<div class="form-group m-form__group row">{{content}}{{error}}</div>',
                            'inputContainer' => '<div class="form-group m-form__group row">{{content}}</div>',
                            'formGroup' => '{{label}}<div class="col-lg-9  col-md-8">{{input}}</div>',
                            'label' => '<label class="col-lg-3 col-md-4 col-form-label" {{attrs}}>{{text}}</label>',
                        ];
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?=$this->Form->control('supervisor_id',['label' => 'Penanggung Jawab Pengawas','autocomplete' => 'off','options'=>$supervisors,'empty'=>'Pilih Pengawas']);?>
                    <?=$this->Form->control('output_date',['label' => 'Tanggal Output','datepicker'=>'true','type'=>'text','autocomplete' => 'off']);?>
                    <?php if(!empty($r->id)):?>
                        <?=$this->Form->control('application_output.id',['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_output.persyaratan_administratif',['label' => 'Persyaratan Administratif']);?>
                    <?=$this->Form->control('application_output.fungsi_bangunan',['label' => 'Fungsi bangunan gedung']);?>
                    <?=$this->Form->control('application_output.peruntukan',['label' => 'Peruntukan']);?>
                    <?=$this->Form->control('application_output.tata_bangunan',['label' => 'Tata bangunan']);?>
                    <?=$this->Form->control('application_output.kelaikan',['label' => 'Kelaikan fungsi bangunan gedung dinyatakan']);?>
                </div>
                <div class="m-form__seperator m-form__seperator--dashed"></div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        $(".checked-form").on("change",function(e){
            var parent = $(this).data("parent");
            if($(this).prop("checked")){
                $("[data-child="+parent+"]").prop("disabled",false);
            }else{
                $("[data-child="+parent+"]").prop("disabled",true);
            }
        })
    </script>

<?php $this->end();?>