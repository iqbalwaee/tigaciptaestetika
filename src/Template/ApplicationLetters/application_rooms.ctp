<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>		
                <?php if(!empty($applicationLetter->application_roomm)):?>
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'printApplicationRooms',$applicationLetter->id]);?>" target="_BLANK" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print Pengawasan" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-print"></i></a>	
                </li>
                <?php endif;?>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <style>
        .table-bordered th, .table-bordered td{
            border: 1px solid #000;
        }
    </style>
    <?= $this->Form->create($applicationLetter,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="m-form__section">
                <?php
                    $myTemplates = [
                        'inputContainerError' => '{{content}}{{error}}',
                        'inputContainer' => '{{content}}',
                        'formGroup' => '{{input}}',
                        'label' => '{{text}}',
                    ];
                    $defaultTanggal = $this->Utilities->indonesiaDateFormat(date('Y-m-d'));
                    if(!empty($applicationLetter->output_date)):
                        $defaultTanggal = $this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));
                    endif;
                    $this->Form->setTemplates($myTemplates);
                ?>
                <?php $valueData = '
                    <table width="100%" class=" table-sm table-content-data" style="
                    table-layout: fixed;width:100%;border:1px solid #000;border-collapse: collapse;">
                        <tbody>
                            <tr>
                            <th width="30%" style="border-bottom:1px solid #000 !important;">No Meteran Air</th>
                            <th width="2%" style="border-bottom:1px solid #000 !important;">:</th>
                            <th style="border-bottom:1px solid #000 !important;"></th>
                            </tr>
                            <tr>
                            <th style="border-bottom:1px solid #000 !important;">No Meteran Listrik</th>
                            <th style="border-bottom:1px solid #000 !important;">:</th>
                            <th style="border-bottom:1px solid #000 !important;"></th>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-bordered table-sm table-content-data" style="
                    table-layout: fixed;width:100%;">
                            <thead>
                                <tr>
                                    <th class="text-center align-middle" width="5%" height="150px;">NO.</th>
                                    <th class="text-center align-middle" width="30%">URAIAN NAMA RUANG</th>
                                    <th class="text-center rotate"> <div>RUANG TAMU</div></th>
                                    <th class="text-center rotate"> <div>R. KELUARGA/MAKAN</div></th>
                                    <th class="text-center rotate"> <div>KAMAR TIDUR 1</div></th>
                                    <th class="text-center rotate"> <div>KAMAR TIDUR 2</div></th>
                                    <th class="text-center rotate"> <div>KAMAR MANDI</div></th>
                                    <th class="text-center rotate"> <div>TERAS</div></th>
                                    <th class="text-center rotate"> <div>HALAMAN</div></th>
                                    <th class="text-center rotate"> <div>UTILITIES</div></th>
                                <th class="text-center  align-middle" width="15%">KETERANGAN</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center">
                                        <b>1</b>
                                    </td>
                                    <td colspan="9">
                                        <b>PINTU</b>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">*Daun Pintu + Kusen </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">*Handle </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Engsel </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                    </tr>
                                    <tr>
                                    <td class="text-center">
                                    <b>2</b>
                                    </td>
                                    <td colspan="9">
                                    <b>JENDELA</b>
                                    </td>
                                    <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Engsel </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                <td>&nbsp;</td>
                                <td style="padding-left:20px;">
                                *Handel </td>
                                <td class="text-center dejavu-font"></td>
                                <td class="text-center dejavu-font"></td>
                                <td class="text-center dejavu-font"></td>
                                <td class="text-center dejavu-font"></td>
                                <td class="text-center dejavu-font"></td>
                                <td class="text-center dejavu-font"></td>
                                <td class="text-center dejavu-font"></td>
                                <td class="text-center dejavu-font"></td>
                                <td class=""></td>
                            </tr>
                            <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Kait Angin </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                    <b>3</b>
                                    </td>
                                    <td colspan="9">
                                    <b>AKSESORIES PLUMBING</b>
                                    </td>
                                    <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Kran </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Klose Jongkok </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                    </tr>
                                    <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Floor Drain </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Septik Tank </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Sumber Air </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Saluran Air Bersih </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Saluran Air Kotor </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                    <b>4</b>
                                    </td>
                                    <td colspan="9">
                                    <b>AKSESORIES LISTRIK &amp; ELEKTRONIK</b>
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Fitting Lampu </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Saklar Lampu </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Stop Kontak </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *MCB + Fuse Box </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Meteran Listrik </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">*Meteran Air </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                    <b>5</b>
                                    </td>
                                    <td colspan="9">
                                    <b>VENTILASI TAMBAHAN</b>
                                    </td>
                                    <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                    <td>&nbsp;</td>
                                    <td style="padding-left:20px;">
                                    *Roster                                        </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                    </tr>
                                    <tr>
                                    <td class="text-center">
                                    <b>6</b>
                                    </td>
                                    <td>
                                    <b>DINDING</b>
                                    </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                </tr>
                                <tr>
                                    <td class="text-center">
                                    <b>7</b>
                                    </td>
                                    <td>
                                    <b>LANTAI</b>
                                    </td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class="text-center dejavu-font"></td>
                                    <td class=""></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                        <b>8</b>
                                        </td>
                                        <td>
                                        <b>PLAFOND</b>
                                        </td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class=""></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                        <b>9</b>
                                        </td>
                                        <td>
                                        <b>ATAP</b>
                                        </td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class=""></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center">
                                        <b>10</b>
                                        </td>
                                        <td>
                                        <b>JML KUNCI + ANAK KUNCI (isian angka)</b>
                                        </td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class="text-center dejavu-font"></td>
                                        <td class=""></td>
                                    </tr>
                                </tbody>
                        </table>
                        <table width="100%" style="font-size:11px;line-height:1;border-collapse: collapse;border:1px solid #000;">
                            <tbody>
                                <tr>
                                    <td>
                                        <table width="100%" style="font-size:11px;line-height:1;border-collapse: collapse;">
                                            <tbody>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><b>Catatan Tambahan<b></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right" width="150px" height="15px" style="vertical-align:top;border-bottom:1px solid #000;">*</td>
                                                    <td style="vertical-align:top;border-bottom:1px solid #000;"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right" width="150px" height="15px" style="vertical-align:top;border-bottom:1px solid #000;">*</td>
                                                    <td style="vertical-align:top;border-bottom:1px solid #000;"></td>
                                                </tr>
                                                <tr>
                                                    <td class="text-right" width="150px" height="15px" style="vertical-align:top;border-bottom:1px solid #000;">*</td>
                                                    <td style="vertical-align:top;border-bottom:1px solid #000;"></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" style="padding-left:15px;">
                                                        <br>
                                                        <div style="display:inline-block;">Jakarta,</div>
                                                        <div style="display:inline-block;"><span style="border-bottom:1px dotted #000;min-width:100px;">'.$defaultTanggal.'</span></div>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" style="font-size:11px;line-height:1;border-collapse: collapse;">
                                            <tbody>
                                                <tr>
                                                    <td width="60%" style="vertical-align:top;padding-left:15px;">
                                                        <div style="width:200px;display:inline-block;text-align:left;font-size:9px !important;line-height:12px;"">
                                                            Diperiksa Oleh,<br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <b><u>'.$consultant->name.'</u></b><br>
                                                            <b>'.$applicationLetter->supervisor->name.'</b>
                                                        </div>
                                                    </td>
                                                    <td width="30%" style="text-align:right;vertical-align:top;">
                                                        <div style="width:200px;display:inline-block;text-align:left;font-size:9px !important;line-height:12px;">
                                                            Disetujui,<br>
                                                            Penanggung Jawab
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <br>
                                                            <b><u>'.$applicationLetter->developer->name.'</u></b><br>
                                                            <b>'.$applicationLetter->developer->ceo_name.'</b>
                                                        </div>
                                                    </td>
                                                    <td>
                                                    <br>
                                                    <br>
                                                    <img src="'.$this->Utilities->generateQRCode($this->Url->build([
                                                        'controller' => 'pages',
                                                        'action' => 'qrCode',
                                                        $applicationLetter->code
                                                    ],true)).'" width="80px"/>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="3" class="text-right">*) Coret yang tidak perlu</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>';?><?php if(!empty($applicationLetter->application_roomm->id)):?>
                        <?php 
                            $valueData = $applicationLetter->application_roomm->data;;
                        ?>
                        <?=$this->Form->control('application_roomm.id',['type'=>'hidden']);?>
                    <?php endif;?>
                <?=$this->Form->control('application_roomm.data',['label' => 'PENGAWASAN UNIT RUMAH','type'=>'textarea','class'=>'summernote',
                        'value' => $valueData,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        $(".summernote").summernote({height:1050})
    </script>

<?php $this->end();?>