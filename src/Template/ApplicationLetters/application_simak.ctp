<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>		
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'printSimakPemeriksaan',$applicationLetter->id]);?>" target="_BLANK" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print Simak" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-print"></i></a>	
                </li>
                <?php if(!empty($applicationLetter->application_simaks_pemeriksaan)):?>
                    <li class="m-portlet__nav-item">
                        <a href="<?=$this->Url->build(['action'=>'clearSimakPemeriksaan',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Clear Simak" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-trash"></i></a>	
                    </li>
                <?php endif;?>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($applicationLetter,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <?php $a = 0;?>
                <div class="m-form__section">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '<div class="form-group m-form__group row">{{content}}{{error}}</div>',
                            'inputContainer' => '<div class="form-group m-form__group row">{{content}}</div>',
                            'formGroup' => '{{label}}<div class="col-lg-{{collg}}  col-md-8">{{input}}</div>',
                            'label' => '<label class="col-lg-2 col-md-4 col-form-label text-left" {{attrs}}>{{text}}</label>',
                            'nestingLabel' => '{{hidden}}<label class="m-radio" {{attrs}}>{{input}}{{text}}<span></span></label>',
                            'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
                            'radioWrapper' => '{{label}}'
                        ];
                        $this->Form->setTemplates($myTemplates);
                    $valueData = '
                        <table width="100%" style="border:1px solid #000;border-top:none;border-bottom:none;" cellpadding=0 cellspacing=0>
                        <thead>
                            <tr>
                                <th style="padding:2px 5px;text-align:center;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="5%">No</th>
                                <th style="padding:2px 5px;text-align:center;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="30%">URAIAN</th>
                                <th style="padding:2px 5px;text-align:center;border-right:1px solid #000;border-bottom:1px solid #000;border-top:1px solid #000;" width="50%">SPESIFIKASI</th>
                                <th style="padding:2px 5px;text-align:center;border-bottom:1px solid #000;border-top:1px solid #000;">KETERANGAN</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">1.</td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;" colspan="2"><b><u>PEKERJAAN PERSIAPAN</u></b></td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;" rowspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>1.1 Galian Tanah Pondasi :</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <td width="50%">- Lebar Atas (La)</td>
                                                <td>: &nbsp;<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>cm</td>
                                            </tr>
                                            <tr>
                                                <td width="50%">- Lebar Bawah (Lbi)</td>
                                                <td>: &nbsp;<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>cm</td>
                                            </tr>
                                            <tr>
                                                <td width="50%">- Kedalaman (Hd)</td>
                                                <td>: &nbsp;<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>cm</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">2.</td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;" colspan="2"><b><u>PEKERJAAN STRUKTUR</u></b></td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;" rowspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>2.1 Jenis Pondasi</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Kontruksi</td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">

                                <table width="100%">
                                    <tr>
                                        <td>: <div style="border-bottom:dotted 1px #000;width:85%;display:inline-block;">&nbsp;</div></td>
                                    </tr>
                                </table>  
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;padding-left:30px;">Dimensi : </td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;">&nbsp;
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;" rowspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <td width="50%">: Lebar Atas (La)</td>
                                                <td>: &nbsp;<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>cm</td>
                                            </tr>
                                            <tr>
                                                <td width="50%">: Lebar Bawah (Lbi)</td>
                                                <td>: &nbsp;<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>cm</td>
                                            </tr>
                                            <tr>
                                                <td width="50%">: Kedalaman (Hd)</td>
                                                <td>: &nbsp;<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>cm</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Campuran</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="40%">
                                        <tr>
                                            <td width="50%" style="vertical-align:bottom;">: <div style="border-bottom:dotted 1px #000;width:70%;max-width:100%;display:inline-block;text-align:center;">&nbsp;</div>Pc</td>
                                            <td width="50%" style="vertical-align:bottom;text-align:center;">: <div style="border-bottom:dotted 1px #000;width:70%;max-width:100%;display:inline-block;">&nbsp;</div>Psr</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Pas. Batu Kosong</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    : &nbsp;<div style="border-bottom:dotted 1px #000;width:90%;max-width:100%;display:inline-block;">&nbsp;</div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Pas. Urug Bawah Pondasi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    : &nbsp;<div style="border-bottom:dotted 1px #000;width:90%;max-width:100%;display:inline-block;">&nbsp;</div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>2.2 Pekerjaan Sloof Beton Bertulang</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;max-width:100%;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Konstruksi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">: Beton Bertulang
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Dimensi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="30%">
                                        <tr>
                                            <td width="3%">:</td>
                                            <td width="38%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="3%" style="text-align:center;">/</td>
                                            <td width="38%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>cm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Tulangan</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Besi Polos</td>
                                            <td width="8%">: </td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="5%" style="text-align:center;">Ø</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>mm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Sengkang/Beugel</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Besi Polos</td>
                                            <td width="8%">: Ø</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="5%" style="text-align:center;">-</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>cm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Campuran</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Perbandingan</td>
                                            <td width="8%">: </td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Pc :</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Psr :</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Krk</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>2.3 Pekerjaan Kolom Beton Bertulang</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;max-width:100%;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Konstruksi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">: Beton Bertulang
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Dimensi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="30%">
                                        <tr>
                                            <td width="3%">:</td>
                                            <td width="38%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="3%" style="text-align:center;">/</td>
                                            <td width="38%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>cm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Tulangan</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Besi Polos</td>
                                            <td width="8%">: </td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="5%" style="text-align:center;">Ø</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>mm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Sengkang/Beugel</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Besi Polos</td>
                                            <td width="8%">: Ø</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="5%" style="text-align:center;">-</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>cm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Campuran</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Perbandingan</td>
                                            <td width="8%">: </td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Pc :</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Psr :</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Krk</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>2.4 Pekerjaan Ringbalk Beton Bertulang</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;max-width:100%;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Konstruksi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">: Beton Bertulang
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Dimensi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="30%">
                                        <tr>
                                            <td width="3%">:</td>
                                            <td width="38%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="3%" style="text-align:center;">/</td>
                                            <td width="38%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>cm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Tulangan</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Besi Polos</td>
                                            <td width="8%">: </td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="5%" style="text-align:center;">Ø</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>mm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Sengkang/Beugel</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Besi Polos</td>
                                            <td width="8%">: Ø</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="5%" style="text-align:center;">-</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>cm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Campuran</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Perbandingan</td>
                                            <td width="8%">: </td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Pc :</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Psr :</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Krk</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>2.5 Pekerjaan Plat Lantai</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;max-width:100%;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Konstruksi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">: Beton Bertulang
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Tulangan Utama</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Besi Polos</td>
                                            <td width="8%">: Ø</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="5%" style="text-align:center;">-</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>mm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Tulangan Bagi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Besi Polos</td>
                                            <td width="8%">: Ø</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="5%" style="text-align:center;">-</td>
                                            <td width="10%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td>cm</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Campuran</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Perbandingan</td>
                                            <td width="8%">: </td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Pc :</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Psr :</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="12%">Krk</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>2.6 Pekerjaan Lantai</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Konstruksi : Beton Tumbuk</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Campuran</td>
                                            <td width="8%">: </td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="8%">Pc :</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="10%">Psr</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Keramik</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width="100%">
                                        <tr>
                                            <td width="30%">: Ukuran</td>
                                            <td width="8%">: </td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="8%" style="text-align:center;">X</td>
                                            <td width="8%"><div style="border-bottom:dotted 1px #000;width:100%;display:inline-block;text-align:center;">&nbsp;</div></td>
                                            <td width="10%">cm</td>
                                            <td >&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">3.</td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;" colspan="2"><b><u>PEKERJAAN FINISHING ARSITEKTUR</u></b></td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;" rowspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>3.1 Pekerjaan Dinding</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;padding-left:30px;">Kontruksi</td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;"> 
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;height:40px;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;" rowspan="3">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;padding-left:30px;">Tebal Pasangan Dinding</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-right:1px solid #000;">
                                    <div style="width:30%;display:inline-block;">: &nbsp;<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>/<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>Batu</div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:30%;display:inline-block;">: &nbsp;<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>/<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;text-align:center;">&nbsp;</div>Batu</div>
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Plesteran</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:40%;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:80%;display:inline-block;">&nbsp;</div>
                                    </div>
                                    <div style="width:50%;display:inline-block;">: &nbsp;<div style="border-bottom:dotted 1px #000;width:15%;display:inline-block;text-align:center;">&nbsp;</div>Pc:<div style="border-bottom:dotted 1px #000;width:15%;display:inline-block;text-align:center;">&nbsp;</div>Psr</div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Acian</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>3.2 Pasangan Pondasi (Menerus)</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Material</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">: &nbsp;
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Dimensi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">&nbsp;
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>3.3 Pekerjaan Daun Pintu</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Rangka Pintu</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:30%;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:80%;display:inline-block;">&nbsp;</div>
                                    </div>
                                    <div style="width:30%;display:inline-block;">: Uk <div style="border-bottom:dotted 1px #000;width:20%;display:inline-block;text-align:center;">&nbsp;</div>/<div style="border-bottom:dotted 1px #000;width:20%;display:inline-block;text-align:center;">&nbsp;</div>cm</div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Jenis Daun Pintu Utama</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Jenis Daun Pintu Kamar</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Kunci Tanam Biasa</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Engsel Biasa</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>3.4 Pekerjaan Daun Jendela</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Rangka Jendela</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:30%;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:80%;display:inline-block;">&nbsp;</div>
                                    </div>
                                    <div style="width:30%;display:inline-block;">: Uk <div style="border-bottom:dotted 1px #000;width:20%;display:inline-block;text-align:center;">&nbsp;</div>/<div style="border-bottom:dotted 1px #000;width:20%;display:inline-block;text-align:center;">&nbsp;</div>cm</div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Jenis Jendela</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Kunci Jendela</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Engsel Jendela</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Kait Angin Jendela</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>3.5 Pekerjaan Atap</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Penutup Atap</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Nak</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Listplank</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:40%;display:inline-block;">: <div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;">&nbsp;</div>=<div style="border-bottom:dotted 1px #000;width:30%;display:inline-block;">&nbsp;</div>cm</div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>3.6 Pekerjaan Plafond</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Rangka Plafond</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:30%;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:80%;display:inline-block;">&nbsp;</div>
                                    </div>
                                    <div style="width:30%;display:inline-block;">: Uk <div style="border-bottom:dotted 1px #000;width:20%;display:inline-block;text-align:center;">&nbsp;</div>/<div style="border-bottom:dotted 1px #000;width:20%;display:inline-block;text-align:center;">&nbsp;</div>cm</div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Plafond</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:30%;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:80%;display:inline-block;">&nbsp;</div>
                                    </div>
                                    <div style="width:40%;display:inline-block;">: &nbsp;<div style="border-bottom:dotted 1px #000;width:15%;display:inline-block;text-align:center;">&nbsp;</div>cm</div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Tinggi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:20%;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:80%;display:inline-block;">&nbsp;</div>
                                    </div>cm
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>3.7 Pengecetan</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Cat Plafond</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Cat Tembok Bagian Dalam</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Cat Bagian Luar</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Cat Listplank</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">4.</td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;" colspan="2"><b><u>MEKANIKAL DAN ELEKTRIKAL</u></b></td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;" rowspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>4.1 Instalasi Penerangan/Listrik</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Jaringan Listrik</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Daya</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;width:100%">
                                        : <div style="border-bottom:dotted 1px #000;width:10%;display:inline-block;">&nbsp;</div> Watt
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Lampu Penerangan</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;width:100%">
                                        : <div style="border-bottom:dotted 1px #000;width:10%;display:inline-block;">&nbsp;</div> Buah
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Saklar Tunggal</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;width:100%">
                                        : <div style="border-bottom:dotted 1px #000;width:10%;display:inline-block;">&nbsp;</div> Buah
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Saklar Ganda</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;width:100%">
                                        : <div style="border-bottom:dotted 1px #000;width:10%;display:inline-block;">&nbsp;</div> Buah
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Stop Kontak</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;width:100%">
                                        : <div style="border-bottom:dotted 1px #000;width:10%;display:inline-block;">&nbsp;</div> Buah
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">MCB</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;width:100%">
                                        : <div style="border-bottom:dotted 1px #000;width:10%;display:inline-block;">&nbsp;</div> Buah
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>4.2 Instalasi Air Bersih</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Jaringan Air Bersih</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Pipa PVC Dia 3/4"</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Pipa PVC Dia 1/2"</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Kran Air</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>4.3 Instalasi Air Kotor</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Pipa Pembuangan PVC Dia. 2"</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Pipa PVC Dia. 3"</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Pipa PVC Dia. 4"</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <table width=80% cellpadding=0 cellspacing=0>
                                        <tbody>
                                            <tr>
                                                <td style="vertical-align:top;" width="5px">:</td>
                                                <td  style="vertical-align:top;border-bottom:dotted 1px #000;"></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>4.4 Sanitary</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Septic Tank</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div> + <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Closet duduk</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div> + <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Kran Air 1/2"</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;">
                                        : &Oslash; <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Floor Drain</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;">
                                        : &Oslash; <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Bak Air</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">5.</td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;" colspan="2"><b><u>SRANA DAN PRASARANA</u></b></td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;" rowspan="2">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>5.1 Jalan Utama</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Konstruksi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:180px;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Lebar Jalan + Bahu</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:90px;display:inline-block;">&nbsp;</div> + <div style="border-bottom:dotted 1px #000;width:90px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>5.2 Jalan Kompleks</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Konstruksi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:180px;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Lebar</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;width:100%">
                                        : <div style="border-bottom:dotted 1px #000;width:10%;display:inline-block;">&nbsp;</div>m
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>5.3 Jalan Setapak</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Konstruksi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:180px;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Lebar</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="display:inline-block;width:100%">
                                        : <div style="border-bottom:dotted 1px #000;width:10%;display:inline-block;">&nbsp;</div>m
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;"><b>5.4 Drainase Lingkungan</b></td>
                                <td style="padding:2px 5px;text-align:left;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Konstruksi</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    <div style="width:180px;display:inline-block;">
                                        : <div style="border-bottom:dotted 1px #000;width:150px;display:inline-block;">&nbsp;</div>
                                    </div>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="padding:2px 5px;text-align:center;border-right:1px solid #000;vertical-align:top;border-bottom:1px solid #000;">&nbsp;</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;padding-left:30px;">Dimensi Saluran</td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;border-right:1px solid #000;">
                                    :<br>
                                    <table width="100%">
                                        <tbody>
                                            <tr>
                                                <td width="40%">- Lebar Atas (La)</td>
                                                <td>: &nbsp;<div style="border-bottom:dotted 1px #000;width:15%;display:inline-block;">&nbsp;</div>cm</td>
                                            </tr>
                                            <tr>
                                                <td width="40%">- Lebar Bawah (Lbi)</td>
                                                <td>: &nbsp;<div style="border-bottom:dotted 1px #000;width:15%;display:inline-block;">&nbsp;</div>cm</td>
                                            </tr>
                                            <tr>
                                                <td width="40%">- Kedalaman (Hd)</td>
                                                <td>: &nbsp;<div style="border-bottom:dotted 1px #000;width:15%;display:inline-block;">&nbsp;</div>cm</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td style="padding:2px 5px;text-align:left;vertical-align:top;border-bottom:1px solid #000;">&nbsp;
                                </td>
                            </tr>
                        </tbody>
                    </table>
                        ';
                        ?>
                    <?php if(!empty($applicationLetter->application_simaks_pemeriksaan->id)):?>
                        <?php 
                            $valueData = $applicationLetter->application_simaks_pemeriksaan->data;;
                        ?>
                        <?=$this->Form->control('application_simaks_pemeriksaan.id',['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_simaks_pemeriksaan.data',['label' => 'DAFTAR SIMAK PEMERIKSAAN','type'=>'textarea','class'=>'summernote',
                        'value' => $valueData,
                        'templateVars' => [
                            'collg' => 12
                        ]
                    ]);?>
                    
                </div>
                <div class="m-form__seperator m-form__seperator--dashed"></div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        $(".summernote").summernote({height:1050})
    </script>

<?php $this->end();?>