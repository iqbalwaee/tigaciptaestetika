<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>			

                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'printSimak',$applicationLetter->id]);?>" target="_BLANK" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print Simak" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-print"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($applicationLetter,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <?php $a = 0;?>
                <div class="m-form__section">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '<div class="form-group m-form__group row">{{content}}{{error}}</div>',
                            'inputContainer' => '<div class="form-group m-form__group row">{{content}}</div>',
                            'formGroup' => '{{label}}<div class="col-lg-{{collg}}  col-md-8">{{input}}</div>',
                            'label' => '<label class="col-lg-2 col-md-4 col-form-label text-left" {{attrs}}>{{text}}</label>',
                            'nestingLabel' => '{{hidden}}<label class="m-radio" {{attrs}}>{{input}}{{text}}<span></span></label>',
                            'radio' => '<input type="radio" name="{{name}}" value="{{value}}"{{attrs}}>',
                            'radioWrapper' => '{{label}}'
                        ];
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php if(!empty($applicationLetter->application_simak->id)):?>
                        <?=$this->Form->control('application_simak.id',['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_simak.element_struktural',['label' => 'Element Struktural','autocomplete' => 'off',
                        'templateVars' => [
                            'collg' => 8
                        ]
                    ]);?>
                    <?=$this->Form->control('application_simak.pondasi',['label' => 'Pondasi','autocomplete' => 'off',
                        'templateVars' => [
                            'collg' => 8
                        ]
                    ]);?>
                    <?=$this->Form->control('application_simak.lokasi',['label' => 'Lokasi','autocomplete' => 'off',
                        'templateVars' => [
                            'collg' => 8
                        ]
                    ]);?>
                    <?=$this->Form->control('application_simak.bagian',['label' => 'Bagian','autocomplete' => 'off',
                        'templateVars' => [
                            'collg' => 3
                        ]
                    ]);?>
                    <?=$this->Form->control('application_simak.tahun',['label' => 'Tahun','autocomplete' => 'off',
                        'templateVars' => [
                            'collg' => 3
                        ]
                    ]);?>
                    <?=$this->Form->control('application_simak.panjang',['label' => 'Panjang (m)','autocomplete' => 'off',
                        'templateVars' => [
                            'collg' => 3
                        ]
                    ]);?>
                    <?=$this->Form->control('application_simak.tinggi',['label' => 'Tinggi rata-rata','autocomplete' => 'off',
                        'templateVars' => [
                            'collg' => 3
                        ]
                    ]);?>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-md-4 col-form-label text-left" for="application-simak-bahan-bangunan">Bahan Bangunan</label>
                        <div class="col-lg-8  col-md-8">
                            <div class="m-radio-inline">
                                <?=$this->Form->radio('application_simak.bahan_bangunan',[
                                        1 => 'Blok Beton', 'Batu bata', 'Lain-lain'
                                ]);?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-md-4 col-form-label text-left" for="application-simak-tipe">Tipe</label>
                        <div class="col-lg-8  col-md-8">
                            <div class="m-radio-inline">
                                <?=$this->Form->radio('application_simak.tipe',[
                                        1 => 'Basement', 'Crawl Space', 'Slab'
                                ]);?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group m-form__group row">
                        <label class="col-lg-12 col-md-12 col-form-label text-left" for="application-simak-tingi">Kerusakan</label>
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th width="10%">Tidak ada</th>
                                            <th width="10%">Kecil</th>
                                            <th width="10%">Sedang</th>
                                            <th width="10%">Besar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Garis</td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.garis',[
                                                1 => 'Tidak ada'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.garis',[
                                                2 => 'Kecil'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.garis',[
                                                3 => 'Sedang'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.garis',[
                                                4 => 'Besar'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                        </tr>
                                        <tr>
                                            <td>Retak Struktur</td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.retak_struktur',[
                                                1 => 'Tidak ada'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.retak_struktur',[
                                                2 => 'Kecil'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.retak_struktur',[
                                                3 => 'Sedang'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.retak_struktur',[
                                                4 => 'Besar'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                        </tr>
                                        <tr>
                                            <td>Retak Permukaan</td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.retak_permukaan',[
                                                1 => 'Tidak ada'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.retak_permukaan',[
                                                2 => 'Kecil'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.retak_permukaan',[
                                                3 => 'Sedang'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.retak_permukaan',[
                                                4 => 'Besar'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                        </tr>
                                        <tr>
                                            <td>Heaving / Pengangkatan</td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.pengangkatan',[
                                                1 => 'Tidak ada'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.pengangkatan',[
                                                2 => 'Kecil'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.pengangkatan',[
                                                3 => 'Sedang'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.pengangkatan',[
                                                4 => 'Besar'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                        </tr>
                                        <tr>
                                            <td>Leaks / kebocoran</td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.kebocoran',[
                                                1 => 'Tidak ada'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.kebocoran',[
                                                2 => 'Kecil'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.kebocoran',[
                                                3 => 'Sedang'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.kebocoran',[
                                                4 => 'Besar'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                        </tr>
                                        <tr>
                                            <td>Settlement  / penurunan</td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.penurunan',[
                                                1 => 'Tidak ada'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.penurunan',[
                                                2 => 'Kecil'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.penurunan',[
                                                3 => 'Sedang'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.penurunan',[
                                                4 => 'Besar'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                        </tr>
                                        <tr>
                                            <td>Sill plate rot</td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.sill_plate_rot',[
                                                1 => 'Tidak ada'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.sill_plate_rot',[
                                                2 => 'Kecil'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.sill_plate_rot',[
                                                3 => 'Sedang'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                            <td class="text-center"><?=$this->Form->radio('application_simak.sill_plate_rot',[
                                                4 => 'Besar'
                                            ],[
                                                'hiddenField'=>false,
                                                'label' => false 
                                            ]);?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="form-group m-form__group row">
                        <label class="col-lg-2 col-md-4 col-form-label text-left" for="application-simak-kondisi">Kondisi</label>
                        <div class="col-lg-8  col-md-8">
                            <div class="m-radio-inline">
                                <?=$this->Form->radio('application_simak.kondisi',[
                                        1 => 'Kurang', 'Sedang', 'Baik', 'Sangat Baik'
                                ]);?>
                            </div>
                        </div>
                    </div>
                    <?=$this->Form->control('application_simak.estimasi',['label' => 'Estimasi sisi masa manfaat ( tahun ) ','autocomplete' => 'off',
                        'templateVars' => [
                            'collg' => 2
                        ]
                    ]);?>
                    <?=$this->Form->control('application_simak.kesimpulan',['label' => 'Kesimpulan ','type'=>'textarea','class'=>'summernote',
                        'templateVars' => [
                            'collg' => 10
                        ]
                    ]);?>
                    <?=$this->Form->control('application_simak.pemeriksa',['label' => 'Pemeriksa','autocomplete' => 'off',
                        'templateVars' => [
                            'collg' => 8
                        ]
                    ]);?>
                    
                </div>
                <div class="m-form__seperator m-form__seperator--dashed"></div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        $(".summernote").summernote({height:150})
    </script>

<?php $this->end();?>