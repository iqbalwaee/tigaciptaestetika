<?php
    $rightButton = "";
    if($userData['group_id'] == 2):
    $rightButton = '<a href="'.$this->Url->build(['action'=>'add']).'" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                            <i class="la la-plus"></i>
                            <span>
                                Tambah Pengajuan Baru
                            </span>
                        </span>
                    </a>';
    endif;
?>
<?=$this->element('widget/index',['rightButton' => $rightButton]);?>
<?php $this->start('script');?>
    <script>
        <?php
            $deleteUrl    = $this->Url->build(['action'=>'delete'])."/";
            $editUrl      = $this->Url->build(['action'=>'edit'])."/";
            $viewUrl = $this->Url->build(['action'=>'view'])."/";
        ?>
        jQuery(document).ready(function() {
            var deleteUrl = "<?=$deleteUrl;?>";
            var editUrl = "<?=$editUrl;?>";
            var viewUrl = "<?=$viewUrl;?>";
            var group_id = "<?=$userData->group_id;?>";
            var columnData = [{
                field: "ApplicationLetters.code",
                title: "Kode Pengajuan",
                sortable: false,
                width: 200,
                template: function(t) {
                    return t.code
                }
            },  {
                field: "ApplicationLetters.name_house",
                title: "Nama Perumahan",
                sort : 'asc',
                template: function(t) {
                    return t.name_house
                }
            }, 
            <?php if($userData['group_id'] != '2'):?>
            {
                field: "Developers.name",
                title: "Nama Perusahaan",
                sort : 'asc',
                width:150,
                template: function(t) {
                    return t.developer.name
                }
            },<?php endif;?>   {
                field: "ApplicationLetters.status",
                title: "Status",
                sort : 'asc',
                template: function(t) {
                    if(group_id == 2){
                        return Utils.statusDeveloper(t.status);
                    }else if(group_id == 4){
                        return Utils.statusDPD(t.status);
                    }else if(group_id == 3){
                        return Utils.statusDPP(t.status);
                    }else if(group_id == 5){
                        return Utils.statusKonsultan(t.status);
                    }
                    
                }
            },  {
                field: "ApplicationLetters.created",
                title: "Tanggal Pengajuan",
                sort : 'asc',
                template: function(t) {
                    return Utils.dateIndonesia(t.created,true,true)
                }
            },
            {
                field: "actions",
                width: 150,
                title: "Actions",
                sortable: false,
                overflow: "visible",
                template: function(t) {
                    var btn =  '<div class="btn-group btn-group-sm" role="group" aria-label="Small button group">'
                    var btnList = '';
                    if(viewUrl != ""){
                        btnList += '<a class="btn btn-sm btn-brand" href="'+viewUrl+t.id+'"><i class="flaticon-search-1"></i> View</a>';
                    }
                    if(editUrl != "" && ((t.status == 0 && group_id == 2) || (t.status == 0 && group_id == 4) || ( group_id == 3)|| ( group_id == 5))){
                        btnList += '<a class=" btn btn-sm btn-success" href="'+editUrl+t.id+'"><i class="flaticon-edit"></i></a>';
                    }
                    if(deleteUrl != "" && ((t.status == 0 && group_id == 2) || (t.status == 0 && group_id == 4) || group_id == 5)){
                        btnList += '<a class="btn-delete-on-table btn btn-sm btn-danger" href="'+deleteUrl+t.id+'"><i class="flaticon-cancel"></i></a>';
                    }
                    
                    if(btnList == ""){
                        btn = "";
                    }else{
                        btn += btnList;
                        btn += '</div>';
                    }
                    return btn;
                }
            }];
            DatatableRemoteAjaxDemo.init("",columnData,"<?=$this->request->getParam('_csrfToken');?>")
        });
    </script>
<?php $this->end();?>