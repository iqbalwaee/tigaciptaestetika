<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>	
                <?php if(!empty($applicationLetter->application_picture)):?>				
                    <li class="m-portlet__nav-item">
                        <a href="<?=$this->Url->build(['action'=>'printPictures',$applicationLetter->id]);?>" target="_BLANK" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-print"></i></a>	
                    </li>		
                <?php endif;?>	
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($applicationLetter,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <?php $a = 0;?>
                <div class="m-form__section">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '<div class="form-group m-form__group row has-danger">{{content}}</div>',
                            'inputContainer' => '<div class="form-group m-form__group row">{{content}}</div>',
                            'formGroup' => '{{label}}<div class="col-lg-9  col-md-8">{{input}}{{error}}</div>',
                            'label' => '<label class="col-lg-3 col-md-4 col-form-label" {{attrs}}>{{text}}</label>',
                        ];
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php if(!empty($r->id)):?>
                        <?=$this->Form->control('application_picture.id',['type'=>'hidden']);?>
                    <?php endif;?>
                    <div class="alert alert-info">
                        <p>Resolusi image minimal 400x300px</p>
                    </div>
                    <?php if(!empty($applicationLetter->application_picture->tampak_depan)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->tampak_depan_dir,$applicationLetter->application_picture->tampak_depan,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'tampak_depan']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.tampak_depan',['label' => 'Tampak Depan','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->jalan_depan)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jalan_depan_dir,$applicationLetter->application_picture->jalan_depan,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'jalan_depan']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.jalan_depan',['label' => 'Jalan Depan Lokasi','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->jalan_utama)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jalan_utama_dir,$applicationLetter->application_picture->jalan_utama,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'jalan_utama']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.jalan_utama',['label' => 'Jalan Utama','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->jalan_komplek)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jalan_komplek_dir,$applicationLetter->application_picture->jalan_komplek,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'jalan_komplek']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.jalan_komplek',['label' => 'Jalan Komplek','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->fasilitas_umum)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->fasilitas_umum_dir,$applicationLetter->application_picture->fasilitas_umum,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'fasilitas_umum']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.fasilitas_umum',['label' => 'Fasilitas Umum','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->ruang_tamu)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->ruang_tamu_dir,$applicationLetter->application_picture->ruang_tamu,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'ruang_tamu']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.ruang_tamu',['label' => 'Ruang Tamu & Ruang Tengah','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->kamar_tidur)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->kamar_tidur_dir,$applicationLetter->application_picture->kamar_tidur,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'kamar_tidur']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.kamar_tidur',['label' => 'Kamar Tidur','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->plafond)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->plafond_dir,$applicationLetter->application_picture->plafond,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'plafond']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.plafond',['label' => 'Plafond','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->pintu_kusen_jendela)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->pintu_kusen_jendela_dir,$applicationLetter->application_picture->pintu_kusen_jendela,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'pintu_kusen_jendela']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.pintu_kusen_jendela',['label' => 'Pintu, kusen & Jendela','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->toilet)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->toilet_dir,$applicationLetter->application_picture->toilet,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'toilet']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.toilet',['label' => 'Toilet','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->sumur_septictank)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->sumur_septictank_dir,$applicationLetter->application_picture->sumur_septictank,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'sumur_septictank']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.sumur_septictank',['label' => 'Sumur & Septictank','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->kwh_listrik)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->kwh_listrik_dir,$applicationLetter->application_picture->kwh_listrik,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'kwh_listrik']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.kwh_listrik',['label' => 'Kwh Listrik','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->jaringan_air)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jaringan_air_dir,$applicationLetter->application_picture->jaringan_air,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'jaringan_air']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.jaringan_air',['label' => 'Jaringan Air Bersih','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->saluran_pembuangan)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->saluran_pembuangan_dir,$applicationLetter->application_picture->saluran_pembuangan,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'saluran_pembuangan']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.saluran_pembuangan',['label' => 'Saluran Pembuangan','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->tampak_atas_1)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->tampak_atas_1_dir,$applicationLetter->application_picture->tampak_atas_1,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'tampak_atas_1']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.tampak_atas_1',['label' => 'Tampak atas 1','type'=>'file']);?>
                    <?php if(!empty($applicationLetter->application_picture->tampak_atas_2)):?>
                        <div class="form-group m-form__group row" style="padding-top:0;padding-bottom:0;">
                            <div class="offset-lg-3 offset-md-4 col-lg-9  col-md-8">
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->tampak_atas_2_dir,$applicationLetter->application_picture->tampak_atas_2,'thumb-');?>" style="max-width:100%;width:200px;">
                                <br>
                                <a href="<?=$this->Url->build(['action'=>'hapusGambar',$applicationLetter->application_picture->id,'?'=>['type'=>'tampak_atas_2']]);?>">
                                    Hapus
                                </a>
                            </div>
                        </div>
                    <?php endif;?>
                    <?=$this->Form->control('application_picture.tampak_atas_2',['label' => 'Tampak atas 2','type'=>'file']);?>
                </div>
                <div class="m-form__seperator m-form__seperator--dashed"></div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>
