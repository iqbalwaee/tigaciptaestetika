<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>	
                <?php if(!empty($applicationLetter->application_invoice)):?>				
                    <li class="m-portlet__nav-item">
                        <a href="<?=$this->Url->build(['action'=>'printInvoice',$applicationLetter->id]);?>" target="_BLANK" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-print"></i></a>	
                    </li>		
                <?php endif;?>	
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($applicationLetter,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <?php $a = 0;?>
                <div class="m-form__section">
                    <?php
                        $myTemplates = [
                            'inputContainerError' => '<div class="form-group m-form__group row has-danger">{{content}}</div>',
                            'inputContainer' => '<div class="form-group m-form__group row">{{content}}</div>',
                            'formGroup' => '{{label}}<div class="col-lg-9  col-md-8">{{input}}{{error}}</div>',
                            'label' => '<label class="col-lg-3 col-md-4 col-form-label" {{attrs}}>{{text}}</label>',
                        ];
                        $this->Form->setTemplates($myTemplates);
                    ?>
                    <?php if(!empty($r->id)):?>
                        <?=$this->Form->control('application_invoice.id',['type'=>'hidden']);?>
                    <?php endif;?>
                    <?=$this->Form->control('application_invoice.date',['label' => 'Tanggal Invoice','type'=>'text','datepicker'=>'true']);?>
                    <?=$this->Form->control('application_invoice.company_name',['label' => 'Nama Perusahaan','type'=>'text','value'=>$applicationLetter->developer->name,'disabled']);?>
                    <?=$this->Form->control('application_invoice.no_pengajuan',['label' => 'No. Pengajuan','type'=>'text','value'=>$applicationLetter->code,'disabled']);?>
                    <?=$this->Form->control('application_invoice.house_name',['label' => 'Nama Perumahaan','type'=>'text','value'=>$applicationLetter->name_house,'disabled']);?>
                    <div class="table-responsive">
                        <table class="table table-bordered table-condensed table-invoice">
                            <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Deskripsi</th>
                                    <th width="15%">Harga</th>
                                    <th width="10%">Qty</th>
                                    <th width="10%">Satuan</th>
                                    <th width="15%">Total</th>
                                </tr>
                            </thead>
                            <?php
                                $myTemplates = [
                                    'inputContainerError' => '{{content}}',
                                    'inputContainer' => '{{content}}',
                                    'formGroup' => '{{input}}{{error}}',
                                    'label' => '{{text}}',
                                ];
                                $this->Form->setTemplates($myTemplates);
                                $qty = count(explode(",",$applicationLetter->building));
                                $rate = 0;
                                if(!empty($applicationLetter->application_invoice)){
                                    $qty    = $applicationLetter->application_invoice->qty;
                                    $rate   = $applicationLetter->application_invoice->rate;
                                }
                                $total = $qty * $rate;
                            ?>
                            <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>Jasa konsultasi pengawasan rumah</td>
                                    <td><?=$this->Form->control('application_invoice.rate',['label' => 'Jumlah Unit','type'=>'text','textnumber'=>'true','data-calculate'=>'true','data-rate'=>'true','value'=>$rate]);?></td>
                                    <td><?=$this->Form->control('application_invoice.qty',['label' => 'Jumlah Unit','type'=>'text','textnumber'=>'true','value'=>$qty,'data-calculate'=>'true','data-qty'=>'true']);?></td>
                                    <td><?=$this->Form->control('application_invoice.unit',['label' => 'Unit','type'=>'text','value'=>'UNIT']);?></td>
                                    <td><?=$this->Form->control('application_invoice.total',['label' => 'total','type'=>'text','data-totaltr'=>'true','readonly','textnumber'=>'true','value'=>$total]);?></td>
                                </tr>
                                <tr>
                                    <td>2</td>
                                    <td>
                                        <?=$this->Form->hidden('application_invoice.application_invoices_details.0.id',['label' => 'Keterangan']);?>
                                        <?=$this->Form->control('application_invoice.application_invoices_details.0.text',['label' => 'Keterangan','type'=>'text','required'=>false,'data-text'=>'true']);?>
                                    </td>
                                    <td><?=$this->Form->control('application_invoice.application_invoices_details.0.rate',['label' => 'Jumlah Unit','type'=>'text','textnumber'=>'true','data-calculate'=>'true','data-rate'=>'true','required'=>false]);?></td>
                                    <td><?=$this->Form->control('application_invoice.application_invoices_details.0.qty',['label' => 'Jumlah Unit','type'=>'text','textnumber'=>'true','data-calculate'=>'true','data-qty'=>'true','required'=>false]);?></td>
                                    <td><?=$this->Form->control('application_invoice.application_invoices_details.0.unit',['label' => 'Unit','type'=>'text','required'=>false]);?></td>
                                    <td><?=$this->Form->control('application_invoice.application_invoices_details.0.total',['label' => 'total','type'=>'text','data-totaltr'=>'true','readonly','textnumber'=>'true','required'=>false]);?></td>
                                </tr>
                                <tr>
                                    <td>3</td>
                                    <td>
                                        <?=$this->Form->hidden('application_invoice.application_invoices_details.1.id',['label' => 'Keterangan']);?>
                                        <?=$this->Form->control('application_invoice.application_invoices_details.1.text',['label' => 'Keterangan','type'=>'text','required'=>false,'data-text'=>'true']);?>
                                    </td>
                                    <td><?=$this->Form->control('application_invoice.application_invoices_details.1.rate',['label' => 'Jumlah Unit','type'=>'text','textnumber'=>'true','data-calculate'=>'true','data-rate'=>'true','required'=>false]);?></td>
                                    <td><?=$this->Form->control('application_invoice.application_invoices_details.1.qty',['label' => 'Jumlah Unit','type'=>'text','textnumber'=>'true','data-calculate'=>'true','data-qty'=>'true','required'=>false]);?></td>
                                    <td><?=$this->Form->control('application_invoice.application_invoices_details.1.unit',['label' => 'Unit','type'=>'text','required'=>false]);?></td>
                                    <td><?=$this->Form->control('application_invoice.application_invoices_details.1.total',['label' => 'total','type'=>'text','data-totaltr'=>'true','readonly','textnumber'=>'true','required'=>false]);?></td>
                                </tr>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="5" class="text-right">TOTAL</th>
                                    <th class="text-right grand-total">0</th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                    
                </div>
                <div class="m-form__seperator m-form__seperator--dashed"></div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>
<?php $this->start('script');?>
    <script>
        $("input[data-calculate='true']").on("keyup",function(e){
            var tr = $(this).closest('tr');
            getTrTotal(tr);
        })
        function getTrTotal(tr){
            var qty = tr.find("input[data-qty='true']").val();
            if(qty == ""){
                qty = 0;
            }
            var rate = tr.find("input[data-rate='true']").val();
            if(rate == ""){
                rate = 0;
            }
            var total = qty * rate;
            tr.find("input[data-totaltr='true']").val(total);
            getGrandTotal();
        }
        function getGrandTotal(){
            var total = 0;
            $.each($("input[data-totaltr='true']"),function(){
                total = total*1 + $(this).val()*1;
            })
            $(".grand-total").html($.number(total,2))
        }

        $.each($(".table-invoice tbody tr"),function(){
            getTrTotal($(this));
        })
        $("input[data-text='true']").on("change",function(e){
            var tr = $(this).closest("tr");
            if($(this).val() == ""){
                tr.find("input").val("");
                getTrTotal(tr);
            }
        })

    </script>
<?php $this->end();?>