<html>
<head>
    <style>
            html,body{
                font-family: Arial,Verdana,Helvetica,"DejaVu Sans",sans-serif;
                font-size:12px;
            }
            @page { margin: 30px 20px; font-size:12px; line-height:16px; }
            .main-top{
                display:block;
                margin-bottom:5px;
            }
            .title-header-wrapper{
                text-align:center;
                padding-top:15px;
                padding-bottom:5px;
            }
            .title-header-wrapper h3{
                margin:0px;
                padding:0px;
                margin-bottom:0px;
                font-size:14px !important;
            }
            .title-header-wrapper p{
                margin:0px;
                padding:0px;
                font-size:14px;
            }
            .text-center{
                text-align:center !important;
            }
            .text-center > div{
                margin:auto;
            }
            main{
                padding:0 15px;
                font-size:14px;
            }
            td{
                vertical-align:middle;
            }
            .bg-fixed{
                position:fixed;
                top:40%;
                left:0;
                right:0;
                transform:translateY(-50%);
                z-index:2;
                text-align:center;
            }
            .bg-fixed{
                opacity:0.1;
            }
            .block{
                display:block;
                margin:auto;
            }
            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            .table-bordered tbody tr:first-child td{
                border-top:1px solid #000;
            }
            .table-sm th,.table-sm td{
                padding:2px 5px;
            }
        
            table.table-bordered {
                border-collapse: separate;
                border :  0px solid #000000;
                border-spacing: 0;
                font-size: 11px !important;
                width: 100%;
                border-color:  #000000 ;
                border-right: 1px solid;
            }
            table.table-bordered tr {
                border-color:  #000000 ;
                border-style: none ;
                border-width: 0 ;
            }
            table.table-bordered td {
                border-color:  #000000 ;
                border-left: 1px solid;
                border-bottom:1px solid ;
            }

            table.table-bordered th {
                border-color:  #000000 ;
                border-left: 1px solid;
                border-top:1px solid ;
                border-bottom:1px solid ;
            }
            table{
                font-size:11px !important;
            }
            .table-child{
                border-collapse: separate;
                border-spacing: 0;
                font-size: 11px !important;
                width: 100%;
            }
            .table-child tr td{
                border:none;
                border-top:none;
                border-bottom:none !important;
                padding:0px;
            }
            .table-child tbody tr:first-child td {
                border-top: none;
            }
            .table-note{
                border-collapse: separate;
                border-spacing: 0;
                font-size: 11px !important;
                width: 100%;
            }
            .table-note td{
                
            }
            .table-note-detail{
                border-collapse: separate;
                border-spacing: 0;
                font-size: 11px !important;
                width: 100%;
            }
            .table-note-detail td{
                vertical-align: top;
                padding:0px;
            }
            .dejavu-font{
                font-family: DejaVu Sans, sans-serif !important;
            }
        </style>
        
</head>
<body>
    <div class="bg-fixed">
        <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="400px">
    </div>
    <header>
        <div class="title-header-wrapper" >
            <h3>LEMBAR PENILAIAN KALAYAKAN UNIT RUMAH DAN<br>KETERSEDIAAN SARANA PRASARANA</h3>
        </div>
    </header>
    <main>
    <?=$applicationLetter->application_judgment->data;?>
  </main>
</body>
</html>