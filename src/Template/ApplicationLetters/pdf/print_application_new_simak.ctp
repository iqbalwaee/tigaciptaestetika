<html>
    <head>
        <style>
            html,body{
                font-family: "Times New Roman", Times,"DejaVu Sans",sans-serif;
                font-size:12px;
                margin: 30px 30px; 
            }
            @page { font-size:12px; line-height:18px; }
            .main-top{
                display:block;
                margin-bottom:5px;
            }
            .title-header-wrapper{
                text-align:center;
                padding-top:15px;
                padding-bottom:5px;
                margin-bottom: 20px;
                line-height: 20px;
            }
            .title-header-wrapper h3{
                margin:0px;
                padding:0px;
                margin-bottom:0px;
                font-size:14px !important;
            }
            .title-header-wrapper p{
                margin:0px;
                padding:0px;
                font-size:14px;
            }
            .text-center{
                text-align:center !important;
            }
            .text-center > div{
                margin:auto;
            }
            main{
                padding: 0px;
                font-size:14px;
                position:relative;
            }
            td{
                vertical-align:middle;
            }
            .bg-fixed{
                position:fixed;
                top:40%;
                left:0;
                right:0;
                transform:translateY(-50%);
                z-index:2;
                text-align:center;
            }
            .bg-fixed{
                opacity:0.1;
            }
            .block{
                display:block;
                margin:auto;
            }
            .text-center{
                text-align:center;
            }
            .text-right{
                text-align:right;
            }
            .table-bordered tbody tr:first-child td{
                border-top:1px solid #000;
            }
            .table-sm th,.table-sm td{
                padding:2px 5px;
            }
        
            table.table-bordered {
                border-collapse: separate;
                border :  0px solid #000000;
                border-spacing: 0;
                font-size: 12px !important;
                width: 100%;
                border-color:  #000000 ;
                border-right: 1px solid;
            }
            table.table-bordered tr {
                border-color:  #000000 ;
                border-style: none ;
                border-width: 0 ;
            }
            table.table-bordered td {
                border-color:  #000000 ;
                border-left: 1px solid;
                border-bottom:1px solid;
                padding: 4px;
            }

            table.table-bordered th {
                border-color:  #000000 ;
                border-left: 1px solid;
                border-top:1px solid ;
                border-bottom:1px solid ;
                padding: 4px;
            }
            .table-thin-bordered{
                font-size:12px;
                font-weight:100;
                width:100%;
                border-collapse: collapse;
                border: 1px solid #000;
                margin-top:5px;
            }
            .table-thin-bordered > thead > tr > th{
                vertical-align:middle;
                border-bottom:1px solid #000;
                border-right:1px solid #000;
                text-align:center;
                padding:5px 10px;;
            }
            .table-thin-bordered > tbody > tr > td{
                vertical-align:top;
                border-bottom:1px solid #000;
                border-right:1px solid #000;
                padding:2px 10px;
            }
            
            table{
                font-size:12px !important;
            }
            .table-child{
                border-collapse: separate;
                border-spacing: 0;
                font-size: 12px !important;
                width: 100%;
            }
            .table-child tr td{
                border:none;
                border-top:none;
                border-bottom:none !important;
                padding:0px;
            }
            .table-child tbody tr:first-child td {
                border-top: none;
            }
            .table-note{
                border-collapse: separate;
                border-spacing: 0;
                font-size: 12px !important;
                width: 100%;
            }
            .table-note td{
                
            }
            .table-note-detail{
                border-collapse: separate;
                border-spacing: 0;
                font-size: 12px !important;
                width: 100%;
            }
            .table-note-detail td{
                vertical-align: top;
                padding:0px;
                line-height: 20px;
            }
            .dejavu-font{
                font-family: DejaVu Sans, sans-serif !important;
            }
            .page-break{
                page-break-before: auto;
                page-break-after: always;
                position:relative;
            }
            .page-break:last-child {
                 page-break-after: auto;
            }
            .table-title{
                border-collapse: separate;
                border-spacing: 0;
                font-size: 14px !important;
                width: 100%;
            }
            .page-num{
                position:absolute;
                left:0;
                right:0;
                bottom:0px;
                top:100%;
                text-align:right;
                width:100%;
            }
        </style>    
    </head>
    <body>
        <div class="bg-fixed">
            <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="400px">
        </div>
        <main>
            <?php $hal = 1;?>
            <header>
                <div class="title-header-wrapper">
                    <h3>LAMPIRAN LAPORAN PEMERIKSAAN</h3>
                    <h3>KELAIKAN FUNGSI BANGUNAN RUMAH</h3>
                </div>
            </header>
            <?=$applicationLetter->application_new_simak->data_1;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_2;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_3;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_4;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_5;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_6;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_7;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_8;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_9;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_10;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break" style=""></div>
            <?=$applicationLetter->application_new_simak->data_11;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
            <div class="page-break"></div>
            <?=$applicationLetter->application_new_simak->data_12;?>
            <div class="page-num">Hal : <?=$hal++;?></div>
        </main>
    </body>
</html>