<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style>
    html,body{
        font-family: Arial,Verdana,Helvetica,"DejaVu Sans",sans-serif;
        font-size:12px;
    }
    @page { margin: 10px 20px; font-size:12px; line-height:16px; }
    .main-top{
        display:block;
        margin-bottom:5px;
    }
    .title-header-wrapper{
        text-align:center;
        padding-top:15px;
        padding-bottom:5px;
    }
    .title-header-wrapper h3{
        margin:0px;
        padding:0px;
        margin-bottom:0px;
        font-size:14px !important;
    }
    .title-header-wrapper p{
        margin:0px;
        padding:0px;
        font-size:14px;
    }
    .text-center{
        text-align:center !important;
    }
    .text-center > div{
        margin:auto;
    }
    main{
        padding:0 15px;
        font-size:14px;
    }
    td{
        vertical-align:top;
    }
    .bg-fixed{
        position:fixed;
        top:40%;
        left:0;
        right:0;
        transform:translateY(-50%);
        z-index:2;
        text-align:center;
    }
    .bg-fixed{
        opacity:0.1;
    }
    .block{
        display:block;
        margin:auto;
    }
    .text-center{
        text-align:center;
    }
    .text-right{
        text-align:right;
    }
    .table-bordered{
    }
    .table-bordered tr th,.table-bordered tr td{
    }
    .table-bordered tr th:last-child,.table-bordered tr td:last-child{
    }
    .table-bordered tr:last-child th,.table-bordered tr:last-child td{
    }
    .table-bordered tbody tr:first-child td{
        border-top:1px solid #000;
    }
    .table-sm th,.table-sm td{
        padding:2px 5px;
    }
   
    table.table-bordered {
        border-collapse: separate;
        border :  0px solid #000000;
        border-spacing: 0;
        font-size: 9px !important;
        width: 100%;
        border-color:  #000000 ;
        border-right: 1px solid;

    }
    table.table-bordered tr {
        border-color:  #000000 ;
        border-style: none ;
        border-width: 0 ;
    }
    table.table-bordered td {
        border-color:  #000000 ;
        border-left: 1px solid;
        border-bottom:1px solid ;
    }

    table.table-bordered th {
        border-color:  #000000 ;
        border-left: 1px solid;
        border-top:1px solid ;
        border-bottom:1px solid ;
    }
    table{
        font-size:10px !important;
    }
    .table-header-captio{
        font-size:10px !important;
        line-height:1;
        border:1px solid #000;
    }
    .table-header-captio th{
        font-size:10px !important;
        line-height:1;
        padding:2px;
    }
    .table-content-data{
        font-size:10px !important;
        line-height:1;
    }
    .table-content-data tbody td{
        font-size:10px !important;
        padding:2px;
    }
    .table-content-data thead th{
        font-size:10px !important;
        line-height:1;
        padding:2px;
    }
    th.rotate  {
        vertical-align: middle;
        text-align: center;
        padding:0px;
        overflow:hidden;
        font-size:8px !important;
        width:3%;
    }
    th.rotate > div{
        transform: rotate(-90deg);
        -webkit-transform: rotate(-90deg); /* Safari/Chrome */
        -moz-transform: rotate(-90deg); /* Firefox */
        -o-transform: rotate(-90deg); /* Opera */
        -ms-transform: rotate(-90deg); /* IE 9 */
        white-space:nowrap;
        display:block;
        height:10px;
        width:15%;
    }

    .dejavu-font{
        font-family: DejaVu Sans, sans-serif !important;
    }
  </style>
        
</head>
<body>
    <div class="bg-fixed">
        <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="400px">
    </div>
  <header>
    <div class="title-header-wrapper" >
        <h3>PENGAWASAN KELENGKAPAN UNIT RUMAH</h3>
    </div>
  </header>
  <main>
    <table width="100%" class="table table-sm table-header-captio">
        <tbody>
            <tr>
                <th width="35%" style="vertical-align:top;">Nama Perumahan</th>
                <th style="vertical-align:top;" class="text-center" width="2%">:</th>
                <th style="vertical-align:top;"><?=$applicationLetter->name_house;?></th>
            </tr>
            <tr>
                <th style="vertical-align:top;">Alamat Perumahan</th>
                <th style="vertical-align:top;" class="text-center">:</th>
                <th style="vertical-align:top;">
                    <?=$applicationLetter->address;?> 
                    <?=$applicationLetter->district->name;?>
                    <?=$applicationLetter->city->name;?>
                    <?=$applicationLetter->province->name;?>
                </th>
            </tr>
            <tr>
                <th style="vertical-align:top;">Blok / Nomor Unit</th>
                <th style="vertical-align:top;" class="text-center">:</th>
                <th style="vertical-align:top;"><?=$applicationLetter->sample_house;?></th>
            </tr>
            <tr>
                <th style="vertical-align:top;">Type Unit</th>
                <th style="vertical-align:top;" class="text-center">:</th>
                <th style="vertical-align:top;"><?=$applicationLetter->house_type->code;?></th>
            </tr>
            <tr>
                <th>Luas Tanah</th>
                <th class="text-center">:</th>
                <th><?=$applicationLetter->surface_area;?></th>
            </tr>
        </tbody>
    </table>
    <?=$applicationLetter->application_roomm->data;?>
  </main>
</body>
</html>