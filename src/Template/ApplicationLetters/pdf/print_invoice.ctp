<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <style>
    html,body{
        font-family: Arial,Verdana,Helvetica,"DejaVu Sans",sans-serif;
        font-size:11px;
    }
    @page { margin: 30px 20px;}
    .text-center{
        text-align:center !important;
    }
    .text-center > div{
        margin:auto;
    }
    main{
        padding:0 15px;
        font-size:14px;
    }
    td{
        vertical-align:top;
    }
    .bg-fixed{
        position:fixed;
        top:40%;
        left:0;
        right:0;
        transform:translateY(-50%);
        z-index:2;
        text-align:center;
    }
    .bg-fixed{
        opacity:0.1;
    }
    .block{
        display:block;
        margin:auto;
    }
    .text-center{
        text-align:center;
    }
    .text-right{
        text-align:right;
    }

    .dejavu-font{
        font-family: DejaVu Sans, sans-serif !important;
    }
    table{
        font-size:11px;
    }
    main{
        padding:40px 30px;
    }
    main table{
        font-size:12px;
    }
    main h3{
        font-size:16px;
        margin:0px;
        padding:0px;
        margin-bottom:0px;
        color:#333;
    }
    .table-detail thead th{
        text-align:center;
        border-bottom: 1px solid #000;
        border-top: 1px solid #000;
        padding:15px 10px;
    }
    .table-detail tbody td{
        border-bottom: 1px dotted #ccc;
        padding:15px 10px;
    }
    .table-detail tfoot th{
        text-align:center;
        border-bottom: 1px solid #000;
        border-top: 1px solid #000;
        padding:15px 10px;
    }
  </style>
        
</head>
<body>
    <div class="bg-fixed">
        <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="400px">
    </div>
  <header>
    <div class="company-address" style="border-bottom:3px double #000;">
        <table style="width:100%">
            <tbody>
                <tr>
                    <td width="18%" style="vertical-align:middle;">
                        <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" width="100px">
                    </td>
                    <td width="">
                        <div style="width:360px">
                            <b>PT. TIGA CIPTA ESTETIKA</b> <br>
                            Jalan raya cileungsi jonggol komp, Jl. Grand Nusa Indah Raya No.8, Setu Sari, Kec. Cileungsi, Bogor, Jawa Barat 16820
                        </div>
                    </td>
                    <td width="30%">
                        <div style="font-size:12px;font-weight:bold;width:50%;display:inline-block;border:2px solid #000;text-align:center;padding:10px;float:right;">INVOICE</div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
  </header>
  <main>
    <table style="width:100%">
        <tbody>
            <tr>
                <td width="40%">
                    <h3>DITUJUKAN :</h3>
                    <p>
                        <?=strtoupper($applicationLetter->developer->name);?><br>
                        <?=strtoupper($applicationLetter->developer->address);?><br>
                        <?=strtoupper($applicationLetter->developer->phone);?>
                    </p>
                </td>
                <td width="10%">&nbsp;</td>
                <td>
                    <h3>NO. INVOICE :</h3>
                    <p>
                        <?=strtoupper($applicationLetter->application_invoice->code);?><br>
                    </p>
                    <h3>TANGGAL INVOICE :</h3>
                    <p>
                        <?=$applicationLetter->application_invoice->date->format('d-m-Y');?><br>
                    </p>
                </td>
            </tr>
        </tbody>
    </table>
    <br><br>
    <table class="table-detail" style="width:100%;border-collapse: collapse;">
        <thead>
            <tr>
                <th width="5%">NO.</th>
                <th>DESKRIPSI</th>
                <th width="15%">HARGA</th>
                <th width="5%">QTY</th>
                <th width="10%">SATUAN</th>
                <th width="15%">TOTAL</th>
            </tr>
        </thead>
        <tbody>
            <?php $total = $applicationLetter->application_invoice->rate * $applicationLetter->application_invoice->qty;$gt = $total;?>
            <tr>
                <td class="text-center">1</td>
                <td><?=strtoupper('Jasa konsultasi pengawasan rumah');?></td>
                <td class="text-center"><?=$this->Utilities->numberFormat($applicationLetter->application_invoice->rate);?></td>
                <td class="text-center"><?=$this->Utilities->numberFormat($applicationLetter->application_invoice->qty);?></td>
                <td class="text-center">UNIT</td>
                <td class="text-center"><b><?=$this->Utilities->numberFormat($total);?></b></td>
            </tr>
            <?php $a = 2;?>
            <?php foreach($applicationLetter->application_invoice->application_invoices_details as $key => $row):?>
                <?php $total = $row->rate * $row->qty;$gt += $total;?>
                <tr>
                    <td class="text-center"><?=$a++;?></td>
                    <td><?=strtoupper($row->text);?></td>
                    <td class="text-center"><?=$this->Utilities->numberFormat($row->rate);?></td>
                    <td class="text-center"><?=$this->Utilities->numberFormat($row->qty);?></td>
                    <td class="text-center"><?=strtoupper($row->unit);?></td>
                    <td class="text-center"><b><?=$this->Utilities->numberFormat($total);?></b></td>
                </tr>
            <?php endforeach;?>
        </tbody>
        <tfoot>
            <tr>
                <th colspan="5">TOTAL INVOICE</th>
                <th><?=$this->Utilities->numberFormat($gt);?></th>
            </tr>
        </tfoot>
    </table>
    <br><br><br>
    <table style="width:100%">
        <tbody>
            <tr>
                <td width="40%">
                    <p>
                    <B>METODE PEMBAYARAN : <br></B>
                    <b>BANK CENTRAL ASIA (BCA)</b><br>
                    ACCOUNT NAME : HERI HERMAWAN <br>
                    ACCOUNT NUMBER : 437 3055428 <br>
                    <br>
                    <b>BANK RAKYAT INDONESIA (BRI)</b><br>
                    ACCOUNT NAME : PT. TIGA CIPTA ESTETIKA <br>
                    ACCOUNT NUMBER : 7887-01-006808-53-8<br>
                    </p>
                </td>
                <td width="20%">
                    &nbsp;
                </td>
                <td class="text-right" width="40%">
                    <div style="display:inline-block;width:200px;text-align:center">
                        <p>
                        <b>HORMAT KAMI
                        <br>
                        <img src="<?=ROOT.DS.'webroot'.DS.'img'.DS.'ttd_1.jpg';?>"  alt="<?=ROOT.DS.'webroot'.DS.'img'.DS.'ttd_1.jpg';?>" width="200px"><br>
                        <u>HERI HERMAWAN</u>
                        </p>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
  </main>
</body>
</html>