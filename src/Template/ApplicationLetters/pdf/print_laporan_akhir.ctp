<html>
<head>
  <style>
    @page { font-size:14px; line-height:20px; }
    header {  z-index:99;height:180px;}
    footer { }
    .logo-wrapper-left{
        display: inline-block;
        width:10%;
        padding-top:0px;
        line-height:180px;
    }
    .title-header-wrapper{
        display: inline-block;
        padding-top:10px;
        padding-left:30px;
        padding-right:30px;
        width:70%;
        
    }
    .title-header-wrapper h3{
        margin:0px;
        padding:0px;
        margin-bottom:5px;
        text-align:center;
    }
    .title-header-wrapper p{
        margin:0px;
        padding:0px;
        font-size:14px;
    }
    main{
        padding:0px;
        z-index:99;
    }
    td{
        vertical-align:top;
    }
    .table-header{
        margin:auto;
        text-align:left;
    }
    .table-header td{
        padding:0px;
        line-height:18px;
    }
    main{
        page-break-after: always;
    }
    main:last-child{
        page-break-after: auto;
    }
    .table-sub td{
        padding:0px 10px;
    }
    .bg-fixed{
        position:fixed;
        top:40%;
        left:0;
        right:0;
        transform:translateY(-50%);
        z-index:2;
        text-align:center;
    }
    .bg-fixed{
        opacity:0.1;
    }
  </style>
</head>
<body>
<div class="bg-fixed">
<img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="400px">
</div>
<header>
    <div class="logo-wrapper-left">
    <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="120px">
    </div>
    <div class="title-header-wrapper">
        <h3>
            LEMBAR PEMERIKSAAN <br>KELAIKAN FUNGSI BANGUNAN GEDUNG
        </h3>
        <table class="table-header">
            <tr>
                <td width="30%">Nomor Surat</td>
                <td width="2%">:</td>
                <td width="68%"><?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->output_date->format('m')).'/CON/'.$applicationLetter->output_date->format('Y');?></td>
            </tr>
            <tr>
                <td width="30%%">Tanggal</td>
                <td width="2%">:</td>
                <td width="68%"><?=$applicationLetter->output_date->format('d-m-Y');?></td>
            </tr>
        </table>
    </div>
</header>
<main>
    <table width="100%" class="table-main">
        <tbody>
            <tr>
                <td>Pada Hari ini, <i><?=$this->Utilities->dayArray($applicationLetter->output_date->format('D'));?></i> tanggal <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('d'));?></i> bulan <i><?=$this->Utilities->monthArray($applicationLetter->output_date->format('m') * 1);?></i> tahun <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('Y'));?></i>, yang bertanda tangan dibawah ini,</td>
            </tr>
            <tr>
                <td>Konsultan Pegawas</td>
            </tr>
        </tbody>
    </table>
    <table width="100%" class="table-main">
        <tbody>
            <tr>
                <td width="250px">a. Nama penanggung jawab</td>
                <td width="20px">:</td>
                <td><b><?=$consultant->pic_name;?></b></td>
            </tr>
            <tr>
                <td width="250px">b. Nama Perusahaan/instansi teknis*</td>
                <td width="20px">:</td>
                <td><b><?=$consultant->name;?></b></td>
            </tr>
        </tbody>
    </table>
    <table width="100%" class="table-main">
        <tbody>
            <tr>
                <td><br></td>
            </tr>
            <tr>
                <td colspan="3">telah melakukan pemeriksaan kelaikan fungsi bangunan gedung pada :<br><br></td>
            </tr>
            <tr>
                <td colspan="3">1. Data Bangunan Gedung</td>
            </tr>
        </tbody>
    </table>
    <table class="table-sub" width="100%">
        <tr>
            <td width="250px">a. Fungsi Utama</td>
            <td width="20px">:</td>
            <td><?=$applicationLetter->primary_function;?></td>
        </tr>
        <tr>
            <td width="250px">b. Fungsi Tambahan</td>
            <td width="20px">:</td>
            <td>-</td>
        </tr>
        <tr>
            <td width="250px">c. Type bangunan gedung</td>
            <td width="20px">:</td>
            <td><?=$applicationLetter->house_type->code;?>/<?=$applicationLetter->surface_area;?></td>
        </tr>
        <tr>
            <td width="250px">d. Letak bangunan</td>
            <td width="20px">:</td>
            <td>
                <div style="min-height:200px;">
                <?php
                    $dataHouse = explode(",",$applicationLetter->building);
                    $dataCount = count($dataHouse) -1;
                    $a = 0;
                    $markred = $this->request->query('build-code');
                    foreach($dataHouse as $r):
                        $a++;
                ?>
                    <?php if($r == $markred):?>
                        <strong style="color:red;"><?=$applicationLetter->sample_house;?></strong>
                    <?php else:?>
                        <?=$r;?>
                    <?php endif;?>
                    <?php
                        $dataCount = $dataCount;
                    ?>
                    <?=($dataCount != 0 ? '|' : '');?>
                <?php endforeach;?>
                </div>
            </td>
        </tr>
    </table>
    <table width="100%" class="table-main">
        <tbody>
        <tr>
            <td><br>2. Data Perumahan</td>
        </tr>
        </tbody>
    </table>
    <table class="table-sub" width="100%">
        <tr>
            <td width="250px">a. Nama bangunan gedung</td>
            <td width="20px">:</td>
            <td><?=$applicationLetter->name_house;?></td>
        </tr>
        <tr>
            <td width="250px">b. Alamat</td>
            <td width="20px">:</td>
            <td><?=$applicationLetter->address;?></td>
        </tr>
        <tr>
            <td width="250px">c. Kecamatan</td>
            <td width="20px">:</td>
            <td><?=$applicationLetter->district->name;?></td>
        </tr>
        <tr>
            <td width="250px">d. Kabupaten/Kota</td>
            <td width="20px">:</td>
            <td><?=$applicationLetter->city->name;?></td>
        </tr>
        <tr>
            <td width="250px">e. Provinsi</td>
            <td width="20px">:</td>
            <td><?=$applicationLetter->province->name;?></td>
        </tr>
        <tr>
            <td width="250px">f. Jumlah Unit</td>
            <td width="20px">:</td>
            <td><?=$a;?></td>
        </tr>
        <tr>
            <td width="250px">g. Harga Jual</td>
            <td width="20px">:</td>
            <td>Rp.<?=number_format($applicationLetter->price_per_unit,0);?></td>
        </tr>
        <tr>
            <td width="250px">h. Terbilang</td>
            <td width="20px">:</td>
            <td><?=$this->Utilities->terbilang($applicationLetter->price_per_unit);?> Rupiah</td>
        </tr>
    </table>
    <table width="100%" class="table-main">
        <tbody>
        <tr>
            <td colspan="3"><br>3. Data Administrasi</td>
        </tr>
        </tbody>
    </table>
    <table class="table-sub" width="100%">
        <tbody>
            <tr>
                <td width="250px">a. Sertifikat Hak Milik</td>
                <td width="20px">:</td>
                <td><?=$applicationLetter->sertifikat_hak;?></td>
            </tr>
            <tr>
                <td width="250px">b. Izin Pemanfaatan Tanah / Izin Lokasi</td>
                <td width="20px">:</td>
                <td><?=$applicationLetter->izin_pemanfaatan;?></td>
            </tr>
            <tr>
                <td width="250px">c. Pengesahaan Site Plan</td>
                <td width="20px">:</td>
                <td><?=$applicationLetter->pengesahaan;?></td>
            </tr>
            <tr>
                <td width="250px">d. Izin Mendirikan Bangunan</td>
                <td width="20px">:</td>
                <td><?=$applicationLetter->nomor_imb;?></td>
            </tr>
        </tbody>
    </table>
    <table width="100%" class="table-main" style="page-break-after: always;">
        <tbody>
        <tr>
            <td><br>Sesuai dengan hasil pemeriksaan yang dilakukan, kami <?=$consultant->name;?>, selaku konsultan pengawas, dengan ini menyatakan bahwa : <br><br><br></td>
        </tr>
        </tbody>
    </table>
    <table class="table-sub-nopad" width="100%">
        <tbody>
            <tr>
                <td width="250px">1. Persyaratan administratif</td>
                <td width="20px">:</td>
                <td><?=$applicationLetter->application_output->persyaratan_administratif;?></td>
            </tr>
            <tr>
                <td width="250px">2. Persyaratan teknis</td>
                <td width="20px">:</td>
                <td>&nbsp;</td>
            </tr>
        </tbody>
    </table>
    <table class="table-main" width="100%">
        <tbody>
            <tr>
                <td width="250px">a. Fungsi bangunan gedung</td>
                <td width="20px">:</td>
                <td><?=$applicationLetter->application_output->fungsi_bangunan;?></td>
            </tr>
            <tr>
                <td width="250px">b. Peruntukan</td>
                <td width="20px">:</td>
                <td><?=$applicationLetter->application_output->peruntukan;?></td>
            </tr>
            <tr>
                <td width="250px">c. Tata bangunan</td>
                <td width="20px">:</td>
                <td><?=$applicationLetter->application_output->tata_bangunan;?></td>
            </tr>
            <tr>
                <td width="250px">d. Kelaikan fungsi bangunan gedung dinyatakan</td>
                <td width="20px">:</td>
                <td><?=$applicationLetter->application_output->kelaikan;?></td>
            </tr>
        </tbody>
    </table>

    <table width="100%" class="table-main">
        <tbody>
            <tr>
                <td>
                    <br>
                    Lembar hasil pemeriksaan ini berlaku sepanjang <strong>TIDAK TERDAPAT PERUBAHAN SPESIFIKASI, MUTU, DAN KELAIKAN</strong>  oleh <?=$applicationLetter->developer->name;?> 
                    <br>
                    <br>
                    Jika terjadi perubahan Spesifikasi, Mutu dan Kelaikan (tidak sesuai rumah contoh atau acuan) dikemudian hari pada Perumahan <?=$applicationLetter->name_house;?> maka <?=$applicationLetter->developer->name;?>, selaku pihak Pengembang Perumahan bertanggung jawab terhadap dampak yang diakibatkan.<br>
                    <br>
                    Lembar hasil pemeriksaan berlaku untuk satu periode (<?=$a;?> unit rumah yang telah disebut diatas) yang pemeriksaannya telah diwakilkan pada 1 unit bangunan rumah Blok <?=$applicationLetter->sample_house;?>
                </td>
            </tr>
            <tr>
                <td style="text-align:right;">
                    <br>
                    <br>
                    <br>
                    <div class="signature" style="width:300px;display:block;text-align:center;margin-left:auto;">
                        Konsultan Pengawas<br>
                        <?=$consultant->name;?>
                        <br><br><br><br><br>
                        <?=$consultant->pic_name;?><br><br><br><br>
                    </div>
                </td>
            </tr>
        </tbody>
    </table>
</main>
</body>
</html>