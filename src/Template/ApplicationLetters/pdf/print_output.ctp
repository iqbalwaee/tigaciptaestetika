
<?php
    $dataHouse = explode(",",$applicationLetter->building);
    $countBuild = count($dataHouse);
    $a =1;
?>

<?php foreach($dataHouse as $key => $r):?>
    <div class="surat-pernyataan">
        
        <div class="kop-surat">
            <table class="table-kop">
                <tbody>
                    <tr>
                        <td width="20%">
                            <img src="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>" alt="logo" class="kop-surat-logo-img">
                        </td>
                        <td style="padding:0 15px;">
                            <div class="kop-surat-title">
                                <h3>
                                    SURAT PERNYATAAN PEMERIKSAAN KELAIKAN FUNGSI BANGUNAN GEDUNG
                                </h3>
                                <table class="table">
                                    <tr>
                                        <td class="text-left" width="40%">Nomor Surat</td>
                                        <td>:</td>
                                        <td class="text-left"><?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->output_date->format('m')).'/CON/'.$applicationLetter->output_date->format('Y');?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-left">Tanggal</td>
                                        <td>:</td>
                                        <td class="text-left"><?=$applicationLetter->output_date->format('d-m-Y');?></td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td width="20%">
                            <div class="title-kop-box">
                                <div class="box-red">
                                    UNTUK BANK
                                </div>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="body-surat-pernyataan">
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td style="">Pada Hari ini, <i><?=$this->Utilities->dayArray($applicationLetter->output_date->format('D'));?></i> tanggal <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('d'));?></i> bulan <i><?=$this->Utilities->monthArray($applicationLetter->output_date->format('m') * 1);?></i> tahun <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('Y'));?></i>, yang bertanda tangan dibawah ini,</td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td colspan="3">Penyedia jasa Pengawasan</td>
                    </tr>
                    <tr>
                        <td width="30%">a. Nama penanggung jawab</td>
                        <td width="5%;">:</td>
                        <td><b><?=$consultant->pic_name;?></b></td>
                    </tr>
                    <tr>
                        <td>b. Nama Perusahaan/instansi teknis*</td>
                        <td>:</td>
                        <td><b><?=$consultant->name;?></b></td>
                    </tr>
                    <tr>
                        <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td colspan="3">telah melaksanakan pemeriksaan kelaikan fungsi bangunan gedung pada</td>
                    </tr>
                    <tr>
                        <td colspan="3">1. Bangunan Gedung</td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td width="30%" style="padding-left:30px;">a. Fungsi Utama</td>
                        <td width="5%">:</td>
                        <td><?=$applicationLetter->primary_function;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">b. Fungsi Tambahan</td>
                        <td>:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">c. Jenis bangunan gedung</td>
                        <td>:</td>
                        <td><?=$applicationLetter->house_type->code;?>/<?=$applicationLetter->surface_area;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">d. Nama bangunan gedung</td>
                        <td>:</td>
                        <td><?=$applicationLetter->name_house;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">e. Nomor pendaftaran bangunan gedung</td>
                        <td>:</td>
                        <td><?=$r;?></td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td>2. Lokasi Bangunan Gedung</td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td width="30%;"  style="padding-left:30px;">a. Kampung</td>
                        <td width="5%">:</td>
                        <td><?=$applicationLetter->village_name;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">b. Kelurahan/desa</td>
                        <td>:</td>
                        <td><?=$applicationLetter->village->name;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">c. Kecamatan</td>
                        <td>:</td>
                        <td><?=$applicationLetter->district->name;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">d. Kabupaten/Kota</td>
                        <td>:</td>
                        <td><?=$applicationLetter->city->name;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">e. Provinsi</td>
                        <td>:</td>
                        <td><?=$applicationLetter->province->name;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">f. Alamat lokasi terletak di</td>
                        <td>:</td>
                        <td><?=$applicationLetter->address;?></td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td>3. Permohonan</td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td width="30%;" style="padding-left:30px;">a. Penerbitan Sertifikat Laik Fungsi</td>
                        <td width="5%">:</td>
                        <td>Nomor <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span> Tanggal <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">b. Perpanjangan Sertifikat Laik Fungsi </td>
                        <td>:</td>
                        <td>Nomor <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span> Tanggal <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;"><span style="width:14px;display:inline-block;">&nbsp;</span>Perpanjangan ke</td>
                        <td>:</td>
                        <td>
                            
                        </td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td>Dengan ini menyatakan bahwa</td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td width="30%;">1. Persyaratan administratif</td>
                        <td width="5%">:</td>
                        <td><?=$applicationLetter->application_output->persyaratan_administratif;?></td>
                    </tr>
                    <tr>
                        <td>2. Persyaratan teknis</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td width="30%;"  style="padding-left:30px;">a. Fungsi bangunan gedung</td>
                        <td width="5%">:</td>
                        <td><?=$applicationLetter->application_output->fungsi_bangunan;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">b. Peruntukan</td>
                        <td>:</td>
                        <td><?=$applicationLetter->application_output->peruntukan;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">c. Tata bangunan</td>
                        <td>:</td>
                        <td><?=$applicationLetter->application_output->tata_bangunan;?></td>
                    </tr>
                    <tr>
                        <td style="padding-left:30px;">d. Kelaikan fungsi bangunan gedung dinyatakan</td>
                        <td>:</td>
                        <td><?=$applicationLetter->application_output->kelaikan;?></td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <br>
                            sesuai dengan kesimpulan berdasarkan analisis terhadap Daftar Simak Pemeriksaan Kelaikan Fungsi Bangunan Gedung terlampir.
                            <br>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="3" >
            Surat pernyataan ini berlaku sepanjang tidak ada perubahan yang dilakukan oleh pemilik/pengguna yang mengubah sistem dan/atau spesifikasi teknis, atau gangguan penyebab lainnya yang dibuktikan kemudian.
                        <br>
                        <br>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="page-break-after:always;">&nbsp;</div>
            <table width="100%" class="table-main">
                <tbody>
                    <tr>
                        <td >
                            
            Selanjutnya pemilik/pengguna bangunan gedung dapat mengurus permohonan penerbitan Sertifikat Laik Fungsi bangunan gedung.<br>Demikian surat pernyataan ini dibuat dengan penuh tanggung jawab profesional.
                        <br>
                        <br>
                        <br>
                        <br>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right;">
                            <table width="100%">
                                <tr>
                                    <td width="50%">
                                        <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                                            'controller' => 'pages',
                                            'action' => 'qrCode',
                                            $applicationLetter->code,
                                            '?' => [
                                                'build-code' => $r
                                            ]
                                        ],true));?>"  width="100px"/>
                                    </td>
                                    <td>
                                        <div style="width:250px;display:inline-block;text-align:center;margin-left:auto;">
                                            Jakarta, <?=$this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));?><br>
                                            Penyedia Jasa Pengawasan <br>selaku Penanggung Jawab
                                            <?php if(!empty($userData)):?>
                                                <?php if($userData['group_id'] != 5):?>
                                                    <br>
                                                    <img src="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>"  alt="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>" width="200px"><br>
                                                <?php else:?>
                                                    <br><br><br><br><br><br><br>
                                                <?php endif;?>
                                                <?php else:?>
                                                    <br>
                                                    <img src="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>"  alt="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>" width="200px"><br>
                                                <?php endif;?>
                                            <b><u><?=$consultant->pic_name;?></u><b><br><br><br>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            
                        </td>
                    </tr>
                    <!-- <tr>
                        <td class="text-center">
                            <div style="width:80%;display:inline-block;text-align:center;">
                                Disetujui, <br>PEMERINTAH PROVINSI/KABUPATEN/KOTA...........................................<br>DINAS...................................................................................
                                <br><br><br><br><br><br>
                                ....................................................<br>NIP :  .....................................<br><br><br>
                            </div>
                        </td>
                    </tr> -->

                    <tr>
                        <td >
                            Keterangan : * Dipilih yang sesuai dengan permohonan dan coret yang tidak sesuai, jika pengisian secara manual. Jika pengisian menggunakan software, yang tidak dipilih didelete (hapus).
                        </td>
                    </tr> 
                    <tr>
                        <td class="text-right">
                            
                        </td>
                    </tr> 
                </tbody>
            </table>
        </div>
        
    </div>
    <?php if($a < $countBuild):?>
        <div style="page-break-after:always;">&nbsp;</div>
        <?php $a++;?>
    <?php endif;?>
<?php endforeach;?>