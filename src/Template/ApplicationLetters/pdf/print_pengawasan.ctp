<div class="surat-pernyataan">
        
    <div class="kop-surat">
        <table class="table-kop">
            <tbody>
                <tr>
                    <td width="20%">
                        <div class="kop-surat-logo">
                            <img src="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>" alt="logo">
                        </div>
                    </td>
                    <td style="padding:0 15px;">
                        <div class="kop-surat-title">
                            <h3>
                                LEMBAR PENGAWASAN
                            </h3>
                            <table class="table">
                                <tr>
                                    <td class="text-left" width="35%">Nomor Surat</td>
                                    <td>:</td>
                                    <td class="text-left"><?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->output_date->format('m')).'/CON/'.$applicationLetter->output_date->format('Y');?></td>
                                </tr>
                                <tr>
                                    <td class="text-left">Tanggal</td>
                                    <td>:</td>
                                    <td class="text-left"><?=$applicationLetter->output_date->format('d-m-Y');?></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td width="20%">
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="body-surat-pernyataan">
        <table width="100%" class="table-main">
            <tbody>
                <tr>
                    <td style="">Pada Hari ini, <i><?=$this->Utilities->dayArray($applicationLetter->output_date->format('D'));?></i> tanggal <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('d'));?></i> bulan <i><?=$this->Utilities->monthArray($applicationLetter->output_date->format('m') * 1);?></i> tahun <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('Y'));?></i>, yang bertanda tangan dibawah ini,<br></td>
                </tr>
            </tbody>
        </table>
        <table width="100%" class="table-main">
            <tbody>
                <tr>
                    <td colspan="3">Konsultan Pengawas</td>
                </tr>
                <tr>
                    <td width="30%">a. Nama penanggung jawab</td>
                    <td width="5%;">:</td>
                    <td><b><?=$consultant->pic_name;?></b></td>
                </tr>
                <tr>
                    <td>b. Nama Perusahaan/instansi teknis*</td>
                    <td>:</td>
                    <td><b><?=$consultant->name;?></b></td>
                </tr>
                <tr>
                    <td colspan="3"><br></td>
                </tr>
            </tbody>
        </table>
        <table width="100%" class="table-main">
                <tr>
                    <td colspan="2">Telah melakukan Pengawasan bangunan perumahan </td>
                    <td width="5px">:<br><br></td>
                </tr>
                <tr>
                    <td width="5%" style="text-align:left;padding-left:20px;padding-right:0;">1.</td>
                    <td width="40%">Nama Perumahan</td>
                    <td>:</td>
                    <td><?=$applicationLetter->name_house;?><br></td>
                </tr>
                <tr>
                    <td width="5%" style="text-align:left;padding-left:20px;padding-right:0;">2.</td>
                    <td width="45%">Alamat</td>
                    <td>:</td>
                    <td><?=$applicationLetter->address;?><br></td>
                </tr>
                <tr>
                    <td width="5%" style="text-align:left;padding-left:20px;padding-right:0;">3.</td>
                    <td width="45%">Kecamatan</td>
                    <td>:</td>
                    <td><?=$applicationLetter->district->name;?><br></td>
                </tr>
                <tr>
                    <td width="5%" style="text-align:left;padding-left:20px;padding-right:0;">4.</td>
                    <td width="45%">Kabupaten/Kota</td>
                    <td>:</td>
                    <td><?=$applicationLetter->city->name;?><br></td>
                </tr>
                <tr>
                    <td width="5%" style="text-align:left;padding-left:20px;padding-right:0;">5.</td>
                    <td width="45%">Provinsi</td>
                    <td>:</td>
                    <td><?=$applicationLetter->province->name;?><br></td>
                </tr>
                <tr>
                    <td width="5%" style="text-align:left;padding-left:20px;padding-right:0;">6.</td>
                    <td width="45%">Luas Rumah / Type</td>
                    <td>:</td>
                    <td><?=$applicationLetter->surface_area;?> / <?=$applicationLetter->house_type->name;?><br></td>
                </tr>
                <?php
                    $dataHouse = explode(",",$applicationLetter->building);
                    $countBuild = count($dataHouse);
                ?>
                <tr>
                    <td width="5%" style="text-align:left;padding-left:20px;padding-right:0;">7.</td>
                    <td width="45%">Jumlah Unit</td>
                    <td>:</td>
                    <td><?=$countBuild ;?><br></td>
                </tr>
                <tr>
                    <td width="5%" style="text-align:left;padding-left:20px;padding-right:0;">8.</td>
                    <td width="45%">Rumah Sample</td>
                    <td>:</td>
                    <td><?=$applicationLetter->sample_house;?><br></td>
                </tr>
            </tbody>
        </table>
        <table class="table-main" width="100%" style="line-height:28px!important;">
            <tbody>
                <tr>
                    <td>
                    <br>
                    Sesuai dengan hasil pengawasan yang dilakukan, kami <b><?=$consultant->name;?></b>, selaku pengawas, dengan ini menyatakan Telah Melaksanakan Survey lapangan, Lembar hasil pemeriksaan ini berlaku sepanjang <b>TIDAK TERDAPAT PERUBAHAN SPESIFIKASI, MUTU, DAN KELAIKAN</b> oleh <b><?=$applicationLetter->developer->name;?></b>  Lembar hasil pemeriksaan berlaku untuk satu periode (1 unit rumah yang telah disebut diatas) yang pemeriksaannya telah diwakilkan pada 1 unit bangunan rumah Blok <b><?=$applicationLetter->sample_house;?></b><br>
                            <br><br>
                            <br>
                    </td>
                </tr>
            </tbody>
        </table>
        <table class="table-main" width="100%">
            <tbody>
                <tr>
                    <td width="5%">&nbsp;</td>
                    <td width="45%">
                        <div style="width:100%;text-align:center;display:inline-block;">
                            Pengawas Lapangan<br>
                            <?=$consultant->name;?><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <?=$applicationLetter->supervisor->name;?><br>
                            NIK: <?=$applicationLetter->supervisor->nik;?>
                        </div>
                    </td>
                    <td width="45%" style="text-align:right;">
                        <div style="width:250px;text-align:center;display:inline-block;margin-left:auto;">
                            Penanggung Jawab<br>
                            <?=$applicationLetter->developer->name;?><br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <br>
                            <?=$applicationLetter->developer->ceo_name;?>
                        </div>
                    </td>
                    <td width="5%">&nbsp;</td>
                </tr>
            </tbody>
        </table>
    </div>
</div>