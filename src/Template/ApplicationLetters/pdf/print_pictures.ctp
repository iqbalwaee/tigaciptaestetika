
<!DOCTYPE html>
<html lang="en" >
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generatePathImage(null,$defaultAppSettings['App.Favico']);?>" />
    <style>
        body{
            padding: 0px;
            margin: 0px;
            color: #000;
            margin: 0px 0px !important; 
        }

        .bg-fixeds {
            display: block;
            position: fixed;
            top: 40%;
            left: 0;
            right: 0;
            transform:translateY(-50%);
            z-index: 11;
            text-align: center; 
        }
        .bg-fixeds img {
            opacity: 0.15; 
            width:300px;
        }

        .bg-fixedss {
            display: block;
            position: absolute;
            top: 50%;
            left: 0;
            right: 0;
            z-index: 11;
            text-align: center; 
        }
        .bg-fixedss img {
            opacity: 0.15; 
        }
        .surat-pernyataan {
            color: #000;
            border-bottom: none;
            font-size: 14px !important;
            z-index: 10;
            position: relative;
            font-family: 'Times New Roman', Times, serif; 
        }
        .surat-pernyataan .kop-surat {
            
        }
        .table-kop{
            width:100%;
        }
        .table-kop > tbody td{
            vertical-align:top;
        }
        .surat-pernyataan .kop-surat .title-kop-box {
            display: block;
        }

        .surat-pernyataan .kop-surat .title-kop-box .box-red {
            font-size: 16px;
            color: red;
            padding: 15px;
            display: inline-block;
            padding: 5px 10px;
            border: 2px solid red;
        }

        .surat-pernyataan .kop-surat .kop-surat-logo {
            padding: 0px;
            text-align: center;
        }

        .surat-pernyataan .kop-surat .kop-surat-logo-img {
            width:150px;
        }

        .surat-pernyataan .kop-surat .kop-surat-title {
            text-align: center;
        }

        .surat-pernyataan .kop-surat .kop-surat-title h3 {
            font-weight: bold;
            font-size: 18px !important;
            margin:0 !important;
            padding:0 !important;
            padding-bottom:15px !important;
        }

        .surat-pernyataan .kop-surat .kop-surat-title .table {
            width: 300px;
            margin: auto;
            margin-bottom: 20px;
        }

        .surat-pernyataan .kop-surat .kop-surat-title .table td {
            padding: 0px 0px;
            border-top: none;
        }

        .surat-pernyataan .kop-surat .kop-surat-label {
            float: left;
            text-align: right;
            width: 20%;
        }

        .surat-pernyataan .kop-surat .kop-surat-label div {
            text-align: center;
            padding: 5px 10px;
            font-weight: bold;
        }

        .surat-pernyataan .kop-surat .kop-surat-label div.red {
            color: red;
            border: 2px solid red;
        }

        .surat-pernyataan .body-surat-pernyataan {
            padding-top: 15px;
        }

        .surat-pernyataan .body-surat-pernyataan .table-main {
            font-size: 14px;
        }

        .surat-pernyataan .body-surat-pernyataan .table-main td {
            vertical-align: top;
        }

        .surat-pernyataan .divider {
            border-bottom: 2px dashed #ddd;
            margin-bottom: 15px;
        }
        .text-right{
            text-align:right;
        }
        .table-lampiran{
            border-collapse: collapse;
            width:100%;
        }
        .table-lampiran td{
            vertical-align:top;
        }
        .table-main-detail{
            border:1px solid #000;
            border-collapse: collapse;
            font-size:11px;
            line-height:11px;
            width:100%;
        }
        .table-main-detail tr.body-detail td{
            border-right:1px solid #000;
            border-bottom:1px solid #000;
            vertical-align:middle;
            text-align:center;
            height:20px;
            font-size:8px;
            line-height:9px;
        }
        .table-main-detail tr.header-detail-tr td{
            border-right:1px solid #000;
            border-bottom:1px solid #000;
            padding:5px 5px;
            height:30px;
            vertical-align:middle;
            text-align:center;
            font-weight:bold;
        }
        .table-main-detail tr.body-detail td:last-child{
        }
        .table-main-detail tr.body-detail:last-child td{
        }
        sup{
            font-size:8px;
        }
        .table-custom{
            border-collapse: collapse;
        }
        .table-custom > tbody > tr > td {
            padding: 10px;
            border-bottom: 1px solid #000;
        }
        .table-custom > tbody > tr >td:first-child {
            border-right: 1px solid #000 !important;
        }
        .table-custom > tbody > tr:nth-child(odd) >td {
            padding: 15px 10px;
            background-color: #ccc !important;
        }
</style>

</head>
<body class="m-page--fluid m--skin- body-print ">
        <?php
            $dataHouse = explode(",",$applicationLetter->building);
            $countBuild = count($dataHouse);
            $a =1;
        ?>

        <div class="surat-pernyataan">
            
            <div class="kop-surat">
                <table class="table-kop">
                    <tbody>
                        <tr>
                            <td width="20%">
                                &nbsp;
                            </td>
                            <td style="padding:0 15px;">
                                <div class="kop-surat-title">
                                    <h3>
                                        DOKUMENTASI PENGAWASAN
                                    </h3>
                                </div>
                            </td>
                            <td width="20%">
                            &nbsp;
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="body-surat-pernyataan">
                <table class="table-main" width="100%">
                    <tbody>
                    <tr>
                            <td width="30%">Nama Perumahan</td>
                            <td width="5%;">:</td>
                            <td><?=$applicationLetter->name_house;?></td>
                        </tr>
                        <tr>
                            <td>Rumah Sample</td>
                            <td>:</td>
                            <td><?=$applicationLetter->sample_house;?></td>
                        </tr>
                        <tr>
                            <td>Dusun</td>
                            <td>:</td>
                            <td><?=$applicationLetter->address;?></td>
                        </tr>
                        <tr>
                            <td>Desa/Kelurahan</td>
                            <td>:</td>
                            <td><?=$applicationLetter->village->name;?></td>
                        </tr>
                        <tr>
                            <td>Kabupaten/Kota</td>
                            <td>:</td>
                            <td><?=$applicationLetter->city->name;?></td>
                        </tr>
                        <tr>
                            <td>Provinsi</td>
                            <td>:</td>
                            <td><?=$applicationLetter->province->name;?></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
                <table width="100%" class="table-main table-custom" style="border:1px solid #000;page-break-after: always;">
                    <tbody>
                        <tr>
                            <td width="50%" style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">Tampak Depan</td>
                            <td width="50%" style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">Jalan Depan</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;border-right:1px solid #000;">
                                    <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->tampak_depan)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->tampak_depan_dir,$applicationLetter->application_picture->tampak_depan,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;border-right:1px solid #000;">
                                
                            <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->jalan_depan)):?>
                                <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->jalan_depan_dir,$applicationLetter->application_picture->jalan_depan,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">Ruang Tamu</td>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">Kamar Tidur</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;border-right:1px solid #000;">
                                
                            <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->ruang_tamu)):?>
                                <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->ruang_tamu_dir,$applicationLetter->application_picture->ruang_tamu,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;border-right:1px solid #000;">
                                
                            <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->kamar_tidur)):?>
                                <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->kamar_tidur_dir,$applicationLetter->application_picture->kamar_tidur,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;" width="50%">Plafond</td>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;" width="50%">Pintu, Kusen dan Jendela</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;border-right:1px solid #000;">
                                
                            <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->plafond)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->plafond_dir,$applicationLetter->application_picture->plafond,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;border-right:1px solid #000;">
                                
                            <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->pintu_kusen_jendela)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->pintu_kusen_jendela_dir,$applicationLetter->application_picture->pintu_kusen_jendela,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width="100%" class="table-main table-custom" style="border:1px solid #000;page-break-after: always;">
                    <tbody>
                        <tr>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">Toilet</td>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">Sumur & Septictank</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;border-right:1px solid #000;">
                                    <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->toilet)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->toilet_dir,$applicationLetter->application_picture->toilet,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;border-right:1px solid #000;">
                                    <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->sumur_septictank)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->sumur_septictank_dir,$applicationLetter->application_picture->sumur_septictank,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">KWh Listrik</td>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">Jaringan Air Bersih</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;border-right:1px solid #000;">
                                
                            <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->kwh_listrik)):?>
                                <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->kwh_listrik_dir,$applicationLetter->application_picture->kwh_listrik,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;border-right:1px solid #000;">
                                
                            <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->jaringan_air)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->jaringan_air_dir,$applicationLetter->application_picture->jaringan_air,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;" width="50%">Saluran Pembuangan</td>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;" width="50%">Tampak Atas 1</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;border-right:1px solid #000;">
                                    <div style="height:170px">
                                
                                <?php if(!empty($applicationLetter->application_picture->saluran_pembuangan)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->saluran_pembuangan_dir,$applicationLetter->application_picture->saluran_pembuangan,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;border-right:1px solid #000;">
                                    <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->tampak_atas_1)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->tampak_atas_1_dir,$applicationLetter->application_picture->tampak_atas_1,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">Tampak Atas 2</td>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;">Jalan Utama</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;border-right:1px solid #000;">
                                
                            <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->tampak_atas_2)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->tampak_atas_2_dir,$applicationLetter->application_picture->tampak_atas_2,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;border-right:1px solid #000;">
                                    <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->jalan_utama)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->jalan_utama_dir,$applicationLetter->application_picture->jalan_utama,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width="100%" class="table-main table-custom" style="border:1px solid #000;">
                    <tbody>
                        <tr>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;" width="50%">Jalan Komplek</td>
                            <td style="text-align:center;font-weight:bold;background-color:#3333;border-right:1px solid #000;" width="50%">Fasilitas Umum</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;border-right:1px solid #000;">
                                
                            <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->jalan_komplek)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->jalan_komplek_dir,$applicationLetter->application_picture->jalan_komplek,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;border-right:1px solid #000;">
                                    <div style="height:170px">
                                <?php if(!empty($applicationLetter->application_picture->fasilitas_umum)):?>
                                    <img src="<?=$this->Utilities->generatePathImage($applicationLetter->application_picture->fasilitas_umum_dir,$applicationLetter->application_picture->fasilitas_umum,'thumb-');?>" class="img img-responsive" style="text-align:center;" height="170px">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br><br>
                <table width="100%" class="table-main">
                    <tbody>
                        <tr>
                            <td width="50%" class="text-left">
                                <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                                    'controller' => 'pages',
                                    'action' => 'qrCode',
                                    $applicationLetter->code
                                ],true));?>" width="100px"/>
                            </td>
                            <td style="text-align:right;">
                                <div style="width:350px;display:inline-block;text-align:center;">
                                    Jakarta, <?=$this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));?><br>
                                    Penyedia Jasa Pengawasan <br>selaku Penanggung Jawab<br>
                                    <?php if(!empty($userData)):?>
                                        <?php if($userData['group_id'] == 2 || empty($userData)):?>
                                            <?php if($applicationLetter->status == 3):?>
                                                <img src="<?=$this->Utilities->generatePathImage(null,'/img/ttd_1.jpg');?>" style="width:180px;"><br>
                                            <?php else:?>
                                                <br><br><br><br><br><br><br><br>
                                            <?php endif;?>
                                        <?php else:?>
                                                <?php if($applicationLetter->status == 3):?>
                                                <img src="<?=$this->Utilities->generatePathImage(null,'/img/ttd_1.jpg');?>" style="width:180px;"><br>
                                            <?php else:?>
                                                <br><br><br><br><br><br><br><br>
                                            <?php endif;?>
                                        <?php endif;?>
                                        <?php else:?>
                                            <br><br><br><br><br><br><br><br>
                                        <?php endif;?>
                                    <b><u><?=$consultant->pic_name;?></u><b><br><br><br>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
    </body>
</html>