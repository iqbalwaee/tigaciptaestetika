<html>
<head>
  <style>
    @page { margin: 40px 20px; font-size:16px; line-height:20px; }
    .main-top{
        display:block;
        margin-bottom:5px;
    }
    .title-header-wrapper{
        text-align:center;
        padding-top:10px;
    }
    .title-header-wrapper h3{
        margin:0px;
        padding:0px;
        margin-bottom:35px;
    }
    .title-header-wrapper p{
        margin:0px;
        padding:0px;
        font-size:14px;
    }
    .text-center{
        text-align:center !important;
    }
    .text-center > div{
        margin:auto;
    }
    main{
        padding:0 30px;
    }
    td{
        vertical-align:top;
    }
    .bg-fixed{
        position:fixed;
        top:40%;
        left:0;
        right:0;
        transform:translateY(-50%);
        z-index:2;
        text-align:center;
    }
    .bg-fixed{
        opacity:0.1;
    }
    .block{
        display:block;
        margin:auto;
    }
  </style>
</head>
<body>
    <div class="bg-fixed">
        <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="400px">
    </div>
  <header>
    <div class="title-header-wrapper">
        <h3>DAFTAR SIMAK</h3>
    </div>
  </header>
  <main>
    <table width="100%">
        <tbody>
            <tr>
                <td>
                    <b><?=$applicationLetter->application_simak->element_struktural;?> ELEMENT STRUKTURAL</b>
                </td>
            </tr>
            <tr>
                <td>
                    <b><?=$applicationLetter->application_simak->pondasi;?> Pondasi</b><br><br>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="3%">1.</td>
                            <td width="15%">Lokasi</td>
                            <td width="2%">:</td>
                            <td colspan="5"><?=$applicationLetter->application_simak->lokasi;?></td>
                        </tr>
                        <tr>
                            <td width="3%">2.</td>
                            <td width="15%">Bagian</td>
                            <td width="2%">:</td>
                            <td width="35%"><?=$applicationLetter->application_simak->bagian;?></td>
                            <td width="3%">3.</td>
                            <td width="20%">Tahun dibangun</td>
                            <td width="2%">:</td>
                            <td><?=$applicationLetter->application_simak->tahun;?></td>
                        </tr>
                        <tr>
                            <td width="3%">4.</td>
                            <td width="15%">Panjang (m)</td>
                            <td width="2%">:</td>
                            <td width="35%"><?=$applicationLetter->application_simak->panjang;?></td>
                            <td width="3%">&nbsp;</td>
                            <td width="20%">Tinggi rata-rata</td>
                            <td width="2%">:</td>
                            <td><?=$applicationLetter->application_simak->tinggi;?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="3%">5.</td>
                            <td width="25%">Bahan Bangunan</td>
                            <td width="2%">:</td>
                            <td colspan="5">
                                <table width="100%">
                                    <tbody>
                                        <tr>
                                            <td width="25px">
                                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                                    <?php if($applicationLetter->application_simak->bahan_bangunan == 1):?>
                                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                                    <?php endif;?>
                                                </div> 
                                            </td>
                                            <td style="vertical-align:middle !important;">
                                                Blok beton
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                                    <?php if($applicationLetter->application_simak->bahan_bangunan == 2):?>
                                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                                    <?php endif;?>
                                                </div> 
                                            </td>
                                            <td style="vertical-align:middle !important;">
                                                Batu bata
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                                    <?php if($applicationLetter->application_simak->bahan_bangunan == 3):?>
                                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                                    <?php endif;?>
                                                </div>  
                                            </td>
                                            <td style="vertical-align:middle !important;">
                                                Lain-lain
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="3%">6.</td>
                            <td width="25%">Tipe</td>
                            <td width="2%">:</td>
                            <td colspan="5">
                                <table width="100%">
                                    <tbody>
                                        <tr>
                                            <td width="25px">
                                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                                    <?php if($applicationLetter->application_simak->tipe == 1):?>
                                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                                    <?php endif;?>
                                                </div> 
                                            </td>
                                            <td style="vertical-align:middle !important;">
                                                <i>Basement</i>
                                            </td>
                                            <td width="25px">
                                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                                    <?php if($applicationLetter->application_simak->tipe == 2):?>
                                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                                    <?php endif;?>
                                                </div> 
                                            </td>
                                            <td style="vertical-align:middle !important;">
                                                <i>Crawl space</i>
                                            </td>
                                            <td width="25px">
                                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                                    <?php if($applicationLetter->application_simak->tipe == 3):?>
                                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                                    <?php endif;?>
                                                </div>  
                                            </td>
                                            <td style="vertical-align:middle !important;">
                                                Slab
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="3%">7.</td>
                            <td width="25%" colspan="3">Kerusakan</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table class="table table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th></th>
                                <th width="5%"></th>
                                <th class="text-center" width="15%">Tidak ada</th>
                                <th class="text-center" width="15%">Kecil</th>
                                <th class="text-center" width="15%">Sedang</th>
                                <th class="text-center" width="15%">Besar</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Garis</td>
                                <td>:</td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->garis == 1):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->garis == 2):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->garis == 3):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->garis == 4):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td>Retak Struktur</td>
                                <td>:</td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->retak_struktur == 1):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->retak_struktur == 2):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->retak_struktur == 3):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->retak_struktur == 4):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td>Retak Permukaan</td>
                                <td>:</td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->retak_permukaan == 1):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->retak_permukaan == 2):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->retak_permukaan == 3):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->retak_permukaan == 4):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td>Heaving / Pengangkatan</td>
                                <td>:</td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->pengangkatan == 1):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->pengangkatan == 2):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->pengangkatan == 3):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->pengangkatan == 4):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td>Leaks / kebocoran</td>
                                <td>:</td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->kebocoran == 1):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->kebocoran == 2):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->kebocoran == 3):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->kebocoran == 4):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td>Settlement  / penurunan</td>
                                <td>:</td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->penurunan == 1):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->penurunan == 2):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->penurunan == 3):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->penurunan == 4):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                            </tr>
                            <tr>
                                <td>Sill plate rot</td>
                                <td>:</td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->sill_plate_rot == 1):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->sill_plate_rot == 2):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->sill_plate_rot == 3):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                                <td class="text-center">
                                    <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                        <?php if($applicationLetter->application_simak->sill_plate_rot == 4):?>
                                            <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                        <?php endif;?>
                                    </div>  
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="3%">8.</td>
                            <td width="35%">Kondisi Menyeluruh</td>
                            <td width="11.4%">:</td>
                            <td width="20px" class="text-center">
                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                    <?php if($applicationLetter->application_simak->kondisi == 1):?>
                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                    <?php endif;?>
                                </div>  
                            </td>
                            <td width="13%">Kurang</td>
                            <td width="20px">
                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                    <?php if($applicationLetter->application_simak->kondisi == 2):?>
                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                    <?php endif;?>
                                </div>  
                            </td>
                            <td width="2%">Sedang</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>
                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                    <?php if($applicationLetter->application_simak->kondisi == 3):?>
                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                    <?php endif;?>
                                </div>  
                            </td>
                            <td>Baik</td>
                            <td>
                                <div style="border:1px solid #000;height:15px; width:15px;text-align:center;line-height:15px;position:relative;">
                                    <?php if($applicationLetter->application_simak->kondisi == 4):?>
                                        <img src="<?=WWW_ROOT.DS."img".DS."check.png";?>" style="width:20px;position:absolute;z-index:10;bottom:0;right:-3px;">
                                    <?php endif;?>
                                </div>  
                            </td>
                            <td width="20%">Sangat Baik</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="3%">9.</td>
                            <td>Estimasi sisi masa manfaat ( tahun ) : <b>(<?=$applicationLetter->application_simak->estimasi;?> Tahun)</b></td>
                        </tr>
                        <tr>
                            <td width="3%">10.</td>
                            <td>Kesimpulan : <?=$applicationLetter->application_simak->kesimpulan;?></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="3%">11.</td>
                            <td>Pemeriksa</td>
                            <td width="3%">:</td>
                            <td><?=$applicationLetter->application_simak->pemeriksa;?></td>
                            <td width="15%">(tanda tangan)</td>
                            <td width="15%"><div style="border-bottom:1px solid #000;width:90%;height:15px;"></div></td>
                            <td width="10%">Tanggal</td>
                            <td width="3%">:</td>
                            <td width="15%"><?=date('d-m-Y');?></td>
                        </tr>
                        <tr>
                            <td width="3%"></td>
                            <td>Nama</td>
                            <td width="3%">:</td>
                            <td></td>
                            <td width="15%"></td>
                            <td width="15%"></td>
                            <td width="10%"></td>
                            <td width="3%"></td>
                            <td width="15%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="30%" style="vertical-align:top;">
                                <br>
                                <br>
                                <br>
                                <br>
                                <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                                    'controller' => 'pages',
                                    'action' => 'qrCode',
                                    $applicationLetter->code,
                                    '?' => [
                                        'build-code' => $applicationLetter->sample_house
                                    ]
                                ],true));?>"  width="100px"/>
                            </td>
                            <td style="text-align:right;vertical-align:top;">
                                <div style="width:200px;display:inline-block;text-align:center;margin-left:auto;">
                                    <br>
                                    Disetujui,<br>
                                    <?=$consultant->name;?>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    <br>
                                    Penanggung Jawab<br>
                                    <b><u><?=$consultant->pic_name;?></u><b>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
  </main>
</body>
</html>