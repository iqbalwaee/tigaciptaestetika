<html>
<head>
  <style>
    @page { margin: 40px 20px; font-size:14px; line-height:20px;margin-bottom:80px }
    .main-top{
        display:block;
        margin-bottom:5px;
    }
    .title-header-wrapper{
        text-align:center;
        padding-top:10px;
    }
    .title-header-wrapper h3{
        margin:0px;
        padding:0px;
        margin-bottom:35px;
    }
    .title-header-wrapper p{
        margin:0px;
        padding:0px;
        font-size:14px;
    }
    .text-center{
        text-align:center !important;
    }
    .text-center > div{
        margin:auto;
    }
    main{
        padding:0 30px;
    }
    td{
        vertical-align:top;
    }
    .bg-fixed{
        position:fixed;
        top:40%;
        left:0;
        right:0;
        transform:translateY(-50%);
        z-index:2;
        text-align:center;
    }
    .bg-fixed{
        opacity:0.1;
    }
    .block{
        display:block;
        margin:auto;
    }
  </style>
</head>
<body>
    <div class="bg-fixed">
        <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="400px">
    </div>
  <main>
    
    <table width="100%" cellpadding=0 cellspacing=0>
        <tbody>
            <tr>
                <td colspan="3"><div style="text-align: center;font-weight:bold;">DAFTAR SIMAK PEMERIKSAAN<br><br></div></td>
            </tr>
            <tr>
                <td width="20%">Nama Perusahaan</td>
                <td width="3%">:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:150px;display:inline-block;">PT. elvin</div></td>
            </tr>
            <tr>
                <td>Dusun</td>
                <td>:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:150px;display:inline-block;">jl. Raya cileungsi jonggol </div></td>
            </tr>
            <tr>
                <td>Desa/Kelurahan</td>
                <td>:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:150px;display:inline-block;">CILEUNGSI</div></td>
            </tr>
            <tr>
                <td>Kabupaten/Kota</td>
                <td>:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:150px;display:inline-block;">KABUPATEN BOGOR</div></td>
            </tr>
            <tr>
                <td>Provinsi</td>
                <td>:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:150px;display:inline-block;">JAWA BARAT</div></td>
            </tr>
        </tbody>
    </table>
    <br>
    <?=$applicationLetter->application_simaks_pemeriksaan->data;?>
  </main>
</body>
</html>