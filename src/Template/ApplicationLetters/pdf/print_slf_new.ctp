
<!DOCTYPE html>
<html lang="en" >
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>" />
    <style>
        body{
            padding: 0px;
            margin: 0px;
            color: #000;
            margin: 0px 0px !important; 
        }

        .bg-fixeds {
            display: block;
            position: fixed;
            top: 40%;
            left: 0;
            right: 0;
            transform:translateY(-50%);
            z-index: 11;
            text-align: center; 
        }
        .bg-fixeds img {
            opacity: 0.15; 
            width:300px;
        }

        .bg-fixedss {
            display: block;
            position: absolute;
            top: 50%;
            left: 0;
            right: 0;
            z-index: 11;
            text-align: center; 
        }
        .bg-fixedss img {
            opacity: 0.15; 
        }
        .surat-pernyataan {
            color: #000;
            border-bottom: none;
            font-size: 14px !important;
            line-height:22px;
            z-index: 10;
            position: relative;
            font-family: 'Times New Roman', Times, serif; 
            padding-left:20px;
            padding-right:20px;
            page-break-after:always;
        }
        .surat-pernyataan:last-child{
            page-break-after:auto;
        }
        .surat-pernyataan .kop-surat {
            border-bottom:3px double #333;
            padding-bottom:10px;
        }
        .table-kop{
            width:100%;
        }
        .table-kop > tbody td{
            vertical-align:top;
        }
        .surat-pernyataan .kop-surat .title-kop-box {
            display: block;
        }

        .surat-pernyataan .kop-surat .kop-surat-logo {
            padding: 0px;
            text-align: center;
        }

        .surat-pernyataan .kop-surat .kop-surat-logo-img {
            width:120px;
        }

        .surat-pernyataan .kop-surat .kop-surat-title {
            text-align: center;
        }

        .surat-pernyataan .kop-surat .kop-surat-title h3 {
            font-weight: bold;
            font-size: 24px !important;
            margin:0 !important;
            padding:0 !important;
            padding-bottom:0px !important;
            margin-bottom:0px !important;
        }

        .surat-pernyataan .kop-surat .kop-surat-title p {
            font-size: 16px !important;
            margin:0 !important;
            padding:0 !important;
            padding-bottom:0px !important;
            margin-bottom:0px !important;
        }

        .surat-pernyataan .kop-surat .kop-surat-title .table {
            width: 300px;
            margin: auto;
            margin-bottom: 20px;
        }

        .surat-pernyataan .kop-surat .kop-surat-title .table td {
            padding: 0px 0px;
            border-top: none;
        }

        .surat-pernyataan .kop-surat .kop-surat-label {
            float: left;
            text-align: right;
            width: 20%;
        }

        .surat-pernyataan .kop-surat .kop-surat-label div {
            text-align: center;
            padding: 5px 10px;
            font-weight: bold;
        }

        .surat-pernyataan .kop-surat .kop-surat-label div.red {
            color: red;
            border: 2px solid red;
        }

        .surat-pernyataan .body-surat-pernyataan {
            padding-top: 15px;
        }

        .surat-pernyataan .body-surat-pernyataan .table-main {
            font-size: 16px;
        }

        .surat-pernyataan .body-surat-pernyataan .table-main td {
            vertical-align: top;
        }

        .surat-pernyataan .divider {
            border-bottom: 2px dashed #ddd;
            margin-bottom: 15px;
        }
        .text-right{
            text-align:right;
        }
        .table-lampiran{
            border-collapse: collapse;
            width:100%;
        }
        .table-lampiran td{
            vertical-align:top;
        }
        .title-slf{
            font-size:16px !important;
            text-align:center;
            text-decoration : underline;
            font-weight:bold;
            margin-bottom:15px;
        }
        sup{
            font-size:8px;
        }
</style>

</head>
<body class="m-page--fluid m--skin- body-print ">
        <div class="bg-fixeds">
            <img src="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>" alt="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>">
        </div>
        <?php
            $dataHouse = explode(",",$applicationLetter->building);
            $countBuild = count($dataHouse);
            $a =1;
        ?>
        <?php for($a = 0; $a < $countBuild; $a++):?>
            <div class="surat-pernyataan">
                
                <div class="kop-surat">
                    <table class="table-kop">
                        <tbody>
                            <tr>
                                <td width="20%">
                                    <img src="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>" alt="logo" class="kop-surat-logo-img">
                                </td>
                                <td style="padding:0 15px;">
                                    <div class="kop-surat-title">
                                        <h3>PT. TIGA CIPTA ESTETIKA</h3>
                                        <p>Office : Jalan  Raya cileungsi Jonggol Komp
                Grand Nusa Indah Ruko Blok A03 - No.14</p>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="body-surat-pernyataan">
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td>
                                    <div class="title-slf">
                                        SURAT PERNYATAAN PEMERIKSAAN KELAIKAN FUNGSI BANGUNAN RUMAH
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="table-header-info" style="padding-bottom:15px;">
                                        <table class="table" style="width:100%;">
                                            <tr>
                                                <td class="text-left" width="25%">Nomor Surat</td>
                                                <td width="5%">:</td>
                                                <td class="text-left"><?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->output_date->format('m')).'/CON/'.$applicationLetter->output_date->format('Y');?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left">Tanggal</td>
                                                <td>:</td>
                                                <td class="text-left"><?=$applicationLetter->output_date->format('d-m-Y');?></td>
                                            </tr>
                                            <tr>
                                                <td class="text-left">Lampiran</td>
                                                <td>:</td>
                                                <td class="text-left">Sertifikat Laik Fungsi</td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="">Pada Hari ini, <b><i><?=$this->Utilities->dayArray($applicationLetter->output_date->format('D'));?></i></b> tanggal <b><i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('d'));?></i></b> bulan <b><i><?=$this->Utilities->monthArray($applicationLetter->output_date->format('m') * 1);?></i></b> tahun <b><i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('Y'));?></i></b>, yang bertanda tangan dibawah ini,</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main" style="padding-left:30px !important;" >
                        <tbody>
                            <tr>
                                <td colspan="3"><div style="height:5px;width:5px;border:1px solid #000;display:inline-block;font-size:6px;background:#000;">&nbsp;</div> Penyedia Jasa Pengkaji Teknis/Penyedia Jasa Konstruksi/Penyedia Jasa Manajemen Konstruksi.</td>
                            </tr>
                            <tr>
                                <td colspan="3" style="padding:15px 30px;">
                                    <table width="100%" class="table-main">
                                        <tbody>
                                            <tr>
                                                <td width="35%"><span style="width:30px;display:inline-block;">a.</span> Nama perusahaan</td>
                                                <td width="2%;">:</td>
                                                <td><?=$consultant->name;?></td>
                                            </tr>
                                            <tr>
                                                <td><span style="width:30px;display:inline-block;">b.</span> Bidang Keahlian</td>
                                                <td>:</td>
                                                <td><?=$consultant->bidang_keahlian;?></td>
                                            </tr>
                                            <tr>
                                                <td><span style="width:30px;display:inline-block;">c.</span> Nomor sertifikat keahlian</td>
                                                <td>:</td>
                                                <td><?=$applicationLetter->supervisor->no_sertifikat_keahlian;?></td>
                                            </tr>
                                            <tr>
                                                <td><span style="width:30px;display:inline-block;">d.</span> Alamat</td>
                                                <td>:</td>
                                                <td>Jalan  Raya cileungsi Jonggol Komp
                Grand Nusa Indah Ruko Blok A03 - No.14</td>
                                            </tr>
                                            <tr>
                                                <td><span style="width:30px;display:inline-block;">e.</span> Telepon</td>
                                                <td>:</td>
                                                <td><?=$consultant->phone;?></td>
                                            </tr>
                                            <tr>
                                                <td><span style="width:30px;display:inline-block;">f.</span> Email</td>
                                                <td>:</td>
                                                <td><?=$consultant->email;?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            
                            <tr>
                                <td colspan="3"></td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td>Telah melaksanakan pemeriksaan kelaikan fungsi bangunan rumah pada:</td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main" style="padding-left:60px !important;" >
                        <tbody>
                            <?php $no = 1;?>
                            <tr>
                                <td width="34%"><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> Nama Bangunan</td>
                                <td width="2%">:</td>
                                <td><?=$applicationLetter->name_house;?></td>
                            </tr>
                            <tr>
                                <td><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> No Blok </td>
                                <td>:</td>
                                <td><?=$dataHouse[$a];?></td>
                            </tr>
                            <tr>
                                <td><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> Alamat bangunan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->address;?></td>
                            </tr>
                            <tr>
                                <td><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> Posisi koordinat</td>
                                <td>:</td>
                                <td><?=$applicationLetter->cordinate_area;?></td>
                            </tr>
                            <tr>
                                <td><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> Fungsi bangunan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->primary_function;?></td>
                            </tr>
                            <tr>
                                <td><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> Klasifikasi kompleksitas</td>
                                <td>:</td>
                                <td><?=$applicationLetter->clasification_kompleksitas;?></td>
                            </tr>
                            <tr>
                                <td><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> Ketinggian Bangunan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->high_building;?> M<sup>2</sup></td>
                            </tr>
                            <tr>
                                <td><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> Jumlah lantai bangunan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->floor_count;?> (<?=$this->Utilities->terbilang($applicationLetter->floor_count);?>) Lantai</td>
                            </tr>
                            <tr>
                                <td><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> Luas lantai bangunan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->surface_building;?> M<sup>2</sup></td>
                            </tr>
                            <tr>
                                <td><span style="width:30px;display:inline-block;"><?=$no++;?>)</span> Luas tanah</td>
                                <td>:</td>
                                <td><?=$applicationLetter->surface_area;?> M<sup>2</sup></td>
                            </tr>
                        </tbody>
                    </table>
                    <div style="page-break-after:always;">&nbsp;</div>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td colspan="2">
                                <br><br><br>
                                Berdasarkan hasil pemeriksaan persyaratan kelaikan fungsi terdiri dari:
                                    <ul style="list-style:none;">
                                    <li>1)	Pemeriksaan dokumen administrative bangunan gedung;</li>
                                    <li>
                                        2)	Pemeriksaan persyaratan teknis bangunan gedung, yaitu:
                                        <ul style="list-style:none;">
                                            <li>
                                                a.	Pemeriksaan persyaratan tata bangunan, meliputi:
                                                <ul style="list-style:none;">
                                                    <li>i.	Persyaratan peruntukan bangunan gedung;</li>
                                                    <li>ii.	Persyaratan intensitas bangunan gedung; dan</li>
                                                    <li>iii.	Persyaratan arsitektur gedung;</li>
                                                </ul>
                                            </li>
                                            <li>
                                                b.	Pemeriksaan persyaratan keandalan bangunan gedung, meliputi:
                                                <ul style="list-style:none;">
                                                    <li>i.	Persyaratan keselamatan;</li>
                                                    <li>ii.	Persyaratan kesehatan;</li>
                                                    <li>iii.	Persyaratan kenyamanan; dan</li>
                                                    <li>iv.	Persyaratan kemudahan.</li>
                                            </li>
                                        </ul>
                                    </li>
                                    </ul>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br>
                                    Dengan ini menyatakan bahwa:
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="font-weight:bold;text-align:center;margin-top:30px; margin-bottom:15px;font-size:16px !important;">
                                        BANGUNAN RUMAH DINYATAKAN LAIK FUNGSI
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div style="text-align:justify !important;">
                                        Sesuai kesimpulan dari analisis dan evaluasi terhadap hasil pemeriksaan dokumen dan pemeriksaan kondisi bangunan rumah sebagaimana termuat dalam laporan pemeriksaan kelaikan fungsi bangunan rumah terlampir.<br><br>

                                        Surat pernyataan ini berlaku sepanjang tidak ada perubahan yang di lakukan oleh pemilik bangunan rumah/pengguna bangunan rumah terhadap bangunan rumah atau penyebab gangguan lainnya yang di buktikan kemudian.<br><br>

                                        Demikian surat pernyataan ini di buat dengan penuh tanggung jawab professional sesuai dengan ketentuan undang-undang nomor 2 tahun 2017 tentang jasa konstruksi.<br><br>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <br><br>
                                <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                                    'controller' => 'pages',
                                    'action' => 'qrCode',
                                    $applicationLetter->code,
                                    '?' => [
                                        'build-code' => $dataHouse[$a]
                                    ]
                                ],true));?>"  width="100px"/>
                                </td>
                                <td style="text-align:right;">
                                    <div style="width:350px;display:inline-block;text-align:center;margin-left:auto;">
                                        Bogor, <?=$this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));?><br>
                                        Pelaksana Pemeriksaan Kelaikan Fungsi,
                                        <?php if(!empty($userData)):?>
                                            <?php if($userData['group_id'] != 5):?>
                                                <br>
                                                <img src="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>"  alt="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>" width="200px"><br>
                                            <?php else:?>
                                                <br><br><br><br><br>
                                            <?php endif;?>
                                            <?php else:?>
                                                <br>
                                                <img src="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>"  alt="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>" width="200px"><br>
                                            <?php endif;?>
                                        <b><u><?=$consultant->pic_name;?></u><b><br><br><br>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </div>
        <?php endfor;?>
    </body>
</html>