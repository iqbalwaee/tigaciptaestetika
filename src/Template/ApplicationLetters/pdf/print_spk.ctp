<html>
<head>
  <style>
    @page { margin: 120px 40px; font-size:12px; line-height:20px; }
    header { position: fixed; top: -120px; left: 0px; right: 0px;height: 120px; border-bottom:double 3px  #333; }
    footer { position: fixed; bottom: -100px; left: 0px; right: 0px; height: 90px; text-align:center; }
    .main-top{
        display:block;
        margin-bottom:5px;
    }
    .logo-wrapper{
        display: inline-block;
        width:90px;
        float:left;
    }
    .title-header-wrapper{
        margin-left:20px;
        display: inline-block;
        padding-top:10px;
    }
    .title-header-wrapper h3{
        margin:0px;
        padding:0px;
        margin-bottom:5px;
    }
    .title-header-wrapper p{
        margin:0px;
        padding:0px;
        font-size:14px;
    }
    main{
        padding:0 30px;
    }
    td{
        vertical-align:top;
    }
    .bg-fixed{
        position:fixed;
        top:40%;
        left:0;
        right:0;
        transform:translateY(-50%);
        z-index:2;
        text-align:center;
    }
    .bg-fixed{
        opacity:0.1;
    }
    .block{
        display:block;
        margin:auto;
    }
  </style>
</head>
<body>
    <div class="bg-fixed">
        <img src="<?=ROOT.$defaultAppSettings['App.Kop.Logo'];?>" alt="logo" class="logo" width="400px">
    </div>
  <header>
    <div class="main-top">
    AHU-0000269.AH.01.08.TAHUN 2017
    </div>
    <div class="logo-wrapper">
        <img src="<?=ROOT.$defaultAppSettings['App.Kop.Logo'];?>" alt="logo" class="logo" width="80px">
    </div>
    <div class="title-header-wrapper">
        <h3>DEWAN PENGURUS PUSAT</h3>
        <?=$defaultAppSettings['App.Kop.Header'];?>
    </div>
  </header>
  <footer><?=$defaultAppSettings['App.Kop.Footer'];?></footer>
  <main>
    <table width="100%">
        <tr>
            <td colspan="3" style="text-align:right;">Jakarta, <?=$this->Utilities->indonesiaDateFormat($applicationLetter->approved_by_dpp_date->format('Y-m-d'));?></td>
        </tr>
        <tr>
            <td width="15%">No.</td>
            <td style="">:</td>
            <td width="83%"><?=$consultant->code;?>-<?=$applicationLetter->spk_code;?>/<?=$applicationLetter->approved_by_dpp_date->format('m');?>/CON/<?=$applicationLetter->approved_by_dpp_date->format('y');?></td>
        </tr>
        <tr>
            <td>Perihal</td>
            <td>:</td>
            <td><b>Surat Perintah Tugas</b></td>
        </tr>
        <tr>
            <td>Lampiran</td>
            <td>:</td>
            <td><b>Site Plan</b></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3" class="body">
                <p>
                    Kepada Yth.<br>
                    <b><?=$consultant->name;?></b><br>
                    di - <br>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<u>Tempat</u>.
                </p>
                <p>
                    Dengan hormat,
                    <br><br>
                    Sehubungan dengan Surat pengajuan dari <?=$applicationLetter->developer->name;?> Nomor : <?=$applicationLetter->code;?> tanggal <?=$this->Utilities->indonesiaDateFormat($applicationLetter->created->format('Y-m-d'));?>, maka dengan ini kami menugaskan <?=$consultant->name;?> melakukan pengawasan pada:
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <table width="100%">
                    <tr>
                        <td width="25%">Nama Perumahaan</td>
                        <td width="2%">:</td>
                        <td><?=$applicationLetter->name_house;?></td>
                    </tr>
                    <tr>
                        <td>Lokasi Perumahaan</td>
                        <td>:</td>
                        <td><?=$applicationLetter->address;?> Kelurahan <?=$applicationLetter->village->name;?> Kecamatan <?=$applicationLetter->district->name;?>, <?=$applicationLetter->city->name;?> <?=$applicationLetter->province->name;?> </td>
                    </tr>
                    <tr>
                        <td width="25%">Nama Pengembang</td>
                        <td width="2%">:</td>
                        <td><?=$applicationLetter->developer->name;?></td>
                    </tr>
                    <tr>
                        <td width="25%">Alamat Pengembang</td>
                        <td width="2%">:</td>
                        <td><?=$applicationLetter->developer->address;?></td>
                    </tr>
                    <tr>
                        <td width="25%">No. Telepon</td>
                        <td width="2%">:</td>
                        <td><?=$applicationLetter->developer->phone;?></td>
                    </tr>
                    <tr>
                        <td width="25%">Email</td>
                        <td width="2%">:</td>
                        <td><?=$applicationLetter->developer->email;?></td>
                    </tr>
                    <tr>
                        <td width="25%">Contact Person</td>
                        <td width="2%">:</td>
                        <td><?=$applicationLetter->developer->pic_phone;?> (<?=$applicationLetter->developer->pic;?>)</td>
                    </tr>
                    <tr>
                        <td width="25%">Jumlah Unit</td>
                        <td width="2%">:</td>
                        <td><?=count(explode(",",$applicationLetter->building));?> unit</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <p>
                    Demikian Surat Perintah Tugas ini agar dapat dilaksanakan sebagaimana mestinya.
                    <br><br>
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:right;margin-left:auto;">
                <div style="width:250px;display:inline-block;text-align:center;float:right;">
                    <img src="<?=ROOT.$defaultAppSettings['App.DPP.PIC.Signature'];?>" alt="logo" class="" width="180px">
                    <br>
                    (<?=$defaultAppSettings['App.DPP.PIC'];?>)
                    <div class="clear:both;display:block;"></div>
                </div>
            </td>
        </tr>
    </table>
  </main>
</body>
</html>