<style>
    th,td{
        vertical-align:top;
    }
</style>
<div class="surat-pernyataan">
        
    <div class="kop-surat">
        &nbsp;
    </div>
    <div class="body-surat-pernyataan">
        <div class="title-body">
            <h4>SURAT PERNYATAAN</h4>
            <div class="sub-title">
                <h3>KESESUAIAN SPESIFIKASI, MUTU, DAN KELAYAKAN</h3>
            </div>
        </div>
        <div class="content-body">
            <table width="100%" style="">
                <tbody>
                <tr>
                    <td colspan="3">Saya yang bertanda tangan dibawah ini :</td>
                </tr>
                <tr>
                    <td width="40%" style="padding-left:30px;">Nama</td>
                    <td width="5px">:</td>
                    <td><?=$applicationLetter->developer->ceo_name;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">NIK</td>
                    <td>:</td>
                    <td><?=$applicationLetter->developer->nik_ceo;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Jabatan</td>
                    <td>:</td>
                    <td>DIREKTUR UTAMA</td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Nama Perusahaan</td>
                    <td>:</td>
                    <td><?=$applicationLetter->developer->name;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Alamat Perusahaan</td>
                    <td>:</td>
                    <td><?=$applicationLetter->developer->address;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Provinsi</td>
                    <td>:</td>
                    <td><?=$applicationLetter->developer->dpp_from->name;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Nama Perumahaan</td>
                    <td>:</td>
                    <td><?=$applicationLetter->name_house;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Alamat Perumahaan</td>
                    <td>:</td>
                    <td><?=$applicationLetter->address;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Kecamatan</td>
                    <td>:</td>
                    <td><?=$applicationLetter->district->name;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Kabupaten</td>
                    <td>:</td>
                    <td><?=$applicationLetter->city->name;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Provinsi</td>
                    <td>:</td>
                    <td><?=$applicationLetter->province->name;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Letak Bangunan</td>
                    <td>:</td>
                    <td>
                    <?php
                        $dataHouse = explode(",",$applicationLetter->building);
                        $dataCount = count($dataHouse) -1;
                        $a = 0;
                        $markred = $this->request->query('build-code');
                        foreach($dataHouse as $r):
                            $a++;
                    ?>
                        <?php if($r == $markred):?>
                            <strong style="color:red;"><?=$r;?></strong>
                        <?php else:?>
                            <?=$r;?>
                        <?php endif;?>
                        <?php
                            $dataCount = $dataCount;
                        ?>
                        <?=($dataCount != 0 ? '|' : '');?>
                    <?php endforeach;?>    
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Rumah Sample</td>
                    <td>:</td>
                    <td><?=$applicationLetter->sample_house;?></td>
                </tr>
                <tr>
                    <td style="padding-left:30px;">Jumlah Unit Rumah</td>
                    <td>:</td>
                    <td><?=$a;?> Unit</td>
                </tr>
                <tr>
                    <td><br></td>
                </tr>
                <tr>
                    <td colspan="3">
                        dengan ini menyatakan sesungguhnya :<br>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left:40px;">
                            <span style="width:30px">1.</span> <b><?=$consultant->name;?></b> selaku Pengawas telah melakukan tugas pengawasan kepada <b><?=$applicationLetter->name_house;?></b>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left:40px;">
                            <span style="width:30px">2.</span>
                                Konsultan Pengawas, pada pelaksanaannya telah melakukan uji sampling pada Nomor Rumah Blok <b><?=$applicationLetter->sample_house;?></b>, yang menjadi contoh/acuan bangunan rumah untuk 1 periode ( <?=$a;?> unit rumah).
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left:40px;">
                            <span style="width:30px">3.</span>
                            Jika terjadi perubahan Spesifikasi, Mutu dan Kelaikan (tidak seusai rumah contoh/acuan) dikemudian hari pada <b><?=$applicationLetter->name_house;?></b> maka selaku Pengembang Perumahan bertanggung jawab terhadap dampak yang diakibatkan.<div class="display-on-p"></div>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left:40px;">
                            <span style="width:30px">4.</span>
                            Apabila terjadi kerusakan pada fisik bangunan rumah dan kelengkapannya paling lama 100 (seratus) hari sejak Akad Kredit rumah ditandatangani, <b><?=$applicationLetter->developer->name;?></b> bersedia untuk memperbaikinya hingga benar-benar dapat berfungsi dengan baik, paling lama 3 (tiga) bulan setelah adanya laporan/aduan dari nasabah/debitur.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left:40px;">
                            <span style="width:30px">5.</span>
                            Apabila Prasarana, Sarana, dan Utilitas Umum belum selesai maka <b><?=$applicationLetter->developer->name;?></b> wajib menyediakannya sesuai dengan peraturan perundang- undangan.
                    </td>
                </tr>
                <tr>
                    <td colspan="3" style="padding-left:40px;">
                            <span style="width:30px">6.</span>
                            Apabila Prasarana, Sarana dan Utilitas Umum mengalami kerusakan, <b><?=$applicationLetter->developer->name;?></b> bersedia untuk memperbaikinya hingga benar-benar dapat berfungsi dengan baik, sampai dengan Prasarana, Sarana dan Utilitas Umum diserahkan kepada pemerintah daerah sesuai peraturan perundang-undangan.
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        Demikian pernyataan ini dibuat dengan sebenar-benarnya dan dapat dipertanggungjawabkan.<br><br><br><br>
                    </td>
                </tr>
                <tr>
                    <td colspan="3" class="">
                        <div style="width:250px;display:inline-block;text-align:center;float:right;">
                            <b>Pengembang Perumahan</b><br>
                            <?=$applicationLetter->developer->name;?>
                            <div style="font-size:11px;"><br><br>Materai Rp.6.000,-<br><br><br></div>
                            <b><u><?=$applicationLetter->developer->ceo_name;?></u></b><br>
                            Direktur Utama
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    
</div>