
<!DOCTYPE html>
<html lang="en" >
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>" />
    <style>
        body{
            padding: 0px;
            margin: 0px;
            color: #000;
            margin: 0px 0px !important; 
        }

        .bg-fixeds {
            display: block;
            position: fixed;
            top: 40%;
            left: 0;
            right: 0;
            transform:translateY(-50%);
            z-index: 11;
            text-align: center; 
        }
        .bg-fixeds img {
            opacity: 0.15; 
            width:300px;
        }

        .bg-fixedss {
            display: block;
            position: absolute;
            top: 50%;
            left: 0;
            right: 0;
            z-index: 11;
            text-align: center; 
        }
        .bg-fixedss img {
            opacity: 0.15; 
        }
        .surat-pernyataan {
            color: #000;
            border-bottom: none;
            font-size: 14px !important;
            z-index: 10;
            position: relative;
            font-family: 'Times New Roman', Times, serif; 
        }
        .surat-pernyataan .kop-surat {
            
        }
        .table-kop{
            width:100%;
        }
        .table-kop > tbody td{
            vertical-align:top;
        }
        .surat-pernyataan .kop-surat .title-kop-box {
            display: block;
        }

        .surat-pernyataan .kop-surat .title-kop-box .box-red {
            font-size: 16px;
            color: red;
            padding: 15px;
            display: inline-block;
            padding: 5px 10px;
            border: 2px solid red;
        }

        .surat-pernyataan .kop-surat .kop-surat-logo {
            padding: 0px;
            text-align: center;
        }

        .surat-pernyataan .kop-surat .kop-surat-logo-img {
            width:150px;
        }

        .surat-pernyataan .kop-surat .kop-surat-title {
            text-align: center;
        }

        .surat-pernyataan .kop-surat .kop-surat-title h3 {
            font-weight: bold;
            font-size: 18px !important;
            margin:0 !important;
            padding:0 !important;
            padding-bottom:15px !important;
        }

        .surat-pernyataan .kop-surat .kop-surat-title .table {
            width: 300px;
            margin: auto;
            margin-bottom: 20px;
        }

        .surat-pernyataan .kop-surat .kop-surat-title .table td {
            padding: 0px 0px;
            border-top: none;
        }

        .surat-pernyataan .kop-surat .kop-surat-label {
            float: left;
            text-align: right;
            width: 20%;
        }

        .surat-pernyataan .kop-surat .kop-surat-label div {
            text-align: center;
            padding: 5px 10px;
            font-weight: bold;
        }

        .surat-pernyataan .kop-surat .kop-surat-label div.red {
            color: red;
            border: 2px solid red;
        }

        .surat-pernyataan .body-surat-pernyataan {
            padding-top: 15px;
        }

        .surat-pernyataan .body-surat-pernyataan .table-main {
            font-size: 14px;
        }

        .surat-pernyataan .body-surat-pernyataan .table-main td {
            vertical-align: top;
        }

        .surat-pernyataan .divider {
            border-bottom: 2px dashed #ddd;
            margin-bottom: 15px;
        }
        .text-right{
            text-align:right;
        }
        .table-lampiran{
            border-collapse: collapse;
            width:100%;
        }
        .table-lampiran td{
            vertical-align:top;
        }
        .table-main-detail{
            border:1px solid #000;
            border-collapse: collapse;
            font-size:11px;
            line-height:11px;
            width:100%;
        }
        .table-main-detail tr.body-detail td{
            border-right:1px solid #000;
            border-bottom:1px solid #000;
            vertical-align:middle;
            text-align:center;
            height:20px;
            font-size:8px;
            line-height:9px;
        }
        .table-main-detail tr.header-detail-tr td{
            border-right:1px solid #000;
            border-bottom:1px solid #000;
            padding:5px 5px;
            height:30px;
            vertical-align:middle;
            text-align:center;
            font-weight:bold;
        }
        .table-main-detail tr.body-detail td:last-child{
            border-right:none;
        }
        .table-main-detail tr.body-detail:last-child td{
            border-bottom:none;
        }
        sup{
            font-size:8px;
        }
</style>

</head>
<body class="m-page--fluid m--skin- body-print ">
        <div class="bg-fixeds">
            <img src="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>" alt="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>">
        </div>
        <?php
            $dataHouse = explode(",",$applicationLetter->building);
            $countBuild = count($dataHouse);
            $a =1;
        ?>

        <div class="surat-pernyataan">
            
            <div class="kop-surat">
                <table class="table-kop">
                    <tbody>
                        <tr>
                            <td width="20%">
                                <img src="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>" alt="logo" class="kop-surat-logo-img">
                            </td>
                            <td style="padding:0 15px;">
                                <div class="kop-surat-title">
                                    <h3>
                                        SURAT PERNYATAAN PEMERIKSAAN KELAIKAN FUNGSI BANGUNAN GEDUNG
                                    </h3>
                                    <table class="table">
                                        <tr>
                                            <td class="text-left" width="40%">Nomor Surat</td>
                                            <td>:</td>
                                            <td class="text-left"><?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->output_date->format('m')).'/CON/'.$applicationLetter->output_date->format('Y');?></td>
                                        </tr>
                                        <tr>
                                            <td class="text-left">Tanggal</td>
                                            <td>:</td>
                                            <td class="text-left"><?=$applicationLetter->output_date->format('d-m-Y');?></td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td width="20%">
                                <div class="title-kop-box">
                                    <div class="box-red">
                                        UNTUK BANK
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="body-surat-pernyataan">
                <table width="100%" class="table-main">
                    <tbody>
                        <tr>
                            <td style="">Pada Hari ini, <i><?=$this->Utilities->dayArray($applicationLetter->output_date->format('D'));?></i> tanggal <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('d'));?></i> bulan <i><?=$this->Utilities->monthArray($applicationLetter->output_date->format('m') * 1);?></i> tahun <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('Y'));?></i>, yang bertanda tangan dibawah ini,</td>
                        </tr>
                    </tbody>
                </table>
                <table width="100%" class="table-main">
                    <tbody>
                        <tr>
                            <td colspan="3"><div style="height:8px;width:8px;border:2px solid #000;display:inline-block;font-size:6px;">&nbsp;</div> Penyedia jasa Pengawasan/MK/Instansi teknis Pembina penyelenggara  bangunan gedung*</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding:15px 30px;">
                                <table width="100%" class="table-main">
                                    <tbody>
                                        <tr>
                                            <td width="40%"><b>a.</b> Nama penanggung jawab</td>
                                            <td width="5%;">:</td>
                                            <td><b><?=$consultant->pic_name;?></b></td>
                                        </tr>
                                        <tr>
                                            <td><b>b.</b> Nama Perusahaan/instansi teknis*</td>
                                            <td>:</td>
                                            <td><b><?=$consultant->name;?></b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                        <tr>
                            <td colspan="3">Telah melaksanakan pemeriksaan kelaikan fungsi bangunan gedung pada</td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
                <div style="padding-left:20px;padding-right:20px;">
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td>1. Bangunan Gedung</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td width="45%" style="padding-left:30px;">a. Fungsi Utama</td>
                                <td width="5%">:</td>
                                <td><?=$applicationLetter->primary_function;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">b. Fungsi Tambahan</td>
                                <td>:</td>
                                <td>Rumah Hunian</td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">c. Jenis bangunan gedung</td>
                                <td>:</td>
                                <td><?=$applicationLetter->house_type->code;?>/<?=$applicationLetter->surface_area;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">d. Nama bangunan gedung</td>
                                <td>:</td>
                                <td><?=$applicationLetter->name_house;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">e. Nomor pendaftaran bangunan gedung</td>
                                <td>:</td>
                                <td>-</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td>2. Lokasi Bangunan Gedung</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td width="45%;"  style="padding-left:30px;">a. Kampung</td>
                                <td width="5%">:</td>
                                <td><?=$applicationLetter->village_name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">b. Kelurahan/desa</td>
                                <td>:</td>
                                <td><?=$applicationLetter->village->name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">c. Kecamatan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->district->name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">d. Kabupaten/Kota</td>
                                <td>:</td>
                                <td><?=$applicationLetter->city->name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">e. Provinsi</td>
                                <td>:</td>
                                <td><?=$applicationLetter->province->name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">f. Alamat lokasi terletak di</td>
                                <td>:</td>
                                <td><?=$applicationLetter->address;?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td>3. Permohonan</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td width="45%;" style="padding-left:30px;">a. Penerbitan Sertifikat Laik Fungsi</td>
                                <td width="5%">:</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">b. Perpanjangan Sertifikat Laik Fungsi </td>
                                <td>:</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">c. Perpanjangan ke</td>
                                <td>:</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <table width="100%" class="table-main">
                    <tbody>
                        <tr>
                            <td>Dengan ini menyatakan bahwa</td>
                        </tr>
                    </tbody>
                </table>
                
                <table width="100%" class="table-main">
                    <tbody>
                        <tr>
                            <td width="45%;" style="padding-left:30px;">1. Persyaratan administratif</td>
                            <td width="5%">:</td>
                            <td><?=$applicationLetter->application_output->persyaratan_administratif;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">2. Persyaratan teknis</td>
                            <td>:</td>
                            <td></td>
                        </tr>
                    </tbody>
                </table>
                <div style="padding-left:20px;padding-right:20px;">
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td width="45%;"  style="padding-left:30px;">a. Fungsi bangunan gedung</td>
                                <td width="5%">:</td>
                                <td><?=$applicationLetter->application_output->fungsi_bangunan;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">b. Peruntukan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->application_output->peruntukan;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">c. Tata bangunan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->application_output->tata_bangunan;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">d. Kelaikan fungsi bangunan gedung dinyatakan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->application_output->kelaikan;?></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <br>
                                    Sesuai dengan kesimpulan berdasarkan analisis terhadap Daftar Simak Pemeriksaan Kelaiakn Fungsi Bangunan Gedung terlampir.
                                    <br>
                                    <br>
                                </td>
                            </tr> 
                            <tr>
                                <td colspan="3" >
                                Surat pernyataan ini berlaku sepanjang tidak ada perubahan yang dilakukan Pemilik/Pengguna yang mengubah sistem dan/atau  spefisikasi teknis, atau  gangguan penyebab lainnya yang dibuktikan kemudian
                                <br>
                                <br>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div style="page-break-after:always;">&nbsp;</div>
                <table width="100%" class="table-main">
                    <tbody>
                        <tr>
                            <td >
                                
                Selanjutnya pemilik/pengguna bangunan gedung dapat mengurus permohonan penerbitan Sertifikat Laik Fungsi bangunan gedung.<br><br>Demikian surat pernyataan ini dibuat dengan penuh tanggung jawab profesional.
                            <br>
                            <br>
                            <br>
                            <br>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right;">
                                <table width="100%">
                                    <tr>
                                        <td width="50%" style="text-align:left">
                                            <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                                                'controller' => 'pages',
                                                'action' => 'qrCode',
                                                $applicationLetter->code
                                            ],true));?>"  width="100px"/>
                                        </td>
                                        <td>
                                            <div style="width:250px;display:inline-block;text-align:center;margin-left:auto;">
                                                Jakarta, <?=$this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));?><br>
                                                Penyedia Jasa Pengawasan <br>selaku Penanggung Jawab
                                                <?php if(!empty($userData)):?>
                                                    <?php if($userData['group_id'] != 5):?>
                                                        <br>
                                                        <img src="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>"  alt="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>" width="200px"><br>
                                                    <?php else:?>
                                                        <br><br><br><br><br><br><br>
                                                    <?php endif;?>
                                                    <?php else:?>
                                                        <br>
                                                        <img src="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>"  alt="<?=ROOT.DS.$consultant->pic_signature_dir.$consultant->pic_signature;?>" width="200px"><br>
                                                    <?php endif;?>
                                                <b><u><?=$consultant->pic_name;?></u><b><br><br><br>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                                
                            </td>
                        </tr>
                        <!-- <tr>
                            <td class="text-center">
                                <div style="width:80%;display:inline-block;text-align:center;">
                                    Disetujui, <br>PEMERINTAH PROVINSI/KABUPATEN/KOTA...........................................<br>DINAS...................................................................................
                                    <br><br><br><br><br><br>
                                    ....................................................<br>NIP :  .....................................<br><br><br>
                                </div>
                            </td>
                        </tr> -->

                        <tr>
                            <td >
                                Keterangan : * Dipilih yang sesuai dengan permohonan dan coret yang tidak sesuai, jika pengisian secara manual. Jika pengisian menggunakan software, yang tidak dipilih didelete (hapus).
                            </td>
                        </tr> 
                        <tr>
                            <td class="text-right">
                                
                            </td>
                        </tr> 
                    </tbody>
                </table>

                <div style="page-break-after:always;">&nbsp;</div>
                <b>LAMPIRAN</b><br><br>
                <table class="table-main" width="100%">
                    <tbody>
                        <tr>
                            <td width="30%">No. Surat Pernyataan</td>
                            <td width="2%">:</td>
                            <td><?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->output_date->format('m')).'/CON/'.$applicationLetter->output_date->format('Y');?></td>
                        </tr>
                        <tr>
                                <td>Kampung</td>
                                <td>:</td>
                                <td><?=$applicationLetter->village_name;?></td>
                            </tr>
                            <tr>
                                <td>Kelurahan/desa</td>
                                <td>:</td>
                                <td><?=$applicationLetter->village->name;?></td>
                            </tr>
                            <tr>
                                <td>Kecamatan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->district->name;?></td>
                            </tr>
                            <tr>
                                <td>Kabupaten/Kota</td>
                                <td>:</td>
                                <td><?=$applicationLetter->city->name;?></td>
                            </tr>
                            <tr>
                                <td>Provinsi</td>
                                <td>:</td>
                                <td><?=$applicationLetter->province->name;?></td>
                            </tr>
                            <tr>
                                <td>Alamat lokasi di</td>
                                <td>:</td>
                                <td><?=$applicationLetter->address;?></td>
                            </tr>
                    </tbody>
                </table>
                <br><br>
                <table class="table-lampiran" width="100%">
                    <tbody>
                        <tr>
                            <td width="49%">
                                <table class="table-main-detail" width="100%">
                                    <tbody>
                                        <tr class="header-detail-tr">
                                            <td width="8%">No</td>
                                            <td>Cluster/Blok/Kavling</td>
                                            <td width="18%">Luas Bangunan</td>
                                            <td width="18%">Luas Tanah</td>
                                        </tr>
                                        <?php for($i=1;$i<=25;$i++):?>
                                            <?php if(!empty($dataHouse[$i-1])):?>
                                                <tr class="body-detail">
                                                    <td><?=$i;?></td>
                                                    <td><?=$dataHouse[$i-1];?></td>
                                                    <td><?=$applicationLetter->house_type->code;?>M&sup2;</td>
                                                    <td><?=$applicationLetter->surface_area;?>M&sup2;</td>
                                                </tr>
                                            <?php else:?>
                                                <tr class="body-detail">
                                                    <td><?=$i;?></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            <?php endif;?>
                                            
                                        <?php endfor;?>
                                    </tbody>
                                </table>
                            </td>
                            <td width="2%">&nbsp;</td>
                            <td>
                                <table class="table-main-detail" width="100%">
                                    <tbody>
                                        <tr class="header-detail-tr">
                                            <td width="8%">No</td>
                                            <td>Cluster/Blok/Kavling</td>
                                            <td width="18%">Luas Bangunan</td>
                                            <td width="18%">Luas Tanah</td>
                                        </tr>
                                        <?php for($i=26;$i<=50;$i++):?>
                                            <?php if(!empty($dataHouse[$i-1])):?>
                                                <tr class="body-detail">
                                                    <td><?=$i;?></td>
                                                    <td><?=$dataHouse[$i-1];?></td>
                                                    <td><?=$applicationLetter->house_type->code;?>M&sup2;</td>
                                                    <td><?=$applicationLetter->surface_area;?>M&sup2;</td>
                                                </tr>
                                            <?php else:?>
                                                <tr class="body-detail">
                                                    <td><?=$i;?></td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                </tr>
                                            <?php endif;?>
                                        <?php endfor;?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
                *) Tabel ini menjadi bagian dari surat Pernyataan Pemeriksaan Kelaikan apabila dipergunakan sebagai 1 lampiran 
untuk 1 Proyek Perumahan
                                <br>
            <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                'controller' => 'pages',
                'action' => 'qrCode',
                $applicationLetter->code
            ],true));?>"  width="100px"/>
            </div>
            
        </div>
    </body>
</html>