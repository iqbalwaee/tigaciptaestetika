<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Sertifikat Laik Fungsi
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>			
                <li class="m-portlet__nav-item">
                    <a href="#" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print" class="m-portlet__nav-link m-portlet__nav-link--icon" onclick="window.print();"><i class="la la-print"></i></a>	
                </li>						
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <?php
            $dataHouse = explode(",",$applicationLetter->building);
            $countBuild = count($dataHouse);
            $a =1;
        ?>

        <div class="bg-fixed">
            <img src="<?=$this->Utilities->generateUrlImage($consultant->picture_dir,$consultant->picture);?>" alt="logo" class="logo" width="400px">
        </div>
        <?php foreach($dataHouse as $key => $r):?>
            <div class="surat-pernyataan">
                <div class="kop-surat">
                    <div class="title-kop-box">
                        <div class="box-red">
                            UNTUK BANK
                        </div>
                    </div>
                    <div class="kop-surat-logo">
                        <img src="<?=$this->Utilities->generateUrlImage($consultant->picture_dir,$consultant->picture);?>" alt="logo" class="logo-image">
                    </div>
                    <div class="kop-surat-title">
                        <h3>
                            SURAT PERNYATAAN PEMERIKSAAN KELAIKAN FUNGSI BANGUNAN GEDUNG
                        </h3>
                        <table class="table">
                            <tr>
                                <td class="text-left" width="40%">Nomor Surat</td>
                                <td>:</td>
                                <td class="text-left"><?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->output_date->format('m')).'/CON/'.$applicationLetter->output_date->format('Y');?></td>
                            </tr>
                            <tr>
                                <td class="text-left">Tanggal</td>
                                <td>:</td>
                                <td class="text-left"><?=$applicationLetter->output_date->format('d-m-Y');?></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="body-surat-pernyataan">
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td style="">Pada Hari ini, <i><?=$this->Utilities->dayArray($applicationLetter->output_date->format('D'));?></i> tanggal <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('d'));?></i> bulan <i><?=$this->Utilities->monthArray($applicationLetter->output_date->format('m') * 1);?></i> tahun <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('Y'));?></i>, yang bertanda tangan dibawah ini,</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td colspan="3">Penyedia jasa Pengawasan</td>
                            </tr>
                            <tr>
                                <td width="30%">a. Nama penanggung jawab</td>
                                <td width="5%;">:</td>
                                <td><b><?=$consultant->pic_name;?></b></td>
                            </tr>
                            <tr>
                                <td>b. Nama Perusahaan/instansi teknis*</td>
                                <td>:</td>
                                <td><b><?=$consultant->name;?></b></td>
                            </tr>
                            <tr>
                                <td colspan="3"></td>
                            </tr>
                            <tr>
                                <td colspan="3">telah melaksanakan pemeriksaan kelaikan fungsi bangunan gedung pada</td>
                            </tr>
                            <tr>
                                <td colspan="3">1. Bangunan Gedung</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td width="30%" style="padding-left:30px;">a. Fungsi Utama</td>
                                <td width="5%">:</td>
                                <td><?=$applicationLetter->primary_function;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">b. Fungsi Tambahan</td>
                                <td>:</td>
                                <td>-</td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">c. Jenis bangunan gedung</td>
                                <td>:</td>
                                <td><?=$applicationLetter->house_type->code;?>/<?=$applicationLetter->surface_area;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">d. Nama bangunan gedung</td>
                                <td>:</td>
                                <td><?=$applicationLetter->name_house;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">e. Nomor pendaftaran bangunan gedung</td>
                                <td>:</td>
                                <td><?=$r;?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td>2. Lokasi Bangunan Gedung</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td width="30%;"  style="padding-left:30px;">a. Kampung</td>
                                <td width="5%">:</td>
                                <td><?=$applicationLetter->village_name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">b. Kelurahan/desa</td>
                                <td>:</td>
                                <td><?=$applicationLetter->village->name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">c. Kecamatan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->district->name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">d. Kabupaten/Kota</td>
                                <td>:</td>
                                <td><?=$applicationLetter->city->name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">e. Provinsi</td>
                                <td>:</td>
                                <td><?=$applicationLetter->province->name;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">f. Alamat lokasi terletak di</td>
                                <td>:</td>
                                <td><?=$applicationLetter->address;?></td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td>3. Permohonan</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td width="30%;" style="padding-left:30px;">a. Penerbitan Sertifikat Laik Fungsi</td>
                                <td width="5%">:</td>
                                <td>Nomor <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span> Tanggal <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">b. Perpanjangan Sertifikat Laik Fungsi </td>
                                <td>:</td>
                                <td>Nomor <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span> Tanggal <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;"><span style="width:14px;display:inline-block;">&nbsp;</span>Perpanjangan ke</td>
                                <td>:</td>
                                <td>
                                    
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td>Dengan ini menyatakan bahwa</td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td width="30%;">1. Persyaratan administratif</td>
                                <td width="5%">:</td>
                                <td><?=$applicationLetter->application_output->persyaratan_administratif;?></td>
                            </tr>
                            <tr>
                                <td>2. Persyaratan teknis</td>
                                <td>:</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td width="30%;"  style="padding-left:30px;">a. Fungsi bangunan gedung</td>
                                <td width="5%">:</td>
                                <td><?=$applicationLetter->application_output->fungsi_bangunan;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">b. Peruntukan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->application_output->peruntukan;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">c. Tata bangunan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->application_output->tata_bangunan;?></td>
                            </tr>
                            <tr>
                                <td style="padding-left:30px;">d. Kelaikan fungsi bangunan gedung dinyatakan</td>
                                <td>:</td>
                                <td><?=$applicationLetter->application_output->kelaikan;?></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <br>
                                    sesuai dengan kesimpulan berdasarkan analisis terhadap Daftar Simak Pemeriksaan Kelaikan Fungsi Bangunan Gedung terlampir.
                                    <br>
                                </td>
                            </tr> 
                            <tr>
                                <td colspan="3" >
                    Surat pernyataan ini berlaku sepanjang tidak ada perubahan yang dilakukan oleh pemilik/pengguna yang mengubah sistem dan/atau spesifikasi teknis, atau gangguan penyebab lainnya yang dibuktikan kemudian.
                                <br>
                                <br>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table width="100%" class="table-main">
                        <tbody>
                            <tr>
                                <td >
                    Selanjutnya pemilik/pengguna bangunan gedung dapat mengurus permohonan penerbitan Sertifikat Laik Fungsi bangunan gedung.<br>Demikian surat pernyataan ini dibuat dengan penuh tanggung jawab profesional.
                                <br>
                                <br>
                                <br>
                                <br>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;">
                                    <div style="width:350px;display:inline-block;text-align:center;">
                                        Jakarta, <?=$this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));?><br>
                                        Penyedia Jasa Pengawasan <br>selaku Penanggung Jawab
                                        <?php if(!empty($userData)):?>
                                            <?php if($userData['group_id'] == 2):?>
                                                <img src="<?=$this->Utilities->generateUrlImage($consultant->pic_signature_dir,$consultant->pic_signature);?>" width="350px"><br>
                                            <?php else:?>
                                                <br><br><br><br><br><br>
                                            <?php endif;?>
                                            <?php else:?>
                                                <br><br><br><br><br><br>
                                            <?php endif;?>
                                        <b><u><?=$consultant->pic_name;?></u><b><br><br><br>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td class="text-center">
                                    <div style="width:80%;display:inline-block;text-align:center;">
                                        Disetujui, <br>PEMERINTAH PROVINSI/KABUPATEN/KOTA...........................................<br>DINAS...................................................................................
                                        <br><br><br><br><br><br>
                                        ....................................................<br>NIP :  .....................................<br><br><br>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td >
                                    Keterangan : * Dipilih yang sesuai dengan permohonan dan coret yang tidak sesuai, jika pengisian secara manual. Jika pengisian menggunakan software, yang tidak dipilih didelete (hapus).
                                </td>
                            </tr> 
                            <tr>
                                <td class="text-right">
                                    <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                                        'controller' => 'pages',
                                        'action' => 'qrCode',
                                        $applicationLetter->code,
                                        '?' => [
                                            'build-code' => $r
                                        ]
                                    ],true));?>"  width="150px"/>
                                </td>
                            </tr> 
                        </tbody>
                    </table>
                </div>
                
            </div>
            <?php if($a < $countBuild):?>
                <div style="page-break-after:always;">&nbsp;</div>
                <?php $a++;?>
            <?php endif;?>
        <?php endforeach;?>
    </div>
</div>
