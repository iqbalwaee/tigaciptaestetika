<style>
    .table-custom{
        border : 1px solid #000;
    }
    .table-custom > tbody > tr > td{
        padding:10px; 
        border-bottom:1px solid #000;
    }
    .table-custom > tbody > tr:nth-child(odd) >td{
        padding:15px 10px; 
        background-color:#ccc;
    }
    .table-custom > tbody > tr >td:first-child{
        border-right:1px solid #000;
    }
</style>
<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Print Dokumentasi Perumahan
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>			
                <li class="m-portlet__nav-item">
                    <a href="#" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print" class="m-portlet__nav-link m-portlet__nav-link--icon" onclick="window.print();"><i class="la la-print"></i></a>	
                </li>						
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">

        <div class="bg-fixed">
            <img src="<?=$this->Utilities->generateUrlImage($consultant->picture_dir,$consultant->picture);?>" alt="logo" class="logo" width="400px">
        </div>
        <div class="surat-pernyataan">
            <div class="kop-surat-t" style="text-align:center;">
                <div class="kop-surat-title" style="text-align:center;" style="color:#000;">
                    <h1 style="color:#000;font-weight:bold;font-size:28px;">
                        DOKUMENTASI PENGAWASAN
                    </h1>
                </div>
            </div>
            <div class="body-surat-pernyataan">
                <table width="100%" class="table-main">
                    <tbody>
                        <tr>
                            <td width="30%">Nama Perumahan</td>
                            <td width="5%;">:</td>
                            <td><?=$applicationLetter->name_house;?></td>
                        </tr>
                        <tr>
                            <td>Rumah Sample</td>
                            <td>:</td>
                            <td><?=$applicationLetter->sample_house;?></td>
                        </tr>
                        <tr>
                            <td>Dusun</td>
                            <td>:</td>
                            <td><?=$applicationLetter->address;?></td>
                        </tr>
                        <tr>
                            <td>Desa/Kelurahan</td>
                            <td>:</td>
                            <td><?=$applicationLetter->village->name;?></td>
                        </tr>
                        <tr>
                            <td>Kabupaten/Kota</td>
                            <td>:</td>
                            <td><?=$applicationLetter->city->name;?></td>
                        </tr>
                        <tr>
                            <td>Provinsi</td>
                            <td>:</td>
                            <td><?=$applicationLetter->province->name;?></td>
                        </tr>
                        <tr>
                            <td colspan="3"></td>
                        </tr>
                    </tbody>
                </table>
                <br>
                <br>
                <table width="100%" class="table-main table-custom" style="border:1px solid #000;page-break-after: always;">
                    <tbody>
                        <tr>
                            <td width="50%" style="text-align:center;font-weight:bold;">Tampak Depan</td>
                            <td width="50%" style="text-align:center;font-weight:bold;">Jalan Depan</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                    <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->tampak_depan)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->tampak_depan_dir,$applicationLetter->application_picture->tampak_depan,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;">
                                
                            <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->jalan_depan)):?>
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jalan_depan_dir,$applicationLetter->application_picture->jalan_depan,'thumb-');?>" class="img img-responsive" style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;">Ruang Tamu</td>
                            <td style="text-align:center;font-weight:bold;">Kamar Tidur</td>
                        </tr>
                        <tr>
                            <td style="text-align:center;">
                                
                            <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->ruang_tamu)):?>
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->ruang_tamu_dir,$applicationLetter->application_picture->ruang_tamu,'thumb-');?>" class="img img-responsive" style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td style="text-align:center;">
                                
                            <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->kamar_tidur)):?>
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->kamar_tidur_dir,$applicationLetter->application_picture->kamar_tidur,'thumb-');?>" class="img img-responsive" style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;" width="50%">Plafond</td>
                            <td style="text-align:center;font-weight:bold;" width="50%">Pintu, Kusen dan Jendela</td>
                        </tr>
                        <tr>
                            <td  style="text-align:center;">
                                
                            <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->plafond)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->plafond_dir,$applicationLetter->application_picture->plafond,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td  style="text-align:center;">
                                
                            <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->pintu_kusen_jendela)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->pintu_kusen_jendela_dir,$applicationLetter->application_picture->pintu_kusen_jendela,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width="100%" class="table-main table-custom" style="border:1px solid #000;page-break-after: always;">
                    <tbody>
                        <tr>
                            <td style="text-align:center;font-weight:bold;">Toilet</td>
                            <td style="text-align:center;font-weight:bold;">Sumur & Septictank</td>
                        </tr>
                        <tr>
                            <td  style="text-align:center;">
                                    <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->toilet)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->toilet_dir,$applicationLetter->application_picture->toilet,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td  style="text-align:center;">
                                    <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->sumur_septictank)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->sumur_septictank_dir,$applicationLetter->application_picture->sumur_septictank,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;">KWh Listrik</td>
                            <td style="text-align:center;font-weight:bold;">Jaringan Air Bersih</td>
                        </tr>
                        <tr>
                            <td  style="text-align:center;">
                                
                            <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->kwh_listrik)):?>
                                <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->kwh_listrik_dir,$applicationLetter->application_picture->kwh_listrik,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td  style="text-align:center;">
                                
                            <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->jaringan_air)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jaringan_air_dir,$applicationLetter->application_picture->jaringan_air,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;" width="50%">Saluran Pembuangan</td>
                            <td style="text-align:center;font-weight:bold;" width="50%">Tampak Atas 1</td>
                        </tr>
                        <tr>
                            <td  style="text-align:center;">
                                    <div style="min-height:250px">
                                
                                <?php if(!empty($applicationLetter->application_picture->saluran_pembuangan)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->saluran_pembuangan_dir,$applicationLetter->application_picture->saluran_pembuangan,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td  style="text-align:center;">
                                    <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->tampak_atas_1)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->tampak_atas_1_dir,$applicationLetter->application_picture->tampak_atas_1,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:center;font-weight:bold;">Tampak Atas 2</td>
                            <td style="text-align:center;font-weight:bold;">Jalan Utama</td>
                        </tr>
                        <tr>
                            <td  style="text-align:center;">
                                
                            <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->tampak_atas_2)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->tampak_atas_2_dir,$applicationLetter->application_picture->tampak_atas_2,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td  style="text-align:center;">
                                    <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->jalan_utama)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jalan_utama_dir,$applicationLetter->application_picture->jalan_utama,'thumb-');?>" class="img img-responsive"  style="text-align:center;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <table width="100%" class="table-main table-custom" style="border:1px solid #000;">
                    <tbody>
                        <tr>
                            <td style="text-align:center;font-weight:bold;" width="50%">Jalan Komplek</td>
                            <td style="text-align:center;font-weight:bold;" width="50%">Fasilitas Umum</td>
                        </tr>
                        <tr>
                            <td  style="text-align:center;">
                                
                            <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->jalan_komplek)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jalan_komplek_dir,$applicationLetter->application_picture->jalan_komplek,'thumb-');?>" class="img img-responsive"  style="text-align:center;max-height:230px;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                            <td  style="text-align:center;">
                                    <div style="min-height:250px">
                                <?php if(!empty($applicationLetter->application_picture->fasilitas_umum)):?>
                                    <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->fasilitas_umum_dir,$applicationLetter->application_picture->fasilitas_umum,'thumb-');?>" class="img img-responsive"  style="text-align:center;max-height:230px;">
                                <?php else:?>
                                        <h3>TIDAK ADA GAMBAR</h3>
                                <?php endif;?>
                                    </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <br><br>
                <table width="100%" class="table-main">
                    <tbody>
                        <tr>
                            <td width="50%" class="text-left">
                                <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                                    'controller' => 'pages',
                                    'action' => 'qrCode',
                                    $applicationLetter->code
                                ],true));?>"  width="150px"/>
                            </td>
                            <td style="text-align:right;">
                                <div style="width:350px;display:inline-block;text-align:center;">
                                    Jakarta, <?=$this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));?><br>
                                    Penyedia Jasa Pengawasan <br>selaku Penanggung Jawab
                                    <?php if(!empty($userData)):?>
                                        <?php if($userData['group_id'] == 2 || empty($userData)):?>
                                            <?php if($applicationLetter->status == 3):?>
                                                <img src="<?=$this->Url->build('/img/ttd_1.jpg');?>" style="width:300px;"><br>
                                            <?php else:?>
                                                <br><br><br><br><br><br>
                                            <?php endif;?>
                                        <?php else:?>
                                                <?php if($applicationLetter->status == 3):?>
                                                <img src="<?=$this->Url->build('/img/ttd_1.jpg');?>" style="width:300px;"><br>
                                            <?php else:?>
                                                <br><br><br><br><br><br>
                                            <?php endif;?>
                                        <?php endif;?>
                                        <?php else:?>
                                            <br><br><br><br><br><br>
                                        <?php endif;?>
                                    <b><u><?=$consultant->pic_name;?></u><b><br><br><br>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</div>
