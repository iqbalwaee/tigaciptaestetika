<html>
<head>
  <style>
    @page{
        margin:80px 15px;
    }
    body{
        font-size:13px;
        margin-bottom:120px;
    }
    table{
        font-size:13px;
    }
    .main-top{
        display:block;
        margin-bottom:5px;
    }
    .title-header-wrapper{
        text-align:center;
        padding-top:10px;
    }
    .title-header-wrapper h3{
        margin:0px;
        padding:0px;
        margin-bottom:35px;
    }
    .title-header-wrapper p{
        margin:0px;
        padding:0px;
        font-size:14px;
    }
    .text-center{
        text-align:center !important;
    }
    .text-center > div{
        margin:auto;
    }
    main{
        padding:0 30px;
    }
    td{
        vertical-align:top;
    }
    .bg-fixed{
        position:fixed;
        top:50%;
        left:0;
        right:0;
        transform:translateY(-50%);
        z-index:2;
        text-align:center;
    }
    .bg-fixed{
        opacity:0.1;
    }
    .block{
        display:block;
        margin:auto;
    }
  </style>
</head>
<body>
    <div class="bg-fixed">
        <!-- <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="400px"> -->
        <img src="<?=$this->Utilities->generateUrlImage($consultant->picture_dir,$consultant->picture);?>" alt="logo" class="logo" width="400px">
    </div>
  <main>
    
    <table width="100%" cellpadding=0 cellspacing=0>
        <tbody>
            <tr>
                <td colspan="3"><div style="text-align: center;font-weight:bold;font-size:18px;">DAFTAR SIMAK PEMERIKSAAN<br><br></div></td>
            </tr>
            <tr>
                <td width="20%">Nama Perumahan</td>
                <td width="3%">:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:100%;display:inline-block;"><?=$applicationLetter->name_house;?></div></td>
            </tr>
            <tr>
                <td width="20%">Rumah Sample</td>
                <td width="3%">:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:100%;display:inline-block;"><?=$applicationLetter->sample_house;?></div></td>
            </tr>
            <tr>
                <td>Dusun</td>
                <td>:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:100%;display:inline-block;"><?=$applicationLetter->address;?> </div></td>
            </tr>
            <tr>
                <td>Desa/Kelurahan</td>
                <td>:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:100%;display:inline-block;"><?=$applicationLetter->district->name;?></div></td>
            </tr>
            <tr>
                <td>Kabupaten/Kota</td>
                <td>:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:100%;display:inline-block;"><?=$applicationLetter->city->name;?></div></td>
            </tr>
            <tr>
                <td>Provinsi</td>
                <td>:</td>
                <td> <div style="border-bottom:dotted 1px #000;min-width:100%;display:inline-block;"><?=$applicationLetter->province->name;?></div></td>
            </tr>
        </tbody>
    </table>
    <br>
    <br>
    <?=$applicationLetter->application_simaks_pemeriksaan->data;?>
    <br>
    <br>
    <br>
    <table width="100%" cellpadding=0 cellspacing=0>
        <tbody>
            <tr>
                <td width="30%" style="text-align:left;">
                    Mengetahui,<br>
                    <?=$consultant->name;?>
                    <?php if($applicationLetter->status == 3):?>
                        <img src="<?=$this->Url->build('/img/ttd_1.jpg');?>"><br>
                    <?php else:?>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                        <br>
                    <?php endif;?>
                    Nama : <br>
                    <?=$consultant->pic_name;?>
                </td>
                <td width="40%"></td>
                <td width="30%" style="text-align:left;">
                    &nbsp;<br>
                    <?=$applicationLetter->developer->name;?>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    <br>
                    Nama : <br>
                    <?=$applicationLetter->developer->ceo_name;?>
                </td>
            </tr>
            <tr>
                <td width="" style="text-align:left;">
                </td>
                <td width="40%"></td>
                <td width="50%" style="text-align:left;">
                    <br>
                    <br>
                    <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                        'controller' => 'pages',
                        'action' => 'qrCode',
                        $applicationLetter->code,
                        '?' => [
                            'build-code' => $applicationLetter->sample_house
                        ]
                    ],true));?>"  width="100px"/>
                </td>
            </tr>
        </tbody>
    </table>
    <br>
    
  </main>
</body>
</html>