<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Surat Pernyataan
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>			
                <li class="m-portlet__nav-item">
                    <a href="#" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print" class="m-portlet__nav-link m-portlet__nav-link--icon" onclick="window.print();"><i class="la la-print"></i></a>	
                </li>						
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>

    <div class="m-portlet__body">
    <?php
        $dataHouse = explode(",",$applicationLetter->building);
        $countBuild = count($dataHouse);
        $a =1;
    ?>
        <div class="dev-surat-pernyataan">
            <div class="kop-surat">
                
            </div>
            <div class="body-surat-pernyataan">
                <div class="title-body">
                    <h4>SURAT PERNYATAAN</h4>
                    <div class="sub-title">
                      <h3>KESESUAIAN SPESIFIKASI, MUTU, DAN KELAYAKAN</h3>
                    </div>
                </div>
                <table class="table-main" style="width:100%">
                    <tbody>
                        <tr>
                            <td colspan="3">Saya yang bertanda tangan dibawah ini :</td>
                        </tr>
                        <tr>
                            <td width="30%" style="padding-left:30px;">Nama</td>
                            <td width="5%">:</td>
                            <td><?=$applicationLetter->developer->ceo_name;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">NIK</td>
                            <td>:</td>
                            <td><?=$applicationLetter->developer->nik_ceo;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Jabatan</td>
                            <td>:</td>
                            <td>DIREKTUR UTAMA</td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Nama Perusahaan</td>
                            <td>:</td>
                            <td><?=$applicationLetter->developer->name;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Alamat Perusahaan</td>
                            <td>:</td>
                            <td><?=$applicationLetter->developer->address;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Provinsi</td>
                            <td>:</td>
                            <td><?=$applicationLetter->developer->dpp_from->name;?></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Nama Perumahaan</td>
                            <td>:</td>
                            <td><?=$applicationLetter->name_house;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Alamat Perumahaan</td>
                            <td>:</td>
                            <td><?=$applicationLetter->address;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Kecamatan</td>
                            <td>:</td>
                            <td><?=$applicationLetter->district->name;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Kabupaten</td>
                            <td>:</td>
                            <td><?=$applicationLetter->city->name;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Provinsi</td>
                            <td>:</td>
                            <td><?=$applicationLetter->province->name;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Letak Bangunan</td>
                            <td>:</td>
                            <td>
                            <?php
                                $dataHouse = explode(",",$applicationLetter->building);
                                $dataCount = count($dataHouse) -1;
                                $a = 0;
                                $markred = $this->request->query('build-code');
                                foreach($dataHouse as $r):
                                    $a++;
                            ?>
                                <?php if($r == $markred):?>
                                    <strong style="color:red;"><?=$r;?></strong>
                                <?php else:?>
                                    <?=$r;?>
                                <?php endif;?>
                                <?php
                                    $dataCount = $dataCount;
                                ?>
                                <?=($dataCount != 0 ? '|' : '');?>
                            <?php endforeach;?>    
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Rumah Sample</td>
                            <td>:</td>
                            <td><?=$applicationLetter->sample_house;?></td>
                        </tr>
                        <tr>
                            <td style="padding-left:30px;">Jumlah Unit Rumah</td>
                            <td>:</td>
                            <td><?=$a;?> Unit</td>
                        </tr>
                        <tr>
                            <td><br><br></td>
                        <tr>
                        <tr>
                            <td colspan="3">
                                dengan ini menyatakan sesungguhnya :<br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-left:40px;">
                                    <span style="width:30px">1.</span> <?=$consultant->name;?> selaku Konsultan Pengawas telah melakukan tugas pengawasan kepada <?=$applicationLetter->name_house;?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-left:40px;">
                                    <span style="width:30px">2.</span>
                                        Konsultan Pengawas, pada pelaksanaannya telah melakukan uji sampling pada Nomor Rumah Blok <?=$applicationLetter->sample_house;?>, yang menjadi contoh/acuan bangunan rumah untuk 1 periode ( <?=$a;?> unit rumah).
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-left:40px;">
                                    <span style="width:30px">3.</span>
                                    Jika terjadi perubahan Spesifikasi, Mutu dan Kelaikan (tidak seusai rumah contoh/acuan) dikemudian hari pada <?=$applicationLetter->name_house;?> maka selaku Pengembang Perumahan bertanggung jawab terhadap dampak yang diakibatkan.<div class="display-on-p"></div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-left:40px;">
                                    <span style="width:30px">4.</span>
                                    Apabila terjadi kerusakan pada fisik bangunan rumah dan kelengkapannya paling lama 100 (seratus) hari sejak Akad Kredit rumah ditandatangani, <?=$applicationLetter->developer->name;?> bersedia untuk memperbaikinya hingga benar-benar dapat berfungsi dengan baik, paling lama 3 (tiga) bulan setelah adanya laporan/aduan dari nasabah/debitur.
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-left:40px;">
                                    <span style="width:30px">5.</span>
                                    Apabila Prasarana, Sarana, dan Utilitas Umum belum selesai maka <?=$applicationLetter->developer->name;?> wajib menyediakannya sesuai dengan peraturan perundang- undangan.
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="padding-left:40px;">
                                    <span style="width:30px">6.</span>
                                    Apabila Prasarana, Sarana dan Utilitas Umum mengalami kerusakan, <?=$applicationLetter->developer->name;?> bersedia untuk memperbaikinya hingga benar-benar dapat berfungsi dengan baik, sampai dengan Prasarana, Sarana dan Utilitas Umum diserahkan kepada pemerintah daerah sesuai peraturan perundang-undangan.
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                Demikian pernyataan ini dibuat dengan sebenar-benarnya dan dapat dipertanggungjawabkan.<br><br><br><br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" class="text-right">
                                <div style="width:250px;display:inline-block;text-align:center;">
                                    <b>Pengembang Perumahan</b><br>
                                    <?=$applicationLetter->developer->name;?>
                                    <div style="font-size:11px;width:100%;display:inline-block;height:130px !important;line-height:130px !important;">Materai Rp.6.000,-</div>
                                    <b><u><?=$applicationLetter->developer->ceo_name;?></u></b><br>
                                    Direktur Utama
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
            
        </div>
    </div>
</div>