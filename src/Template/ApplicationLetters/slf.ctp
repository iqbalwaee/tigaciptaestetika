<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    DRAFT SLF
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>					
                <li class="m-portlet__nav-item">
                    <a href="#" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Print" class="m-portlet__nav-link m-portlet__nav-link--icon" onclick="window.print();"><i class="la la-print"></i></a>	
                </li>			
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <?php
        $dataHouse = explode(",",$applicationLetter->building);
        $countBuild = count($dataHouse);
        $a =1;
    ?>
    <div class="m-portlet__body">
        <div class="bg-fixed">
            <img src="<?=$this->Utilities->generateUrlImage($consultant->picture_dir,$consultant->picture);?>" alt="logo" class="logo" width="400px">
        </div>
            <div class="sertifikat-laik">
                <div class="kop-surat">
                    <div class="kop-surat-logo">
                        <img src="<?=$this->Utilities->generateUrlImage($consultant->picture_dir,$consultant->picture);?>" alt="logo" class="logo-image">
                    </div>
                    <div class="kop-surat-title">
                        <h3>
                            SERTIFIKAT LAIK FUNGSI<br>BANGUNAN GEDUNG
                        </h3>
                    </div>
                    <div class="kop-surat-label">
                        <div class="green">
                            ARSIP
                        </div>
                    </div>
                </div>
                <div class="sertifikat-laik-body">
                    <b>No. registrasi: <?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->output_date->format('m')).'/CON/'.$applicationLetter->output_date->format('Y');?></b><br>
                    <br>
                    <h5>LEMBARAN HASIL PEMERIKSAAN KELAIKAN FUNGSI BANGUNAN GEDUNG</h5><br>
                    <i>Data Pengembang</i><br>
                    <table class="table-info-dev">
                        <tr>
                            <td width="20%">Nama Perusahaan</td>
                            <td width="5%">:</td>
                            <td><?=$applicationLetter->developer->name;?></td>
                        </tr>
                        <tr>
                            <td width="20%">Direktur</td>
                            <td width="5%">:</td>
                            <td><?=$applicationLetter->developer->ceo_name;?></td>
                        </tr>
                        <!-- <tr>
                            <td width="20%">Nomor KTA </td>
                            <td width="5%">:</td>
                            <td><?=($applicationLetter->developer->kta_number == "-" ? 'SEDANG PROSES' : $applicationLetter->developer->kta_number);?></td>
                        </tr> -->
                    </table>
                    <br>
                    <i>Uraian Kelaikan Fungsi Bangunan</i>
                    <table class="table-info-house">
                        <tr>
                            <td width="40%" class="text-center">
                                <b>Data Bangunan Gedung</b>
                            </td>
                            <td width="60%"  class="text-center">
                                <b>Data Perumahan</b>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table class="table-sub-info">
                                    <tr>
                                        <td width="30%">Fungsi Utama</td>
                                        <td width="5%">:</td>
                                        <td><?=$applicationLetter->primary_function;?></td>
                                    </tr>
                                    <tr>
                                        <td>Type Bangunan</td>
                                        <td>:</td>
                                        <td><?=$applicationLetter->house_type->code;?>/<?=$applicationLetter->surface_area;?></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            Letak Bangunan <i>(Blok Rumah)</i> :
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <table class="table-info-blok">
                                                <tbody>
                                                    <?php $newRow = 1; $count = 0;?>
                                                    <?php for($i = 0;$i<=49;$i++):?>
                                                        <?=($newRow ? '<tr>' : '');?>
                                                        <td>
                                                            <?=$i+1;?>. <b><?=(!empty($dataHouse[$i]) ? $dataHouse[$i] : '_');?></b>
                                                        </td>
                                                        <?php
                                                            $count++;
                                                            if($count == 5):
                                                                $newRow = 1;
                                                                $count = 0;
                                                                echo "</tr>";
                                                            else:
                                                                $newRow = 0;
                                                            endif;

                                                        ?>
                                                    <?php endfor;?>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table class="table-sub-info">
                                    <tr>
                                        <td width="30%">Nama Perumahan</td>
                                        <td width="5%">:</td>
                                        <td><?=$applicationLetter->name_house;?></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat Perumahan</td>
                                        <td>:</td>
                                        <td><?=$applicationLetter->address;?></td>
                                    </tr>
                                    <tr>
                                        <td>Kecamatan</td>
                                        <td>:</td>
                                        <td><?=$applicationLetter->district->name;?></td>
                                    </tr>
                                    <tr>
                                        <td>Kabupaten/Kota</td>
                                        <td>:</td>
                                        <td><?=$applicationLetter->city->name;?></td>
                                    </tr>
                                    <tr>
                                        <td>Provinsi</td>
                                        <td>:</td>
                                        <td><?=$applicationLetter->province->name;?></td>
                                    </tr>
                                    <tr>
                                        <td>Jumlah Unit</td>
                                        <td>:</td>
                                        <td><?=count($dataHouse);?> Unit</td>
                                    </tr>
                                    <tr>
                                        <td>Harga Jual</td>
                                        <td>:</td>
                                        <td>Rp.<?=$this->Number->format($applicationLetter->price_per_unit,['precision'=>2]);?></td>
                                    </tr>
                                    <tr>
                                        <td>Terbilang</td>
                                        <td>:</td>
                                        <td><?=ucwords($this->Utilities->terbilang($applicationLetter->price_per_unit));?> Rupiah</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="text-center">
                                <b>Data Administrasi</b>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table class="table-sub-info">
                                    <tr>
                                        <td width="20%">Sertifikat Hak Atas Tanah</td>
                                        <td width="3%">:</td>
                                        <td><?=$applicationLetter->sertifikat_hak;?></td>
                                    </tr>
                                    <tr>
                                        <td>Ijin Pemanfaatan Tanah</td>
                                        <td>:</td>
                                        <td><?=$applicationLetter->izin_pemanfaatan;?></td>
                                    </tr>
                                    <tr>
                                        <td>Pengesahan Site Plan</td>
                                        <td>:</td>
                                        <td><?=$applicationLetter->pengesahaan;?></td>
                                    </tr>
                                    <tr>
                                        <td>Izin Mendirikan Bangunan</td>
                                        <td>:</td>
                                        <td><?=$applicationLetter->nomor_imb;?></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br>
                    Sesuai dengan hasil pemeriksaan yang telah dilakukan, <b><?=$consultant->name;?></b><br>
                    <table class="table-info-house">
                        <tr>
                            <td width="20%" class="text-center">
                                <b>Persyaratan Administrasi</b>
                            </td>
                            <td width="60%"  class="text-center">
                                <b>Data Perumahan</b>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center v-mid">
                                <?=$applicationLetter->application_output->persyaratan_administratif;?>
                            </td>
                            <td>
                                <table class="table-sub-info">
                                    <tr>
                                        <td width="30%">Fungsi Bangunan</td>
                                        <td width="5%">:</td>
                                        <td><?=$applicationLetter->application_output->fungsi_bangunan;?></td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Peruntukan</td>
                                        <td width="5%">:</td>
                                        <td><?=$applicationLetter->application_output->peruntukan;?></td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Tata Bangunan</td>
                                        <td width="5%">:</td>
                                        <td><?=$applicationLetter->application_output->tata_bangunan;?></td>
                                    </tr>
                                    <tr>
                                        <td width="30%">Kelaikan fungsi bangunan dinyatakan</td>
                                        <td width="5%">:</td>
                                        <td><?=$applicationLetter->application_output->kelaikan;?></td>
                                        </tr>
                                    </table>
                            </td>
                        </tr>
                    </table>
                    <br>                               
                    Lembar Hasil Pemeriksaan ini berlaku sepanjang <b>TIDAK TERDAPAT PERUBAHAN SPESIFIKASI, MUTU, DAN KELAIKAN</b> oleh Pengembang Perumahan. Jika terjadi Perubahan spesifikasi, Mutu, dan Kelaikan (tidak sesuai rumah contoh/acuan) dikemudian hari pada Perumahan <?=$applicationLetter->name_house;?> maka <?=$applicationLetter->developer->name;?> Selaku Pengembang perumahan bertanggung jawab terhadap dampak yang diakibatkan. Lembar hasil pemeriksaan ini berlaku untuk satu periode (<?=count($dataHouse);?> unit rumah yang telah disebut diatas) yang pemeriksaannya telah diwakilkan pada 1 unit bangunan rumah Blok <?=$applicationLetter->sample_house;?>
                    <table class="table-footer-sign">
                        <tbody>
                            <tr>
                                <td width="50%" style="text-align:left">
                                    <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                                        'controller' => 'pages',
                                        'action' => 'qrCode',
                                        $applicationLetter->code
                                    ],true));?>"  width="150px"/>
                                </td>
                                <td width="50%" class="text-right">
                                    <div class="signature">
                                        Jakarta, <?=$this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));?><br>
                                        <?php if(!empty($userData)):?>
                                        <?php if($userData['group_id'] == 2 || empty($userData)):?>
                                            <img src="<?=$this->Utilities->generateUrlImage($consultant->pic_signature_dir,$consultant->pic_signature);?>" style="width:200px;"><br>
                                        <?php else:?>
                                        <br><br><br><br><br><br><br>
                                        <?php endif;?>
                                <?php else:?>
                                <img src="<?=$this->Utilities->generateUrlImage($consultant->pic_signature_dir,$consultant->pic_signature);?>" style="width:200px;"><br>
                                <?php endif;?>
                                        <u><?=$consultant->pic_name;?></u><br>Penanggung Jawab<br><br><br>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <table class="table-footer-sign">
                        <tbody>
                            <tr>
                                <td width="100%" style="text-align:left">
                                    Keterangan : * Dipilih yang sesuai dengan permohonan dan coret yang tidak sesuai, jika pengisian secara manual. Jika pengisian menggunakan software, yang tidak dipilih didelete (hapus).
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
    </div>
</div>
