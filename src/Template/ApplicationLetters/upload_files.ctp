<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Upload File
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'view',$applicationLetter->id]);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>			
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="row m--margin-bottom-15">
            <div class="col-md-12">
                <div class="m-dropzone dropzone m-dropzone--primary" action='<?=$this->Url->build(['action'=>'uploadFiles',$applicationLetter->id]);?>'  id="m-dropzone-two">
                    <div class="m-dropzone__msg dz-message needsclick">
                        <h3 class="m-dropzone__msg-title">
                            Drop files here or click to upload.
                        </h3>
                        <span class="m-dropzone__msg-desc">
                            Upload up to <span>10</span> files
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row row-galleries">
            <?php foreach($applicationLetter->application_letters_pictures as $key => $r):?>
                <div class="col-md-4 col-lg-3 col-gallery">
                    <div class="gallery-item">
                        <a href="<?=$this->Url->build(['action'=>'deletePicture',$r->id]);?>" class="btn-delete-picture">
                            <i class="fa fa-times"></i>
                        </a>
                        <a href="#" class="item">
                            <img src="<?=$this->Utilities->generateUrlImage($r->picture_dir,$r->picture);?>" class="img img-responsive">
                        </a>
                        <div class="caption-image">
                            <p>
                                <a href="#" class="edit-data-caption" data-id="<?=$r->id;?>" data-name="<?=$r->name;?>">
                                    <?=$r->name;?>
                                </a
                            ></p>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</div>
<div class="modal fade" id="m_modal_2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Edit Caption
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <?= $this->Form->create(null,['class'=>'m-form m-form--fit m-form--label-align-right m-add-files','url'=>[
                    'action' => 'editCaption'
                ],
                'id' => 'editCaption',
                'type' => 'file']) ?>
                    <div class="row">
                        <div class="col-md-12">
                            <?=$this->Form->control('id',['type'=>'hidden','id'=>'id_image']);?>
                            <?=$this->Form->control('name',['label'=>'Judul Gambar','id'=>'caption_image']);?>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-submit-edit btn-primary">
                    Simpan
                </button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">
                    Close
                </button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
<?php $this->start('script');?>
    <script>
        Dropzone.autoDiscover = false;
        
        var myDropzone = null;
        function initDropzone(){
            var dropZoneCount = 10 - $(".col-gallery").length;
            console.log(dropZoneCount);
            if(myDropzone != null){
                myDropzone.destroy();
            }
            $(".m-dropzone__msg-desc span").html(dropZoneCount);
            if(dropZoneCount <= 0){
                $("#m-dropzone-two").hide();
            }else{
                $("#m-dropzone-two").show();
                myDropzone = new Dropzone("#m-dropzone-two", {
                paramName: "picture", // The name that will be used to transfer the file
                maxFiles: dropZoneCount,
                maxFilesize: 200, // MB
                addRemoveLinks: true,
                acceptedFiles: "image/*",
                accept: function(file, done) {
                    done();
                },   
                init: function() {
                    this.on("processing", function(file) {
                        this.options.url = "<?=$this->Url->build(['action'=>'uploadFiles',$applicationLetter->id]);?>"
                    });
                    var cd;
                    this.on("success", function(file, response) {
                        $('.dz-progress').hide();
                        $('.dz-size').hide();
                        $('.dz-error-mark').hide();
                        $('.dz-remove').hide();
                        cd = response;
                        if(cd.code ==200){
                            console.log(cd.applicationPicture.picture_dir);
                            var baseUrl = "<?=$this->Url->build('/',true);;?>"
                            var html = '<div class="col-md-4 col-lg-3 col-gallery"><div class="gallery-item"><a href="'+baseUrl+'/application-letters/delete-picture/'+cd.applicationPicture.id+'" class="btn-delete-picture"><i class="fa fa-times"></i></a><a href="#" class="item"><img src="'+baseUrl+'/'+cd.applicationPicture.picture_dir+'/'+cd.applicationPicture.picture+'" class="img img-responsive"></a><div class="caption-image"><p><a href="#" class="edit-data-caption" data-id="'+cd.applicationPicture.id+'" data-name="'+cd.applicationPicture.name+'">'+cd.applicationPicture.name+'</a></p></div></div></div>';
                            $(".row-galleries").prepend(html);
                        }
                    });
                },
                complete : function(file,resp){
                    if (this.getUploadingFiles().length === 0 && this.getQueuedFiles().length === 0) {
                        myDropzone.removeAllFiles(true)
                        initDropzone();
                    }
                    console.log(this.getUploadingFiles());
                }
            });
            }
            
        }

        initDropzone()
        $('#m_modal_2').on('show.bs.modal', function (e) {
            
        })
        $('#m_modal_2').on('hidden.bs.modal', function (e) {
            
        });

        $("body").on("click",".edit-data-caption",function(e){
            e.preventDefault()
            var divThis = $(this).closest("div");
            var dataId = $(this).data("id");
            var dataName = $(this).data("name");
            $("#id_image").val(dataId);
            $("#caption_image").val(dataName);
            $("#m_modal_2").modal("show");
        })
        $(".btn-submit-edit").on("click",function(e){
            e.preventDefault();
            $("#editCaption").submit();
        })
        $("#editCaption").submit(function(e){
            e.preventDefault();
            var data = $(this).serialize();
            var thisValName = $("#caption_image").val();
            var thisValId = $("#id_image").val();
            var form = $(this);
            var btn = $(".btn-submit-edit");
            var url = $(this).attr("action");
            if(thisValName.length != 0){
                btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                $("#caption_image").addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                $.ajax({
                    url : url,
                    data : data,
                    dataType : 'json',
                    type : 'post',
                    success : function(result){
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        $("#caption_image").removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        if(result.code == 200){
                            $(".edit-data-caption[data-id='"+thisValId+"']").html(thisValName);
                            swal(
                                'Success',
                                result.message,
                                'success'
                            )
                        }else{
                            swal("Ooopp!!!", result.message,"error");
                        }
                        
                        //$("#m_modal_2").modal("hide");
                    },
                    error : function(){
                        btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                        $("#caption_image").removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    }
                })
            }else{

            }
           
        })

        $("body").on("click",".btn-delete-picture",function(e){
            e.preventDefault();
            var thisUrl = $(this).attr("href");
            var thisCol = $(this).closest('.col-gallery');
            swal({
                title: 'Are you sure?',
                text: 'Please make sure if you want to delete this record',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, keep it'
            }).then((result) => {
                if (result.value) {
                    $.ajax({
                        url : thisUrl,
                        type : "delete",
                        dataType : "json",
                        beforeSend : function(){
                            swal('Please Wait', 'Requesting Data', 'info')
                        },
                        success : function(result){
                            if(result.code == 200){
                                swal(
                                    'Success',
                                    result.message,
                                    'success'
                                )
                                thisCol.remove(); 
                                initDropzone()
                            }else if(result.code == 99){
                                swal("Ooopp!!!", result.message,"error");
                            }else{
                                swal("Ooopp!!!", result.message,"error");
                            }
                        },
                        error : function()
                        {
                            swal("Ooopp!!!","Failed to deleted record, please try again","error");
                        }
                    })
                // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                } else if (result.dismiss === 'cancel') {
                    
                }
            })
        });
    </script>
<?php $this->end();?>