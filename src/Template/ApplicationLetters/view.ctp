<div class="m-portlet  m-portlet--head-sm" m-portlet="true" id="m_portlet_tools_1">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                <?=$titlesubModule;?>
                </h3>
            </div>			
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">						
                <li class="m-portlet__nav-item">
                    <a href="<?=$this->Url->build(['action'=>'index']);?>" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Back" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-left"></i></a>	
                </li>						
                <?php if(($userData['group_id'] == 4 && $applicationLetter->status == 0) || ($userData['group_id'] == 3 && $applicationLetter->status == 1) || ($userData['group_id'] == 5 && $applicationLetter->status == 2)):?>
                    <li class="m-portlet__nav-item">
                        <a href="#" data-container="body" data-toggle="m-tooltip" data-placement="bottom" title="" data-original-title="Set Persetujuan" class="m-portlet__nav-link m-portlet__nav-link--icon btn-show-modal"><i class="la la-pencil-square"></i>
                        </a>
                    </li>
                <?php endif;?>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="toggle" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-angle-down"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#"  m-portlet-tool="fullscreen" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-expand"></i></a>	
                </li>
                <li class="m-portlet__nav-item">
                    <a href="#" m-portlet-tool="remove" class="m-portlet__nav-link m-portlet__nav-link--icon"><i class="la la-close"></i></a>	
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <div class="m-portlet m-portlet--bordered m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            DATA PERUSAHAAN
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th scope="row" width="200px"><?= __('Nama Perusahaan') ?></th>
                            <td><?= h($applicationLetter->developer->name) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Nomor Telepon') ?></th>
                            <td><?= h($applicationLetter->developer->phone) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Nama Direktur') ?></th>
                            <td><?= h($applicationLetter->developer->ceo_name) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Nomor KTA') ?></th>
                            <td><?= h($applicationLetter->developer->kta_number) ?></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--bordered m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            DATA ADMINISTRASI
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="table-responsive">
                        
                    <table class="table">
                        <tr>
                            <th scope="row" width="200px"><?= __('Sertifikat Hak Atas Tanah') ?></th>
                            <td><?= h($applicationLetter->sertifikat_hak) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Izin Pemanfaatan Tanah') ?></th>
                            <td><?= h($applicationLetter->izin_pemanfaatan  ) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Pengesahan Site Plan') ?></th>
                            <td><?= h($applicationLetter->pengesahaan) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Izin Mendirikan Bangunan') ?></th>
                            <td><?= h($applicationLetter->nomor_imb) ?></td>
                        </tr>
                        <?php if(!empty($applicationLetter->izin_lainnya_label) && !empty($applicationLetter->izin_lainnya_value)):?>
                            <tr>
                                <th scope="row">Izin Lainnya (<?= $applicationLetter->izin_lainnya_label;?>)</th>
                                <td><?= h($applicationLetter->izin_lainnya_value) ?></td>
                            </tr>
                        <?php endif;?>
                    </table>
                </div>
            </div>
        </div>
        <div class="m-portlet m-portlet--bordered m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            DATA PERUMAHAN
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th scope="row" width="15%"><?= __('Kode Pengajuan') ?></th>
                            <td width="35%"><?= h($applicationLetter->code) ?></td>
                            <th scope="row" width="15%"><?= __('Nama Perumahan') ?></th>
                            <td width="35%"><?= h($applicationLetter->name_house  ) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Nomor Register') ?></th>
                            <td>
                                <?php if(!empty($applicationLetter->approved_by_dpp_date)):?>
                                    <?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y');?>
                                <?php endif;?>
                            </td>
                            <th scope="row"><?= __('Status Pengajuan') ?></th>
                            <td><?= $this->Utilities->statusDeveloper($applicationLetter->status); ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Fungsi Utama') ?></th>
                            <td><?= h($applicationLetter->primary_function) ?></td>
                            <th scope="row"><?= __('Tipe Bangunan') ?></th>
                            <td><?= h($applicationLetter->house_type->name) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Posisi Kordinat') ?></th>
                            <td><?= h($applicationLetter->cordinate_area) ?></td>
                            <th scope="row"><?= __('Klasifikasi Kompleksitas') ?></th>
                            <td><?= h($applicationLetter->clasification_kompleksitas) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Tinggi Bangunan') ?></th>
                            <td><?= h($applicationLetter->high_building) ?></td>
                            <th scope="row"><?= __('Jumlah Lantai') ?></th>
                            <td><?= h($applicationLetter->floor_count) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Luas Bangunan') ?></th>
                            <td><?= h($applicationLetter->surface_building) ?></td>
                            <th scope="row"><?= __('Luas Tanah') ?></th>
                            <td><?= h($applicationLetter->surface_area) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Letak Bangunan') ?></th>
                            <td colspan="3" ><?= h(str_replace(","," | ",$applicationLetter->building)) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Jumlah Unit') ?></th>
                            <td><?= h(count(explode(",",$applicationLetter->building)))?> Unit</td>
                            <th scope="row"><?= __('Provinsi') ?></th>
                            <td><?= h($applicationLetter->province->name) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Kota / Kabupaten') ?></th>
                            <td><?= h($applicationLetter->city->name) ?></td>
                            <th scope="row"><?= __('Kecamatan') ?></th>
                            <td><?= h($applicationLetter->district->name) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Kelurahan') ?></th>
                            <td><?= h($applicationLetter->village->name) ?></td>
                            <th scope="row"><?= __('Kampung') ?></th>
                            <td><?= h($applicationLetter->village_name) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Alamat') ?></th>
                            <td colspan="3"><?= h($applicationLetter->address) ?></td>
                        </tr>
                        <tr>
                            <th scope="row"><?= __('Rumah Sample') ?></th>
                            <td><?= h($applicationLetter->sample_house) ?></td>
                            <th scope="row"><?= __('Harga Per-Unit') ?></th>
                            <td>Rp.<?= $this->Utilities->numberFormat($applicationLetter->price_per_unit) ?></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <?php if($userData['group_id'] == 2 || $userData['group_id'] == 5 || $userData['group_id'] == 3):?>
                                    <?php if($userData['group_id'] == 2 || $userData['group_id'] == 5):?>
                                        <a href="<?=$this->Url->build(['action'=>'uploadFiles',$applicationLetter->id]);?>">
                                            <i class="fa fa-upload"></i> Upload Files
                                        </a>
                                        &nbsp;
                                    <?php endif;?>
                                    <?php if($userData['group_id'] == 2):?>
                                        <a href="<?=$this->Url->build(['action'=>'printSuratPernyataan',$applicationLetter->id]);?>">
                                            <i class="la la-print"></i> Print Surat Pernyataan
                                        </a>
                                    <?php endif;?>
                                    <?php if($userData['group_id'] == 5):?>
                                        <!-- <a href="<?=$this->Url->build(['action'=>'insertPictures',$applicationLetter->id]);?>" >
                                            <i class="fa fa-upload"></i> Upload Gambar Perumahan
                                        </a> -->
                                    &nbsp;
                                    <?php endif;?>
                                    <?php if($userData['group_id'] == 2 || $userData['group_id'] == 5 || $userData['group_id'] == 3):?>
                                        <?php if(!empty($applicationLetter->spk_code) && $userData['group_id'] != 2):?>
                                            <a href="<?=$this->Url->build(['action'=>'print-spk',$applicationLetter->id]);?>" target="_BLANK">
                                                <i class="la la-print"></i> Print SPK
                                            </a>
                                            &nbsp;
                                        <?php endif;?>
                                        <?php if($userData['group_id'] == 5):?>
                                            <a href="<?=$this->Url->build(['action'=>'printSuratPernyataan',$applicationLetter->id]);?>">
                                                <i class="la la-print"></i> Print Surat Pernyataan
                                            </a>
                                            <a href="<?=$this->Url->build(['action'=>'applicationOutput',$applicationLetter->id]);?>">
                                                <i class="la la-archive"></i> <?=(!empty($applicationLetter->application_output) ? 'Edit' : 'Buat');?> SLF
                                            </a>
                                        &nbsp;
                                        <?php endif;?>
                                        <?php if(!empty($applicationLetter->application_output)):?>
                                            <a href="<?=$this->Url->build(['action'=>'printOutput',$applicationLetter->id]);?>" target="_BLANK">
                                                <i class="la la-print"></i> Print SLF
                                            </a>
                                            <a href="<?=$this->Url->build(['action'=>'printSuratPernyataanV2',$applicationLetter->id]);?>" target="_BLANK">
                                                <i class="la la-print"></i> Print SLF V2
                                            </a>
                                            <a href="<?=$this->Url->build(['action'=>'printSlfNew',$applicationLetter->id]);?>" target="_BLANK">
                                                <i class="la la-print"></i> SLF BARU
                                            </a>
                                        &nbsp;
                                        <?php endif;?>
                                    <?php endif;?>
                                    <?php if($applicationLetter->status == 3 && !empty($applicationLetter->output_date)):?>
                                        <a href="<?=$this->Url->build(['action'=>'slf',$applicationLetter->id]);?>" target="_BLANK">
                                            <i class="la la-print"></i> Draft SLF
                                        </a>
                                    <?php endif;?>
                                    <?php if($userData['group_id'] == 5):?>
                                        <br>
                                        <br>
                                        <!-- <a href="<?=$this->Url->build(['action'=>'daftarSimak',$applicationLetter->id]);?>">
                                            <i class="la la-archive"></i> Daftar SIMAK
                                        </a> -->
                                    <!-- &nbsp;
                                        <a href="<?=$this->Url->build(['action'=>'applicationSimak',$applicationLetter->id]);?>">
                                            <i class="la la-archive"></i> Daftar SIMAK Pemeriksaan
                                        </a>
                                        &nbsp; -->
                                        <!-- <a href="<?=$this->Url->build(['action'=>'applicationNewSimak',$applicationLetter->id]);?>">
                                            <i class="la la-archive"></i> <?=(!empty($applicationLetter->application_new_simak) ? 'Edit' : 'Buat');?> SIMAK Baru
                                        </a> -->
                                        <!-- &nbsp;
                                        <a href="<?=$this->Url->build(['action'=>'applicationRooms',$applicationLetter->id]);?>">
                                            <i class="la la-archive"></i> <?=(!empty($applicationLetter->application_roomm) ? 'Edit' : 'Buat');?> Master Kelengkapan Rumah
                                        </a>
                                        &nbsp; -->
                                        <!-- <a href="<?=$this->Url->build(['action'=>'applicationJudgments',$applicationLetter->id]);?>">
                                            <i class="la la-archive"></i> <?=(!empty($applicationLetter->application_judgment) ? 'Edit' : 'Buat');?> Penilaian Kelayakan Kelengkapan Rumah
                                        </a>
                                        &nbsp; -->
                                        <a href="<?=$this->Url->build(['action'=>'invoicing',$applicationLetter->id]);?>">
                                            <i class="la la-archive"></i> Invoicing
                                        </a>
                                    <!-- &nbsp;
                                        <a href="<?=$this->Url->build(['action'=>'kelengkapanRumah',$applicationLetter->id]);?>">
                                            <i class="la la-archive"></i> Kelengkapan Rumah
                                        </a> -->
                                    &nbsp;
                                    <?php endif;?>
                                <?php endif;?>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <?php if(($userData['group_id'] == 2 || $userData['group_id'] == 5) && count($applicationLetter->application_letters_pictures) > 0):?>
            <div class="m-portlet m-portlet--bordered m-portlet--unair">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Daftar File
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="row row-galleries">
                    <?php foreach($applicationLetter->application_letters_pictures as $key => $r):?>
                        <div class="col-md-4 col-lg-3 col-gallery">
                            <div class="gallery-item">
                                <a href="#" class="item">
                                    <img src="<?=$this->Utilities->generateUrlImage($r->picture_dir,$r->picture);?>" class="img img-responsive">
                                </a>
                                <div class="caption-image">
                                    <p><?=$r->name;?></p>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <?php if($userData['group_id'] == 5 && !empty($applicationLetter->application_picture)):?>
            <div class="m-portlet m-portlet--bordered m-portlet--unair">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-caption">
                        <div class="m-portlet__head-title">
                            <h3 class="m-portlet__head-text">
                                Gambar Perumahan
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="m-portlet__body">
                    <div class="row row-galleries">
                        <?php if(!empty($applicationLetter->application_picture->tampak_depan)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->tampak_depan_dir,$applicationLetter->application_picture->tampak_depan);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->tampak_depan_dir,$applicationLetter->application_picture->tampak_depan);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Tampak Depan</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->jalan_depan)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jalan_depan_dir,$applicationLetter->application_picture->jalan_depan);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jalan_depan_dir,$applicationLetter->application_picture->jalan_depan);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Jalan Depan</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->ruang_tamu)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->ruang_tamu_dir,$applicationLetter->application_picture->ruang_tamu);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->ruang_tamu_dir,$applicationLetter->application_picture->ruang_tamu);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Ruang Tamu</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->kamar_tidur)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->kamar_tidur_dir,$applicationLetter->application_picture->kamar_tidur);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->kamar_tidur_dir,$applicationLetter->application_picture->kamar_tidur);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Kamar Tidur</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->plafond)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->plafond_dir,$applicationLetter->application_picture->plafond);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->plafond_dir,$applicationLetter->application_picture->plafond);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Plafond</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->pintu_kusen_jendela)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->pintu_kusen_jendela_dir,$applicationLetter->application_picture->pintu_kusen_jendela);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->pintu_kusen_jendela_dir,$applicationLetter->application_picture->pintu_kusen_jendela);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Pintu, kusen & Jendela</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->toilet)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->toilet_dir,$applicationLetter->application_picture->toilet);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->toilet_dir,$applicationLetter->application_picture->toilet);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Toilet</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->sumur_septictank)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->sumur_septictank_dir,$applicationLetter->application_picture->sumur_septictank);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->sumur_septictank_dir,$applicationLetter->application_picture->sumur_septictank);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Sumur & Septictank</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->kwh_listrik)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->kwh_listrik_dir,$applicationLetter->application_picture->kwh_listrik);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->kwh_listrik_dir,$applicationLetter->application_picture->kwh_listrik);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>KWh Listrik</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->jaringan_air)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jaringan_air_dir,$applicationLetter->application_picture->jaringan_air);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->jaringan_air_dir,$applicationLetter->application_picture->jaringan_air);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Jaringan Air Bersih</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                        <?php if(!empty($applicationLetter->application_picture->saluran_pembuangan)):?>
                            <div class="col-md-4 col-lg-3 col-gallery">
                                <div class="gallery-item">
                                    <a href="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->saluran_pembuangan_dir,$applicationLetter->application_picture->saluran_pembuangan);?>" class="item" target="_BLANK">
                                        <img src="<?=$this->Utilities->generateUrlImage($applicationLetter->application_picture->saluran_pembuangan_dir,$applicationLetter->application_picture->saluran_pembuangan);?>" class="img img-responsive">
                                    </a>
                                    <div class="caption-image">
                                        <p>Saluran Pembuangan</p>
                                    </div>
                                </div>
                            </div>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <?php if(!empty($applicationLetter->surat_pernyataan) || !empty($applicationLetter->resi_pengiriman) || !empty($applicationLetter->lembar_pemeriksaan)):?>
        <div class="m-portlet m-portlet--bordered m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            DATA PENDUKUNG
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="table-responsive">
                    <table class="table">
                    <?php if(!empty($applicationLetter->surat_pernyataan)):?>
                        <tr>
                            <th scope="row" width="200px"><?= __('Surat Pernyataan') ?></th>
                            <td width=""><a href="<?= $this->Utilities->generateUrlImage($applicationLetter->surat_pernyataan_dir,$applicationLetter->surat_pernyataan);?>" target="_BLANK"><i class="fa fa-download"></i> <?=$applicationLetter->surat_pernyataan;?></a></td>
                        </tr>
                    <?php endif;?>
                    <?php if( !empty($applicationLetter->resi_pengiriman)):?>
                        <tr>
                            <th scope="row" width="200px"><?= __('Resi Pengiriman') ?></th>
                            <td width=""><a href="<?= $this->Utilities->generateUrlImage($applicationLetter->resi_pengiriman_dir,$applicationLetter->resi_pengiriman);?>" target="_BLANK"><i class="fa fa-download"></i> <?=$applicationLetter->resi_pengiriman;?></a></td>
                        </tr>
                    <?php endif;?>
                    <?php if( !empty($applicationLetter->lembar_pemeriksaan)):?>
                        <tr>
                            <th scope="row" width="200px"><?= __('Lembar Pengawasan') ?></th>
                            <td width=""><a href="<?= $this->Utilities->generateUrlImage($applicationLetter->lembar_pemeriksaan_dir,$applicationLetter->lembar_pemeriksaan);?>" target="_BLANK"><i class="fa fa-download"></i> <?=$applicationLetter->lembar_pemeriksaan;?></a></td>
                        </tr>
                    <?php endif;?>
                    </table>
                </div>
            </div>
        </div>
        <?php endif;?>
        <?php if(!empty($applicationLetter->supervisor)):?>
        <div class="m-portlet m-portlet--bordered m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            DATA PENGAWAS
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="table-responsive">
                    <table class="table">
                        <tr>
                            <th scope="row" width="200px"><?= __('NIK') ?></th>
                            <td width=""><?=$applicationLetter->supervisor->nik;?></td>
                        </tr>
                        <tr>
                            <th scope="row" width="200px"><?= __('Nama') ?></th>
                            <td width=""><?=$applicationLetter->supervisor->name;?></td>
                        </tr>
                        <tr>
                            <th scope="row" width="200px"><?= __('Alamat') ?></th>
                            <td width=""><?=$applicationLetter->supervisor->address;?></td>
                        </tr>
                        <tr>
                            <th scope="row" width="200px"><?= __('No. Telepon') ?></th>
                            <td width=""><?=$applicationLetter->supervisor->phone;?></td>
                        </tr>
                        <tr>
                            <th scope="row" width="200px"><?= __('Identitas Pengawas') ?></th>
                            <td width="">
                                <a href="<?= $this->Utilities->generateUrlImage($applicationLetter->supervisor->identity_file_dir,$applicationLetter->supervisor->identity_file);?>" target="_BLANK"><i class="fa fa-download"></i> Download</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <?php endif;?>
        <div class="m-portlet m-portlet--bordered m-portlet--unair">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            LOG PENGAJUAN
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-striped">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th width="15%">User</th>
                                <th width="15%">Status</th>
                                <th width="25%">Pesan</th>
                                <th>Catatan</th>
                                <th width="15%">Tanggal</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($applicationLetter->histories_letters as $key => $r):?>
                                <tr>
                                    <td><?=$key+1;?></td>
                                    <td><?=$r->created_user->name;?></td>
                                    <td><?=$r->status;?></td>
                                    <td><?=$r->message;?></td>
                                    <td><?=$r->note;?></td>
                                    <td><?=$r->created->format('d-m-Y H:i:s');?></td>
                                </tr>
                            <?php endforeach;?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

            <?php if(($userData['group_id'] == 4 && $applicationLetter->status == 0) || ($userData['group_id'] == 3 && $applicationLetter->status == 1) || ($userData['group_id'] == 5 && $applicationLetter->status == 2)):?>
                <div class="m-portlet m-portlet--bordered m-portlet--unair">
                        <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
                                <h3 class="m-portlet__head-text">
                                    SET PENGAJUAN
                                </h3>
                            </div>
                        </div>
                    </div>
                    <?=$this->Form->create();?>
                    <div class="m-portlet__body">
                            <?=$this->Form->control('message',['label'=>'Pesan','type'=>'textarea','class'=>'form-control m-input','placeholder' => ($userData->group_id == 4 ? 'Pesan ditujukan kepada Developer': ($userData->group_id == 5 ?  'Pesan ditujukan kepada DPP' : 'Pesan ditujukan kepada DPD'))]);?>
                            <?php if($userData['group_id'] != 5):?>
                                <?=$this->Form->control('note',['label'=>'Catatan','type'=>'textarea','class'=>'form-control m-input','placeholder' => ($userData->group_id == 4 ? 'Catatan ditujukan kepada DPP': 'Catatan ditujukan kepada konsultant')]);?>
                            <?php endif;?>
                    </div>
                    <div class="m-portlet__foot">
                        <div class="row align-items-center">
                            <div class="col-lg-12">
                                <button type="button" class="btn btn-danger btn-submit-status" value="<?=($userData->group_id == 4 ? 11 : ($userData->group_id == 5 ? 33 : 22));?>">
                                    <i class="fa fa-ban"></i> Tolak
                                </button>
                                <button type="button" class="btn btn-primary btn-submit-status" value="<?=($userData->group_id == 4 ? 1 : ($userData->group_id == 5 ? 3 : 2));?>">
                                    <i class="fa fa-check-circle"></i> Approve
                                </button>
                            </div>
                        </div>
                    </div>
                    <?=$this->Form->end();?>
                </div>
                
            <?php endif;?>
    </div>
</div>

<!--begin::Modal-->
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">
                    Form Persetujuan
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">
                        &times;
                    </span>
                </button>
            </div>
            <div class="modal-body">
                <?=$this->Form->create();?>
                    <?=$this->Form->control('message',['label'=>'Pesan','type'=>'textarea','class'=>'form-control m-input','placeholder' => ($userData->group_id == 4 ? 'Pesan ditujukan kepada Developer': ($userData->group_id == 5 ?  'Pesan ditujukan kepada DPP' : 'Pesan ditujukan kepada DPD')),'id'=>'message_2']);?>
                    <?php if($userData['group_id'] != 5):?>
                        <?=$this->Form->control('note',['label'=>'Catatan','type'=>'textarea','class'=>'form-control m-input','placeholder' => ($userData->group_id == 4 ? 'Catatan ditujukan kepada DPP': 'Catatan ditujukan kepada konsultant'),'id'=>'note_2']);?>
                    <?php endif;?>
                <?=$this->Form->end();?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn-submit-status"_2 value="<?=($userData->group_id == 4 ? 11 : ($userData->group_id == 5 ? 33 : 22));?>">
                    <i class="fa fa-ban"></i> Tolak
                </button>
                <button type="button" class="btn btn-primary btn-submit-status_2" value="<?=($userData->group_id == 4 ? 1 : ($userData->group_id == 5 ? 3 : 2));?>">
                    <i class="fa fa-check-circle"></i> Approve
                </button>
            </div>
        </div>
    </div>
</div>
<!--end::Modal-->
<?php $this->start('script');?>
    <script>
        $(".btn-show-modal").on("click",function(e){
            $("#m_modal_1").modal('show');
        })
        $(".btn-submit-status").on("click",function(e){
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            var data = {
                'status' : btn.val(),
                'note' : $("#note").val(),
                'message' : $("#message").val(),
            }
            $.ajax({
                data : data,
                dataType : 'json',
                type : 'post',
                beforeSend : function(){
                    swal('Harap Menunggu','Sedang mengirim data','info')
                },
                success : function(result){
                    if(result.code == 200){
                        swal("Berhasil",result.message,'success')
                        location.reload();
                    }else{
                        swal("Error",'Terjadi kesalahan','error')
                    }
                        
                    
                },
                error : function(){
                    swal("Error",'Terjadi kesalahan','error')
                }
            })
        })
        $(".btn-submit-status_2").on("click",function(e){
            e.preventDefault();
            var btn = $(this);
            var form = $(this).closest('form');
            var data = {
                'status' : btn.val(),
                'note' : $("#note_2").val(),
                'message' : $("#message_2").val(),
            }
            $.ajax({
                data : data,
                dataType : 'json',
                type : 'post',
                beforeSend : function(){
                    swal('Harap Menunggu','Sedang mengirim data','info')
                },
                success : function(result){
                    if(result.code == 200){
                        swal("Berhasil",result.message,'success')
                        location.reload();
                    }else{
                        swal("Error",'Terjadi kesalahan','error')
                    }
                        
                    
                },
                error : function(){
                    swal("Error",'Terjadi kesalahan','error')
                }
            })
        })
    </script>
<?php $this->end();?>