<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?php unset($consultant->user->password);;?>
    <?= $this->Form->create($consultant,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-2">
                    <?=$this->Form->control('code');;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('name',['label'=>'Nama Konsultan']);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('email');;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-8">
                    <?=$this->Form->control('address',['class'=>'form-control m-input','rows'=>'2']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('phone');;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('pic_name',['class'=>'form-control m-input','label'=>'Nama PIC']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('pic_signature',['label'=>'Tanda Tangan PIC','type'=>'file','Tanda Tangan PIC']);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('picture',['label'=>'Logo','type'=>'file']);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('compro',['label'=>'Compro','type'=>'file']);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('form_checklist',['label'=>'Form Checklist','type'=>'file']);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('ska_1',['label'=>'SKA 1','type'=>'file']);;?>
                    <?php if(!empty($consultant->ska_1)):?>
                        <a href="<?=$this->Url->build(['action'=>'removeSka',1]);?>"><i class="fa fa-times"></i> Remove SKA</a>
                    <?php endif;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('ska_2',['label'=>'SKA 2','type'=>'file']);;?>
                    <?php if(!empty($consultant->ska_2)):?>
                        <a href="<?=$this->Url->build(['action'=>'removeSka',2]);?>"><i class="fa fa-times"></i> Remove SKA</a>
                    <?php endif;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('ska_3',['label'=>'SKA 3','type'=>'file']);;?>
                    <?php if(!empty($consultant->ska_3)):?>
                        <a href="<?=$this->Url->build(['action'=>'removeSka',3]);?>"><i class="fa fa-times"></i> Remove SKA</a>
                    <?php endif;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('user.username',['class'=>'form-control m-input']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('user.password',['class'=>'form-control m-input']);?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>