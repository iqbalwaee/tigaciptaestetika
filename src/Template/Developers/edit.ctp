<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?php unset($developer->user->password);?>
    <?= $this->Form->create($developer,['class'=>'m-form m-form--fit m-form--label-align-right']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('name',['label'=>'Nama Perusahaan']);;?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->control('ceo_name',['label'=>'Nama CEO']);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('kta_number',['label'=>'Nomor KTA']);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('email');;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('phone',['label'=>'No. Telepon']);;?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->control('dpp_from_id',['label'=>'Asal DPD ','options'=>$dppFroms]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('status',['class'=>'form-control m-input','label'=>'status','options'=>['BELUM DIVERIFIKASI','TELAH DIVERIFIKASI']]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-12">
                    <?=$this->Form->control('address',['class'=>'form-control m-input','rows'=>'2','label'=>'Alamat']);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('user.username',['class'=>'form-control m-input']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('user.password',['class'=>'form-control m-input']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('user.status',['class'=>'form-control m-input','label'=>'Status User','options'=>['DISABLED','ENABLED']]);?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        $("#dpp-from-id").select2();
    </script>

<?php $this->end();?>