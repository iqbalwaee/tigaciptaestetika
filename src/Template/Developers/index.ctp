<?php
    $rightButton = "";
?>
<?=$this->element('widget/index',['rightButton' => $rightButton]);?>
<?php $this->start('script');?>
    <script>
        <?php
            $deleteUrl    = $this->Url->build(['action'=>'delete'])."/";
            $editUrl      = $this->Url->build(['action'=>'edit'])."/";
            $viewUrl = $this->Url->build(['action'=>'view'])."/";
        ?>
        jQuery(document).ready(function() {
            var deleteUrl = "<?=$deleteUrl;?>";
            var editUrl = "<?=$editUrl;?>";
            var viewUrl = "<?=$viewUrl;?>";
            var columnData = [
                {
                    field: "actions",
                    width: 50,
                    title: "#",
                    sortable: false,
                    overflow: "visible",
                    template: function(t) {
                        var btn =  '<div class="dropdown"><a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"><i class="la la-ellipsis-h"></i></a>  <div class="dropdown-menu ">'
                        var btnList = '';
                        if(viewUrl != ""){
                            btnList += '<a class="dropdown-item" href="'+viewUrl+t.id+'"><i class="flaticon-search-1"></i> View</a>';
                        }
                        if(deleteUrl != ""){
                            btnList += '<a class="btn-delete-on-table dropdown-item" href="'+deleteUrl+t.id+'"><i class="flaticon-cancel"></i> Delete</a>';
                        }
                        if(editUrl != ""){
                            btnList += '<a class="dropdown-item" href="'+editUrl+t.id+'"><i class="flaticon-edit"></i> Edit</a>';
                        }
                        
                        if(btnList == ""){
                            btn = "";
                        }else{
                            btn += btnList;
                            btn += '</div></div>';
                        }
                        return btn;
                    }
                },
                {
                field: "Developers.id",
                title: "ID",
                sortable: false,
                width: 40,
                selector: false,
                textAlign: "center",
                template: function(t) {
                    return t.id
                }
            },
            {
                field: "Developers.status",
                title: "Status",
                sort : 'asc',
                template: function(t) {
                    var status = t.status
                    if(status == 0){
                        return '<span class="m-badge  m-badge--danger m-badge--wide">BELUM DI VERIFIKASI</span>';
                    }else if(status == 1){
                        return '<span class="m-badge  m-badge--info m-badge--wide">TELAH DI VERIFIKASI</span>';
                    }
                }
            },
            {
                field: "DppFroms.name",
                title: "Asal DPD ",
                sort : 'asc',
                template: function(t) {
                    return t.dpp_from.name
                }
            }, {
                field: "Developers.name",
                title: "Nama Perusahaan",
                sort : 'asc',
                width :250,
                template: function(t) {
                    return t.name
                }
            },   {
                field: "Developers.ceo_name",
                title: "Nama CEO",
                sort : 'asc',
                width :250,
                template: function(t) {
                    return t.ceo_name 
                }
            },   {
                field: "Developers.email",
                title: "Email",
                sort : 'asc',
                width :250,
                template: function(t) {
                    return t.email
                }
            },   {
                field: "Developers.phone",
                title: "No. Telepon",
                sort : 'asc',
                width :150,
                template: function(t) {
                    return t.phone
                }
            },   {
                field: "Developers.created",
                title: "Tanggal Daftar",
                sort : 'asc',
                width :150,
                template: function(t) {
                    return Utils.dateIndonesia(t.created,true,true)
                }
            }];
            DatatableRemoteAjaxDemo.init("",columnData,"<?=$this->request->getParam('_csrfToken');?>")
        });
    </script>
<?php $this->end();?>