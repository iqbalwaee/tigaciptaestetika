<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table">
            <tr>
                <th scope="row" width="200px"><?= __('Nama Perusahaan') ?></th>
                <td><?= h($developer->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Nama CEO') ?></th>
                <td><?= h($developer->ceo_name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('DPD Asal ') ?></th>
                <td><?= h($developer->dpp_from->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('email') ?></th>
                <td><?= h($developer->email) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('No. Telepon') ?></th>
                <td><?= h($developer->phone) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Alamat') ?></th>
                <td><?= h($developer->address) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Tanggal Daftar') ?></th>
                <td><?= h($developer->created->format('d-m-Y')) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Status') ?></th>
                <td><?= $this->Utilities->statusAccountLabel($developer->status)?></td>
            </tr>
        </table>
        <!--end: Datatable -->
    </div>
</div>
