    <!-- begin::Body -->
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor-desktop m-grid--desktop m-body">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-container m-container--responsive m-container--xxl m-container--full-height">
        <div class="m-grid__item m-grid__item--fluid m-wrapper">
            <!-- BEGIN: Subheader -->
            <div class="m-subheader ">
                <div class="d-flex align-items-center">
                    <div class="mr-auto">
                        <?php if(!empty($titleModule)):?>
                            <h3 class="m-subheader__title "><?=$titleModule;?></h3>	
                        <?php endif;?>
                        <?php if(!empty($breadCrumbs)):?>
                        <ul class="m-subheader__breadcrumbs m-nav m-nav--inline">
                            <li class="m-nav__item m-nav__item--home">
                                <a href="<?=$this->request->base;?>" class="m-nav__link m-nav__link--icon">
                                    <i class="m-nav__link-icon la la-home"></i>
                                </a>
                            </li>
                            <?php
                                foreach($breadCrumbs as $link => $label):
                            ?>
                                <li class="m-nav__separator">
                                    -
                                </li>
                                <li class="m-nav__item">
                                    <a href="<?=$link;?>" class="m-nav__link">
                                        <span class="m-nav__link-text">
                                            <?=$label;?>
                                        </span>
                                    </a>
                                </li>
                            <?php
                                endforeach;
                            ?>
                        </ul>
                        <?php endif;?>		
                    </div>
                </div>
            </div>
            <!-- END: Subheader -->		        
            <div class="m-content">
                <?=$this->Flash->render();?>
                <?=$this->fetch('content');?>
            </div>
        </div>
    </div>
</div>