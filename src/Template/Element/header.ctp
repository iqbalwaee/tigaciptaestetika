<?php $imageLogo = $this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Logo']);?>
<!-- BEGIN: Header -->
<header id="m_header" class="m-grid__item    m-header "  m-minimize="minimize" m-minimize-mobile="minimize" m-minimize-offset="200" m-minimize-mobile-offset="200" >
	<div class="m-container m-container--fluid m-container--full-height">
		<div class="m-stack m-stack--ver m-stack--desktop  m-header__wrapper">		
			<!-- BEGIN: Brand -->
            <div class="m-stack__item m-brand m-brand--mobile">
                <div class="m-stack m-stack--ver m-stack--general">
                    <div class="m-stack__item m-stack__item--middle m-brand__logo">
                        <a href="<?=$this->request->base;?>/" class="m-brand__logo-wrapper">
                        <img alt="" src="<?=$imageLogo;?>"/>
                        </a>  
                    </div>
                    <div class="m-stack__item m-stack__item--middle m-brand__tools">
                            <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                            <!-- <a href="javascript:;" id="m_aside_left_toggle_mobile" class="m-brand__icon m-brand__toggler m-brand__toggler--left">
                                <span></span>
                            </a> -->
                            <!-- END -->
                            

                            <!-- BEGIN: Responsive Header Menu Toggler -->
                            <a id="m_aside_header_menu_mobile_toggle" href="javascript:;" class="m-brand__icon m-brand__toggler">
                                <span></span>
                            </a>
                            <!-- END -->
                                    
                            <?php if(!empty($userData)):?>
                                <!-- BEGIN: Topbar Toggler -->
                                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;" class="m-brand__icon">
                                    <i class="flaticon-more"></i>
                                </a>
                                <!-- BEGIN: Topbar Toggler -->
                            <?php endif;?>
                    </div>
                </div>
            </div>
            <!-- END: Brand -->			
                        
            <div class="m-stack__item m-stack__item--middle m-stack__item--left m-header-head" id="m_header_nav">
                <div class="m-stack m-stack--ver m-stack--desktop">
                    <!-- <div class="m-stack__item m-stack__item--middle m-stack__item--fit"> -->
                        <!-- BEGIN: Aside Left Toggle -->
                        <!-- <a href="javascript:;" id="m_aside_left_toggle" class="m-aside-left-toggler m-aside-left-toggler--left m_aside_left_toggler">
                            <span></span>
                        </a> -->
                        <!-- END: Aside Left Toggle -->
                    <!-- </div> -->
                    <div class="m-stack__item m-stack__item--middle m-stack__item--fluid" style="width:150px">
                        <!-- BEGIN: Brand -->
                        <a href="<?=$this->request->base;?>/" class="m-brand m-brand--desktop">
                            <img alt="" src="<?=$imageLogo;?>" style="max-width:100%;"/>
                        </a>
                        <!-- END: Brand -->			
                    </div>

                    <div class="m-stack__item m-stack__item--fluid">
                    <!-- BEGIN: Horizontal Menu -->
                        <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark " id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                        <div id="m_header_menu" class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark "  >	
                            <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                                <?php if(empty($userData)):?>
                                    <li class="m-menu__item " >
                                        <a  href="<?=$this->Url->build(['controller'=>'Pages','action'=>'registerDeveloper']);?>" class="m-menu__link" title="Register Developer">
                                            <span class="m-menu__item-here"></span>
                                            <i class="m-menu__link-icon flaticon-user"></i>
                                            <span class="m-menu__link-text">Register Developer</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item " >
                                        <a  href="<?=$this->Url->build(['controller'=>'Pages','action'=>'loginDeveloper']);?>" class="m-menu__link" title="Login Developer">
                                            <span class="m-menu__item-here"></span>
                                            <i class="m-menu__link-icon flaticon-user"></i>
                                            <span class="m-menu__link-text">Login Developer</span>
                                        </a>
                                    </li>
                                    <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" aria-haspopup="true">
                                        <a  href="javascript:;" class="m-menu__link m-menu__toggle" title="Login Pengurus">
                                            <span class="m-menu__item-here"></span>
                                            <i class="m-menu__link-icon flaticon-users"></i>
                                            <span class="m-menu__link-text">Login Pengurus</span>
                                            <i class="m-menu__hor-arrow la la-angle-down"></i>
                                            <i class="m-menu__ver-arrow la la-angle-right"></i>
                                        </a>
                                        <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                                            <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                            <ul class="m-menu__subnav">
                                                <li class="m-menu__item "  aria-haspopup="true">
                                                    <a  href="<?=$this->Url->build(['controller'=>'Pages','action'=>'loginDpd']);?>" class="m-menu__link " title="Login DPD" >
                                                        <i class="m-menu__link-icon flaticon-user"></i>
                                                        <span class="m-menu__link-text">Login DPD</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item "  aria-haspopup="true">
                                                    <a  href="<?=$this->Url->build(['controller'=>'Pages','action'=>'loginDpp']);?>" class="m-menu__link " title="Login DPP" >
                                                        <i class="m-menu__link-icon flaticon-user"></i>
                                                        <span class="m-menu__link-text">Login DPP</span>
                                                    </a>
                                                </li>
                                                <li class="m-menu__item "  aria-haspopup="true">
                                                    <a  href="<?=$this->Url->build(['controller'=>'Pages','action'=>'loginKonsultan']);?>" class="m-menu__link " title="Login Konsultan" >
                                                        <i class="m-menu__link-icon flaticon-user"></i>
                                                        <span class="m-menu__link-text">Login Konsultan</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="m-menu__item " >
                                        <a  href="<?=$this->Url->build(['controller'=>'Pages','action'=>'checkPengawas']);?>" class="m-menu__link" title="Check Pengawas">
                                            <span class="m-menu__item-here"></span>
                                            <i class="m-menu__link-icon flaticon-user"></i>
                                            <span class="m-menu__link-text">Check Pengawas</span>
                                        </a>
                                    </li>
                                    <?php else:?>
                                        <?php if(array_key_exists('Dashboard',$sidebarList)):?>
                                            <li class="m-menu__item " >
                                                <a  href="<?=$this->Url->build(['controller'=>'Dashboard','action'=>'index']);?>" class="m-menu__link" title="Dashboard">
                                                    <span class="m-menu__item-here"></span>
                                                    <i class="m-menu__link-icon flaticon-user"></i>
                                                    <span class="m-menu__link-text">Dashboard</span>
                                                </a>
                                            </li>
                                        <?php endif;?>
                                        <?php 
                                            $countCheckSideBar = $this->Utilities->sideBarArrayCheck($sidebarList,['MasterDpds','MasterDpps']);
                                            if($countCheckSideBar > 0):
                                        ?>
                                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" aria-haspopup="true">
                                                <a  href="javascript:;" class="m-menu__link m-menu__toggle" title="Master Data">
                                                    <span class="m-menu__item-here"></span>
                                                    <i class="m-menu__link-icon flaticon-users"></i>
                                                    <span class="m-menu__link-text">Master Data</span>
                                                    <i class="m-menu__hor-arrow la la-angle-down"></i>
                                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                                </a>
                                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                                                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                                    <ul class="m-menu__subnav">
                                                    <?php 
                                                        $navList = (object)[
                                                            'MasterDpds' => (object)[
                                                                'label' => 'Master Dpd',
                                                                'icon' => 'flaticon-users',
                                                                'url'  => $this->Url->build(['controller'=>'MasterDpds','action'=>'index'])
                                                            ],
                                                            'MasterDpps' => (object)[
                                                                'label' => 'Master Dpp',
                                                                'icon' => 'flaticon-users',
                                                                'url'  => $this->Url->build(['controller'=>'MasterDpps','action'=>'index'])
                                                            ],
                                                        ];
                                                        foreach($navList as $key => $nav):
                                                            if(array_key_exists($key,$sidebarList)):
                                                    ?>
                                                        <li class="m-menu__item "  aria-haspopup="true">
                                                            <a  href="<?=$nav->url;?>" class="m-menu__link " title="<?=$nav->label;?>" >
                                                                <i class="m-menu__link-icon <?=$nav->icon;?>"></i>
                                                                <span class="m-menu__link-text"><?=$nav->label;?></span>
                                                            </a>
                                                        </li>
                                                        <?php endif?>
                                                    <?php endforeach;?>
                                                    </ul>
                                                </div>
                                            </li>
                                        <?php endif?>
                                        <?php 
                                            $countCheckSideBar = $this->Utilities->sideBarArrayCheck($sidebarList,['Groups','Users']);
                                            if($countCheckSideBar > 0):
                                        ?>
                                            <li class="m-menu__item  m-menu__item--submenu m-menu__item--rel" m-menu-submenu-toggle="click" aria-haspopup="true">
                                                <a  href="javascript:;" class="m-menu__link m-menu__toggle" title="Accesibility">
                                                    <span class="m-menu__item-here"></span>
                                                    <i class="m-menu__link-icon flaticon-users"></i>
                                                    <span class="m-menu__link-text">Accesibility</span>
                                                    <i class="m-menu__hor-arrow la la-angle-down"></i>
                                                    <i class="m-menu__ver-arrow la la-angle-right"></i>
                                                </a>
                                                <div class="m-menu__submenu m-menu__submenu--classic m-menu__submenu--left">
                                                    <span class="m-menu__arrow m-menu__arrow--adjust"></span>
                                                    <ul class="m-menu__subnav">
                                                    <?php 
                                                        $navList = (object)[
                                                            'Groups' => (object)[
                                                                'label' => 'Group',
                                                                'icon' => 'flaticon-users',
                                                                'url'  => $this->Url->build(['controller'=>'Groups','action'=>'index'])
                                                            ],
                                                            'Users' => (object)[
                                                                'label' => 'User',
                                                                'icon' => 'flaticon-users',
                                                                'url'  => $this->Url->build(['controller'=>'Users','action'=>'index'])
                                                            ],
                                                        ];
                                                        foreach($navList as $key => $nav):
                                                            if(array_key_exists($key,$sidebarList)):
                                                    ?>
                                                        <li class="m-menu__item "  aria-haspopup="true">
                                                            <a  href="<?=$nav->url;?>" class="m-menu__link " title="Login DPD" >
                                                                <i class="m-menu__link-icon <?=$nav->icon;?>"></i>
                                                                <span class="m-menu__link-text"><?=$nav->label;?></span>
                                                            </a>
                                                        </li>
                                                        <?php endif?>
                                                    <?php endforeach;?>
                                                    </ul>
                                                </div>
                                            </li>
                                        <?php endif?>
                                        <?php if(array_key_exists('AppSettings',$sidebarList)):?>
                                            <li class="m-menu__item " >
                                                <a  href="<?=$this->Url->build(['controller'=>'AppSettings','action'=>'index']);?>" class="m-menu__link" title="App Settings">
                                                    <span class="m-menu__item-here"></span>
                                                    <i class="m-menu__link-icon flaticon-user"></i>
                                                    <span class="m-menu__link-text">App Settings</span>
                                                </a>
                                            </li>
                                        <?php endif;?>
                                        <?php if($userData->group_id == 2 || $userData->group_id == 4 || $userData->group_id == 3 || $userData->group_id == 5):?>
                                            <li class="m-menu__item " >
                                                <a  href="<?=$this->request->base;?>/" class="m-menu__link" title="Home">
                                                    <span class="m-menu__item-here"></span>
                                                    <i class="m-menu__link-icon flaticon-home"></i>
                                                    <span class="m-menu__link-text">Home</span>
                                                </a>
                                            </li>
                                            <?php 
                                                $navList = [
                                                    'ApplicationLetters' => (object)[
                                                        'label' => 'Form Pengajuan',
                                                        'icon' => 'flaticon-app',
                                                        'url' => $this->Url->build(['controller'=>'ApplicationLetters','action'=>'index'])
                                                    ],
                                                ];
                                                if($userData->group_id == 2){
                                                    $navList += [
                                                        'Data Konsultan' => (object)[
                                                            'label' => 'Data Konsultan',
                                                            'icon' => 'flaticon-presentation',
                                                            'url' => $this->Url->build(['controller'=>'Pages','action'=>'supervisor-profile'])
                                                        ],
                                                        'Check Pengawas' => (object)[
                                                            'label' => 'Check Pengawas',
                                                            'icon' => 'flaticon-user',
                                                            'url' => $this->Url->build(['controller'=>'Pages','action'=>'checkPengawas'])
                                                        ],
                                                    ];
                                                }
                                                if($userData->group_id == 3){
                                                    $navList += [
                                                        'Developers' => (object)[
                                                            'label' => 'Data Developer',
                                                            'icon' => 'flaticon-network',
                                                            'url' => $this->Url->build(['controller'=>'Developers','action'=>'index'])
                                                        ],
                                                        'AppSettings' => (object)[
                                                            'label' => 'DPP Settings',
                                                            'icon' => 'flaticon-cogwheel-2',
                                                            'url' => $this->Url->build(['controller'=>'AppSettings','action'=>'index'])
                                                        ],
                                                        'MasterDpds' => (object)[
                                                            'label' => 'Data DPD',
                                                            'icon' => 'flaticon-network',
                                                            'url' => $this->Url->build(['controller'=>'MasterDpds','action'=>'index'])
                                                        ],
                                                        'Consultants' => (object)[
                                                            'label' => 'Data Konsultan',
                                                            'icon' => 'flaticon-cogwheel-2',
                                                            'url' => $this->Url->build(['controller'=>'Consultants','action'=>'index'])
                                                        ],
                                                    ];
                                                }
                                                if($userData->group_id == 5){
                                                    $navList += [
                                                        'Supervisors' => (object)[
                                                            'label' => 'Data Pengawas',
                                                            'icon' => 'flaticon-network',
                                                            'url' => $this->Url->build(['controller'=>'Supervisors','action'=>'index'])
                                                        ],
                                                    ];
                                                }
                                                foreach($navList as $key => $nav):
                                            ?>
                                                <li class="m-menu__item " >
                                                    <a  href="<?=$nav->url;?>" class="m-menu__link" title="<?=$nav->label;?>">
                                                        <span class="m-menu__item-here"></span>
                                                        <i class="m-menu__link-icon <?=$nav->icon;?>"></i>
                                                        <span class="m-menu__link-text"><?=$nav->label;?></span>
                                                    </a>
                                                </li>
                                            <?php endforeach;?>
                                            <li class="m-menu__item " >
                                                <a  href="<?=$this->request->base;?>/logout" class="m-menu__link" title="Logout">
                                                    <span class="m-menu__item-here"></span>
                                                    <i class="m-menu__link-icon flaticon-logout"></i>
                                                    <span class="m-menu__link-text">Logout</span>
                                                </a>
                                            </li>
                                        <?php endif;?>
                                <?php endif?>
                            </ul>
                        </div>
                    <!-- END: Horizontal Menu -->
                    </div>
                </div>			
            </div>
            <?php if(!empty($userData)):?>
                <div class="m-stack__item m-stack__item--right" style="width:150px;">
                    <!-- BEGIN: Topbar -->
                    <div id="m_header_topbar" class="m-topbar  m-stack m-stack--ver m-stack--general">
                        
                        <div class="m-stack__item m-topbar__nav-wrapper">
                            <ul class="m-topbar__nav m-nav m-nav--inline">
                                <li class="m-nav__item ">
                                    <a href="<?=$this->Url->build(['controller'=>'Pages','action'=>'editProfile']);?>" class="m-nav__link">		
                                    <?php if($userData['group_id'] != 2):?>    
                                        <span class="m-topbar__username m--hidden-mobile"><?=$userData['username'];?></span>	
                                        <span class="m-topbar__userpic">
                                            <img src="<?=$this->request->base;?>/assets/img/user.png" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                                        </span>
                                        <span class="m-nav__link-icon m-topbar__usericon  m--hide">
                                            <span class="m-nav__link-icon-wrapper"><i class="flaticon-user-ok"></i></span>
                                        </span>			
                                    <?php else:?>
                                        <span class="m-topbar__username m--hidden-mobile"><?=$userData['developer']['name'];?></span>	
                                        <span class="m-topbar__userpic">
                                            <?php $imageUsr = $this->Utilities->generateUrlImage($userData['developer']['logo_dir'],$userData['developer']['logo'],'','assets/img/user.png');?>
                                            <img src="<?=$imageUsr;?>" class="m--img-rounded m--marginless m--img-centered" alt=""/>
                                        </span>
                                        <span class="m-nav__link-icon m-topbar__usericon  m--hide">
                                            <span class="m-nav__link-icon-wrapper"><i class="flaticon-user-ok"></i></span>
                                        </span>
                                    <?php endif;?>
                                    </a>
                                </li>	
                            </ul>
                        </div>
                    </div>
                <!-- END: Topbar -->			
                </div>
            <?php endif;?>
		</div>
	</div>
</header>
<!-- END: Header -->		