<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <?=$this->element('header');?>
    <?=$this->element('body');?>
    <?=$this->element('footer');?>
</div>
<?=$this->element('aside');?>