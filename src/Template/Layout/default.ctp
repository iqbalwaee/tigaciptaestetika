
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$this->fetch('meta-description');?>">
    <meta name="keywords" content="<?=$this->fetch('meta-keywords');?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>?v2" />
    
    <meta name="google-site-verification" content="F91Lc2EfHG5LCp5Zse7qVNhsqhuKJ_ZJy5xXTGMiEXo" />
    <!-- Favicon icon -->
    <?php
        $cssExternal = [];
        $cssDefault = [
            'dist/vendors/base/vendors.bundle.css?v=3',
            'dist/demo/default/base/style.bundle.css?v=3',
        ];
        
        $cssMain = [
            
        ];
        
        $this->Html->css($cssExternal,['block'=>'cssExternal']);
        $this->Html->css($cssDefault,['block'=>'cssDefault','pathPrefix' => '/assets/']);
        $this->Html->css($cssMain,['block'=>'cssMain','pathPrefix' => '/assets/']);
    
        echo $this->fetch('cssExternal');
        echo $this->fetch('cssDefault');
        echo $this->fetch('cssPlugin');
        echo $this->fetch('cssMain');
    ?>

</head>
    <body class="m--skin- m-page--loading-enabled m-page--loading m-content--skin-light m-header--fixed m-header--fixed-mobile m-aside-left--offcanvas-default m-aside-left--enabled m-aside-left--fixed m-aside-left--skin-dark m-aside--offcanvas-default">

        <!-- begin::Page loader -->
            <div class="m-page-loader m-page-loader--base">
                <div class="m-blockui">
                    <span>Please wait...</span>
                    <span><div class="m-loader m-loader--brand"></div></span>
                </div>
            </div>
            
        <!-- end::Page Loader -->   
        <?=$this->element('main');?>
        <script>
            var urlActivities = "<?=$this->Url->build(['controller'=>'Pages','action'=>'activitiesLog']);?>";
        </script>
        <?php
            $jsDefault = [
                'dist/vendors/base/vendors.bundle.js?v=3',
                'dist/demo/default/base/scripts.bundle.js?v=3',
                'dist/demo/default/custom/crud/metronic-datatable/base/data-ajax.js?v=3',
            ];
            $jsMain = [
                
            ];
            $this->Html->script($jsDefault,[
                'block'=>'jsDefault',
                'pathPrefix' => '/assets/'
                ]
            );
            $this->Html->script($jsDefault,[
                'block'=>'jsPlugin',
                'pathPrefix' => '/assets/'
                ]
            );
            $this->Html->script($jsMain,['block'=>'jsMain','pathPrefix' => '/assets/']);

            echo $this->fetch('jsDefault');
            echo $this->fetch('jsPlugin');
            echo $this->fetch('jsMain');
            echo $this->fetch('script');
        ?>
         <!-- begin::Page Loader -->
         <script>
            $(window).on('load', function() {
                $('body').removeClass('m-page--loading');         
            });
        </script>       
        <!-- end::Page Loader -->
        <script>
            
        </script>
        <!-- </body></html> -->
    </body>
</html>