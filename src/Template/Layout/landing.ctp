
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>" />
    <!-- Favicon icon -->
    <?php
        $cssExternal = [];
        $cssDefault = [
            'dist/css/style.bundle.css?v=2',
        ];
        
        $cssMain = [
            
        ];
        
        $this->Html->css($cssExternal,['block'=>'cssExternal']);
        $this->Html->css($cssDefault,['block'=>'cssDefault','pathPrefix' => '/assets/landing/']);
        $this->Html->css($cssMain,['block'=>'cssMain','pathPrefix' => '/assets/assets/landing/']);
    
        echo $this->fetch('cssExternal');
        echo $this->fetch('cssDefault');
        echo $this->fetch('cssPlugin');
        echo $this->fetch('cssMain');
    ?>

</head>
    <body class="">
        <script>
            var baseUrl = "<?=$this->Url->build('/',true);?>";
        </script>
        <section class="header-nav">
            <nav class="navbar">
                <ul>
                    <li>
                        <a href="#" class="show-aside" data-target="login-dev"><i class="fa fa-user"></i> Login Developer</a>
                    </li>
                    <li>
                        <a href="#" class="show-aside" data-target="register-dev"><i class="fa fa-user"></i> Register Developer</a>
                    </li>
                    <li>
                        <a href="#" class="show-aside" data-target="login-dpd"><i class="fa fa-user"></i> Login DPD</a>
                    </li>
                    <li>
                        <a href="#" class="show-aside" data-target="login-dpp"><i class="fa fa-user"></i> Login DPP</a>
                    </li>
                    <li>
                        <a href="#" class="show-aside" data-target="login-consultant"><i class="fa fa-user"></i> Login Konsultan</a>
                    </li>
                    <li>
                        <a href="#" class="show-aside" data-target="login-pengawas"><i class="fa fa-user"></i> Pengawas</a>
                    </li>
                </ul>
            </nav>
        </section>
        <section class="welcome-message">
            <div class="wrapper">
                <h1>Selamat Datang di Website <?=$defaultAppSettings['App.Name'];?></h1>
                <p>Anda dapat melakukan pengajuan di website ini dengan melakukan registrasi di menu login dengan melengkapi data perusahaan anda. Semua pengajuan yang masuk akan kami layani 24 jam. </p>
            </div>
        </section>
        <aside class="aside-form" id="login-dev">
            <a class="close-aside" href="#"><i class="fa fa-times"></i></a>
            <section class="wrapper">
                <h3 class="title-wrapper">Login Developer</h3>
                <?=$this->Form->create(null,['class'=>'m-login__form m-form','id'=>'log-dev']);?>
                <input type="hidden" name="type" value="developer" id="type-dev">
                <?=$this->Form->controls([
                    'username' => ['label'=>'Username','autocomplete'=>'off','placeholder'=>'Masukan Username','class'=>'form-control m-input m-login__form-input--last','id'=>'username-dev'],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <?php
                    $this->Form->setTemplates([
                        'input' => '<input type="{{type}}" name="{{name}}" {{attrs}}/>',
                    ]);
                ?>
                <?=$this->Form->controls([
                    'password' => ['label'=>'Password','autocomplete'=>'off','placeholder'=>'Masukan Password','class'=>'form-control m-input m-login__form-input--last','id'=>'pass-dev',],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <div class="m-login__form-action">
                    <button id="m_login_signin_submit1" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                        Sign In
                    </button>
                </div>
            </form>
            </section>
        </aside>
        <aside class="aside-form aside-form__full" id="register-dev">
            <a class="close-aside" href="#"><i class="fa fa-times"></i></a>
            <section class="wrapper">
                <h3 class="title-wrapper">Register Developer</h3>
                <?=$this->Form->create($developer,['class'=>'m-register__form m-form','id'=>'reg-dev','url'=>['action'=>'register']]);?>
                    <div class="col-2">
                        <?=$this->Form->controls([
                            'name' => ['label'=>'Nama Perusahaan','autocomplete'=>'off','placeholder'=>'Masukan Nama Perusahaan','class'=>'form-control m-input m-login__form-input--last','id'=>'name'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?=$this->Form->controls([
                            'ceo_name' => ['label'=>'Nama Direktur','autocomplete'=>'off','placeholder'=>'Masukan Nama Direktur','class'=>'form-control m-input m-login__form-input--last','id'=>'ceo-name'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?=$this->Form->controls([
                            'nik_ceo' => ['label'=>'NIK Direktur','autocomplete'=>'off','placeholder'=>'Masukan NIK Direktur','class'=>'form-control m-input m-login__form-input--last','id'=>'ceo-nik'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?=$this->Form->controls([
                            'kta_number' => ['label'=>'No Kta ','autocomplete'=>'off','placeholder'=>'Masukan No Kta ','class'=>'form-control m-input m-login__form-input--last'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?=$this->Form->controls([
                            'dpp_from_id' => ['label'=>'Asal DPD ','autocomplete'=>'off','empty'=>'Pilih Asal DPD ','class'=>'form-control m-input m-login__form-input--last', 'options' => $provinces],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?=$this->Form->controls([
                            'address' => ['label'=>'Alamat','autocomplete'=>'off','placeholder'=>'Masukan Alamat','class'=>'form-control m-input m-login__form-input--last'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                    </div> 
                    <div class="col-2">
                        <?=$this->Form->controls([
                            'email' => ['label'=>'Email','autocomplete'=>'off','placeholder'=>'Masukan Email','class'=>'form-control m-input m-login__form-input--last'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?=$this->Form->controls([
                            'phone' => ['label'=>'No. Telepon','autocomplete'=>'off','placeholder'=>'Masukan No. Telepon','class'=>'form-control m-input m-login__form-input--last'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?=$this->Form->controls([
                            'pic' => ['label'=>'Penanggung Jawab','autocomplete'=>'off','placeholder'=>'Masukan Penanggung Jawab','class'=>'form-control m-input m-login__form-input--last'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?=$this->Form->controls([
                            'pic_phone' => ['label'=>'No. Telp. Penanggung Jawab','autocomplete'=>'off','placeholder'=>'Masukan No. Telp. Penanggung Jawab','class'=>'form-control m-input m-login__form-input--last'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?=$this->Form->controls([
                            'user.username' => ['label'=>'Username','autocomplete'=>'off','placeholder'=>'Masukan Username','class'=>'form-control m-input m-login__form-input--last','id'=>'username-rdev'],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <?php
                            $this->Form->setTemplates([
                                'input' => '<input type="{{type}}" name="{{name}}" {{attrs}}/>',
                            ]);
                        ?>
                        <?=$this->Form->controls([
                            'user.password' => ['label'=>'Password','autocomplete'=>'off','placeholder'=>'Masukan Password','class'=>'form-control m-input m-login__form-input--last','id'=>'pass-rdev',],
                        ],[
                            'legend'=>false,
                            'fieldset' => false
                        ]);?>
                        <div class="m-login__form-action">
                            <button id="m_register_signin_submit1" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                Sign Up
                            </button>
                        </div>
                    </div>
                    
            </form>
            </section>
        </aside>
        <aside class="aside-form" id="login-dpd">
            <a class="close-aside" href="#"><i class="fa fa-times"></i></a>
            <section class="wrapper">
                <h3 class="title-wrapper">Login DPD</h3>
                <?=$this->Form->create(null,['class'=>'m-login__form m-form','id'=>'log-dpd']);?>
                <input type="hidden" name="type" value="dpd" id="type-dpd">
                <?=$this->Form->controls([
                    'username' => ['label'=>'Provinsi','autocomplete'=>'off','empty'=>'Pilih Provinsi','class'=>'form-control m-input m-login__form-input--last','id'=>'username-dpd','options'=>$provinces],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <?php
                    $this->Form->setTemplates([
                        'input' => '<input type="{{type}}" name="{{name}}" {{attrs}}/>',
                    ]);
                ?>
                <?=$this->Form->controls([
                    'password' => ['label'=>'Password','autocomplete'=>'off','placeholder'=>'Masukan Password','class'=>'form-control m-input m-login__form-input--last','id'=>'pass-dpd'],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <div class="m-login__form-action">
                    <button id="m_login_signin_submit4" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                        Sign In
                    </button>
                </div>
            </form>
            </section>
        </aside>
        <aside class="aside-form" id="login-dpp">
            <a class="close-aside" href="#"><i class="fa fa-times"></i></a>
            <section class="wrapper">
                <h3 class="title-wrapper">Login DPP</h3>
                <?=$this->Form->create(null,['class'=>'m-login__form m-form','id'=>'log-dpp']);?>
                <input type="hidden" name="type" value="dpp" id="type-dpp">
                <?=$this->Form->controls([
                    'username' => ['label'=>'Username','autocomplete'=>'off','placeholder'=>'Masukan Username','class'=>'form-control m-input m-login__form-input--last','id'=>'username-dpp'],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <?php
                    $this->Form->setTemplates([
                        'input' => '<input type="{{type}}" name="{{name}}" {{attrs}}/>',
                    ]);
                ?>
                <?=$this->Form->controls([
                    'password' => ['label'=>'Password','autocomplete'=>'off','placeholder'=>'Masukan Password','class'=>'form-control m-input m-login__form-input--last','id'=>'pass-dpp'],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <div class="m-login__form-action">
                    <button id="m_login_signin_submit2" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                        Sign In
                    </button>
                </div>
            </form>
            </section>
        </aside>
        <aside class="aside-form" id="login-consultant">
        <a class="close-aside" href="#"><i class="fa fa-times"></i></a>
            <section class="wrapper">
                <h3 class="title-wrapper">Login Konsultan</h3>
                <?=$this->Form->create(null,['class'=>'m-login__form m-form','id'=>'log-con']);?>
                <input type="hidden" name="type" value="consultant" id="type-con">
                <?=$this->Form->controls([
                    'username' => ['label'=>'Username','autocomplete'=>'off','placeholder'=>'Masukan Username','class'=>'form-control m-input m-login__form-input--last','id'=>'username-con'],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <?php
                    $this->Form->setTemplates([
                        'input' => '<input type="{{type}}" name="{{name}}" {{attrs}}/>',
                    ]);
                ?>
                <?=$this->Form->controls([
                    'password' => ['label'=>'Password','autocomplete'=>'off','placeholder'=>'Masukan Password','class'=>'form-control m-input m-login__form-input--last','id'=>'pass-con'],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <div class="m-login__form-action">
                    <button id="m_login_signin_submit3" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                        Sign In
                    </button>
                </div>
            </form>
            </section>
        </aside>
        <aside class="aside-form" id="login-pengawas">
        <a class="close-aside" href="#"><i class="fa fa-times"></i></a>
            <section class="wrapper">
                <h3 class="title-wrapper">Cek Data Ketersediaan Pengawas</h3>
                <?=$this->Form->create(null,['class'=>'m-login__form m-form','id'=>'log-pengawas','url' => [
                    'action' => 'checkPengawas'
                ]]);?>
                <?=$this->Form->controls([
                    'nik' => ['label'=>'NIK','autocomplete'=>'off','placeholder'=>'Masukan NIK','class'=>'form-control m-input m-login__form-input--last','id'=>'nik'],
                ],[
                    'legend'=>false,
                    'fieldset' => false
                ]);?>
                <div class="m-login__form-action">
                    <button id="m_login_signin_submit3" type="submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                        Cek
                    </button>
                </div>
            </form>
            </section>
        </aside>
        <div class="overlay">
            <div class="overlay-wrapper">
            <div class="overlay-wrapper-title">
                    <h3>Data Pengawas</h3>
                </div>
                <div class="overlay-wrapper-body">
                    <table class="table table-header">
                        <tr>
                            <td width="20%">Nama Pengawas</td>
                            <td width="2%">:</td>
                            <td width="28%" class="supervisor-name"></td>
                            <td width="20%">NIK</td>
                            <td width="2%">:</td>
                            <td width="28%" class="supervisor-nik"></td>
                        </tr>
                        <tr>
                            <td width="20%">Email</td>
                            <td width="2%">:</td>
                            <td width="28%" class="supervisor-email"></td>
                            <td width="20%">No. Telepon</td>
                            <td width="2%">:</td>
                            <td width="28%" class="supervisor-phone"></td>
                        </tr>
                        <tr>
                            <td width="20%">Alamat</td>
                            <td width="2%">:</td>
                            <td width="78%" class="supervisor-address" colspan="4"></td>
                        </tr>
                        <tr>
                            <td width="20%">File Identitas</td>
                            <td width="2%">:</td>
                            <td width="28%">
                                <a href="#" target="_BLANK" class="supervisor-identity"><i class="fa fa-download"></i> Download</a>
                            </td>
                            <td width="20%">Form Checklist</td>
                            <td width="2%">:</td>
                            <td width="28%" class="">
                            <a href="<?=$this->Utilities->generateUrlImage($consultant->form_checklist_dir,$consultant->form_checklist);?>" target="_BLANK" class=""><i class="fa fa-download"></i> Download</a>
                            </td>
                        </tr>
                    </table>
                    <div class="table-responsive">
                        <table class="table table-detail" cellspacing=0>
                            <thead>
                                <tr>
                                    <th width="10%">No.</th>
                                    <th width="30%">Developer</th>
                                    <th>Nama Perumahan</th>
                                    <th width="10%">Upload</th>
                                </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>
                    <form class="form-table" action="<?=$this->Url->build(['action'=>'uploadPengawas']);?>" enctype="multipart/form-data">
                        <input type="hidden" value="0" name="id" id="id_app">
                        <table class="table table-form">
                            <tbody>
                                <tr>
                                    <td colspan="3">Upload File Pendukung: </td>
                                </tr>
                                <tr>
                                    <td width="20%">Nama Perumahan</td>
                                    <td width="5%">:</td>
                                    <td class="application-name"></td>
                                </tr>
                                <tr>
                                    <td width="20%">Kode Pengajuan</td>
                                    <td width="5%">:</td>
                                    <td class="application-code"></td>
                                </tr>
                                <tr>
                                    <td width="20%">Surat Pernyataan</td>
                                    <td width="5%">:</td>
                                    <td class="application-surat-pernyataan">
                                        <input type="file" name="surat_pernyataan" id="surat_pernyataan" accept="application/pdf" >
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">Lembar Pemeriksaan</td>
                                    <td width="5%">:</td>
                                    <td class="application-lembar-pemeriksaan">
                                        <input type="file" name="lembar_pemeriksaan" id="lembar_pemeriksaan" accept="application/pdf">
                                    </td>
                                </tr>
                                <tr>
                                    <td width="20%">Form Checklist</td>
                                    <td width="5%">:</td>
                                    <td class="application-form-checklist">
                                        <input type="file" name="form_checklist" id="form_chekclist" accept="application/pdf">
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <button type="submit" class="btn inline">UPLOAD</button>
                                        <a href="#" class="btn inline default btn-close-table">TUTUP</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
        <script>
        </script>
        <?php
            $jsDefault = [
                'dist/js/script.bundle.js?v=2',
            ];
            $jsMain = [
                
            ];
            $this->Html->script($jsDefault,[
                'block'=>'jsDefault',
                'pathPrefix' => '/assets/landing/'
                ]
            );
            $this->Html->script($jsDefault,[
                'block'=>'jsPlugin',
                'pathPrefix' => '/assets/landing/'
                ]
            );
            $this->Html->script($jsMain,['block'=>'jsMain','pathPrefix' => '/assets/landing/']);

            echo $this->fetch('jsDefault');
            echo $this->fetch('jsPlugin');
            echo $this->fetch('jsMain');
            echo $this->fetch('script');
        ?>
        <script>
            
        </script>
    </body>
</html>