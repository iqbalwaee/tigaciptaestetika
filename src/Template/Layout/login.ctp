
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->request->base;?><?=$defaultAppSettings['App.Favico'];?>" />
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
        google: {"families":["Poppins:300,400,500,600,700","Roboto:300,400,500,600,700"]},
        active: function() {
            sessionStorage.fonts = true;
        }
        });
    </script>
    <!-- Favicon icon -->
    <?php
        $cssExternal = [];
        $cssDefault = [
            'dist/vendors/base/vendors.bundle.css',
            'dist/demo/default/base/style.bundle.css',
        ];
        
        $cssMain = [
            
        ];
        
        $this->Html->css($cssExternal,['block'=>'cssExternal']);
        $this->Html->css($cssDefault,['block'=>'cssDefault','pathPrefix' => '/assets/']);
        $this->Html->css($cssMain,['block'=>'cssMain','pathPrefix' => '/assets/']);
    
        echo $this->fetch('cssExternal');
        echo $this->fetch('cssDefault');
        echo $this->fetch('cssPlugin');
        echo $this->fetch('cssMain');
    ?>

</head>
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--signin" id="m_login">
            <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
                <div class="m-stack m-stack--hor m-stack--desktop">
                    <?=$this->fetch('content');?>
                </div>
            </div>
            <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url(<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Login.Cover']);?>)">
                <div class="m-grid__item m-grid__item--middle">
                    <h3 class="m-login__welcome">
                        Welcome to <br><?=$defaultAppSettings['App.Name'];?>
                    </h3>
                    <p class="m-login__msg">
                        <?=$defaultAppSettings['App.Description'];?><br>
                    Powered By : <a href="http://ngecrik.co">@ngecrik__</a> <i class="fa fa-copyright"></i> <?=date('Y');?>
                    </p>
                </div>
            </div>
        </div>
    </div>
                        
                    
    <?php
        $jsDefault = [
            'dist/vendors/base/vendors.bundle.js',
            'dist/demo/default/base/scripts.bundle.js',
            'dist/snippets/pages/user/login.js',
        ];
        $jsMain = [
            
        ];
        $this->Html->script($jsDefault,['block'=>'jsDefault','pathPrefix' => '/assets/']);
        $this->Html->script($jsDefault,['block'=>'jsPlugin','pathPrefix' => '/assets/']);
        $this->Html->script($jsMain,['block'=>'jsMain','pathPrefix' => '/assets/']);

        echo $this->fetch('jsDefault');
        echo $this->fetch('jsPlugin');
        echo $this->fetch('jsMain');
        echo $this->fetch('script');
    ?>
    <script>
        
    </script>
</body>
</html>