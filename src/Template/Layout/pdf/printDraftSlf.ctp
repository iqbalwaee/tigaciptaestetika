
<!DOCTYPE html>
<html lang="en" >
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>" />
    <style>
        body{
            padding: 0px;
            margin: 0px;
            color: #000;
            margin: 0px 0px !important; 
        }

        .bg-fixeds {
            display: block;
            position: fixed;
            top: 40%;
            left: 0;
            right: 0;
            transform:translateY(-50%);
            z-index: 11;
            text-align: center; 
        }
        .bg-fixeds img {
            opacity: 0.15; 
            width:300px;
        }

        .bg-fixedss {
            display: block;
            position: absolute;
            top: 50%;
            left: 0;
            right: 0;
            z-index: 11;
            text-align: center; 
        }
        .bg-fixedss img {
            opacity: 0.15; 
        }
        table {  
    border-collapse: collapse;
}
        .sertifikat-laik {
  color: #000;
  font-family: "Times New Roman", Times, serif;
  font-size:12px;
}
.sertifikat-laik .kop-surat {
  margin-bottom: 30px;
}
.sertifikat-laik .kop-surat .kop-surat-logo {
  float: left;
  padding: 0px;
  width: 20%;
  text-align: center;
}
.sertifikat-laik .kop-surat .kop-surat-logo h3 {
  font-size: 16px;
  margin: 0px;
  padding: 0px;
}
.sertifikat-laik .kop-surat .kop-surat-logo img {
  max-width: 100%;
  width: 120px;
  display: block;
  margin: auto;
}
.sertifikat-laik .kop-surat .kop-surat-title {
  float: left;
  text-align: center;
  width: 60%;
}
.sertifikat-laik .kop-surat .kop-surat-title .table {
  width: 200px;
  text-align: left;
  margin: auto;
  margin-top: 10px;
  margin-bottom: 10px;
}
.sertifikat-laik .kop-surat .kop-surat-title .table th, .sertifikat-laik .kop-surat .kop-surat-title .table td {
  padding: 2px 5px;
  border-top: none;
}
.sertifikat-laik .kop-surat .kop-surat-label {
  float: left;
  text-align: right;
  width: 20%;
}
.sertifikat-laik .kop-surat .kop-surat-label div {
  text-align: center;
  padding: 5px 10px;
  font-weight: bold;
}
.sertifikat-laik .kop-surat .kop-surat-label div.red {
  color: red;
  border: 2px solid red;
}
.sertifikat-laik .kop-surat .kop-surat-label div.green {
  color: green;
  border: 2px solid green;
}
.sertifikat-laik .kop-surat:after {
  clear: both;
  content: " ";
  display: block;
}
.sertifikat-laik .table-info-dev {
  width: 100%;
}
.sertifikat-laik .table-info-dev td {
  padding: 2px 5px;
}
.sertifikat-laik .table-info-house {
  width: 100%;
  margin-top: 10px;
  border: 1px solid #333;
}
.sertifikat-laik .table-info-house td {
  padding: 2px 5px;
}
.sertifikat-laik .table-info-house > tbody > tr > td {
  border-right: 1px solid #333;
  border-bottom: 1px solid #333;
  vertical-align: top;
}
.sertifikat-laik .table-info-house > tbody > tr > td.v-mid {
  vertical-align: middle;
}
.sertifikat-laik .table-info-house > tbody > tr > td:last-child {
  border-right: none;
}
.sertifikat-laik .table-info-house > tbody > tr:last-child td {
  border-bottom: none;
}
.sertifikat-laik .table-info-house .table-info-blok {
  border: 1px solid #333;
  margin-bottom: 10px;
  width: 100%;
}
.sertifikat-laik .table-info-house .table-info-blok tbody tr td {
  width: 20%;
  padding: 2px;
  border-bottom: 1px solid #333;
  border-right: 1px solid #333;
}
.sertifikat-laik .table-info-house .table-info-blok tbody tr td:last-child {
  border-right: none;
}
.sertifikat-laik .table-info-house .table-info-blok tbody tr:last-child td {
  border-bottom: none;
}
.sertifikat-laik .table-info-house .table-sub-info {
  width: 100%;
}
.sertifikat-laik .table-info-house .table-sub-info > tr td {
  text-align: left;
  padding: 2px 5px;
}
.sertifikat-laik .table-footer-sign {
  width: 100%;
  margin-top: 0px;
}
.sertifikat-laik .table-footer-sign .signature {
  display: inline-block;
  width: 350px;
  text-align: center;
}
.sertifikat-laik .divider {
  border-bottom: 2px dashed #ddd;
  margin-bottom: 15px;
}
</style>

</head>
<body class="m-page--fluid m--skin- body-print ">
        <div class="bg-fixeds">
            <img src="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>" alt="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>">
        </div>
        <?=$this->fetch('content');?>
    </body>
</html>