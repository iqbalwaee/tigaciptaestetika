
<!DOCTYPE html>
<html lang="en" >
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>" />
    <style>
        body{
            padding: 0px;
            margin: 0px;
            color: #000;
            margin: 0px 0px !important; 
        }

        .bg-fixeds {
            display: block;
            position: fixed;
            top: 40%;
            left: 0;
            right: 0;
            transform:translateY(-50%);
            z-index: 11;
            text-align: center; 
        }
        .bg-fixeds img {
            opacity: 0.15; 
            width:300px;
        }
        .surat-pernyataan {
            color: #000;
            border-bottom: none;
            font-size: 14px !important;
            z-index: 10;
            position: relative;
            font-family: 'Times New Roman', Times, serif; 
            padding-bottom:140px;
        }
        .kop-surat{
            height:100px;
            border-bottom:4px double #333;
        }
        .title-body{
            display:block;
            text-align:center;
            padding-top:30px;
            padding-bottom:30px;
        }
        .title-body h3,h4{
            display:block;
            margin:0px;
            padding:0px;
        }
        .title-body h4{
            font-size:22px !important;
        }
        .sub-title h3{
            font-size:22px !important;
            text-decoration:underline;
        }
        .text-right{
            text-align:right;
        }
        .content-body table{
            line-height:22px;
        }
</style>


</head>
<body class="m-page--fluid m--skin- body-print ">
        <?=$this->fetch('content');?>
    </body>
</html>