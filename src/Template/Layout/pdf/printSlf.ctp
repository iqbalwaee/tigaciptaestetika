
<!DOCTYPE html>
<html lang="en" >
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>" />
    <style>
        body{
            padding: 0px;
            margin: 0px;
            color: #000;
            margin: 0px 0px !important; 
        }

        .bg-fixeds {
            display: block;
            position: fixed;
            top: 40%;
            left: 0;
            right: 0;
            transform:translateY(-50%);
            z-index: 11;
            text-align: center; 
        }
        .bg-fixeds img {
            opacity: 0.15; 
            width:300px;
        }

        .bg-fixedss {
            display: block;
            position: absolute;
            top: 50%;
            left: 0;
            right: 0;
            z-index: 11;
            text-align: center; 
        }
        .bg-fixedss img {
            opacity: 0.15; 
        }
        .surat-pernyataan {
            color: #000;
            border-bottom: none;
            font-size: 14px !important;
            z-index: 10;
            position: relative;
            font-family: 'Times New Roman', Times, serif; 
        }
        .surat-pernyataan .kop-surat {
            
        }
        .table-kop{
            width:100%;
        }
        .table-kop > tbody td{
            vertical-align:top;
        }
        .surat-pernyataan .kop-surat .title-kop-box {
            display: block;
        }

        .surat-pernyataan .kop-surat .title-kop-box .box-red {
            font-size: 16px;
            color: red;
            padding: 15px;
            display: inline-block;
            padding: 5px 10px;
            border: 2px solid red;
        }

        .surat-pernyataan .kop-surat .kop-surat-logo {
            padding: 0px;
            text-align: center;
        }

        .surat-pernyataan .kop-surat .kop-surat-logo-img {
            width:150px;
        }

        .surat-pernyataan .kop-surat .kop-surat-title {
            text-align: center;
        }

        .surat-pernyataan .kop-surat .kop-surat-title h3 {
            font-weight: bold;
            font-size: 18px !important;
            margin:0 !important;
            padding:0 !important;
            padding-bottom:15px !important;
        }

        .surat-pernyataan .kop-surat .kop-surat-title .table {
            width: 300px;
            margin: auto;
            margin-bottom: 20px;
        }

        .surat-pernyataan .kop-surat .kop-surat-title .table td {
            padding: 0px 0px;
            border-top: none;
        }

        .surat-pernyataan .kop-surat .kop-surat-label {
            float: left;
            text-align: right;
            width: 20%;
        }

        .surat-pernyataan .kop-surat .kop-surat-label div {
            text-align: center;
            padding: 5px 10px;
            font-weight: bold;
        }

        .surat-pernyataan .kop-surat .kop-surat-label div.red {
            color: red;
            border: 2px solid red;
        }

        .surat-pernyataan .body-surat-pernyataan {
            padding-top: 30px;
        }

        .surat-pernyataan .body-surat-pernyataan .table-main {
            font-size: 14px;
        }

        .surat-pernyataan .body-surat-pernyataan .table-main td {
            vertical-align: top;
        }

        .surat-pernyataan .divider {
            border-bottom: 2px dashed #ddd;
            margin-bottom: 15px;
        }
        .text-right{
            text-align:right;
        }
</style>

</head>
<body class="m-page--fluid m--skin- body-print ">
        <div class="bg-fixeds">
            <img src="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>" alt="<?=ROOT.DS.$consultant->picture_dir.$consultant->picture;?>">
        </div>
        <?=$this->fetch('content');?>
    </body>
</html>