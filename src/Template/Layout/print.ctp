
<!DOCTYPE html>
<html lang="en">
<head>
  <title><?=$defaultAppSettings['App.Name'];?><?=(!empty($titleModule) ? ' | '.$titleModule : '');?></title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="keywords" content="<?=$defaultAppSettings['App.Name'];?>">
    <meta name="author" content="Iqbal Ardiansyah">
    <link rel="shortcut icon" href="<?=$this->Utilities->generateUrlImage(null,$defaultAppSettings['App.Favico']);?>" />
    <!-- Favicon icon -->
    <?php
        $cssExternal = [];
        $cssDefault = [
            'dist/css/style.bundle.css',
        ];
        
        $cssMain = [
            
        ];
        
        $this->Html->css($cssExternal,['block'=>'cssExternal']);
        $this->Html->css($cssDefault,['block'=>'cssDefault','pathPrefix' => '/assets/print/']);
        $this->Html->css($cssMain,['block'=>'cssMain','pathPrefix' => '/assets/assets/print/']);
    
        echo $this->fetch('cssExternal');
        echo $this->fetch('cssDefault');
        echo $this->fetch('cssPlugin');
        echo $this->fetch('cssMain');
    ?>

</head>
    <body class="">
        <div class="wrapper">
            <?=$this->fetch('content');?>
        </div>
        <script>
        </script>
        <?php
            $jsDefault = [
                'dist/js/script.bundle.js',
            ];
            $jsMain = [
                
            ];
            $this->Html->script($jsDefault,[
                'block'=>'jsDefault',
                'pathPrefix' => '/assets/print/'
                ]
            );
            $this->Html->script($jsDefault,[
                'block'=>'jsPlugin',
                'pathPrefix' => '/assets/print/'
                ]
            );
            $this->Html->script($jsMain,['block'=>'jsMain','pathPrefix' => '/assets/landing/']);

            echo $this->fetch('jsDefault');
            echo $this->fetch('jsPlugin');
            echo $this->fetch('jsMain');
            echo $this->fetch('script');
        ?>
        <script>
            
        </script>
    </body>
</html>