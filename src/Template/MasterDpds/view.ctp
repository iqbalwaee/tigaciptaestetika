<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table">
            <tr>
                <th scope="row" width="200px"><?= __('Provinsi') ?></th>
                <td><?= h($masterDpd->province->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Nama DPD') ?></th>
                <td><?= h($masterDpd->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('email') ?></th>
                <td><?= h($masterDpd->email) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('No. Telepon') ?></th>
                <td><?= h($masterDpd->phone) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Alamat') ?></th>
                <td><?= h($masterDpd->address) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Tanggal Dibuat') ?></th>
                <td><?= h($masterDpd->created->format('d-m-Y')) ?></td>
            </tr>
        </table>
        <!--end: Datatable -->
    </div>
</div>
