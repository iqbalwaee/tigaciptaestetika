<?php
    $this->assign('meta-description',"PT. Tiga Cipta Estetika. Anda bisa check data pengawas dan melihat data pengurusan pengawas yang telah disetujui oleh konsultan.");
    $this->assign('meta-keywords',"PT. Tiga Cipta Estetika, Tiga Cipta, Tiga Cipta Estetika, Estetika Tiga Cipta, Pengajuan Developer, Developer Rumah, Property, Perusahaan Konsultan, Konsultan, Konsultan Properti, Konsultan Bogor, Konsultan Jakarta, Konsultan Developer Jakarta, Konsultan Developer Bogor, Konsultan Developer Indonesia, Konsultan Developer Property,check data pengawas,,Asosiasi Pengembang Perumahan dan Pemukiman Seluruh Indonesia");
?>
<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Check Data Pengawas
                </h3>
            </div>
        </div>
    </div>
    <?=$this->Form->create(null,['class'=>'m-form m-form--fit m-form--label-align-right','id'=>'reg-dev']);?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <div class="form-group m-form__group">
						<div class="input-group">
							<input type="text" class="form-control" name="nik" placeholder="Masukan NIK">
							<div class="input-group-append">
								<button class="btn btn-primary" type="submit">Go!</button>
							</div>
						</div>
					</div>
                </div>
            </div>
        </div>
    </form>
    <div class="m-portlet__body">
        <div class="row m--margin-bottom-15">
            <div class="col-md-6">
               <h3>Data Pengawas</h3>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-bordered">
                    <tbody>
                        <tr>
                            <td width="20%">Nama Pengawas</td>
                            <td width="2%">:</td>
                            <td width="28%" class="supervisor-name"></td>
                            <td width="20%">NIK</td>
                            <td width="2%">:</td>
                            <td width="28%" class="supervisor-nik"></td>
                        </tr>
                        <tr>
                            <td width="20%">Email</td>
                            <td width="2%">:</td>
                            <td width="28%" class="supervisor-email"></td>
                            <td width="20%">No. Telepon</td>
                            <td width="2%">:</td>
                            <td width="28%" class="supervisor-phone"></td>
                        </tr>
                        <tr>
                            <td width="20%">Alamat</td>
                            <td width="2%">:</td>
                            <td width="78%" class="supervisor-address" colspan="4"></td>
                        </tr>
                        <tr>
                            <td width="20%">File Identitas</td>
                            <td width="2%">:</td>
                            <td width="28%">
                                <a href="#" target="_BLANK" class="supervisor-identity"><i class="fa fa-download"></i> Download</a>
                            </td>
                            <td width="20%">Surat Kesesuaian Spek</td>
                            <td width="2%">:</td>
                            <td width="28%" class="">
                            <a href="<?=$this->Utilities->generateUrlImage($consultant->form_checklist_dir,$consultant->form_checklist);?>" target="_BLANK" class=""><i class="fa fa-download"></i> Download</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="table-responsive">
                    <table class="table table-detail" cellspacing=0>
                        <thead>
                            <tr>
                                <th width="10%">No.</th>
                                <th width="30%">Developer</th>
                                <th>Nama Perumahan</th>
                                <th width="10%">Upload</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--begin::Modal-->
<div class="modal fade" id="m_modal_1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Upload Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form class="form-upload" action="<?=$this->Url->build(['action'=>'uploadPengawas']);?>" enctype="multipart/form-data">
            <input type="hidden" value="0" name="id" id="id_app">
            <table class="table table-form">
                <tbody>
                    <tr>
                        <td colspan="3">Upload File Pendukung: </td>
                    </tr>
                    <tr>
                        <td width="20%">Nama Perumahan</td>
                        <td width="5%">:</td>
                        <td class="application-name"></td>
                    </tr>
                    <tr>
                        <td width="20%">Kode Pengajuan</td>
                        <td width="5%">:</td>
                        <td class="application-code"></td>
                    </tr>
                    <tr>
                        <td width="20%">Surat Pernyataan</td>
                        <td width="5%">:</td>
                        <td class="application-surat-pernyataan">
                            <input type="file" name="surat_pernyataan" id="surat_pernyataan" accept="application/pdf" >
                        </td>
                    </tr>
                    <tr>
                        <td width="20%">Form Pengawasan</td>
                        <td width="5%">:</td>
                        <td class="application-lembar-pemeriksaan">
                            <input type="file" name="lembar_pemeriksaan" id="lembar_pemeriksaan" accept="application/pdf">
                        </td>
                    </tr>
                    <tr>
                        <td width="20%">Surat Kesesuaian Spek</td>
                        <td width="5%">:</td>
                        <td class="application-form-checklist">
                            <input type="file" name="form_checklist" id="form_chekclist" accept="application/pdf">
                        </td>
                    </tr>
                </tbody>
            </table>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary btn-submit-form-upload">Submit</button>
      </div>
    </div>
  </div>
</div>
<!--end::Modal-->
<?php $this->start('script');?>
    <script>
        function closeAllDataPengawas(){
            $(".table-detail").show();
            $(".table-detail tbody").html('');
        }
        var form = $(".m-form");
        var btn = $(".btn-submit");
        var application_letters = [];
        form.validate({
            rules: {
                nik: {
                    required: true,
                },
            },
            messages : {
                nik : {
                    required : 'Harap masukan nik'
                },
            }
        });
        console.log(form);
        form.on("submit",function(e){
            e.preventDefault();
            if (!form.valid()) {
                return;
            }
            form.ajaxSubmit({
                url: form.attr('action'),
                dataType : 'json',
                type : 'post',
                beforeSend : function(){
                    form.find("input,select,textarea").prop("disabled",true);
                    btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                    swal({
                        type : 'info',
                        title: 'Harap menunggu',
                        text: 'Sedang mengirim data',
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick : false,
                        allowEscapeKey : false,
                        allowEnterKey : false
                    })
                    application_letters = [];
                },
                success: function(response, status, xhr, $form) {
                    form.find("input,select,textarea").prop("disabled",false);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if(response.code == 50){
                        swal("Ooopss!!",response.message,'error');
                    }else{
                        closeAllDataPengawas()
                        swal.close(); 
                        var supervisor = response.supervisor;
                        application_letters = response.supervisor.application_letters;
                        $(".supervisor-name").html(supervisor.name);
                        $(".supervisor-nik").html(supervisor.nik);
                        $(".supervisor-email").html(supervisor.email);
                        $(".supervisor-phone").html(supervisor.phone);
                        $(".supervisor-address").html(supervisor.address);
                        $(".supervisor-identity").attr("href",supervisor.url_image);
                        var html = "";
                        if(application_letters.length != 0){
                            var no = 1;
                            $.each(application_letters,function(e,i){
                                html += "<tr>"
                                        +  "<td>" + i.code + "</td>"
                                        +  "<td>" + i.developer.name + "</td>"
                                        +  "<td>" + i.name_house + "<br><a href='<?=$this->request->base;?>/application-letters/print-slf-new/"+i.id+"' target='_BLANK'>Download SLF</a> | <a href='<?=$this->request->base;?>/application-letters/slf/"+i.id+"' target='_BLANK'>Download DRAFT SLF</a> | <a href='<?=$this->request->base;?>/application-letters/print-pengawasan/"+i.id+"' target='_BLANK'>Download Form Pengawasan</a></td>"
                                        +  "<td><a href='#' class='upload-data' data-key="+e+"><i class='fa fa-upload'></i> Upload</a></td>"
                                        +  "</tr>";
                                no = no +1;
                            })
                            $(".table-detail tbody").html(html);
                        }
                    }
                },
                error : function(){
                    form.find("input,select,textarea").prop("disabled",false);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    swal("Ooopss!!","Terjadi kesalahan harap ulangi kembali",'error');
                }
            });
        });
        var dataKey = 0;
        $("body").on("click",".upload-data",function(e){
            e.preventDefault();
            $("#m_modal_1").modal('show');
            dataKey = $(this).data("key");
            var apps = application_letters[dataKey];
            $(".application-code").html(apps.code);
            $(".application-name").html(apps.name_house);
            $("#id_app").val(apps.id);
            if(apps.url_surat_pernyataan != ""){
                var htmlSuratPernyataan = '<a href="'+apps.url_surat_pernyataan+'" target="_BLANK"><i class="fa fa-download"></i> DOWNLOAD</a>';
            }else{
                var htmlSuratPernyataan = '<input type="file" name="surat_pernyataan" id="surat_pernyataan" accept="application/pdf" >';
            }
            $(".application-surat-pernyataan").html(htmlSuratPernyataan);
            if(apps.url_lembar_pemeriksaan != ""){
                var htmlLembarPemeriksaan = '<a href="'+apps.url_lembar_pemeriksaan+'" target="_BLANK"><i class="fa fa-download"></i> DOWNLOAD</a>';
            }else{
                var htmlLembarPemeriksaan = '<input type="file" name="lembar_pemeriksaan" id="lembar_pemeriksaan" accept="application/pdf" >';
            }
            $(".application-lembar-pemeriksaan").html(htmlLembarPemeriksaan);
            if(apps.url_form_checklist != ""){
                var htmlFormCheckList = '<a href="'+apps.url_form_checklist+'" target="_BLANK"><i class="fa fa-download"></i> DOWNLOAD</a>';
            }else{
                var htmlFormCheckList = '<input type="file" name="form_checklist" id="form_checklist" accept="application/pdf" >';
            }
            $(".application-form-checklist").html(htmlFormCheckList);
            console.log(apps);
        })

        $(".btn-submit-form-upload").on("click",function(e){
            e.preventDefault();
            $(".form-upload").submit();
        })
        
        $(".form-upload").on("submit",function(e){
            var form2 = $(this);
            var btn2 = $(".btn-submit-form-upload");
            var id = form2.attr("id");
            e.preventDefault();
            if(form2.find(".alert").length > 0){
                form2.find(".alert").remove();
            }
           
            
            form2.ajaxSubmit({
                url: form2.attr('action'),
                dataType : 'json',
                type : 'post',
                beforeSend : function(){
                    form2.find("input,button,select").prop("disabled",true);
                    swal({
                        type : 'info',
                        title: 'Harap menunggu',
                        text: 'Sedang mengirim data',
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick : false,
                        allowEscapeKey : false,
                        allowEnterKey : false
                    })

                },
                success: function(response, status, xhr, $form) {
                    form2.find("input,button,select").prop("disabled",false);
                    if(response.code == 50){
                        swal("Ooopss!!",response.message,'error');
                    }else{
                        swal.close(); 
                        var apps = response.application_letter;
                        if(apps.url_surat_pernyataan != ""){
                            var htmlSuratPernyataan = '<a href="'+apps.url_surat_pernyataan+'" target="_BLANK"><i class="fa fa-download"></i> DOWNLOAD</a>';
                        }else{
                            var htmlSuratPernyataan = '<input type="file" name="surat_pernyataan" id="surat_pernyataan" accept="application/pdf" >';
                        }
                        $(".application-surat-pernyataan").html(htmlSuratPernyataan);
                        if(apps.url_lembar_pemeriksaan != ""){
                            var htmlLembarPemeriksaan = '<a href="'+apps.url_lembar_pemeriksaan+'" target="_BLANK"><i class="fa fa-download"></i> DOWNLOAD</a>';
                        }else{
                            var htmlLembarPemeriksaan = '<input type="file" name="lembar_pemeriksaan" id="lembar_pemeriksaan" accept="application/pdf" >';
                        }
                        $(".application-lembar-pemeriksaan").html(htmlLembarPemeriksaan);
                        if(apps.url_form_checklist != ""){
                            var htmlFormCheckList = '<a href="'+apps.url_form_checklist+'" target="_BLANK"><i class="fa fa-download"></i> DOWNLOAD</a>';
                        }else{
                            var htmlFormCheckList = '<input type="file" name="form_checklist" id="form_checklist" accept="application/pdf" >';
                        }
                        $(".application-form-checklist").html(htmlFormCheckList);
                        application_letters[dataKey] = apps;
                    }
                },
                error : function(){
                    form2.find("input,button,select").prop("disabled",false);
                        swal("Ooopss!!",'Terjadi Kesalahan','error');
                }
            });
        })
    </script>
<?php $this->end();?>