<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?php
        unset($user->password);
    ?>
    <?= $this->Form->create($user,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('username',['readonly'=>($userData['group_id'] == 4 ? true : false)]);;?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('name');?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('email');?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('current_password',['type'=>'password']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('password');?>
                </div>
            </div>
            <?php if($userData['group_id'] == 2):?>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-4">
                        <?=$this->Form->control('developer.ceo_name',['label'=>'Nama Direktur']);?>
                    </div>
                    <div class="col-md-4">
                        <?=$this->Form->control('developer.nik_ceo',['label'=>'NIK Direktur']);?>
                    </div>
                </div>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-4">
                        <?=$this->Form->control('developer.bidang_keahlian',['label'=>'Bidang Keahlian']);?>
                    </div>
                    <div class="col-md-4">
                        <?=$this->Form->control('developer.no_badan_usaha',['label'=>'No. Badan Usaha']);?>
                    </div>
                </div>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-4">
                        <?=$this->Form->control('developer.kta_number',['label'=>'No. KTA']);?>
                    </div>
                    <div class="col-md-4">
                        <?=$this->Form->control('developer.phone',['label'=>'No. Telepon']);?>
                    </div>
                </div>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-8">
                        <?=$this->Form->control('developer.address',['label'=>'Alamat','class'=>'form-control m-input']);?>
                    </div>
                </div>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-8">
                        <?=$this->Form->control('developer.logo',['label'=>'Logo','class'=>'form-control m-input','type'=>'file']);?>
                    </div>
                </div>
            <?php endif;?>
            <?php if($userData['group_id'] == 3):?>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-8">
                        <?=$this->Form->control('master_dpp.address',['label'=>'Alamat','class'=>'form-control m-input']);?>
                    </div>
                    <div class="col-md-4">
                        <?=$this->Form->control('master_dpp.phone',['label'=>'No. Telepon']);?>
                    </div>
                </div>
            <?php endif;?>
            <?php if($userData['group_id'] == 4):?>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-8">
                        <?=$this->Form->control('master_dpd.address',['label'=>'Alamat','class'=>'form-control m-input']);?>
                    </div>
                    <div class="col-md-4">
                        <?=$this->Form->control('master_dpd.phone',['label'=>'No. Telepon']);?>
                    </div>
                </div>
            <?php endif;?>
            <?php if($userData['group_id'] == 5):?>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-8">
                        <?=$this->Form->control('consultant.address',['label'=>'Alamat','class'=>'form-control m-input']);?>
                    </div>
                    <div class="col-md-4">
                        <?=$this->Form->control('consultant.phone',['label'=>'No. Telepon']);?>
                    </div>
                </div>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-4">
                        <?=$this->Form->control('consultant.pic_name',['class'=>'form-control m-input','label'=>'Nama PIC']);?>
                    </div>
                    <div class="col-md-4">
                        <?=$this->Form->control('consultant.pic_signature',['label'=>'Logo','type'=>'file','Tanda Tangan PIC']);;?>
                    </div>
                </div>
                <div class="row m--margin-bottom-15">
                    <div class="col-md-4">
                        <?=$this->Form->control('consultant.picture',['label'=>'Logo','type'=>'file']);;?>
                    </div>
                    <div class="col-md-4">
                        <?=$this->Form->control('consultant.compro',['label'=>'Compro','type'=>'file']);;?>
                    </div>
                    <div class="col-md-4">
                        <?=$this->Form->control('consultant.form_checklist',['label'=>'Form Checklist','type'=>'file']);;?>
                    </div>
                </div>
            <?php endif;?>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>