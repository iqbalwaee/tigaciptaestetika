<?php
    $this->assign('meta-description',"PT. Tiga Cipta Estetika. Anda dapat melakukan pengajuan di website ini dengan melakukan registrasi di menu login dengan melengkapi data perusahaan anda. Semua pengajuan yang masuk akan kami layani 24 jam.");
    $this->assign('meta-keywords',"PT. Tiga Cipta Estetika, Tiga Cipta, Tiga Cipta Estetika, Estetika Tiga Cipta, Pengajuan Developer, Developer Rumah, Property, Perusahaan Konsultan, Konsultan, Konsultan Properti, Konsultan Bogor, Konsultan Jakarta, Konsultan Developer Jakarta, Konsultan Developer Bogor, Konsultan Developer Indonesia, Konsultan Developer Property,,Asosiasi Pengembang Perumahan dan Pemukiman Seluruh Indonesia");
?>
<div class="m-portlet m-portlet--tab">
<div class="m-portlet__body">
    <div class="row m--margin-bottom-15">
        <div class="col-md-12">
            <h1>Selamat datang Website PT. Tiga Cipta Estetika.</h1><br>
            <p style="font-size:16px;">Anda dapat melakukan pengajuan di website ini dengan melakukan registrasi di menu login dengan melengkapi data perusahaan anda. Semua pengajuan yang masuk akan kami layani 24 jam.<br>
            Jam kerja kami. <span class="m-badge m-badge--success m-badge--wide m-badge--rounded" style="font-size:14px;">Senin - Jum'at : 08:00 - 16:00 WIB Sabtu : 08:00 - 13:00 WIB .</span>
            </p>
            <br>
            <address style="font-size:16px;">
                <b style="font-size:20px;">PT. Tiga Cipta Estetika</b><br><br>
                <abbr style="text-decoration:none;" title="Address"><i class="fa fa-map-marker"></i> <b>Alamat </b> :</abbr>
                <?=$consultant->address;?>
                <br>
                <abbr style="text-decoration:none;" title="Phone"><i class="fa fa-phone"></i> <b>Telepon / WA  </b> :</abbr>
                <?=$consultant->phone;?>
                <br>
                <abbr style="text-decoration:none;" title="Email"><i class="fa fa-envelope"></i> <b>Email  </b> :</abbr>
                <a href="mailto:<?=$consultant->email;?>"><?=$consultant->email;?></a>
                <br>
            </address><br>
            <?php if(empty($userData)):?>
                <a href="<?=$this->Url->build(['controller'=>'Pages','action'=>'loginDeveloper']);?>" class="btn btn-primary " style="">Login Sebagai Developer</a> <br><br>
                Belum punya akun developer?<br><br>
                <a href="<?=$this->Url->build(['controller'=>'Pages','action'=>'registerDeveloper']);?>" class="btn btn-primary ">Daftar Sekarang</a> |  
            <?php endif;?><a href="<?=$this->Url->build('/files/manual_book.pdf');?>" class="btn btn-info "><i class="fa fa-download"></i> Download Manual Book Developer</a>
        </div>
    </div>
</div>
</div>