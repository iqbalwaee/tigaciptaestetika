<?php
    $this->assign('meta-description',"PT. TIga Cipta Estetika. Login sebagai developer anda bisa mengajukan permohonan property anda dan memantau status pengajuan anda.");
    $this->assign('meta-keywords',"PT. TIga Cipta Estetika, Tiga Cipta, Tiga Cipta Estetika, Estetika Tiga Cipta, Pengajuan Developer, Developer Rumah, Property, Perusahaan Konsultan, Konsultan, Konsultan Properti, Konsultan Bogor, Konsultan Jakarta, Konsultan Developer Jakarta, Konsultan Developer Bogor, Konsultan Developer Indonesia, Konsultan Developer Property,Login Developer,,Asosiasi Pengembang Perumahan dan Pemukiman Seluruh Indonesia");
?>
<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Login Developer
                </h3>
            </div>
        </div>
    </div>
    <?=$this->Form->create(null,['class'=>'m-form m-form--fit m-form--label-align-right','id'=>'reg-dev']);?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'username' => ['label'=>'Username','autocomplete'=>'off','placeholder'=>'Masukan Username','class'=>'form-control m-input m-login__form-input--last','id'=>'username-dev'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'password' => ['label'=>'Password','autocomplete'=>'off','placeholder'=>'Masukan Password','class'=>'form-control m-input m-login__form-input--last','id'=>'pass-dev',],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary btn-submit">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        var form = $(".m-form");
            var btn = $(".btn-submit");
        form.validate({
            rules: {
                username: {
                    required: true,
                },
                password: {
                    required: true,
                },
            },
            messages : {
                username : {
                    required : 'Harap masukan username'
                },
                password: {
                    required: 'Harap masukan password',
                },
            }
        });
        console.log(form);
        form.on("submit",function(e){
            e.preventDefault();
            if (!form.valid()) {
                return;
            }
            form.ajaxSubmit({
                url: form.attr('action'),
                dataType : 'json',
                type : 'post',
                beforeSend : function(){
                    form.find("input,select,textarea").prop("disabled",true);
                    btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                    swal({
                        type : 'info',
                        title: 'Harap menunggu',
                        text: 'Sedang mengirim data',
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick : false,
                        allowEscapeKey : false,
                        allowEnterKey : false
                    })
                },
                success: function(response, status, xhr, $form) {
                    form.find("input,select,textarea").prop("disabled",false);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    if(response.code == 50){
                        swal("Ooopss!!",response.message,'error');
                    }else{
                        swal({
                            title: 'Berhasil',
                            text: response.message,
                            type: 'success',
                            confirmButtonText: 'OK',
                        }).then((result) => {
                            document.location.href="<?=$this->request->base;?>/"
                        });
                        form.find("input,select,textarea").val("");
                    }
                },
                error : function(){
                    form.find("input,select,textarea").prop("disabled",false);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    swal("Ooopss!!","Terjadi kesalahan harap ulangi kembali",'error');
                }
            });
        });
    </script>
<?php $this->end();?>