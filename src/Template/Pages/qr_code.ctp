<html>
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=0.5, maximum-scale=3.0, minimum-scale=0.5" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <style>
    html,body{
        width:100%;
        padding:0px;
        margin:0px;
        font-size:12px;
        line-height:20px;
    }
    table{
        font-size:12px; line-height:20px;font-family: 'Times New Roman', Times, serif;;
    }
    h3{
        font-family: 'Times New Roman', Times, serif;
        font-size:16px;
    }
    table{
        font-size:12px;
        line-height:20px;
    }
    header {  z-index:99;}
    footer { }
    .logo-wrapper-left{
        display: inline-block;
        width:15%;
        padding-top:0px;
        float:left;
    }
    .logo-wrapper-left img{
        max-width:100%;
    }
    .title-header-wrapper{
        display: inline-block;
        padding-top:10px;
        width:70%;
        
    }
    .title-header-wrapper h3{
        margin:0px;
        padding:0px;
        margin-bottom:5px;
        text-align:center;
        font-weight:bold;
        font-size:16px;
    }
    .title-header-wrapper p{
        margin:0px;
        padding:0px;
        font-size:14px;
    }
    main{
        padding:0 0px;
        z-index:99;
    }
    td{
        vertical-align:top;
    }
    .table-header{
        margin:auto;
        text-align:left;
    }
    .table-header td{
        padding:0px;
        line-height:18px;
    }
    main{
        page-break-after: always;
    }
    main:last-child{
        page-break-after: auto;
    }
    .table-sub td{
        padding:0px 10px;
    }
    .bg-fixed{
        position:fixed;
        top:40%;
        left:0;
        right:0;
        transform:translateY(-50%);
        z-index:2;
        text-align:center;
    }
    .bg-fixed{
        opacity:0.1;
    }

    .container{
        width: 970px !important;
        margin:auto;
    }
    .pdfobject-container { height: 500px;margin-bottom:30px;}
    .pdfobject { border: 1px solid #666; }
    .ska{
        margin-top:60px;
        border-top:2px solid #bbb;
        padding-top:30px;
    }
    .ska h3{
        margin-bottom:30px;
    }
    .ska img{
        display:block;
        margin-bottom:30px;
        max-width:100%;
        border:3px solid #333;
    }
  </style>
</head>
<body>
<header>
    <div class="logo-wrapper-left">
    <img src="<?=$this->Utilities->generateUrlImage($consultant->picture_dir,$consultant->picture);?>" alt="logo" class="logo" width="130px">
    </div>
    <div class="title-header-wrapper">
        <h3>
            SURAT PERNYATAAN PEMERIKSAAN KELAIKAN FUNGSI BANGUNAN GEDUNG
        </h3>
        <table class="table-header">
            <tr>
                <td width="30%">Nomor Surat</td>
                <td width="2%">:</td>
                <td width="68%"><?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y');?></td>
            </tr>
            <tr>
                <td width="30%%">Tanggal</td>
                <td width="2%">:</td>
                <td width="68%"><?=$applicationLetter->approved_by_dpp_date->format('d-m-Y');?></td>
            </tr>
        </table>
    </div>
    <div style="clear:both;display:block">&nbsp;</div>
</header>
<main>

<table width="100%" class="table-main">
    <tr>
        <td colspan="3"  style="" style="">Pada Hari ini, <i><?=$this->Utilities->dayArray($applicationLetter->output_date->format('D'));?></i> tanggal <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('d'));?></i> bulan <i><?=$this->Utilities->monthArray($applicationLetter->output_date->format('m') * 1);?></i> tahun <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('Y'));?></i>, yang bertanda tangan dibawah ini,</td>
    </tr>
    <tr>
        <td colspan="3"  style="">Konsultan Pegawas</td>
    </tr>
    <tr>
        <td width="25%">a. Nama penanggung jawab</td>
        <td width="5%">:</td>
        <td style=""><b><?=$consultant->pic_name;?></b></td>
    </tr>
    <tr>
        <td width="25%">b. Nama Perusahaan/instansi teknis*</td>
        <td width="5%">:</td>
        <td style=""><b><?=$consultant->name;?></b></td>
    </tr>
    <tr>
        <td colspan="3"  style=""><br></td>
    </tr>
    <tr>
        <td colspan="3"  style="">telah melakukan pemeriksaan kelaikan fungsi bangunan gedung pada :<br><br></td>
    </tr>
    <tr>
        <td colspan="3"  style="">1. Data Bangunan Gedung</td>
    </tr>
    <tr>
        <td colspan="3"  style="" style="padding:0px;">
            <table class="table-sub" width="100%">
                <tr>
                    <td width="25%">a. Fungsi Utama</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->primary_function;?></td>
                </tr>
                <tr>
                    <td width="25%">b. Fungsi Tambahan</td>
                    <td width="5%">:</td>
                    <td style="">-</td>
                </tr>
                <tr>
                    <td width="25%">c. Type bangunan gedung</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->house_type->code;?>/<?=$applicationLetter->surface_area;?></td>
                </tr>
                <tr>
                    <td width="25%">d. Letak bangunan</td>
                    <td width="5%">:</td>
                    <td style="">
                        <?php
                            $dataHouse = explode(",",$applicationLetter->building);
                            $dataCount = count($dataHouse) -1;
                            $a = 0;
                            $markred = $this->request->query('build-code');
                            foreach($dataHouse as $r):
                                $a++;
                        ?>
                            <?php if($r == $markred):?>
                                <strong style="color:red;"><?=$r;?></strong>
                            <?php else:?>
                                <?=$r;?>
                            <?php endif;?>
                            <?php
                                $dataCount = $dataCount;
                            ?>
                            <?=($dataCount != 0 ? '|' : '');?>
                        <?php endforeach;?>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" style=""><br>2. Data Perumahan</td>
    </tr>
    <tr>
        <td colspan="3" style="" style="padding:0px;">
            <table class="table-sub" width="100%">
                <tr>
                    <td width="25%">a. Nama bangunan gedung</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->name_house;?></td>
                </tr>
                <tr>
                    <td width="25%">b. Alamat</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->address;?></td>
                </tr>
                <tr>
                    <td width="25%">c. Kecamatan</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->district->name;?></td>
                </tr>
                <tr>
                    <td width="25%">d. Kabupaten/Kota</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->city->name;?></td>
                </tr>
                <tr>
                    <td width="25%">e. Provinsi</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->province->name;?></td>
                </tr>
                <tr>
                    <td width="25%">f. Jumlah Unit</td>
                    <td width="5%">:</td>
                    <td style=""><?=$a;?></td>
                </tr>
                <tr>
                    <td width="25%">g. Harga Jual</td>
                    <td width="5%">:</td>
                    <td style="">Rp.<?=number_format($applicationLetter->price_per_unit,0);?></td>
                </tr>
                <tr>
                    <td width="25%">h. Terbilang</td>
                    <td width="5%">:</td>
                    <td style=""><?=$this->Utilities->terbilang($applicationLetter->price_per_unit);?> Rupiah</td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" style=""><br>3. Data Administrasi</td>
    </tr>
    <tr>
        <td colspan="3" style="" style="padding:0px;">
            <table class="table-sub" width="100%">
                <tr>
                    <td width="25%">a. Sertifikat Hak Milik</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->sertifikat_hak;?></td>
                </tr>
                <tr>
                    <td width="25%">b. Izin Pemanfaatan Tanah / Izin Lokasi</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->izin_pemanfaatan;?></td>
                </tr>
                <tr>
                    <td width="25%">c. Pengesahaan Site Plan</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->pengesahaan;?></td>
                </tr>
                <tr>
                    <td width="25%">d. Izin Mendirikan Bangunan</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->nomor_imb;?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" style=""><br>Sesuai dengan hasil pemeriksaan yang dilakukan, kami <?=$consultant->name;?>, selaku konsultan pengawas, dengan ini menyatakan bahwa : <br><br></td>
    </tr>
    <tr>
        <td colspan="3" style="" style="padding:0px;">
            <table class="table-sub-nopad" width="100%">
                <tr>
                    <td width="278px">1. Persyaratan administratif</td>
                    <td width="36px">:</td>
                    <td style=""><?=$applicationLetter->application_output->persyaratan_administratif;?></td>
                </tr>
                <tr>
                    <td>2. Persyaratan teknis</td>
                    <td>:</td>
                    <td style=""></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="" style="padding:0px;">
            <table class="table-sub" width="100%">
                <tr>
                    <td width="25%">a. Fungsi bangunan gedung</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->application_output->fungsi_bangunan;?></td>
                </tr>
                <tr>
                    <td width="25%">b. Peruntukan</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->application_output->peruntukan;?></td>
                </tr>
                <tr>
                    <td width="25%">c. Tata bangunan</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->application_output->tata_bangunan;?></td>
                </tr>
                <tr>
                    <td width="25%">d. Kelaikan fungsi bangunan gedung dinyatakan</td>
                    <td width="5%">:</td>
                    <td style=""><?=$applicationLetter->application_output->kelaikan;?></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="">
            <br>
            Lembar hasil pemeriksaan ini berlaku sepanjang <strong>TIDAK TERDAPAT PERUBAHAN SPESIFIKASI, MUTU, DAN KELAIKAN</strong>  oleh <?=$applicationLetter->developer->name;?> 
            <br>
            <br>
            Jika terjadi perubahan Spesifikasi, Mutu dan Kelaikan (tidak sesuai rumah contoh atau acuan) dikemudian hari pada Perumahan <?=$applicationLetter->name_house;?> maka <?=$applicationLetter->developer->name;?>, selaku pihak Pengembang Perumahan bertanggung jawab terhadap dampak yang diakibatkan.<br>
            <br>
            Lembar hasil pemeriksaan berlaku untuk satu periode (<?=$a;?> unit rumah yang telah disebut diatas) yang pemeriksaannya telah diwakilkan pada 1 unit bangunan rumah Blok <?=$applicationLetter->sample_house;?>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="" style="text-align:right;">
            <br>
            <br>
            <br>
            <div class="signature" style="width:300px;display:block;text-align:center;margin-left:auto;">
                Konsultan Pengawas<br>
                <?=$consultant->name;?>
                <div>
                <img src="<?=$this->Utilities->generateUrlImage($consultant->pic_signature_dir,$consultant->pic_signature);?>" width="250px">
                </div>
                <b><u><?=$consultant->pic_name;?></u><b><br><br><br><br>
            </div>
        </td>
    </tr>
</table>
<div class="pdf-wrapper" style="<?=($this->request->is('mobile') ? 'display:none;' : '');?>">
    <div id="example1"></div>
</div>

<div class="ska">
    <div class="title-header">
        <h3>SKA KONSULTAN</h3>
    </div>
    <?php if(!empty($consultant->ska_1)):?>
        <img src="<?=$this->Utilities->generateUrlImage($consultant->ska_1_dir,$consultant->ska_1);?>">
    <?php endif;?>
    <?php if(!empty($consultant->ska_2)):?>
        <img src="<?=$this->Utilities->generateUrlImage($consultant->ska_2_dir,$consultant->ska_2);?>">
    <?php endif;?>
    <?php if(!empty($consultant->ska_3)):?>
        <img src="<?=$this->Utilities->generateUrlImage($consultant->ska_3_dir,$consultant->ska_3);?>">
    <?php endif;?>
</div>
</main>
<?php if($this->request->is('mobile')):?>
<script src="<?=$this->request->base;?>/js/pdfobject.min.js"></script> 
<script>
    var options = {
        height: "500px",
        pdfOpenParams: { view: 'FitV'}
    };
    PDFObject.embed("<?=$this->Utilities->generateUrlImage($consultant->compro_dir,$consultant->compro);?>", "#example1",options);
</script>
<?php endif;?>
</body>
</html>