<?php
    $this->assign('meta-description',"PT. Tiga Cipta Estetika. Register sebagai developer anda bisa mengajukan permohonan property anda dan memantau status pengajuan anda. Dapatkan berbagai fitur kemudahan untuk pengajuan property anda.");
    $this->assign('meta-keywords',"PT. Tiga Cipta Estetika, Tiga Cipta, Tiga Cipta Estetika, Estetika Tiga Cipta, Pengajuan Developer, Developer Rumah, Property, Perusahaan Konsultan, Konsultan, Konsultan Properti, Konsultan Bogor, Konsultan Jakarta, Konsultan Developer Jakarta, Konsultan Developer Bogor, Konsultan Developer Indonesia, Konsultan Developer Property,Register Developer,,Asosiasi Pengembang Perumahan dan Pemukiman Seluruh Indonesia");
?>
<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    Register Developer
                </h3>
            </div>
        </div>
    </div>
    <?=$this->Form->create($developer,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file','id'=>'reg-dev']);?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-12">
                    <?=$this->Form->controls([
                        'name' => ['label'=>'Nama Perusahaan','autocomplete'=>'off','placeholder'=>'Masukan Nama Perusahaan','id'=>'name'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'ceo_name' => ['label'=>'Nama Direktur','autocomplete'=>'off','placeholder'=>'Masukan Nama Direktur','id'=>'ceo-name'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'nik_ceo' => ['label'=>'NIK Direktur','autocomplete'=>'off','placeholder'=>'Masukan NIK Direktur','id'=>'ceo-nik'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'kta_number' => ['label'=>'Anggota Asosiasi','autocomplete'=>'off','placeholder'=>'Masukan Anggota Asosiasi'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'dpp_from_id' => ['label'=>'Asal DPD Asosiasi','autocomplete'=>'off','empty'=>'Pilih Asal DPD Asosiasi', 'options' => $provinces],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-12">
                    <?=$this->Form->controls([
                        'address' => ['label'=>'Alamat','autocomplete'=>'off','placeholder'=>'Masukan Alamat','class'=>'form-control m-input'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'email' => ['label'=>'Email','autocomplete'=>'off','placeholder'=>'Masukan Email','class'=>'form-control m-input m-login__form-input--last'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'phone' => ['label'=>'No. Telepon','autocomplete'=>'off','placeholder'=>'Masukan No. Telepon','class'=>'form-control m-input m-login__form-input--last'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'pic' => ['label'=>'Penanggung Jawab','autocomplete'=>'off','placeholder'=>'Masukan Penanggung Jawab','class'=>'form-control m-input m-login__form-input--last'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'pic_phone' => ['label'=>'No. Telp. Penanggung Jawab','autocomplete'=>'off','placeholder'=>'Masukan No. Telp. Penanggung Jawab','class'=>'form-control m-input m-login__form-input--last'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'logo' => ['label'=>'Logo','autocomplete'=>'off','type'=>'file','class'=>'form-control m-input m-login__form-input--last'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'user.username' => ['label'=>'Username','autocomplete'=>'off','placeholder'=>'Masukan Username','class'=>'form-control m-input m-login__form-input--last','id'=>'username-rdev'],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
                <div class="col-md-6">
                    <?=$this->Form->controls([
                        'user.password' => ['label'=>'Password','autocomplete'=>'off','placeholder'=>'Masukan Password','class'=>'form-control m-input m-login__form-input--last','id'=>'pass-rdev',],
                    ],[
                        'legend'=>false,
                        'fieldset' => false
                    ]);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary btn-submit">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?php $this->start('script');?>
    <script>
        var form = $(".m-form");
            var btn = $(".btn-submit");
        form.validate({
            rules: {
                name: {
                    required: true,
                },
                ceo_name: {
                    required: true,
                },
                nik_ceo: {
                    required: true,
                },
                kta_number: {
                    required: true,
                },
                dpp_from_id: {
                    required: true,
                },
                address: {
                    required: true,
                },
                email: {
                    required: true,
                },
                phone: {
                    required: true,
                },
                pic: {
                    required: true,
                },
                pic_phone: {
                    required: true,
                },
                logo: {
                    extension: "png|jpg|jpeg"
                },
                "user[username]": {
                    required: true,
                },
                "user[password]": {
                    required: true,
                },
            },
            messages : {
                name : {
                    required : 'Harap masukan nama perusahaan'
                },
                ceo_name: {
                    required: 'Harap masukan nama direktur',
                },
                nik_ceo: {
                    required: 'Harap masukan NIK direktur',
                },
                kta_number: {
                    required: 'Harap masukan anggota asosiasi, jika bukan anggota isi strip (-)',
                },
                dpp_from_id: {
                    required: 'Harap pili asal dpd',
                },
                address: {
                    required: 'Harap masukan alamat',
                },
                email: {
                    required: 'Harap masukan email',
                },
                phone: {
                    required: 'Harap masukan no. telepon',
                },
                pic: {
                    required: 'Harap masukan penanggung jawab',
                },
                pic_phone: {
                    required: 'Harap masukan no. telepon penanggung jawab',
                },
                logo: {
                    extension: 'Harap masukan file extensi png,jpg atau jpeg',
                },
                "user[username]": {
                    required: 'Harap masukan username',
                },
                "user[password]": {
                    required: 'Harap masukan password',
                },
            }
        });
        form.on("submit",function(e){
            e.preventDefault();
            if (!form.valid()) {
                return;
            }
            form.ajaxSubmit({
                url: form.attr('action'),
                dataType : 'json',
                type : 'post',
                beforeSend : function(){
                    form.find("input,select,textarea").prop("disabled",true);
                    btn.addClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    swal({
                        type : 'info',
                        title: 'Harap menunggu',
                        text: 'Sedang mengirim data',
                        showCancelButton: false,
                        showConfirmButton: false,
                        allowOutsideClick : false,
                        allowEscapeKey : false,
                        allowEnterKey : false
                    })
                },
                success: function(response, status, xhr, $form) {
                    form.find("input,select,textarea").prop("disabled",false);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', true);
                    if(response.code == 50){
                        swal("Ooopss!!",response.message,'error');
                    }else{
                        swal({
                            title: 'Berhasil',
                            text: response.message,
                            type: 'success',
                            confirmButtonText: 'OK',
                        });
                        form.find("input,select,textarea").val("");
                    }
                },
                error : function(){
                    form.find("input,select,textarea").prop("disabled",false);
                    btn.removeClass('m-loader m-loader--right m-loader--light').attr('disabled', false);
                    swal("Ooopss!!","Terjadi kesalahan harap ulangi kembali",'error');
                }
            });
        });
    </script>
<?php $this->end();?>