<html>
<head>
  <style>
    @page { font-size:11px; line-height:20px; }
    header {  z-index:99;}
    footer { }
    .logo-wrapper{
        display: inline-block;
        width:10%;
        padding-top:0px;
    }
    .logo-wrapper-right{
        display: inline-block;
        width:10%;
        padding-top:0px;
    }
    .title-header-wrapper{
        display: inline-block;
        padding-top:10px;
        padding-left:30px;
        padding-right:30px;
        width:70%;
        
    }
    .title-header-wrapper h3{
        margin:0px;
        padding:0px;
        margin-bottom:5px;
        text-align:center;
    }
    .title-header-wrapper p{
        margin:0px;
        padding:0px;
        font-size:14px;
    }
    main{
        padding:0 30px;
        z-index:99;
    }
    td{
        vertical-align:top;
    }
    .table-header{
        margin:auto;
        text-align:left;
    }
    .table-header td{
        padding:0px;
        line-height:18px;
    }
    main{
        page-break-after: always;
    }
    main:last-child{
        page-break-after: auto;
    }
    .table-sub td{
        padding:0px 10px;
    }
    .bg-fixed{
        position:fixed;
        top:40%;
        left:0;
        right:0;
        transform:translateY(-50%);
        z-index:2;
        text-align:center;
    }
    .bg-fixed{
        opacity:0.1;
    }
  </style>
</head>
<body>
<div class="bg-fixed">
<img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="400px">
</div>
<?php
    $dataHouse = explode(",",$applicationLetter->building);
?>
<?php foreach($dataHouse as $key => $r):?>
  <header>
    <div class="top">
        <div class="" style="width:120px;border:2px solid red;color:red;display:block;margin-left:auto;padding:5px;font-size:12px;font-weight:bold;text-align:center;margin-bottom:10px;">
            UNTUK BANK
        </div>
    </div>
    <div class="logo-wrapper">
        <img src="<?=ROOT.DS.$consultant->picture_dir.DS.$consultant->picture;?>" alt="logo" class="logo" width="80px">
    </div>
    <div class="title-header-wrapper">
        <h3>
            SURAT PERNYATAAN PEMERIKSAAN <br>KELAIKAN FUNGSI BANGUNAN GEDUNG
        </h3>
        <table class="table-header">
            <tr>
                <td>Nomor Surat</td>
                <td>:</td>
                <td><?=$consultant->code.'-'.$applicationLetter->spk_code.'/'.$this->Utilities->romawi($applicationLetter->approved_by_dpp_date->format('m')).'/CON/'.$applicationLetter->approved_by_dpp_date->format('Y');?></td>
            </tr>
            <tr>
                <td>Tanggal</td>
                <td>:</td>
                <td><?=$applicationLetter->approved_by_dpp_date->format('d-m-Y');?></td>
            </tr>
        </table>
    </div>
  </header>
  <main>
    
    <table width="100%" class="table-main">
        <tr>
            <td colspan="3" style="">Pada Hari ini, <i><?=$this->Utilities->dayArray($applicationLetter->output_date->format('D'));?></i> tanggal <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('d'));?></i> bulan <i><?=$this->Utilities->monthArray($applicationLetter->output_date->format('m') * 1);?></i> tahun <i><?=$this->Utilities->terbilang($applicationLetter->output_date->format('Y'));?></i>, yang bertanda tangan dibawah ini,</td>
        </tr>
        <tr>
            <td colspan="3">Penyedia jasa Pengawasan</td>
        </tr>
        <tr>
            <td width="280px">a. Nama penanggung jawab</td>
            <td width="10px;">:</td>
            <td><b><?=$consultant->pic_name;?></b></td>
        </tr>
        <tr>
            <td>b. Nama Perusahaan/instansi teknis*</td>
            <td>:</td>
            <td><b><?=$consultant->name;?></b></td>
        </tr>
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="3">telah melaksanakan pemeriksaan kelaikan fungsi bangunan gedung pada</td>
        </tr>
        <tr>
            <td colspan="3">1. Bangunan Gedung</td>
        </tr>
        <tr>
            <td colspan="3" style="padding:0px;">
                <table class="table-sub" width="100%">
                    <tr>
                        <td width="200px;">a. Fungsi Utama</td>
                        <td width="10px">:</td>
                        <td><?=$applicationLetter->primary_function;?></td>
                    </tr>
                    <tr>
                        <td>b. Fungsi Tambahan</td>
                        <td>:</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>c. Jenis bangunan gedung</td>
                        <td>:</td>
                        <td><?=$applicationLetter->house_type->code;?>/<?=$applicationLetter->surface_area;?></td>
                    </tr>
                    <tr>
                        <td>d. Nama bangunan gedung</td>
                        <td>:</td>
                        <td><?=$applicationLetter->name_house;?></td>
                    </tr>
                    <tr>
                        <td>e. Nomor pendaftaran bangunan gedung</td>
                        <td>:</td>
                        <td><?=$r;?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">2. Lokasi Bangunan Gedung</td>
        </tr>
        <tr>
            <td colspan="3" style="padding:0px;">
                <table class="table-sub" width="100%">
                    <tr>
                        <td width="200px;">a. Kampung</td>
                        <td width="10px">:</td>
                        <td><?=$applicationLetter->village_name;?></td>
                    </tr>
                    <tr>
                        <td>b. Kelurahan/desa</td>
                        <td>:</td>
                        <td><?=$applicationLetter->village->name;?></td>
                    </tr>
                    <tr>
                        <td>c. Kecamatan</td>
                        <td>:</td>
                        <td><?=$applicationLetter->district->name;?></td>
                    </tr>
                    <tr>
                        <td>d. Kabupaten/Kota</td>
                        <td>:</td>
                        <td><?=$applicationLetter->city->name;?></td>
                    </tr>
                    <tr>
                        <td>e. Provinsi</td>
                        <td>:</td>
                        <td><?=$applicationLetter->province->name;?></td>
                    </tr>
                    <tr>
                        <td>f. Alamat lokasi terletak di</td>
                        <td>:</td>
                        <td><?=$applicationLetter->address;?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">3. Permohonan</td>
        </tr>
        <tr>
            <td colspan="3" style="padding:0px;">
                <table class="table-sub" width="90%">
                    <tr>
                        <td width="200px;">a. Penerbitan Sertifikat Laik Fungsi</td>
                        <td width="10px">:</td>
                        <td>Nomor <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span> Tanggal <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span></td>
                    </tr>
                    <tr>
                        <td>b. Perpanjangan Sertifikat Laik Fungsi </td>
                        <td>:</td>
                        <td>Nomor <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span> Tanggal <span style="width:100px;border-bottom:2px dotted #333;display:inline-block;">&nbsp;</span></td>
                    </tr>
                    <tr>
                        <td><span style="width:8px;display:inline-block;">&nbsp;</span>Perpanjangan ke</td>
                        <td>:</td>
                        <td>
                            
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">Dengan ini menyatakan bahwa</td>
        </tr>
        <tr>
            <td colspan="3" style="padding:0px;">
                <table class="table-sub-nopad" width="100%">
                    <tr>
                        <td width="230px;">1. Persyaratan administratif</td>
                        <td width="28px">:</td>
                        <td><?=$applicationLetter->application_output->persyaratan_administratif;?></td>
                    </tr>
                    <tr>
                        <td>2. Persyaratan teknis</td>
                        <td>:</td>
                        <td></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="padding:0px;">
                <table class="table-sub" width="100%">
                    <tr>
                        <td width="200px;">a. Fungsi bangunan gedung</td>
                        <td width="10px">:</td>
                        <td><?=$applicationLetter->application_output->fungsi_bangunan;?></td>
                    </tr>
                    <tr>
                        <td>b. Peruntukan</td>
                        <td>:</td>
                        <td><?=$applicationLetter->application_output->peruntukan;?></td>
                    </tr>
                    <tr>
                        <td>c. Tata bangunan</td>
                        <td>:</td>
                        <td><?=$applicationLetter->application_output->tata_bangunan;?></td>
                    </tr>
                    <tr>
                        <td>d. Kelaikan fungsi bangunan gedung dinyatakan</td>
                        <td>:</td>
                        <td><?=$applicationLetter->application_output->kelaikan;?></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="3">
            <br>
            sesuai dengan kesimpulan berdasarkan analisis terhadap Daftar Simak Pemeriksaan Kelaikan Fungsi Bangunan Gedung terlampir.
            <br>
            <br>
Surat pernyataan ini berlaku sepanjang tidak ada perubahan yang dilakukan oleh pemilik/pengguna yang mengubah sistem dan/atau spesifikasi teknis, atau gangguan penyebab lainnya yang dibuktikan kemudian.
            <br>
            <br>
            </td>
        </tr>
        <tr>
            <td colspan="3">
Selanjutnya pemilik/pengguna bangunan gedung dapat mengurus permohonan penerbitan Sertifikat Laik Fungsi bangunan gedung. Demikian surat pernyataan ini dibuat dengan penuh tanggung jawab profesional.
            <br>
            <br>
            <br>
            <br>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="text-align:right;">
                <div class="signature" style="width:300px;display:block;text-align:center;float:right;">
                    Jakarta, <?=$this->Utilities->indonesiaDateFormat($applicationLetter->output_date->format('Y-m-d'));?><br>
                    Penyedia Jasa Pengawasan selaku Penanggung Jawab
                    <br><br><br><br><br><br><br>
                    <?=$consultant->pic_name;?><br><br><br><br>
                </div>
                <div class="signature" style="width:150px;display:block;text-align:left;float:left;">
                    <img src="<?=$this->Utilities->generateQRCode($this->Url->build([
                        'controller' => 'pages',
                        'action' => 'qrCode',
                        $applicationLetter->code,
                        '?' => [
                            'build-code' => $r
                        ]
                    ],true));?>"  width="150px"/>
                </div>
                <div style="clear:both;display:block;">&nbsp;</div>
            </td>
        </tr>
        <!-- <tr>
            <td colspan="3">
                Keterangan : * Dipilih yang sesuai dengan permohonan dan coret yang tidak sesuai, jika pengisian secara manual. Jika pengisian menggunakan software, yang tidak dipilih didelete (hapus).
            </td>
        </tr> -->
        <!-- <tr>
            <td colspan="3" style="text-align:left;">
                <div class="signature" style="width:200px;display:block;text-align:center;margin-left:auto;">
                    
                </div>
            </td>
        </tr> -->
    </table>
  </main>
<?php endforeach;?>
</body>
</html>