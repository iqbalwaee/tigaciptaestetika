<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($supervisor,['class'=>'m-form m-form--fit m-form--label-align-right','type'=>'file']) ?>
        <div class="m-portlet__body">
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('nik',['label'=>'NIK']);?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('name',['label'=>'Nama Pengawas']);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('email');?>
                </div>
                <div class="col-md-4">
                    <?=$this->Form->control('phone',['label'=>'No. Telepon']);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('no_sertifikat_keahlian');?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-8">
                    <?=$this->Form->control('address',['label'=>'Alamat','class'=>'form-control m-input']);?>
                </div>
            </div>
            <div class="row m--margin-bottom-15">
                <div class="col-md-4">
                    <?=$this->Form->control('identity_file',['label'=>'File Identitas','type'=>'file']);;?>
                </div>
            </div>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>