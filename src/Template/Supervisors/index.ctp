<?php
    $rightButton = "";
    $rightButton = '<a href="'.$this->Url->build(['action'=>'add']).'" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                        <span>
                            <i class="la la-plus"></i>
                            <span>
                                Create
                            </span>
                        </span>
                    </a>';
?>
<?=$this->element('widget/index',['rightButton' => $rightButton]);?>
<?php $this->start('script');?>
    <script>
        <?php
            $deleteUrl    = $this->Url->build(['action'=>'delete'])."/";
            $editUrl      = $this->Url->build(['action'=>'edit'])."/";
            $viewUrl = $this->Url->build(['action'=>'view'])."/";
        ?>
        jQuery(document).ready(function() {
            var deleteUrl = "<?=$deleteUrl;?>";
            var editUrl = "<?=$editUrl;?>";
            var viewUrl = "<?=$viewUrl;?>";
            var group_id = "<?=$userData->group_id;?>";
            var columnData = [{
                field: "Supervisors.nik",
                title: "NIK",
                sortable: false,
                width: 200,
                template: function(t) {
                    return t.nik
                }
            },  {
                field: "Supervisors.name",
                title: "Nama Pengawas",
                sort : 'asc',
                template: function(t) {
                    return t.name
                }
            },  {
                field: "Supervisors.email",
                title: "Email",
                sort : 'asc',
                template: function(t) {
                    return t.email
                }
            },   {
                field: "Supervisors.phone",
                title: "Phone",
                sort : 'asc',
                template: function(t) {
                    return t.email
                }
            }, 
            {
                field: "actions",
                width: 150,
                title: "Actions",
                sortable: false,
                overflow: "visible",
                template: function(t) {
                    var btn =  '<div class="btn-group btn-group-sm" role="group" aria-label="Small button group">'
                    var btnList = '';
                    if(viewUrl != ""){
                        btnList += '<a class="btn btn-sm btn-brand" href="'+viewUrl+t.id+'"><i class="flaticon-search-1"></i> View</a>';
                    }
                    if(editUrl != "" ){
                        btnList += '<a class=" btn btn-sm btn-success" href="'+editUrl+t.id+'"><i class="flaticon-edit"></i></a>';
                    }
                    if(deleteUrl != ""){
                        btnList += '<a class="btn-delete-on-table btn btn-sm btn-danger" href="'+deleteUrl+t.id+'"><i class="flaticon-cancel"></i></a>';
                    }
                    
                    if(btnList == ""){
                        btn = "";
                    }else{
                        btn += btnList;
                        btn += '</div>';
                    }
                    return btn;
                }
            }];
            DatatableRemoteAjaxDemo.init("",columnData,"<?=$this->request->getParam('_csrfToken');?>")
        });
    </script>
<?php $this->end();?>