<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <div class="m-portlet__body">
        <!--begin: Datatable -->
        <table class="table">
            <tr>
                <th scope="row" width="200px"><?= __('NIK') ?></th>
                <td><?= h($supervisor->nik) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Nama Pengawas') ?></th>
                <td><?= h($supervisor->name) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Email') ?></th>
                <td><?= h($supervisor->email) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('No. Telepon') ?></th>
                <td><?= h($supervisor->phone) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Alamat') ?></th>
                <td><?= h($supervisor->address) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('File Indetitas') ?></th>
                <td>
                    <img src="<?=$this->Utilities->generateUrlImage($supervisor->identity_file_dir,$supervisor->identity_file);?>" style="max-width:100%;">
                </td>
            </tr>
            <tr>
                <th scope="row"><?= __('Id') ?></th>
                <td><?= $this->Number->format($supervisor->id) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Created') ?></th>
                <td><?= h($supervisor->created) ?></td>
            </tr>
            <tr>
                <th scope="row"><?= __('Modified') ?></th>
                <td><?= h($supervisor->modified) ?></td>
            </tr>
        </table>
        <!--end: Datatable -->
    </div>
</div>
