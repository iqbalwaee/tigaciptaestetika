<div class="m-portlet m-portlet--tab">
    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                    <?=$titlesubModule;?>
                </h3>
            </div>
        </div>
        <div class="m-portlet__head-tools">
            <ul class="m-portlet__nav">
                <li class="m-portlet__nav-item">
                </li>
            </ul>
        </div>
    </div>
    <?= $this->Form->create($user,['class'=>'m-form m-form--fit m-form--label-align-right']) ?>
        <div class="m-portlet__body">
            <table class="table m-table">
                <thead>
                    <tr>
                        <th width="30px"><input type="checkbox" id="allCheck"></th>
                        <th>Module Name</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($acos as $key => $aco):?>
                        <tr class="m-table__row--primary">
                            <td>
                                <input type="hidden" id="checkHide" name="aco_id[controllers/<?=$aco->alias;?>]" value="0">
                                <input data-id="<?=$aco->id;?>" <?=($aco->read == 1 ? 'checked="checked"' : "" );?> type="checkbox" class="checkParent" name="aco_id[controllers/<?=$aco->alias;?>]" value="1">
                            </td>
                            <td><?=$aco->name;?></td>
                        </tr>
                        <?php foreach($aco->children as $ckey => $child):?>
                            <tr class="">
                                <td>
                                    <input type="hidden" id="checkHide" name="aco_id[controllers/<?=$aco->alias;?>][controllers/<?=$aco->alias;?>/<?=$child->alias;?>]" value="0">
                                    <input data-parent="<?=$aco->id;?>" <?=($child->read == 1 ? 'checked="checked"' : "" );?> type="checkbox" class="checkChild" name="aco_id[controllers/<?=$aco->alias;?>][controllers/<?=$aco->alias;?>/<?=$child->alias;?>]" value="1">
                                </td>
                                <td><?=$child->name;?></td>
                            </tr>
                        <?php endforeach;?>
                    <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">
            <div class="m-form__actions">
                <button type="submit" class="btn btn-primary">
                    Submit
                </button>
                <button type="reset" class="btn btn-secondary">
                    Cancel
                </button>
            </div>
        </div>
    </form>
</div>

<?=$this->Html->scriptBlock(
    '
        $("#allCheck").on("click",function(e){
            if($(this).prop("checked")){
                $("input[type=\"checkbox\"]").prop("checked",true);
            }else{
                $("input[type=\"checkbox\"]").prop("checked",false);
            }
        })
        $(".checkParent").on("click",function(e){
            console.log($(this).val())
            if($(this).prop("checked")){
                $("input[data-parent=\""+$(this).data("id")+"\"]").prop("checked",true);
            }else{
                $("input[data-parent=\""+$(this).data("id")+"\"]").prop("checked",false);
            }
        })
        $(".checkChild").on("click",function(e){
            var thisParentVal = $(this).data("parent");
            console.log(thisParentVal);
            if($(this).prop("checked")){
                $("input[data-id=\""+thisParentVal+"\"]").prop("checked",true);
            }else{
                var countCheck = $("input[data-parent=\""+thisParentVal+"\"]:checked").length;
                if(countCheck == 0){
                    $("input[data-id=\""+thisParentVal+"\"]").prop("checked",false);
                }
                //$("input[data-parent=\""+$(this).val()+"\"]").prop("checked",false);
            }
        })
    ',[
        'block'=>'script'
    ]
);?>