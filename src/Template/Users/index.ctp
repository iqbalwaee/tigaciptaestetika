<?php
    $rightButton = "";
    if($this->Acl->check(['action'=>'add']) == true):
        $rightButton = '<a href="'.$this->Url->build(['action'=>'add']).'" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill">
                            <span>
                                <i class="la la-plus"></i>
                                <span>
                                    Create
                                </span>
                            </span>
                        </a>';
    endif;
?>
<?=$this->element('widget/index',['rightButton' => $rightButton]);?>
<?php $this->start('script');?>
    <script>
        <?php
            $deleteUrl    = $this->Url->build(['action'=>'delete'])."/";
            if($this->Acl->check(['action'=>'delete']) == false){
                $deleteUrl = "";
            }
            $editUrl      = $this->Url->build(['action'=>'edit'])."/";
            if($this->Acl->check(['action'=>'edit']) == false){
                $editUrl = "";
            }
            $configureUrl = $this->Url->build(['action'=>'configure'])."/";
            if($this->Acl->check(['action'=>'configure']) == false){
                $configureUrl = "";
            }
            $viewUrl = $this->Url->build(['action'=>'view'])."/";
            if($this->Acl->check(['action'=>'view']) == false){
                $viewUrl = "";
            }
        ?>
        jQuery(document).ready(function() {
            var deleteUrl = "<?=$deleteUrl;?>";
            var editUrl = "<?=$editUrl;?>";
            var configureUrl = "<?=$configureUrl;?>";
            var viewUrl = "<?=$viewUrl;?>";
            var columnData = [{
                field: "Users.id",
                title: "ID",
                sortable: false,
                width: 40,
                selector: false,
                textAlign: "center",
                template: function(t) {
                    return t.id
                }
            },  {
                field: "Users.username",
                title: "Username",
                sort : 'asc',
                template: function(t) {
                    return t.username
                }
            },  {
                field: "Groups.name",
                title: "Group",
                sort : 'asc',
                template: function(t) {
                    return t.group.name
                }
            },  {
                field: "Users.created",
                title: "Created",
                sort : 'asc',
                template: function(t) {
                    return Utils.dateIndonesia(t.created,true,true)
                }
            },  {
                field: "Users.modified",
                title: "Modified",
                sort : 'asc',
                template: function(t) {
                    return Utils.dateIndonesia(t.modified,true,true)
                }
            },
            {
                field: "actions",
                width: 100,
                title: "Actions",
                sortable: false,
                overflow: "visible",
                template: function(t) {
                    var btn =  '<div class="dropdown"><a href="#" class="btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown"><i class="la la-ellipsis-h"></i></a>  <div class="dropdown-menu dropdown-menu-right">'
                    var btnList = '';
                    if(viewUrl != ""){
                        btnList += '<a class="dropdown-item" href="'+viewUrl+t.id+'"><i class="flaticon-search-1"></i> View</a>';
                    }
                    if(deleteUrl != ""){
                        btnList += '<a class="btn-delete-on-table dropdown-item" href="'+deleteUrl+t.id+'"><i class="flaticon-cancel"></i> Delete</a>';
                    }
                    if(editUrl != ""){
                        btnList += '<a class="dropdown-item" href="'+editUrl+t.id+'"><i class="flaticon-edit"></i> Edit</a>';
                    }
                    if(configureUrl != ""){
                        btnList += '<a class="dropdown-item" href="'+configureUrl+t.id+'"><i class="flaticon-cogwheel-2"></i> Configure</a>';
                    }
                    
                    if(btnList == ""){
                        btn = "";
                    }else{
                        btn += btnList;
                        btn += '</div></div>';
                    }
                    return btn;
                }
            }];
            DatatableRemoteAjax.init("",columnData)
        });
    </script>
<?php $this->end();?>