<?php
namespace App\View\Helper;

use Cake\View\Helper;
use \chillerlan\QRCode\QRCode;

class UtilitiesHelper extends Helper
{
    public $helpers = ['Url'];

    public function dayArray($key = null)
    {
        $array = array(
            'Sun' => 'Minggu',
            'Mon' => 'Senin',
            'Tue' => 'Selasa',
            'Wed' => 'Rabu',
            'Thu' => 'Kamis',
            'Fri' => 'Jumat',
            'Sat' => 'Sabtu'
        );
        if($key == null){
            return $array;
        }else{
            return $array[$key];
        }
    }

    public function monthArray($key =null)
    {
        $array = array (1 =>   'Januari',
                    'Februari',
                    'Maret',
                    'April',
                    'Mei',
                    'Juni',
                    'Juli',
                    'Agustus',
                    'September',
                    'Oktober',
                    'November',
                    'Desember'
                );
        if($key == null){
            return $array;
        }else{
            return $array[$key];
        }
        
    }
    
    public function indonesiaDateFormat($tanggal,$time = false,$toIndonesia = true)
    {
        $bulan = $this->monthArray();
        $datepick = explode(" ", $tanggal);
        $split = explode("-", $datepick[0]);
        if($toIndonesia == true){
            if($time == false){
                return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0];
            }else{
                return $split[2] . ' ' . $bulan[ (int)$split[1] ] . ' ' . $split[0]. ' ' . $datepick[1];
            }
        }else{
            if($time == false){
                return $split[2] . '-' . $split[1]. '-' . $split[0];
            }else{
                return $split[2] . '-' . $split[1] . '-' . $split[0].' '.$datepick[1];
            }
        }
        
    }  

    public function numberFormat($number){
        return number_format($number,2);
    }
    
    public function sideBarArrayCheck( array $array, $keys ) {
        $count = 0;
        if ( ! is_array( $keys ) ) {
            $keys = func_get_args();
            array_shift( $keys );
        }
        foreach ( $keys as $key ) {
            if ( isset( $array[$key] ) || array_key_exists( $key, $array ) ) {
                $count ++;
            }
        }
    
        return $count;
    }

    public function labelSettings($label){
        return str_replace(".","",$label);
    }

    public function statusLabel($label){
        if($label){
            return "<span class=\"m-badge m-badge--brand m-badge--wide\"> ENABLE</span>";
        }else{
            return "<span class=\"m-badge m-badge--danger m-badge--wide\"> DISABLED</span>";
        }
    }

    public function statusAccountLabel($label){
        if($label){
            return "<span class=\"m-badge m-badge--brand m-badge--wide\">TELAH DIVERIFIKASI</span>";
        }else{
            return "<span class=\"m-badge m-badge--danger m-badge--wide\">BELUM DIVERIFIKASI</span>";
        }
    }


    public function statusDeveloper($status){
        if($status == 0){
            return "<span class=\"m-badge m-badge--brand m-badge--wide\"> Pengajuan Terkirim</span>";
        }elseif($status == 1){
            return "<span class=\"m-badge m-badge--info m-badge--wide\"> Disetujui oleh DPD</span>";
        }elseif($status == 2){
            return "<span class=\"m-badge m-badge--info m-badge--wide\"> Disetujui oleh DPD</span>";
        }elseif($status == 3){
            return "<span class=\"m-badge m-badge--success m-badge--wide\"> Disetujui oleh Konsultan</span>";
        }elseif($status == 11){
            return "<span class=\"m-badge m-badge--danger m-badge--wide\"> Ditolak oleh DPD</span>";
        }elseif($status == 22){
            return "<span class=\"m-badge m-badge--danger m-badge--wide\"> Ditolak oleh DPP</span>";
        }
    }

    public function generateUrlImage($img_dir=null,$img,$prefix = null,$img_not_found = null)
    {
		//full_path_dir
        $baseDir = "";
        if($img_dir == null){
            $img_dir = $img;
            if(substr($img_dir,0,1) == "/" || substr($img_dir,0,1) == "\""){
                $img_dir = substr($img_dir,1);
            }
            $changeSlash 		= str_replace("\\",DS,$img_dir);
            $changeSlash		= str_replace("/",DS,$changeSlash); 
            $changeSlash        = str_replace("webroot\\","",$changeSlash);
            $changeSlash        = str_replace("webroot/","",$changeSlash);
            $full_path = WWW_ROOT.$changeSlash;
            $noDir = true;
        }else{
            $img_dir = $baseDir.$img_dir."/";
            $img = $prefix.$img;
            $changeSlash 		= str_replace("\\",DS,$img_dir);
            $changeSlash		= str_replace("/",DS,$changeSlash); 
            $full_path = ROOT.DS.$changeSlash.$img;
            $noDir = false;
        }
        
		//check image exist
		if(file_exists($full_path)){
            if($noDir == false){
                $dir 		= str_replace("\\","/",$img_dir).$img;
            }else{
                $dir 		= str_replace("\\","/",$img_dir);
            }
			
			$dir = str_replace("webroot/","",$dir);
			$url = $this->Url->build("/".$dir,true);
		}else{
			if($img_not_found == null){
				$url = $this->Url->build("/img/no-image.png",true);
			}else{
				$url = $this->Url->build("/".$img_not_found,true);
			}
		}
		return $url;
    }

    public function generatePathImage($img_dir=null,$img,$prefix = null,$img_not_found = null)
    {
		//full_path_dir
        $baseDir = "";
        if($img_dir == null){
            $img_dir = $img;
            if(substr($img_dir,0,1) == "/" || substr($img_dir,0,1) == "\""){
                $img_dir = substr($img_dir,1);
            }
            $changeSlash 		= str_replace("\\",DS,$img_dir);
            $changeSlash		= str_replace("/",DS,$changeSlash); 
            $changeSlash        = str_replace("webroot\\","",$changeSlash);
            $changeSlash        = str_replace("webroot/","",$changeSlash);
            $full_path = WWW_ROOT.$changeSlash;
            $noDir = true;
        }else{
            $img_dir = $baseDir.$img_dir."/";
            $img = $prefix.$img;
            $changeSlash 		= str_replace("\\",DS,$img_dir);
            $changeSlash		= str_replace("/",DS,$changeSlash); 
            $full_path = ROOT.DS.$changeSlash.$img;
            $noDir = false;
        }
        
		//check image exist
		if(file_exists($full_path)){
            return $full_path;
		}else{
			if($img_not_found == null){
				$url = $this->Url->build("/img/no-image.png",true);
			}else{
				$url = $this->Url->build("/".$img_not_found,true);
			}
		}
		return $url;
    }

    public function romawi($n){
        $hasil = "";
        $iromawi = array("","I","II","III","IV","V","VI","VII","VIII","IX","X",20=>"XX",30=>"XXX",40=>"XL",50=>"L",
        60=>"LX",70=>"LXX",80=>"LXXX",90=>"XC",100=>"C",200=>"CC",300=>"CCC",400=>"CD",500=>"D",600=>"DC",700=>"DCC",
        800=>"DCCC",900=>"CM",1000=>"M",2000=>"MM",3000=>"MMM");
        if(array_key_exists($n,$iromawi)){
            $hasil = $iromawi[$n];
        }elseif($n >= 11 && $n <= 99){
            $i = $n % 10;
            $hasil = $iromawi[$n-$i] . $this->romawi($n % 10);
        }elseif($n >= 101 && $n <= 999){
            $i = $n % 100;
            $hasil = $iromawi[$n-$i] . $this->romawi($n % 100);
        }else{
            $i = $n % 1000;
            $hasil = $iromawi[$n-$i] . $this->romawi($n % 1000);
        }
        return $hasil;
    }

    public function penyebut($nilai) {
		$nilai = abs($nilai);
		$huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
		$temp = "";
		if ($nilai < 12) {
			$temp = " ". $huruf[$nilai];
		} else if ($nilai <20) {
			$temp = $this->penyebut($nilai - 10). " belas";
		} else if ($nilai < 100) {
			$temp = $this->penyebut($nilai/10)." puluh". $this->penyebut($nilai % 10);
		} else if ($nilai < 200) {
			$temp = " seratus" . $this->penyebut($nilai - 100);
		} else if ($nilai < 1000) {
			$temp = $this->penyebut($nilai/100) . " ratus" . $this->penyebut($nilai % 100);
		} else if ($nilai < 2000) {
			$temp = " seribu" . $this->penyebut($nilai - 1000);
		} else if ($nilai < 1000000) {
			$temp = $this->penyebut($nilai/1000) . " ribu" . $this->penyebut($nilai % 1000);
		} else if ($nilai < 1000000000) {
			$temp = $this->penyebut($nilai/1000000) . " juta" . $this->penyebut($nilai % 1000000);
		} else if ($nilai < 1000000000000) {
			$temp = $this->penyebut($nilai/1000000000) . " milyar" . $this->penyebut(fmod($nilai,1000000000));
		} else if ($nilai < 1000000000000000) {
			$temp = $this->penyebut($nilai/1000000000000) . " trilyun" . $this->penyebut(fmod($nilai,1000000000000));
		}     
		return $temp;
	}
 
	public function terbilang($nilai) {
		if($nilai<0) {
			$hasil = "minus ". trim($this->penyebut($nilai));
		} else {
			$hasil = trim($this->penyebut($nilai));
		}     		
		return $hasil;
    }
    
    public function generateQRCode($data){
        return (new QRCode)->render($data);
    }
}