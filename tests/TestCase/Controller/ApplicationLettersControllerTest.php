<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ApplicationLettersController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\ApplicationLettersController Test Case
 */
class ApplicationLettersControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_letters',
        'app.house_types',
        'app.provinces',
        'app.cities',
        'app.districts',
        'app.villages',
        'app.developers',
        'app.dpp_froms',
        'app.users',
        'app.groups',
        'app.created_users',
        'app.aros',
        'app.acos',
        'app.permissions',
        'app.master_dpds',
        'app.modified_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
