<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MasterDpdsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MasterDpdsController Test Case
 */
class MasterDpdsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.master_dpds',
        'app.provinces',
        'app.users',
        'app.groups',
        'app.created_users',
        'app.aros',
        'app.acos',
        'app.permissions',
        'app.developers',
        'app.dpp_froms',
        'app.modified_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
