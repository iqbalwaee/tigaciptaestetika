<?php
namespace App\Test\TestCase\Controller;

use App\Controller\MasterDppsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\MasterDppsController Test Case
 */
class MasterDppsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.master_dpps',
        'app.users',
        'app.groups',
        'app.created_users',
        'app.aros',
        'app.acos',
        'app.permissions',
        'app.developers',
        'app.dpp_froms',
        'app.master_dpds',
        'app.provinces',
        'app.modified_users'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
