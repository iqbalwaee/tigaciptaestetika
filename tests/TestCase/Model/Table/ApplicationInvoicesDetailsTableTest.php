<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationInvoicesDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationInvoicesDetailsTable Test Case
 */
class ApplicationInvoicesDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationInvoicesDetailsTable
     */
    public $ApplicationInvoicesDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_invoices_details',
        'app.application_invoices'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationInvoicesDetails') ? [] : ['className' => ApplicationInvoicesDetailsTable::class];
        $this->ApplicationInvoicesDetails = TableRegistry::get('ApplicationInvoicesDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationInvoicesDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
