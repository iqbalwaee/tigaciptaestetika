<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationInvoicesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationInvoicesTable Test Case
 */
class ApplicationInvoicesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationInvoicesTable
     */
    public $ApplicationInvoices;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_invoices',
        'app.application_letters',
        'app.application_invoices_details'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationInvoices') ? [] : ['className' => ApplicationInvoicesTable::class];
        $this->ApplicationInvoices = TableRegistry::get('ApplicationInvoices', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationInvoices);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
