<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationJudgmentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationJudgmentsTable Test Case
 */
class ApplicationJudgmentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationJudgmentsTable
     */
    public $ApplicationJudgments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_judgments',
        'app.application_letters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationJudgments') ? [] : ['className' => ApplicationJudgmentsTable::class];
        $this->ApplicationJudgments = TableRegistry::get('ApplicationJudgments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationJudgments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
