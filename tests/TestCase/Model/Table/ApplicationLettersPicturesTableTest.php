<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationLettersPicturesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationLettersPicturesTable Test Case
 */
class ApplicationLettersPicturesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationLettersPicturesTable
     */
    public $ApplicationLettersPictures;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_letters_pictures',
        'app.application_letters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationLettersPictures') ? [] : ['className' => ApplicationLettersPicturesTable::class];
        $this->ApplicationLettersPictures = TableRegistry::get('ApplicationLettersPictures', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationLettersPictures);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
