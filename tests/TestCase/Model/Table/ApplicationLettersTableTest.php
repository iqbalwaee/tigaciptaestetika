<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationLettersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationLettersTable Test Case
 */
class ApplicationLettersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationLettersTable
     */
    public $ApplicationLetters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_letters',
        'app.house_types',
        'app.provinces',
        'app.cities',
        'app.districts',
        'app.villages',
        'app.developers',
        'app.dpp_froms',
        'app.users',
        'app.groups',
        'app.created_users',
        'app.aros',
        'app.acos',
        'app.permissions',
        'app.master_dpds',
        'app.modified_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationLetters') ? [] : ['className' => ApplicationLettersTable::class];
        $this->ApplicationLetters = TableRegistry::get('ApplicationLetters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationLetters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
