<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationNewSimaksTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationNewSimaksTable Test Case
 */
class ApplicationNewSimaksTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationNewSimaksTable
     */
    public $ApplicationNewSimaks;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_new_simaks',
        'app.application_letters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationNewSimaks') ? [] : ['className' => ApplicationNewSimaksTable::class];
        $this->ApplicationNewSimaks = TableRegistry::get('ApplicationNewSimaks', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationNewSimaks);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
