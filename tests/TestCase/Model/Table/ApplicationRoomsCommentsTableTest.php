<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationRoomsCommentsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationRoomsCommentsTable Test Case
 */
class ApplicationRoomsCommentsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationRoomsCommentsTable
     */
    public $ApplicationRoomsComments;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_rooms_comments',
        'app.application_letters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationRoomsComments') ? [] : ['className' => ApplicationRoomsCommentsTable::class];
        $this->ApplicationRoomsComments = TableRegistry::get('ApplicationRoomsComments', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationRoomsComments);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
