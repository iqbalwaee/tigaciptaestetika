<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationRoomsDetailsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationRoomsDetailsTable Test Case
 */
class ApplicationRoomsDetailsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationRoomsDetailsTable
     */
    public $ApplicationRoomsDetails;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_rooms_details',
        'app.application_letters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationRoomsDetails') ? [] : ['className' => ApplicationRoomsDetailsTable::class];
        $this->ApplicationRoomsDetails = TableRegistry::get('ApplicationRoomsDetails', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationRoomsDetails);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
