<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationRoomsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationRoomsTable Test Case
 */
class ApplicationRoomsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationRoomsTable
     */
    public $ApplicationRooms;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_rooms',
        'app.application_letters',
        'app.rooms',
        'app.rooms_utilities',
        'app.room_options'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationRooms') ? [] : ['className' => ApplicationRoomsTable::class];
        $this->ApplicationRooms = TableRegistry::get('ApplicationRooms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationRooms);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
