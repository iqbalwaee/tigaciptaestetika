<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ApplicationSimaksPemeriksaansTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ApplicationSimaksPemeriksaansTable Test Case
 */
class ApplicationSimaksPemeriksaansTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ApplicationSimaksPemeriksaansTable
     */
    public $ApplicationSimaksPemeriksaans;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.application_simaks_pemeriksaans',
        'app.application_letters'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ApplicationSimaksPemeriksaans') ? [] : ['className' => ApplicationSimaksPemeriksaansTable::class];
        $this->ApplicationSimaksPemeriksaans = TableRegistry::get('ApplicationSimaksPemeriksaans', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ApplicationSimaksPemeriksaans);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
