<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\HistoriesLettersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\HistoriesLettersTable Test Case
 */
class HistoriesLettersTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\HistoriesLettersTable
     */
    public $HistoriesLetters;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.histories_letters',
        'app.application_letters',
        'app.house_types',
        'app.provinces',
        'app.cities',
        'app.districts',
        'app.villages',
        'app.developers',
        'app.dpp_froms',
        'app.users',
        'app.groups',
        'app.created_users',
        'app.aros',
        'app.acos',
        'app.permissions',
        'app.master_dpds',
        'app.modified_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('HistoriesLetters') ? [] : ['className' => HistoriesLettersTable::class];
        $this->HistoriesLetters = TableRegistry::get('HistoriesLetters', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->HistoriesLetters);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
