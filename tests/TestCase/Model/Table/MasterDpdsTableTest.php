<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MasterDpdsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MasterDpdsTable Test Case
 */
class MasterDpdsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MasterDpdsTable
     */
    public $MasterDpds;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.master_dpds',
        'app.provinces',
        'app.users',
        'app.groups',
        'app.created_users',
        'app.aros',
        'app.acos',
        'app.permissions',
        'app.developers',
        'app.dpp_froms',
        'app.modified_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MasterDpds') ? [] : ['className' => MasterDpdsTable::class];
        $this->MasterDpds = TableRegistry::get('MasterDpds', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MasterDpds);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
