<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MasterDppsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MasterDppsTable Test Case
 */
class MasterDppsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\MasterDppsTable
     */
    public $MasterDpps;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.master_dpps',
        'app.users',
        'app.groups',
        'app.created_users',
        'app.aros',
        'app.acos',
        'app.permissions',
        'app.developers',
        'app.dpp_froms',
        'app.master_dpds',
        'app.provinces',
        'app.modified_users'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('MasterDpps') ? [] : ['className' => MasterDppsTable::class];
        $this->MasterDpps = TableRegistry::get('MasterDpps', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->MasterDpps);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
