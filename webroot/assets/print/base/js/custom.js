var Utils = function(){
    var asideShow = function(){
        var $win = $(window); // or $box parent container
        var $box = $(".aside-form");
        var linkShow = $(".show-aside");
        
        $win.on("click.Bst", function(event){		
            if ( 
            ($box.has(event.target).length == 0 //checks if descendants of $box was clicked
            &&
            !$box.is(event.target) ) && (linkShow.has(event.target).length == 0 
            &&
            !linkShow.is(event.target) )//checks if the $box itself was clicked
            ){
                $(".aside-form").removeClass('active');
            }
        });

        $(".close-aside").click(function(e){
            e.preventDefault();
            $(".aside-form").removeClass('active');
        });

        $("body").on("keyup",function(e){
            //e.preventDefault();
            if(e.keyCode == 27){
                $(".aside-form").removeClass('active');
            }
            
        });
        
            
        $(".show-aside").click(function(e){
            e.preventDefault();
            var thisTarget = $(this).data("target");
            if($(".aside-form.active").length == 0){
                $("#"+thisTarget).addClass("active");
            }else{
                if($("#"+thisTarget).hasClass('active')){
                    $("#"+thisTarget).removeClass('active');
                }else{
                    $(".aside-form").removeClass('active');
                    setTimeout(function(){
                        $("#"+thisTarget).addClass("active");
                    },250)
                }
                
                
            }
        });
    };

    var showErrorMsg = function(form, type,title, msg) {
        var alert = $('<div class="alert ' + type + ' alert-dismissible" role="alert">\
			<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times"></i></button>\
			<h4></h4><span></span>\
		</div>');

        form.find('.alert').remove();
        alert.prependTo(form);
        alert.slideDown();
        alert.find('h4').html(title);
        alert.find('span').html(msg);
    }

    var closeAllert = function(){
        $("body").on("click",".alert",function(e){
            e.preventDefault();
            $(this).remove();
        })
    }

    var handleLogin = function(){
        var form = $("form");
        
        form.on("submit",function(e){
            form = $(this);
            var id = form.attr("id");
            e.preventDefault();
            if(form.find(".alert").length > 0){
                form.find(".alert").remove();
            }
            //form.prepend("<div class='alert info'><h4><i class='fa fa-spin fa-refresh'></i> Harap Menunggu</h4> Sedang mengirim data</div>");
            if(id == "register-dev"){
                form.validate();
            }else{
                form.validate({
                    rules: {
                        username: {
                            required: true,
                        },
                        password: {
                            required: true
                        }
                    }
                });
            }
            

            if (!form.valid()) {
                return;
            }
            
            form.ajaxSubmit({
                url: form.attr('action'),
                dataType : 'json',
                type : 'post',
                beforeSend : function(){
                    form.find("input,button,select").prop("disabled",true);
                    showErrorMsg(form, 'info',"<i class='fa fa-spin fa-refresh'></i> Harap Menunggu", "Sedang mengirim data.");
                },
                success: function(response, status, xhr, $form) {
                    form.find("input,button,select").prop("disabled",false);
                    if(response.code == 50){
                        showErrorMsg(form, 'danger','<i class="fa fa-warning"></i> Gagal', response.message);
                    }else{
                        showErrorMsg(form, 'success','<i class="fa fa-check"></i> Sukses', response.message);
                        form.find("input,button,select").val("");
                        console.log(id);
                        if(id != "reg-dev"){
                            location.reload();
                            
                        }
                    }
                },
                error : function(){
                    form.find("input,button,select").prop("disabled",false);
                    showErrorMsg(form, 'danger','<i class="fa fa-warning"></i> Gagal', "Terjadi kesalahan harap ulangi kembali");
                }
            });
        })
    };
    return{
        init : function(){
            closeAllert();
            handleLogin();
            asideShow();
        }
    }
}();
$(document).ready(function(){
    Utils.init();
})