var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');
var watchSass = require("gulp-watch-sass")

var config = {
    path :{
        node       : 'node_modules',
        base       : '../base',
        dist       : '../dist'
    }
};

gulp.task('sass', function(done) {
    gulp.src(config.path.base+'/scss/style.scss')
      .pipe(sass().on('error', sass.logError))
      .pipe(concat('style.bundle.css'))
      .pipe(gulp.dest(config.path.dist+'/css'));
    done();
});

gulp.task('sass:watch', function(done) {
    watchSass(config.path.dist+'/css/style.bundle.css', ['sass']);
    done();
});

gulp.task('fonts', function(done) {
    gulp.src([config.path.base+'/fonts/**/*',config.path.node+'/font-awesome/fonts/*'])
    .pipe(gulp.dest(config.path.dist+'/fonts/'));
    done();
});
gulp.task('media', function(done) {
    gulp.src([config.path.base+'/media/**/*'])
    .pipe(gulp.dest(config.path.dist+'/media/'));
    done();
});

gulp.task('script', function(done) {
    gulp.src([
        config.path.node+'/jquery/dist/jquery.js',
        config.path.node+'/jquery-form/dist/jquery.form.min.js',
        config.path.node+'/popper.js/dist/umd/popper.js',
        config.path.node+'/bootstrap/dist/js/bootstrap.js',
        config.path.node+'/owl.carousel/dist/owl.carousel.js',
        config.path.node+'/jquery-validation/dist/jquery.validate.min.js',
        config.path.base+'/js/custom.js'
    ])
    .pipe(sourcemaps.init())
    .pipe(concat('script.bundle.js'))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.path.dist+'/js'));
done();
});
gulp.task('default', ['sass', 'script','fonts','media']);
// gulp.task('default', [ 'sass', 'script' ]);