//== Class definition

var DatatableRemoteAjaxDemo = function(url,columsData,token) {
  //== Private functions

  // basic demo
  var demo = function(url,columsData,token) {
      var options = {
        data: {
            type: 'remote',
            source: {
                read: {
                    url: url,
                    method: 'POST',
                    params: {
                        _csrfToken: token
                    },
                    map: function(raw) {
                        // sample data mapping
                        var dataSet = raw;
                        if (typeof raw.data !== 'undefined') {
                            dataSet = raw.data;
                        }
                        return dataSet;
                    },
                }
            },
            pageSize: 20,
            saveState: {
                cookie: false,
                webstorage: false
            },
    
            serverPaging: true,
            serverFiltering: true,
            serverSorting: true,
            autoColumns: false
        },
    
        layout: {
            theme: 'default',
            class: 'm-datatable--brand',
            scroll: false,
            height: null,
            footer: false,
            header: true,
    
            smoothScroll: {
                scrollbarShown: true
            },
    
            spinner: {
                overlayColor: '#000000',
                opacity: 0,
                type: 'loader',
                state: 'brand',
                message: true
            },
    
            icons: {
                sort: {asc: 'la la-arrow-up', desc: 'la la-arrow-down'},
                pagination: {
                    next: 'la la-angle-right',
                    prev: 'la la-angle-left',
                    first: 'la la-angle-double-left',
                    last: 'la la-angle-double-right',
                    more: 'la la-ellipsis-h'
                },
                rowDetail: {expand: 'fa fa-caret-down', collapse: 'fa fa-caret-right'}
            }
        },
    
        pagination: true,
    
        search: {
          // enable trigger search by keyup enter
          onEnter: false,
          // input text for search
          input: $('#m_form_search'),
          // search delay in milliseconds
          delay: 400,
        },
    
        rows: {
          callback: function() {},
          // auto hide columns, if rows overflow. work on non locked columns
          autoHide: false,
        },
    
        // columns definition
        columns: columsData,
    
        toolbar: {
            layout: ['pagination', 'info'],
    
            placement: ['bottom'],  //'top', 'bottom'
    
            items: {
                pagination: {
                    type: 'default',
    
                    pages: {
                        desktop: {
                            layout: 'default',
                            pagesNumber: 6
                        },
                        tablet: {
                            layout: 'default',
                            pagesNumber: 3
                        },
                        mobile: {
                            layout: 'compact'
                        }
                    },
    
                    navigation: {
                        prev: true,
                        next: true,
                        first: true,
                        last: true
                    },
    
                    pageSizeSelect: [10, 20, 30, 50, 100]
                },
    
                info: true
            }
        },
    
        translate: {
            records: {
                processing: 'Please wait...',
                noRecords: 'No records found'
            },
            toolbar: {
                pagination: {
                    items: {
                        default: {
                            first: 'First',
                            prev: 'Previous',
                            next: 'Next',
                            last: 'Last',
                            more: 'More pages',
                            input: 'Page number',
                            select: 'Select page size'
                        },
                        info: 'Displaying {{start}} - {{end}} of {{total}} records'
                    }
                }
            }
        }
    };

    var datatable = $('.m_datatable').mDatatable(options);
    datatable.on("m-datatable--on-layout-updated",function(e){
      mApp.initTooltips();
    })

    $("body").on("click",".btn-delete-on-table",function(e){
      e.preventDefault();
      var thisUrl = $(this).attr("href");
      swal({
          title: 'Are you sure?',
          text: 'Please make sure if you want to delete this record',
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Yes, delete it!',
          cancelButtonText: 'No, keep it'
      }).then((result) => {
          if (result.value) {
              $.ajax({
                  url : thisUrl,
                  type : "delete",
                  dataType : "json",
                  data : {
                    _csrfToken: token
                  },
                  beforeSend : function(){
                      swal('Please Wait', 'Requesting Data', 'info')
                  },
                  success : function(result){
                      if(result.code == 200){
                          swal(
                              'Success',
                              result.message,
                              'success'
                          )
                          var a = datatable.getDataSourceQuery(); datatable.load();
                      }else if(result.code == 99){
                          swal("Ooopp!!!", result.message,"error");
                      }else{
                          swal("Ooopp!!!", result.message,"error");
                      }
                  },
                  error : function()
                  {
                      swal("Ooopp!!!","Failed to deleted record, please try again","error");
                  }
              })
          // result.dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
          } else if (result.dismiss === 'cancel') {
              
          }
      })
  });

    // $('#m_form_status').on('change', function() {
    //   datatable.search($(this).val(), 'Status');
    // });

    // $('#m_form_type').on('change', function() {
    //   datatable.search($(this).val(), 'Type');
    // });

    // $('#m_form_status, #m_form_type').selectpicker();

  };

  return {
    // public functions
    init: function(url,columsData,token) {
      demo(url,columsData,token);
    },
  };
}();

// jQuery(document).ready(function() {
//   DatatableRemoteAjaxDemo.init();
// });